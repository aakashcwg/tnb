<?php

//tbls.php

/*==============Formats START==============*/
define('COMPUTERNAME', getenv('COMPUTERNAME'));

define('DATE_FORMAT', 'm/d/Y');

define('DATE_TIME_FORMAT', 'm/d/Y h:i:s');
/*===============Formats END===============*/

/*==============Tables START==============*/
define('TBL_ADDRESS_TYPES', 'tblAddressTypes');

define('TBL_AUDIT_CUSTOMER_LOG', 'tblAudit_CustomersLog');

define('TBL_AUDIT_INVENTORY', 'tblAudit_Inventory');

define('TBL_AUDIT_SALES_LOG', 'tblAudit_SalesLog');

define('TBL_AUDIT_VENDORS_LOG', 'tblAudit_VendorsLog');

define('TBL_CASH_DRAWER_ACTIVITY', 'tblCashDrawerActivity');

define('TBL_CASH_DRAWERS', 'tblCashDrawers');

define('TBL_CASH_OUT', 'tblCashOut');

define('TBL_CASH_OUT_DETAILS', 'tblCashOutDetails');

define('TBL_CONTACT_TYPES', 'tblContactTypes');

define('TBL_CUSTOMER_ACCOUNT_TYPE', 'tblCustomerAccountType');

define('TBL_CUSTOMER_ACCOUNT_TYPE_LIST', 'tblCustomerAccountTypesList');

define('TBL_CUSTOMER_ADDRESSES', 'tblCustomerAddresses');

define('TBL_CUSTOMER_CONTACT', 'tblCustomerContact');

define('TBL_CUSTOMERS', 'tblCustomers');

define('TBL_CUSTOMER_TAX_DATA', 'tblCustomerTaxData');

define('TBL_EMPLOYEE_CONTACT', 'tblEmployeeContact');

define('TBL_EMPLOYEE_HISTORY', 'tblEmployeeHistory');

define('TBL_EMPLOYEE_LOGIN', 'tblEmployeeLogin');

define('TBL_EMPLOYEE_ROLES', 'tblEmployeeRoles');

define('TBL_EMPLOYEES', 'tblEmployees');

define('TBL_GENERAL_LEDGER_NUMBERS', 'tblGeneralLedgerNumbers');

define('TBL_INSTALLATIONS', 'tblInstallations');

define('TBL_INVENTORY', 'tblInventory');

define('TBL_INVENTORY_ON_HAND', 'tblInventoryOnHand');

define('TBL_INVENTORY_ON_HAND_PRICING', 'tblInventoryOnHandPricing');

define('TBL_INVENTORY_ORDER_DETAILS', 'tblInventoryOrderDetails');

define('TBL_INVENTORY_ORDERS', 'tblInventoryOrders');

define('TBL_INVENTORY_ORDERS_RECEIVED', 'tblInventoryOrdersReceived');

define('TBL_INVENTORY_ORDERS_RECEIVED_DETAILS', 'tblInventoryOrdersReceivedDetails');

define('TBL_NONINVENTORY_ITEMS', 'tblNonInventoryItems');

define('TBL_NON_INVENTORY_PRICING', 'tblNonInventoryPricing');

define('TBL_PAYMENT_METHODS', 'tblPaymentMethods');

define('TBL_PRICE_LIST_ITEMS', 'tblPriceListItems');

define('TBL_PRICE_LISTS', 'tblPriceLists');

define('TBL_PRICING_TYPES', 'tblPricingTypes');

define('TBL_SALES', 'tblSales');

define('TBL_SALES_DETAILS', 'tblSalesDetails');

define('TBL_SALES_PAYMENTS', 'tblSalesPayments');

define('TBL_SECTIONS', 'tblSections');

define('TBL_SECURITY_LEVELS', 'tblSecurityLevels');

define('TBL_SHELVES', 'tblShelves');

define('TBL_TAX_RATES', 'tblTaxRates');

define('TBL_VENDOR_ADDRESSES', 'tblVendorAddresses');

define('TBL_VENDOR_CONTACT', 'tblVendorContact');

define('TBL_VENDOR_PAYMENT_DETAILS', 'tblVendorPaymentDetails');

define('TBL_VENDOR_PAYMENTS', 'tblVendorPayments');

define('TBL_VENDORS', 'tblVendors');

define('TBL_VENDOR_TAX_DATA', 'tblVendorTaxData');

define('TBL_WAREHOUSES', 'tblWarehouses');

/*==============Tables END==============*/

?>