<?php

$itemIDs = explode(',', $_POST['myitems']);
$myItemTypes = explode(',', $_POST['myItemTypes']);
$num = $_POST['btnid'];
$newNum = $num+1;
$selectedInvs = array();
$selectedNonInvs = array();

for ($i=0; $i < count($itemIDs); $i++) {
	if($myItemTypes[$i] == 'inv'){
		array_push($selectedInvs, $itemIDs[$i]);
	}else{
		if($myItemTypes[$i] == 'non-inv'){
			array_push($selectedNonInvs, $itemIDs[$i]);
		}
	}
}


// foreach ($itemIDs as $itemID) {
// 	$selectedInvs[] = $itemID;
// }

// $AllItems = getAllInventoryIds();
// $resaultatIds = array_diff($AllItems, $selectedItems);


$html = '';

$html .= '<div class="item-block">';
	
	//Quantity
	$html .= '<div class="col-md-1 qty-grid">';
		$html .= '<div class="form-group">';
			$html .= '<label for="qty'.$newNum.'">Qty</label>';
			$html .= '<input type="text" name="qty" id="qty'.$newNum.'" min="1" class="form-control qty" value="1" onkeyup="getItemNetPrice(this)">';
		$html .= '</div>';
	$html .= '</div>';

	//Item
	$html .= '<div class="col-md-2 item-grid">';
		$html .= '<div class="form-group">';
			$html .= '<label for="item'.$newNum.'">Item</label>';
			$html .= GetInvAndNonInvItems('item'.$newNum, $selectedInvs, $selectedNonInvs);
			/*$html .= '<select class="form-control sd-select item" id="item'.$newNum.'" name="item" data-show-subtext="true" data-live-search="true" class="form-control" onchange="getItemPrice(this)">';
				$html .= '<option value="">Select Item</option>';
				foreach ($resaultatIds as $item) {
					$html .= '<option value="'.$item.'">'.getItemWithNumber($item).'</option>';
				}*/
			// $html .= '</select>';
		$html .= '</div>';
	$html .= '</div>';

	//Cost
	$html .= '<div class="col-md-2 sales-grid">';
		$html .= '<div class="form-group">';
			$html .= '<label for="cost'.$newNum.'">Sales Cost</label>';
			$html .= '<input type="text" name="cost" id="cost'.$newNum.'" class="form-control cost" readonly>';
		$html .= '</div>';
	$html .= '</div>';

	//Discount
	$html .= '<div class="col-md-2 discount-grid">';
		$html .= '<div class="form-group">';
			$html .= '<label for="disc'.$newNum.'">Discount</label>';
			$html .= '<input type="text" name="disc" id="disc'.$newNum.'" class="form-control disc disc-format" onkeyup="getItemNetPrice(this)" onblur="getItemNetPrice(this)">';
			$html .= '<input type="hidden" class="discvalue" name="discvalue" id="discvalue'.$newNum.'">';
		$html .= '</div>';
	$html .= '</div>';

	//Tax
	$html .= '<div class="col-md-1 tax-grid">';
		$html .= '<div class="form-group">';
			$html .= '<label for="tax'.$newNum.'">Tax</label>';
			$html .= '<input type="text" name="tax" id="tax'.$newNum.'" class="form-control tax" readonly>';
			$html .= '<input type="hidden" name="taxvalue" id="taxvalue'.$newNum.'" class="form-control taxvalue" readonly>';
			$html .= '<input type="hidden" name="unit-tax" id="unit-tax'.$newNum.'" class="form-control unit-tax" readonly>';
		$html .= '</div>';
	$html .= '</div>';

	$html .= '<div class="col-md-2 ext-cost-grid">';
		$html .= '<div class="form-group">';
			$html .= '<label for="ext-cost'.$newNum.'">Ext Cost</label>';
			$html .= '<input type="text" name="ext_cost" id="ext-cost'.$newNum.'" class="form-control ext-cost" readonly>';
		$html .= '</div>';
	$html .= '</div>';

	$html .= '<div class="col-md-2 note-grid">';
        $html .= '<div class="form-group">';
            $html .= '<label for="note'.$newNum.'">Note</label>';
            $html .= '<textarea name="note" id="note'.$newNum.'" class="form-control note"></textarea>';
        $html .= '</div>';
    $html .= '</div>';

	$html .= '<div class="col-md-2">';
		$html .= '<a href="javascript:void(0)" class="add-remove-btn remove-selling-item" style="background:red;margin-top:58px;" onclick="RemoveSellingItem(this)" data-btnid="'.$newNum.'">-</a><a href="javascript:void(0)" class="add-remove-btn add-selling-item" style="margin-top:58px;" onclick="addSellingItem(this)" data-btnid="'.$newNum.'">+</a>';
	$html .= '</div>';

	$html .= '<div class="clearfix"></div>';

$html .= '</div>';


$data['htmlContent'] = $html;

echo json_encode($data);




?>