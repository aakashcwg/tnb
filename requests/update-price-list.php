<?php
//update-price-list.php

//print_r($_POST);

$validation_error = '';
$message = '';
$tr = '';
$plDropDown = '';
$pl_name = $_POST['pl_name'];
$pl_amount = $_POST['pl_amount'] ?? null;
$pl_color = $_POST['pl_color'] ?? null;
$pl_is_percentage = isset($_POST['pl_is_percentage']) ? 1 : 0;
$has_items = isset($_POST['has_items']) ? 1 : 0;
$plid = $_POST['plid'];

$pl_amount = str_replace(['%', '$'], '', $pl_amount);

$color = '';

if(empty($pl_name)){
	$error[] = 'You must enter a list name';
}
if(empty($error)){
	
	if($SDPDO->CheckExistanceExcept(TBL_PRICE_LISTS, 'PLID', $plid, 'PLName', $pl_name)){
		$validation_error = 'There already exists a Price List with same name';
	}else{
		$update = $SDPDO->update(TBL_PRICE_LISTS, array('PLName', 'PLIsPercentage', 'PLAmount', 'PLHasItems', 'PLColor'), array($pl_name, $pl_is_percentage, $pl_amount, $has_items, $pl_color), array('PLID', $plid));
		if($update){
			$message = 'Price list successfully updated';

			$tr = '';
			$obj = getPriceList($plid);
			$tr .= '<td>'.$obj->PLName.'</td>';
			$PLIsPercentage = $obj->PLIsPercentage==1 ? 'Yes' : 'No';
			$tr .= '<td>'.$PLIsPercentage.'</td>';
			$tr .= '<td>'.number_format($obj->PLAmount, 2).'</td>';

			$PLHasItems = $obj->PLHasItems==1 ? 'Yes' : 'No';
			$tr .= '<td>'.$PLHasItems.'</td>';
			//$tr .= '<td>'.$obj->PLColor.'</td>';
			//$tr .= '<td><i class="fa fa-edit sd-tootip edit-cash-drawer" data-id="'.$obj->PLID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editPriceList(this)"></i></td>';

			$tr .= '<td><img src="'.SITE_URL.'/dist/images/tnbpos-multi-edit-pic.png" style="cursor:pointer;" data-id="'.$obj->PLID.'" data-original-title="Edit" onclick="editPriceList(this)"></td>';


			$color = $obj->PLColor;



			//Price list dropdown
			$plDropDown = '';
			$plDropDown .= '<label for="price-list">Price List <sup>*</sup></label>';
		    $plDropDown .= '<select class="form-control sd-select" id="price-list" name="price-list" data-live-search="true">';
		    $plDropDown .= '<option value="">Select Price List</option>';
		    $selected = '';
		    foreach (getPriceLists() as $PL) {
		    	if($PL->PLID == $plid){
		    		$selected = 'selected';
		    	}else{
		    		$selected = '';
		    	}
		        $plDropDown .= '<option value="'.$PL->PLID.'" '.$selected.'>'.$PL->PLName.'</option>';
		    }
		    $plDropDown .= '</select>';



		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message,
	'tr' => $tr,
	'plid' => $plid,
	'color' => $color,
	'has_items' => $has_items,
	'plDropDown' => $plDropDown
);

echo json_encode($output);