<?php
//check-employee-role-while-selling.php
$AuthRole = '';
$EID = currentUser('EmployeeID');
if(isset($_SESSION['manager_auth_for_'.$EID]) && !empty($_SESSION['manager_auth_for_'.$EID])){
	$auth = true;
}else{
	$RoleID = currentUser('ELRoleID');
	$Role = get_the_role($RoleID);
	$auth = false;
	$AuthRole = $Role;
}

$output = array(
	'auth' => $auth,
	'role' => $AuthRole
);

echo json_encode($output);


