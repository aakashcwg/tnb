<?php

$url = SITE_URL.'/pages/receiving/receiving.php';
$data = http_build_query( array( 'RID' => $_POST['RID'], 'EID' => $_POST['EID'] ) );
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded",
        'method'  => 'POST',
        'content' => $data,
    ),
);
$context = stream_context_create( $options );
$result = file_get_contents( $url, false, $context );

$output = array(
	'content' => $result
);

echo json_encode($output);

?>