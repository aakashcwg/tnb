<?php
$WH_Name = $_POST['WH_Name'];
$WH_Address = $_POST['WH_Address'];
$WH_City = $_POST['WH_City'];
$WH_State = $_POST['WH_State'] ?? '';
$WH_Zip = $_POST['WH_Zip'];
$WH_Phone = $_POST['WH_Phone'];
$WH_ID = $_POST['WH_ID'];

$sql = ' UPDATE tblWarehouses SET WName = ?, WAddress = ?, WCity = ?, WState = ?, WZip = ?, WMainPhone = ? WHERE WarehouseID = ? ';
$stmt = $pdo->prepare($sql);
$stmt->execute([$WH_Name, $WH_Address, $WH_City, $WH_State, $WH_Zip, $WH_Phone, $WH_ID ]);
if($stmt->rowCount() > 0){
	$data['response'] = true;

	$WH = getWareHouse($WH_ID);
	$StateName = $WH->WState?getUSAStates()[$WH->WState]:'';
	$row = '';
	$row .= '<td>'.$WH->WName.'</td>';
	$row .= '<td>'.$WH->WAddress.'</td>';
	$row .= '<td>'.$WH->WCity.'</td>';
	$row .= '<td>'.$StateName.'</td>';
	$row .= '<td>'.$WH->WZip.'</td>';
	$row .= '<td>'.$WH->WMainPhone.'</td>';
	$row .= '<td><i class="fa fa-edit sd-tootip" title="" data-id="'.$WH_ID.'" style="font-size:24px;cursor:pointer;" onclick="editWareHouse(this)" data-original-title="Quick Edit"></i></td>';
	$data['row'] = $row;
	$data['WH_ID'] = $WH_ID;
}else{
	$data['response'] = false;
}
echo json_encode($data);
?>