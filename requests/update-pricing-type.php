<?php
$typeName = $_POST['typeName'];
$id = $_POST['id'];

if(!empty($typeName)){
	if($SDPDO->CheckExistanceExcept(TBL_PRICING_TYPES, 'PTID', $id, 'PTName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Pricing type already exists';
	}else{
		$update = $ListTypeOBJ->UpdatePricingType(array($typeName), array('PTID', $id));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Pricing type successfully updated';
			$data['id'] = $id;
			$data['typeName'] = $typeName;
			$data['table_id'] = 'pricing-type-table';
		}
	}	
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>