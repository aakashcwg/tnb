<?php
//update_cash_drawer.php
//print_r($_POST);
$CDID = $_POST['cdid'];
$CashDrawer = $_POST['drawer-name'] ?? null;
$Warehouse = $_POST['warehouse'] ?? null;
$ComputerName = $_POST['computer-name'] ?? null;
$validation_error = '';
$message = '';
$tr = '';
if(empty($CashDrawer)){
	$error[] = 'Drawer name is required';
}
if(empty($ComputerName)){
	$error[] = 'Computer name is required';
}
if(empty($error)){
	if($SDPDO->CheckExistanceExcept(TBL_CASH_DRAWERS, 'CashDrawerID', $CDID, 'CashDrawerName', $CashDrawer)){
		$validation_error = 'Drawer name is already exist';
	}else{

		$update = $SDPDO->update(TBL_CASH_DRAWERS, array('CashDrawerName', 'CDWarehouseID', 'CDComputerName'), array($CashDrawer, $Warehouse, $ComputerName), array('CashDrawerID', $CDID));
		if($update === true){
			$message = 'Drawer successfully updated';

			$tr = '';
			$obj = GetTheCashDrawer($CDID);
			$tr .= '<td>'.$obj->CashDrawerName.'</td>';
			$tr .= '<td>'.getWareHouseName($obj->CDWarehouseID).'</td>';
			$tr .= '<td>'.$obj->CDComputerName.'</td>';
			$tr .= '<td><i class="fa fa-edit sd-tootip edit-cash-drawer" data-id="'.$obj->CashDrawerID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="EditCashDrawer(this)"></i></td>';

		}else{
			$validation_error = 'Error :: data is not saved';
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message,
	'tr'  => $tr,
	'cdid' => $CDID
);
echo json_encode($output);