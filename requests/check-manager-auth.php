<?php

//check-cash-adjustment-auth.php

$EID = currentUser('EmployeeID');
$RoleID = currentUser('ELRoleID');
$Role = get_the_role($RoleID);
$PRoles = array('Administrator', 'Manager', 'Admin', 'manager', 'administrator', 'admin');
if(!in_array($Role, $PRoles)){
	if(isset($_SESSION['manager_auth_for_'.$EID]) && !empty($_SESSION['manager_auth_for_'.$EID])){
		$permission = true;
	}else{
		$permission = false;
	}
}else{
	$permission = true;
}

$Request = $_POST['RequestTo'];

$output = array(
	'permission' => $permission,
	'request' => $Request
);

echo json_encode($output);

?>