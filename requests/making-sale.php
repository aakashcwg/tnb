<?php

//echo '<pre>';
//print_r($_POST); die;

$employee_id = currentUser('EmployeeID');

$drawer_id = null;
if(isset($_SESSION['drawer_id'])){
	$drawer_id = $_SESSION['drawer_id'];
}

$qtys 		= explode(',', $_POST['qtys']);
$items 		= explode(',', $_POST['items']);
$itemTypes	= explode(',', $_POST['itemTypes']);

$itemTypes 	= explode(',', $_POST['itemTypes']);
$costs 		= explode(',', $_POST['costs']);
$discs 		= explode(',', $_POST['discs']);
$taxs 		= explode(',', $_POST['taxs']);
$ItemNote 	= explode(',', $_POST['ItemNote']);

$pm_name = $_POST['pm_name'] ?? null;

$ctm = 0.00;
if($pm_name == 'Cash'){
	$ctm = $_POST['ctm'];
}

$taxVal = 0;
foreach ($taxs as $tax) {
	$taxVal = $tax+$taxVal;
}

$url = '';

$data = array();
//Data for tblSales
$invoice 			= time();
$customer 			= $_POST['customer'] ?? null;
$date 				= date(DATE_TIME_FORMAT);
$salesAmount 		= $_POST['paid_amount'] ?? null;
$salesTax 			= $taxVal;
$shipingCost 		= 0;
$paymentMethod 		= $_POST['payment_method'] ?? null;
$isPaid 			= 0;
$previousSalesID 	= null; //LastSalesID($customer);
$isVoid 			= 0;
$startDate 			= $date;
$endDate 			= $date;
$SNote 				= $_POST['note'] ?? null;
$purchaseOrder 		= $_POST['purchaseOrder'] ?? null;

if(!empty($salesAmount)){
	$isPaid = 1;
	$isVoid = 0;
}
/*else{
	$isPaid = 0;
	$isVoid = 1;
	$endDate = null;
}*/

$tblSales_values = array( $invoice, $customer, $date, $salesAmount, $salesTax, $shipingCost, $paymentMethod, $isPaid, $previousSalesID, $isVoid, $startDate, $endDate, $SNote, $employee_id, $drawer_id, $purchaseOrder );

$salesID = $SalesOBJ->MakeSale($tblSales_values);
if($salesID){

	$LoggingOBJ->CustomerMakeSaleLog(array($customer, $employee_id, $date, CUSTOMER_MAKE_SALE_LOG));

	if(isset($_SESSION['manager_auth_for_'.$employee_id])){
		unset($_SESSION['manager_auth_for_'.$employee_id]);
	}
	

	for ($i=0; $i < count($items) ; $i++) {

		//Data for tblSalesDetails
		$OnHandID = null;
		$SDNonInvID = null;
		if($itemTypes[$i] == 'inv'){
			$OnHandID 			= GetOnHandID($items[$i]);
		}else{
			if($itemTypes[$i] == 'non-inv'){
				$SDNonInvID			= $items[$i];
			}
		}
		
		$SDPrice 			= $costs[$i];
		$SDPricePaid 		= null;
		$SDQuantity 		= $qtys[$i];
		$SDNotes 			= $ItemNote[$i];
		$SDIsCompanyInstall = 0;
		$SDDOE 				= $date;
		$SDDiscountAmount 	= $discs[$i];
		$SDTaxAmount		= $taxs[$i];

		$tblSalesDetails_values = array( $salesID, $OnHandID, $SDNonInvID,  $SDPrice, $SDPricePaid, $SDQuantity, $SDNotes, $SDIsCompanyInstall, $SDDOE, $SDDiscountAmount, $SDTaxAmount );

		$SalesDetailID = $SalesOBJ->AddSalesData($tblSalesDetails_values);

		if($isVoid == 0){
			//update onhand quantity
			$NewQuantity = GetOnHandQty($OnHandID) - $SDQuantity;
			$InventoryOBJ->UpdateOnHandItemQuantity(array($NewQuantity), array('IOHID', $OnHandID));
		}
		
		//Sales log
		if($SalesDetailID){
			$ASLNotes = 'Sales Details ID : '.$SalesDetailID.' // '.SALES_COMPLETE_NOTE;
			$LogValues = array($salesID, $SalesDetailID, $employee_id,  $date, $ASLNotes, $OnHandID);
			$LoggingOBJ->CompleteSalesLog($LogValues);
		}
		
	}

	$SPCheckNumber = '';
	$SPLast5Digits = '';
	$SPAuthCode = '';
	$SPAmount = $SPAmount = $_POST['_receipt_total'] ?? null;;
	if($pm_name == 'Check'){
		$SPCheckNumber = $_POST['_check_no'] ?? null;
	}
	if($pm_name == 'Visa' || $pm_name == 'Mastercard' || $pm_name == 'Discover' || $pm_name == 'American Express'){
		$SPLast5Digits = $_POST['_5digit'] ?? null;
		$SPAuthCode = $_POST['_auth_code'] ?? null;
	}

	$tblSalesPaymentsData = array(
		'SPSalesID' => $salesID, 
		'SPPaymentMethodID' => $paymentMethod, 
		'SPAmount' => $SPAmount, 
		'SPAuthCode' => $SPAuthCode ?? null, 
		'SPLast5Digits' => $SPLast5Digits ?? null, 
		'SPCheckNumber' => $SPCheckNumber ?? null, 
		'SPEmpID' => $employee_id, 
		'SPDOE' => $date 
	);
	$SDPDO->InsertData(TBL_SALES_PAYMENTS, $tblSalesPaymentsData);


	$data['response'] = true;
	if($isVoid == 0){
		$url = SITE_URL.'?destination=sales';
	}else{
		$url = SITE_URL.'?destination=sales&status=true&msg=invoice-quoted';
	}
	$data['url'] = $url;
}

$SalesID = '';
if($isVoid == 0){
	$SalesID = $salesID;
	$cont = '';
	$cont .= SalesReceipt($SalesID);
	$data['cont'] = $cont;
}

$due = 0;
$PaymentLoop = false;
if($_POST['pm_name'] != 'House Account'){
	if($SalesOBJ->PaidAmt($salesID) < $SalesOBJ->Total($salesID)){
		$PaymentLoop = true;
		$due = $SalesOBJ->Total($salesID) - $SalesOBJ->PaidAmt($salesID);
	}
}
$lpmd = '';
$lpmd .= LPMD($salesID);
$lpmd .= '<div class="remaining-panel">Balance Remaining : <span>$'.$due.'</span></div>';
$data['lpmd'] = $lpmd;
$data['PaymentLoop'] = $PaymentLoop;
$data['SalesID'] = $salesID;
$data['due'] = $due;

echo json_encode($data);

?>