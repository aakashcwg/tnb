<?php
$WH_Name = $_POST['WH_Name'];
$WH_Address = $_POST['WH_Address'];
$WH_City = $_POST['WH_City'];
$WH_State = $_POST['WH_State'];
$WH_Zip = $_POST['WH_Zip'];
$WH_Phone = $_POST['WH_Phone'];
$sql = ' INSERT INTO tblWarehouses VALUES ( ?, ?, ?, ?, ?, ? ) ';
$stmt = $pdo->prepare($sql);
$stmt->execute([$WH_Name, $WH_Address, $WH_City, $WH_State, $WH_Zip, $WH_Phone]);
$lastInsertId = $pdo->lastInsertId();
if( $stmt->rowCount() > 0 ){
	$data['response'] = true;
	$row = '<tr id="row-'.$lastInsertId.'">
				<td>'.$WH_Name.'</td>
				<td>'.$WH_Address.'</td>
				<td>'.$WH_City.'</td>
				<td>'.$WH_State?getUSAStates()[$WH_State]:''.'</td>
				<td>'.$WH_Zip.'</td>
				<td>'.$WH_Phone.'</td>
				<td><i class="fa fa-edit sd-tootip" title="" data-id="'.$lastInsertId.'" style="font-size:24px;cursor:pointer;" onclick="editWareHouse(this)" data-original-title="Quick Edit"></i></td>
			</tr>';
	$data['row'] = $row;
}else{
	$data['response'] = false;
}
echo json_encode($data);
?>