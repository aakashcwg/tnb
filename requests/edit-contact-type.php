<?php 
$ctid = $_POST['id'];
$ctname = $ListTypeOBJ->GetTheContactTypeByID($ctid);
$data['list_type'] = $ctname;
$data['id'] = $ctid;
$data['request_action'] = str_replace('edit', 'update', $_POST['action']);
$data['modal_title'] = ucwords(str_replace('_', ' ', $_POST['action']));
$data['label_1'] = ucwords(str_replace('_', ' ', str_replace('edit_', '', $_POST['action'])));
echo json_encode($data);
?>
