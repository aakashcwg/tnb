<?php

$status = $_POST['status'];
$order_id = isset($_POST['order_id'])?$_POST['order_id']:'';
$itemID = isset($_POST['itemID'])?$_POST['itemID']:'';
$items = getOrderDetails($order_id);
$date = date('m/d/Y H:i:sa');
switch ($status) {
	case 'completed':
		if(!empty($itemID)){
			$sql = ' UPDATE dbo.tblInventoryOrderDetails SET IODDateShipped = ? WHERE IODID = ? ';
			$statement = $pdo->prepare($sql);
			$statement->execute([$date, $itemID]);
			if($statement->rowCount()){
				$data['resp'] = true;
				$data['msg'] = 'Item status updated succesfully';
				$data['date'] = date('d-m-Y H:i:sa');
				$data['item'] = $itemID;
			}
		}else{
			for ($i=0; $i < count($items); $i++) { 
				$sql = ' UPDATE dbo.tblInventoryOrderDetails SET IODDateShipped = ? WHERE IODIOID = ? ';
				$statement = $pdo->prepare($sql);
				$statement->execute([$date, $order_id]);
			}
			if($statement->rowCount()){
				$data['resp'] = true;
				$data['msg'] = 'Order status updated succesfully';
				$data['date'] = date('d-m-Y H:i:sa');
				$data['status'] = 'C';
			}
		}
		break;

	case 'pending':
		if(!empty($itemID)){
			$sql = ' UPDATE dbo.tblInventoryOrderDetails SET IODDateShipped = ? WHERE IODID = ? ';
			$statement = $pdo->prepare($sql);
			$statement->execute([null, $itemID]);
			if($statement->rowCount()){
				$data['resp'] = true;
				$data['msg'] = 'Item status updated succesfully';
				$data['date'] = null;
				$data['item'] = $itemID;
			}
		}else{
			for ($i=0; $i < count($items); $i++) { 
				$sql = ' UPDATE dbo.tblInventoryOrderDetails SET IODDateShipped = ? WHERE IODIOID = ? ';
				$statement = $pdo->prepare($sql);
				$statement->execute([null, $order_id]);
			}
			if($statement->rowCount()){
				$data['resp'] = true;
				$data['msg'] = 'Order status updated succesfully';
				$data['date'] = null;
				$data['status'] = 'P';
			}
		}
		break;
	
	default:
		# code...
		break;
}

echo json_encode($data);



?>