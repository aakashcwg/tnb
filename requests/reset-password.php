<?php

$validation_error = '';
$message = '';

$ActiveUser = currentUser('EmployeeID');

$NewPass 	= $_POST['password'];
$EmployeeID = $_POST['employee_id'];

if(empty($NewPass)){
	$error[] = 'You must enter a password';
}

if(is_admin() || is_manager()){
	if(empty($error)){
		$update = $EmployeeOBJ->ResetPassword($EmployeeID, $NewPass);
		if($update){
			$message = 'Password successfully updated';
		}
	}else{
		$validation_error = implode(", ", $error);
	}
}else{
	$validation_error = "You are not allowed to reset password. Contact to Manager or Administrator";
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);
echo json_encode($output);

?>