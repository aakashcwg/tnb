<?php

$EID = currentUser('EmployeeID');

$ManagerUsername = $_POST['ManagerUsername'];
$ManagerPassword = $_POST['ManagerPassword'];

$message = '';
$validation_error = '';


if(empty($ManagerUsername)){
	$error[] = 'Username is required';
}
if(empty($ManagerPassword)){
	$error[] = 'Password is required';
}

if(empty($error)){
	$TheEmployee = $EmployeeOBJ->GetTheEmployee($ManagerUsername);
	if($TheEmployee){
		$Role = get_the_role($TheEmployee[0]->ELRoleID);
		if($Role == 'Manager' || $Role == 'manager'){
			if($TheEmployee[0]->ELPassword === $ManagerPassword){
				$_SESSION['manager_auth_for_'.$EID] = $TheEmployee[0]->ELRoleID;
				$_SESSION['auth_manager'] = $TheEmployee[0]->EmployeeID;
				$message = 'You are now authenticated by manager : '.$TheEmployee[0]->ELUsername;
			}else{
				$validation_error = "Wrong password";
			}
		}else{
			$validation_error = "You need only manager authentication";
		}
	}else{
		$validation_error = "Username doesn't exist";
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);

echo json_encode($output);

?>