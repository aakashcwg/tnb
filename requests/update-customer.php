<?php
$customer_EmployeeID = currentUser('EmployeeID');

$CustomerID = $_POST['customer-id'];
$customer_FirstName = $_POST['cfname'] ?? null;
$customer_LastName = $_POST['clname'] ?? null;
$customer_Company = $_POST['ccompany'] ?? null;
$customer_AccountNumber = $_POST['caccno'] ?? null;
$customer_TaxExempt = $_POST['tax_exempt'] ?? null;
$customer_PLID = $_POST['customer-pl'] ?? null;
$customer_date = date(DATE_TIME_FORMAT);
$customer_IsActive = 0;
if(isset($_POST['cisactivecheck']) && $_POST['cisactivecheck'] == 'on'){
    $customer_IsActive = 1;
}

$customer_Type = $_POST['customertype'] ?? null;
$customer_ContactType = $_POST['ccontacttype'] ?? null;
$customer_ContactData = $_POST['ccdata'] ?? null;
$customer_IsPrimaryContact = 0;
if(isset($_POST['cpcontact']) && $_POST['cpcontact'] == 'on'){
  $customer_IsPrimaryContact = 1;  
}

$customer_AddressType = $_POST['caddrtype'] ?? null;
$customer_Address1 = $_POST['caddr1'] ?? null;
$customer_Address2 = $_POST['caddr2'] ?? null;
$customer_City = $_POST['ccity'] ?? null;
$customer_State = $_POST['cstate'] ?? null;
$customer_Zip = $_POST['czip'] ?? null;

$taxID = $_POST['tax-id'] ?? null;
$taxExpireDate = $_POST['expiration-date'] ?? null;

$message = '';
$validation_error = '';

if(empty($customer_FirstName)){
    $error[] = 'First name is required';
}
if(empty($customer_LastName)){
    $error[] = 'Last name is required';
}
if(empty($customer_AddressType)){
    $error[] = 'Address type is required';
}
// if(empty($customer_Address1)){
//     $error[] = 'Address1 is required';
// }
// if(empty($customer_City)){
//     $error[] = 'City is required';
// }
// if(empty($customer_State)){
//     $error[] = 'State is required';
// }
if(empty($customer_Zip)){
    $error[] = 'Zip code is required';
}
if(empty($customer_Type)){
    $error[] = 'Customer type is required';
}
if(empty($customer_ContactType)){
    $error[] = 'Contact type is required';
}
if(empty($customer_ContactData)){
    $error[] = 'Contact data is required';
}



if(empty($error)){

    if($SDPDO->CheckExistanceExcept(TBL_CUSTOMERS, 'CustomerID', $CustomerID, 'CustomerAccountNumber', $customer_AccountNumber) === true){

        $C_OBJ = $CustomerOBJ->GetTheCustomerByAccountNumber($customer_AccountNumber);
        if($C_OBJ){
            $C_NAME = VC_Name($C_OBJ->CustomerFName, $C_OBJ->CustomerLName, $C_OBJ->CustomerCompanyName);
        }else{
            $C_NAME = 'Anonymous';
        }
        $validation_error = 'You have already used that account number in the system for '.trim($C_NAME).', please enter a different account number';
    }else{

        // 'tblCustomers' data
        $tblCustomers_values = array($customer_FirstName, $customer_LastName, $customer_Company, $customer_AccountNumber, $customer_TaxExempt, $customer_PLID, $customer_EmployeeID, $customer_date, $customer_IsActive);

        $update1 = $SDPDO->update(TBL_CUSTOMERS, array('CustomerFName', 'CustomerLName', 'CustomerCompanyName', 'CustomerAccountNumber', 'CustomerIsTaxExempt', 'CustomerPLID', 'IsActive'), array($customer_FirstName, $customer_LastName, $customer_Company, $customer_AccountNumber, $customer_TaxExempt, $customer_PLID, $customer_IsActive), array('CustomerID', $CustomerID));

        //Logging
        if($update1){
            $LoggingOBJ->UpdateCustomerLog(array($CustomerID, $customer_EmployeeID, $customer_date, UPDATE_CUSTOMER_LOG_NOTE));
        }

        $update2 = $SDPDO->update(TBL_CUSTOMER_CONTACT, array('CCCTID', 'CCData', 'CCIsPrimary', 'CCIDFName', 'CCIDLname'), array($customer_ContactType, $customer_ContactData, $customer_IsPrimaryContact, $customer_FirstName, $customer_LastName), array('CCIDCustomerID', $CustomerID));
        //Logging
        if($update2){
            $LoggingOBJ->UpdateCustomerLog(array($CustomerID, $customer_EmployeeID, $customer_date, 'Customer Contact ID : '.$CustomerOBJ->CCID($CustomerID).' // Updated customer contact'));
        }

        $update3 = $SDPDO->update(TBL_CUSTOMER_ADDRESSES, array('CAAddressTypeID', 'CAAddress1', 'CAAddress2', 'CACity', 'CAState', 'CAZip'), array($customer_AddressType, $customer_Address1, $customer_Address2, $customer_City, $customer_State, $customer_Zip), array('CACustomerID', $CustomerID));
        //Logging
        if($update3){
            $LoggingOBJ->UpdateCustomerLog(array($CustomerID, $customer_EmployeeID, $customer_date, 'Customer Address ID : '.$CustomerOBJ->CAID($CustomerID).' // Updated customer address'));
        }

        $update4 = $SDPDO->update(TBL_CUSTOMER_ACCOUNT_TYPE, array('CATCATLID'), array($customer_Type), array('CATCustomerID', $CustomerID));
        //Logging
        if($update4){
            $LoggingOBJ->UpdateCustomerLog(array($CustomerID, $customer_EmployeeID, $customer_date, 'Customer Account Type ID : '.$CustomerOBJ->CATID($CustomerID).' // Updated customer account type'));
        }

        $update5 = $SDPDO->update(TBL_CUSTOMER_TAX_DATA, array('CTDTaxIDNumber', 'CTDExpirationDate'), array($taxID, $taxExpireDate), array('CTDCustomerID', $CustomerID));
        //Logging
        if($update5){
            $LoggingOBJ->UpdateCustomerLog(array($CustomerID, $customer_EmployeeID, $customer_date, 'Customer Tax Data ID : '.$CustomerOBJ->CTDID($CustomerID).' // Updated customer tax data'));
        }

        $message = 'Customer successfully updated';
    }

}else{
    $validation_error = implode(", ", $error);
}

$output = array(
    'error' => $validation_error,
    'message' => $message,
    'redirect_url' => SITE_URL.'?destination=customers&action=customer-profile&cid='.base64_encode($CustomerID),
);

echo json_encode($output);
