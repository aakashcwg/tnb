<?php 
$Shelf_ID = $_POST['Shelf_ID'];
$Shelf = getShelf($Shelf_ID);
$data['Shelf_Name'] = $Shelf->ShelfName;
$data['Shelf_Location'] = $Shelf->ShelfLocation;

$Warehouse = '';
$Warehouse .= '<label for="warehouse">Warehouse<sup>*</sup></label>';
$Warehouse .= '<select id="warehouse" class="form-control sd-select" data-show-subtext="true" data-live-search="true">';
$Warehouse .= '<option value="">-- Select Warehouse --</option>';
foreach (getWareHouses() as $WH) {
	if($Shelf->SWarehouseID == $WH->WarehouseID){
		$selected = 'selected';
	}else{
		$selected = '';
	}
	$Warehouse .= '<option value="'.$WH->WarehouseID.'" '.$selected.'>'.$WH->WName.'</option>';
}

$Warehouse .= '</select>';

$data['Shelf_Warehouse'] = $Warehouse;
$data['Shelf_ID'] = $Shelf_ID;

echo json_encode($data);
?>