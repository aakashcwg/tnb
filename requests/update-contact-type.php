<?php
$ctname = $_POST['typeName'];
$ctid = $_POST['id'];
if(!empty($ctname)){
	if($SDPDO->CheckExistanceExcept(TBL_CONTACT_TYPES, 'CTID', $ctid, 'CTName', $ctname)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Contact Type already exists';
	}else{
		$update = $ListTypeOBJ->UpdateContactType(array($ctname), array('CTID', $ctid));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Contact Type successfully updated';
			$data['id'] = $ctid;
			$data['typeName'] = $ctname;
			$data['table_id'] = 'contact-type-table';
		}
	}
}else{
	$data['blank'] = true;
}
echo json_encode($data);
?>