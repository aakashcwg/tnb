<?php
//void-sale.php
$employee_id = currentUser('EmployeeID');
$SalesID = $_POST['SalesID'] ?? null;
$CustomerID = $_POST['CustomerID'] ?? null;

$VoidNote = $_POST['VoidNote'] ?? null;

$drawer_id = null;
if(isset($_SESSION['drawer_id'])){
	$drawer_id = $_SESSION['drawer_id'];
}

$data = array();

$resp = '';
if($SalesID){
	if(is_admin() || is_manager() || is_cashier()) {

		$SalesOBJ->MakeVoid($SalesID);

		$SPreviousSalesID = $SalesID;
		$Old_invoice = $SalesOBJ->GetSalesInfo($SalesID, 'SalesInvoiceNumber');
		$CustomerID = $SalesOBJ->GetSalesInfo($SalesID, 'SCustomerID');
		$Old_salesAmount = $SalesOBJ->GetSalesInfo($SalesID, 'SAmount');
		$Old_salesTax = $SalesOBJ->GetSalesInfo($SalesID, 'STax');
		$Old_shipingCost = $SalesOBJ->GetSalesInfo($SalesID, 'SShippingCost');
		$Old_paymentMethod = $SalesOBJ->GetSalesInfo($SalesID, 'SPaymentMethodID');
		$Old_SIsPaid = $SalesOBJ->GetSalesInfo($SalesID, 'SIsPaid');
		$Old_SDOEStart = $SalesOBJ->GetSalesInfo($SalesID, 'SDOEStart');
		$Old_SDOEEnd = $SalesOBJ->GetSalesInfo($SalesID, 'SDOEEnd');
		$Old_SNotes = $SalesOBJ->GetSalesInfo($SalesID, 'SNotes');
		$Old_SPONumber = $SalesOBJ->GetSalesInfo($SalesID, 'SPONumber');

		$invoice 			= $Old_invoice;
		$date 				= date(DATE_TIME_FORMAT);
		$salesAmount 		= '-'.$Old_salesAmount;
		$salesTax 			= '-'.$Old_salesTax;
		$shipingCost 		= '-'.$Old_shipingCost;
		$paymentMethod 		= $Old_paymentMethod;
		$SIsPaid			= $Old_SIsPaid;
		$startDate 			= $Old_SDOEStart;
		$endDate 			= $Old_SDOEEnd;
		$SNote 				= $VoidNote; //Void Note
		$purchaseOrder		= $Old_SPONumber;

		$tblSales_values = array( $invoice, $CustomerID, $date, $salesAmount, $salesTax, $shipingCost, $paymentMethod, $SIsPaid, $SPreviousSalesID, 0, $startDate, $endDate, $SNote, $employee_id, $drawer_id, $purchaseOrder );

		$InsertID = $SalesOBJ->MakeSale($tblSales_values);

		$LogValues = array($SalesID, null, $employee_id, $date, SALES_MAKE_VOID_NOTE.' | Reason : '.$VoidNote, null);
		$LoggingOBJ->SalesMakeVoidLog($LogValues);

		if($InsertID){
			$resp = true;
		}else{
			$resp = false;
		}
		$data['resp'] = $resp;
		
	}else{
		$data['error_msg'] = 'Permission denied! You must be a Cashier at least.';
	}
echo json_encode($data);
}