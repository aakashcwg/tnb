<?php
//update-price-list-item.php

//print_r($_POST);

$priceList = $_POST['price-list'];
$item = $_POST['item'];
$isPercentage = isset($_POST['is-percentage']) ? 1 : 0;
$plAmount = $_POST['pl-amount'];
$PLIID = $_POST['pliid'];

$plAmount = str_replace(['%', '$'], '', $plAmount);

$error = '';
$resp = '';
$message = '';
$tr = '';
if(empty($priceList)){
	$error = 'Price List is required';
}elseif(empty($item)){
	$error = 'Item is required';
}

if(empty($error)){
	$PLIINVID_IsExist = $SDPDO->CheckExistanceExcept(TBL_PRICE_LIST_ITEMS, 'PLIID', $PLIID, 'PLIINVID', $item);
	//if($PLIINVID_IsExist){
		//$error = 'Item already added in a list';
	//}else{
		$update = $SDPDO->update(TBL_PRICE_LIST_ITEMS, array('PLIPLID', 'PLIINVID', 'PLIIsPercentage', 'PLIAmount'), array($priceList, $item, $isPercentage, $plAmount), array('PLIID', $PLIID));
		if($update){
			$message = 'Price list successfully updated';
			$PLI = GetPriceListItem($PLIID);
			$PLIIsPercentage = $PLI->PLIIsPercentage==1?'Yes':'No';
			$tr = '';
			$tr .= '<td>'.PriceListName($PLI->PLIPLID).'</td>';
			$tr .= '<td><a href="'.ItemUrl($PLI->PLIINVID).'">'.getItemWithNumber($PLI->PLIINVID).'</a></td>';
			$tr .= '<td>'.$PLIIsPercentage.'</td>';
			$tr .= '<td>'.number_format($PLI->PLIAmount, 2).'</td>';
			$tr .= '<td><i class="fa fa-edit sd-tootip edit-price-list-item" data-id="'.$PLI->PLIID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editPriceListItem(this)"></i></td>';
		}
	//}
}

//Response data
$data['pliid'] = $PLIID;
$data['error'] = $error;
$data['message'] = $message;
$data['tr'] = $tr;

echo json_encode($data);