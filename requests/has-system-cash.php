<?php
//has-system-cash.php
$resp = false;
if(is_user_logged_in()){
	if(is_cashier()){
		if(isset($_SESSION['system_cash']) && !empty($_SESSION['system_cash'])){
			$resp = true;
		}
	}
}
$data['resp'] = $resp;
echo json_encode($resp);