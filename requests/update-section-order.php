<?php 

$SectionIDs = explode(',', $_POST['section_id_array']);

for ($i = 0; $i < count($SectionIDs) ; $i++) {

	$sql = ' UPDATE tblSections SET SectionOrder = ? WHERE SectionID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([ $i, $SectionIDs[$i] ]);

}
$data['msg'] = 'Section order updated successfully';
echo json_encode($data);
?>