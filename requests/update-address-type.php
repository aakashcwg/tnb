<?php
$atname = $_POST['typeName'];
$atid = $_POST['id'];
if(!empty($atname)){
	if($SDPDO->CheckExistanceExcept(TBL_ADDRESS_TYPES, 'ATID', $atid, 'ATName', $atname)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Address Type already exists';
	}else{
		$update = $ListTypeOBJ->UpdateAddressType(array($atname), array('ATID', $atid));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Address Type successfully updated';
			$data['id'] = $atid;
			$data['typeName'] = $atname;
			$data['table_id'] = 'address-type-table';
		}
	}
}else{
	$data['blank'] = true;
}
echo json_encode($data);
?>