<?php
$itemID = $_POST['itemID'] ?? null;
$itemType = $_POST['itemType'] ?? null;
$data = array();
if($itemID){
	if($itemType == 'inv'){
		$PL = '';
		if(!empty($itemID)){
			$price = getItemPrice($itemID);
			//special
			if(isset($_POST['Customer']) && !empty($_POST['Customer'])){
				$PL = GetCustomerPL($_POST['Customer']);
			}
			if($PL){
				$PLOBJ = getPriceList($PL);
				if($PLOBJ){
					if($PLOBJ->PLHasItems == 0){
						if($PLOBJ->PLIsPercentage == 1){
							$price = $price*(1 - floatval($PLOBJ->PLAmount)/100);
						}else{
							$price = $price - floatval($PLOBJ->PLAmount);
						}
						$data['color'] = GetPriceListColor($PLOBJ->PLID);
					}else{
						$Amt = GetPLIAmountByPLIDAndItemID($PL, $itemID);
						$pliidOBJ = GetPriceListItemByPLIDAndItemID($PL, $itemID);
						if($Amt){

							if($pliidOBJ->PLIIsPercentage == 1){
								$Amt = $price*(1 - floatval($pliidOBJ->PLIAmount)/100);
							}else{
								$Amt = $price - floatval($pliidOBJ->PLIAmount);
							}

							$price = $Amt;
							$data['sp'] = true;
							$data['color'] = GetPriceListColor($PL);
						}
					}
				}
			}
			$data['price'] = number_format($price, 2);
			$tax = getSalesTax();
			if($CustomerOBJ->IsTaxExempt($_POST['Customer'])){
				$tax = 0.00;
			}
			$data['tax'] = number_format($price*($tax/100), 2);
			$data['qty'] = GetOnHandQty(GetOnHandID($itemID));
		}
	}
	if($itemType == 'non-inv'){
		$data['price'] = number_format($InventoryOBJ->GetNonInvPrice($itemID), 2);
		$tax = getSalesTax();
		if($CustomerOBJ->IsTaxExempt($_POST['Customer'])){
			$tax = 0.00;
		}
		$data['tax'] = number_format($InventoryOBJ->GetNonInvPrice($itemID)*($tax/100), 2);
	}
	echo json_encode($data);
}
?>