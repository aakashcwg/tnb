<?php

//add-inventory-item.php

$itemname = $_POST['itemname'] ?? null;
$itemnumber = $_POST['itemnumber'] ?? null;
$itembarcode = $_POST['itembarcode'] ?? null;
$itemserialnumber = $_POST['itemserialnumber'] ?? null;
$itemdescription = $_POST['itemdescription'] ?? null;

$itemwarrenty = $_POST['itemwarrenty'] ?? 0;
$itemisactive = 0;
if(isset($_POST['itemisactive'])){
	$itemisactive = 1;
}

$minqty = $_POST['minqty'] ?? 1;
$maxqty = $_POST['maxqty'] ?? 10;

$Values = array( $itemname, $itemnumber, $itembarcode, $itemserialnumber, $itemdescription, $itemwarrenty, $itemisactive, $maxqty, $minqty );

$message = null;
$validation_error = null;
if(trim($itemname) == ''){
	$error[] = 'Item name is required';
}
if(trim($itemnumber) == ''){
	$error[] = 'Item number is required';
}
if(trim($minqty) == ''){
	$error[] = 'MinQty is required';
}
if(trim($maxqty) == ''){
	$error[] = 'MaxQty is required';
}

if(trim($minqty) != ''){
	if($minqty <= 0){
		$error[] = 'MinQty value should be atleast 1';
	}
}

if(trim($maxqty) != ''){
	if($maxqty <= $minqty){
		$error[] = 'MaxQty value should be greater than MinQty';
	}
}

if(empty($error)){
	if( $SDPDO->CheckExistance(TBL_INVENTORY, 'INVItemNumber', $_POST['itemnumber']) === false ){
		$InventoryOBJ->AddNewInventory($Values);
		$message = 'Item successfully added.';

		//Logging
		$EmpID = currentUser('EmployeeID');
		$AIIOID = null;
		$AIIOHID = null;
		$LogValues = array( $EmpID, $AIIOID, $AIIOHID );
		$LoggingOBJ->AddNewInventoryItemLog($LogValues);	

	}else{
		$validation_error = 'Item number already exists!';
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' 	=> $validation_error,
	'message' 	=> $message
);

echo json_encode($output);

?>