<?php
$typeName = $_POST['typeName'];
$data['action'] = $_POST['action'];
if(!empty($typeName)){
	if($SDPDO->CheckExistance(TBL_CUSTOMER_ACCOUNT_TYPE_LIST, 'CATLName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Customer type already exists';
	}else{
		$InsertID = $ListTypeOBJ->AddNewCustomerAccountType(array($typeName));
		if($InsertID){
			$id = $InsertID;
			$data['resp'] = true;
			$data['successMsg'] = 'Customer type successfully added';
			$data['id'] = $id;
			$data['table_id'] = 'customer-type-table';
			$data['typeName'] = $typeName;
			$data['row'] = '<tr id="'.$id.'">
					<td>'.$typeName.'</td>
					<td><i class="fa fa-edit sd-tootip edit-customer-type" data-action="edit_customer_type" data-id="'.$id.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
				</tr>';
		}
	}
}else{
	$data['blank'] = true;
}
echo json_encode($data);
?>
