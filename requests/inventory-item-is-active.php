<?php

$item_id = $_POST['item_id'];
switch ($_POST['status']) {
	case 'active':
		$status = $InventoryOBJ->DoInActive($item_id);
		break;

	case 'inactive':
		$status = $InventoryOBJ->DoActive($item_id);
		break;
}

if($status === true){
	$theInventory = $InventoryOBJ->GetTheInventoryByID($item_id);
	$isActive = $theInventory[0]->IsActive==1?'<img src="dist/images/active.png" class="active-inv-item sd-tootip isactive" title="Active" onclick="invIsActive(this)" data-status="active">':'<img src="dist/images/disabled.png" class="inactive-inv-item sd-tootip isactive" title="Inactive" onclick="invIsActive(this)" data-status="inactive">';

	if($theInventory[0]->IsActive==1){
		$text = 'You recently <span class="active-state">ACTIVE</span> the Item : '.$theInventory[0]->INVItemNumber;
	}
	if($theInventory[0]->IsActive==0){
		$text = 'You recently <span class="inactive-state">INACTIVE</span> the Item : '.$theInventory[0]->INVItemNumber;
	}

	$tr = $isActive;
	$data['resp'] = true;
	$data['tr'] = $tr;
	$data['item_id'] = $item_id;
	$data['message'] = $text;
}else{
	$data['resp'] = false;
	$data['errorMsg'] = 'RESPONSE :: ERROR';
}
echo json_encode($data);

?>