<?php 
$id = $_POST['id'];
$NUM = getLedgerNum($id);
$NUMDesc = getLedgerNumDesc($id);
$data['list_type'] = $NUM;
$data['list_type_desc'] = $NUMDesc;
$data['id'] = $id;
$data['request_action'] = str_replace('edit', 'update', $_POST['action']);
$data['modal_title'] = ucwords(str_replace('_', ' ', $_POST['action']));
$data['label_1'] = ucwords(str_replace('_', ' ', str_replace('edit_', '', $_POST['action'])));
echo json_encode($data);
?>