<?php
//search-inventory.php

//print_r($_POST);
$page = $_POST['PageNum'];
$items = $_POST['PerPageItem'];

$result = $InventoryOBJ->GetSearchResults($_POST['SearchValue']);
$html = '';
if(empty(trim($_POST['SearchValue']))){
	foreach (FetchInventoriesByPage($page, $items) as $inventory) {
		$isActive = $inventory->IsActive==1?'<img src="dist/images/active.png" class="active-inv-item sd-tootip isactive" title="Active" onclick="invIsActive(this)" data-status="active">':'<img src="dist/images/disabled.png" class="inactive-inv-item sd-tootip isactive" title="Inactive" onclick="invIsActive(this)" data-status="inactive">';

		$html .= '<tr id="row-'.$inventory->INVID.'">';

            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.ItemUrl($inventory->INVID).'">'.$inventory->INVItemName.'</td>';

            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.ItemUrl($inventory->INVID).'">'.$inventory->INVItemNumber.'</td>';

            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.ItemUrl($inventory->INVID).'">'.$inventory->INVDescription.'</td>';


            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.ItemUrl($inventory->INVID).'">
                        '.$InventoryOBJ->GetOnHandQtyByInvID($inventory->INVID).'</td>';

            $IsWarrent = $inventory->INVIsWarrantyItem==1?'Yes':'No';

            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.ItemUrl($inventory->INVID).'">'.$IsWarrent.'</td>';

            $html .= '<td class="isactive">'.$isActive.'</td>';

            $html .= '<td class="action-btns">';
            	$html .= '<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="'.$inventory->INVID.'" style="font-size:24px;cursor:pointer;" onclick="editInventory(this)"></i>';
                $html .= '<a style="margin-left: 10px;" href="'.SITE_URL.'?destination=purchasing&itemID='.$inventory->INVID.'"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="Order this item"></i></a>';
			$html .= '</td>';

        $html .= '</tr>';
	}
}else{
	if($result){
		foreach ($result as $inventory) {
			$isActive = $inventory->IsActive==1?'<img src="dist/images/active.png" class="active-inv-item sd-tootip isactive" title="Active" onclick="invIsActive(this)" data-status="active">':'<img src="dist/images/disabled.png" class="inactive-inv-item sd-tootip isactive" title="Inactive" onclick="invIsActive(this)" data-status="inactive">';

			$html .= '<tr id="row-'.$inventory->INVID.'" data-item="'.$inventory->INVID.'">';

	            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)">'.$inventory->INVItemName.'</td>';

	            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)">'.$inventory->INVItemNumber.'</td>';

	            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)">'.$inventory->INVDescription.'</td>';


	            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)">
                        '.$InventoryOBJ->GetOnHandQtyByInvID($inventory->INVID).'</td>';


	            $IsWarrent = $inventory->INVIsWarrantyItem==1?'Yes':'No';

	            $html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)">'.$IsWarrent.'</td>';

	            $html .= '<td class="isactive">'.$isActive.'</td>';

	            $html .= '<td class="action-btns">';
	            	$html .= '<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="'.$inventory->INVID.'" style="font-size:24px;cursor:pointer;" onclick="editInventory(this)"></i>';
	                $html .= '<a style="margin-left: 10px;" href="'.SITE_URL.'?destination=purchasing&itemID='.$inventory->INVID.'"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="Order this item"></i></a>';
				$html .= '</td>';

	        $html .= '</tr>';
		}
	}else{
		$html .= '<tr>';
	        $html .= '<td colspan=7>No match item found</td>';
	    $html .= '</tr>';
	}
}



$output= array(
	'content' => $html,
);

echo json_encode($output);
?>