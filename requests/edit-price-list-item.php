<?php
//edit-price-list-item.php

//print_r($_POST);

$ListItemID = $_POST['ListItemID'] ?? null;
if($ListItemID){
	$PLI = GetPriceListItem($ListItemID);

	$PLIPLID = $PLI->PLIPLID;
	$PLIINVID = $PLI->PLIINVID;
	$PLIIsPercentage = $PLI->PLIIsPercentage;

	//$PLIAmount = $PLI->PLIAmount;

$PLIAmount = number_format($PLI->PLIAmount, 2);
if($PLI->PLIIsPercentage == 1){
  $PLIAmount = $PLIAmount.'%';
}
if($PLI->PLIIsPercentage == 0){
  $PLIAmount = '$'.$PLIAmount;
}


	//Price list dropdown
	$plDropDown = '';
	$plDropDown .= '<label for="price-list">Price List <sup>*</sup></label>';
    $plDropDown .= '<select class="form-control sd-select" id="price-list" name="price-list" data-live-search="true">';
    $plDropDown .= '<option value="">Select Price List</option>';
    $selected = '';
    foreach (getPriceLists() as $PL) {
    	if($PL->PLID == $PLIPLID){
    		$selected = 'selected';
    	}else{
    		$selected = '';
    	}
        $plDropDown .= '<option value="'.$PL->PLID.'" '.$selected.'>'.$PL->PLName.'</option>';
    }
    $plDropDown .= '</select>';

    //Item dropdown
	$itemDropdown = '';
	$itemDropdown .= '<label for="item">Item <sup>*</sup></label>';
	$itemDropdown .= '<select class="form-control sd-select" id="item" name="item" data-live-search="true">';
	$itemDropdown .= '<option value="">Select an Item</option>';
	$selected = '';
   	foreach (getInventories() as $INV) {
   		if($INV->INVID == $PLIINVID){
    		$selected = 'selected';
    	}else{
    		$selected = '';
    	}
    	$itemDropdown .= '<option value="'.$INV->INVID.'" '.$selected.'>'.getItemWithNumber($INV->INVID).'</option>';
    }
   	$itemDropdown .= '</select>';

   	//Response data
   	$data['pliid'] 				     = $ListItemID;
   	$data['plDropDown'] 		   = $plDropDown;
	  $data['itemDropdown'] 		 = $itemDropdown;
	  $data['PLIIsPercentage'] 	 = $PLIIsPercentage;
	  $data['PLIAmount'] 			   = $PLIAmount;

	echo json_encode($data);

}else{
	echo 'Invalid list item...';
}
