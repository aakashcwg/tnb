<?php
//login.php

$username = $_POST['username'];
$password = $_POST['password'];
$message = '';
$validation_error = '';
$url = '';
if(empty($username)){
	$error[] = "Username is required";
}
if(empty($password)){
	$error[] = "Password is required";
}

if(empty($error)){
	$TheEmployee = $EmployeeOBJ->GetTheEmployee($username);
	//echo '<pre>';
	//print_r($TheEmployee);
	if($TheEmployee){
		if($TheEmployee[0]->IsActive == 1){
			if($TheEmployee[0]->ELPassword === $password){

				//if($TheEmployee[0]->ELRoleID < 20){
					$DWCA = DrawerWithComputerArray();
					if(array_key_exists(COMPUTERNAME, $DWCA)){
						$drawer_id = $DWCA[COMPUTERNAME];
						$_SESSION['drawer_id'] = $drawer_id;
					}
						$_SESSION['systemuser'] = $username;
						$_SESSION['time_start_login'] = date('Y-m-d H:i:s');

						
			    		if($_POST["remember"] == 1) {
							setcookie ("member_login", $_POST["username"],time()+ (10 * 365 * 24 * 60 * 60));
							setcookie ("member_password", $_POST["password"],time()+ (10 * 365 * 24 * 60 * 60));
						} else {
							if(isset($_COOKIE["member_login"])) {
								setcookie ("member_login","");
							}
							if(isset($_COOKIE["member_password"])) {
								setcookie ("member_password","");
							}
						}
						$message = 'You are successfully logged in...';
					/*}else{
						$validation_error = "No Drawer is registered with this computer";
					}*/
				// }else{
				// 	$_SESSION['systemuser'] = $username;
				// 	$_SESSION['time_start_login'] = time();

		  //   		if($_POST["remember"] == 1) {
				// 		setcookie ("member_login", $_POST["username"],time()+ (10 * 365 * 24 * 60 * 60));
				// 		setcookie ("member_password", $_POST["password"],time()+ (10 * 365 * 24 * 60 * 60));
				// 	} else {
				// 		if(isset($_COOKIE["member_login"])) {
				// 			setcookie ("member_login","");
				// 		}
				// 		if(isset($_COOKIE["member_password"])) {
				// 			setcookie ("member_password","");
				// 		}
				// 	}
				// 	$message = 'You are successfully logged in...';
				// }

			}else{
				$validation_error = "Wrong password";
			}
		}else{
			$validation_error = "You are in deactive mode. Please contact to the Administrator";
		}
	}else{
		$validation_error = "Username doesn't exist";
	}
}else{
	$validation_error = implode(", ", $error);
}

if(!is_admin() && !is_manager()){
	$url = SITE_URL.'?destination=sales';
}
$output = array(
	'error' => $validation_error,
	'message' => $message,
	'url' => $url,
);

echo json_encode($output);

?>