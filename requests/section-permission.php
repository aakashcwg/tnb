<?php
//section-permission.php

$SectionID = $_POST['SectionID'];
$Permission = $_POST['Permission'];

$Exist = $SDPDO->CheckExistance(TBL_SECURITY_LEVELS, 'SLSectionID', $SectionID);
$resp = false;
if($Exist){
	$update = $SDPDO->update(TBL_SECURITY_LEVELS, array('SLRoleNeeded'), array($Permission), array('SLSectionID', $SectionID));
	if($update){
		$resp = true;
	}
}else{
	$InsertID = $SDPDO->InsertData(TBL_SECURITY_LEVELS, array('SLSectionID' => $SectionID, 'SLRoleNeeded' => $Permission));
	if($InsertID){
		$resp = true;
	}
}

$data['resp'] = $resp;

echo json_encode($data);

