<?php

$venID = $_POST['venID'];
$vcompany = $_POST['vcompany'] ?? null;
$vaccno = $_POST['vaccno'] ?? null;
$vfname = $_POST['vfname'] ?? null;
$vlname = $_POST['vlname'] ?? null;
$vaddrtype = $_POST['vaddrtype'] ?? null;
$vaddr1 = $_POST['vaddr1'] ?? null;
$vaddr2 = $_POST['vaddr2'] ?? null;
$vcity = $_POST['vcity'] ?? null;
$vstate = $_POST['vstate'] ?? null;
$vzip = $_POST['vzip'] ?? null;
$vcontacttype = $_POST['vcontacttype'] ?? null;
$vcdata = $_POST['vcdata'] ?? null;
$vpcontactcheck = $_POST['vpcontactcheck'] ?? null;
$visactivecheck = $_POST['visactivecheck'] ?? null;

$date = date(DATE_TIME_FORMAT);
$ActiveEmployee = currentUser('EmployeeID');


$update1 = $VendorOBJ->UpdateVendor(array($vcompany, $vaccno, $visactivecheck), array('VendorID', $venID));

if($update1){
	$LoggingOBJ->VendorLog(array($venID, $ActiveEmployee, $date, UPDATE_VENDOR_LOG_NOTE));
}

$update2 = $VendorOBJ->UpdateVendorAddresses(array($vaddrtype, $vaddr1, $vaddr2, $vcity, $vstate, $vzip), array('VAVendorID', $venID));
if($update2){
	$LoggingOBJ->VendorLog(array($venID, $ActiveEmployee, $date, 'Vendor Address ID : '.$VendorOBJ->VAID($venID).' // Updated vendor address'));
}

$update3 = $VendorOBJ->UpdateVendorContact(array($vcontacttype, $vcdata, $vpcontactcheck, $vfname, $vlname), array('VCIDVendorID', $venID));
if($update3){
	$LoggingOBJ->VendorLog(array($venID, $ActiveEmployee, $date, 'Vendor Contact ID : '.$VendorOBJ->VAID($venID).' // Updated vendor contact'));
}

$VS = ''; 
if(array_key_exists($vstate, getUSAStates())){
	$VS = getUSAStates()[$vstate];
}

$row = '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=vendors&action=vendor-profile&vid='.$venID.'" style="cursor: pointer;">'.$vcompany.'</td>
    	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=vendors&action=vendor-profile&vid='.$venID.'" style="cursor: pointer;">'.$vaccno.'</td>
                    <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=vendors&action=vendor-profile&vid='.$venID.'" style="cursor: pointer;">'.$vcity.'</td>
                    <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=vendors&action=vendor-profile&vid='.$venID.'" style="cursor: pointer;">'.$VS.'</td>
    	            <td class="action-btns">
    	            	<i class="fa fa-edit sd-tootip" title="" data-id="'.$venID.'" style="font-size:24px;cursor:pointer;" onclick="editVendor(this)" data-original-title="Quick Edit"></i>
    	            	<a style="margin-left: 10px;" href="'.SITE_URL.'?destination=purchasing&vid='.$venID.'"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="" data-original-title="place order"></i></a>
    				</td>';

//$data['resp'] = true;
$data['venID'] = $venID;
$data['vcompany'] = $vcompany;
$data['vaccno'] = $vaccno;
$data['vcity'] = $vcity;
$data['vstate'] = $VS;
$data['row'] = $row;
$data['request_from'] = $_POST['request_from'];
$data['profile_url'] = SITE_URL.'?destination=vendors&action=vendor-profile&vid='.base64_encode($venID);


echo json_encode($data);
