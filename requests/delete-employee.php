<?php

$eId = $_POST['eId'];
$LeaveNote = $_POST['LeaveNote'] ?? null;
$message = '';
$delete = $EmployeeOBJ->DeleteEmployee($eId, $LeaveNote);
if($delete === true){
	$message = 'Employee successfully removed';
}

$output = array(
	'eid' => $eId,
	'message' => $message
);

echo json_encode($output);

?>