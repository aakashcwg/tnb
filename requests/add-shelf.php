<?php
$Shelf_Name = $_POST['Shelf_Name'];
$Shelf_Location = $_POST['Shelf_Location'];
$Shelf_Warehouse = $_POST['Shelf_Warehouse'];
$sql = ' INSERT INTO tblShelves VALUES ( ?, ?, ? ) ';
$stmt = $pdo->prepare($sql);
$stmt->execute([$Shelf_Warehouse, $Shelf_Name, $Shelf_Location]);
$lastInsertId = $pdo->lastInsertId();
if( $stmt->rowCount() > 0 ){
	$data['response'] = true;
	$row = '<tr id="row-'.$lastInsertId.'">
				<td>'.$Shelf_Name.'</td>
				<td>'.$Shelf_Location.'</td>
				<td>'.getWareHouseName($Shelf_Warehouse).'</td>
				<td><i class="fa fa-edit sd-tootip" title="" data-id="'.$lastInsertId.'" style="font-size:24px;cursor:pointer;" onclick="editShelf(this)" data-original-title="Quick Edit"></i></td>
			</tr>';
	$data['row'] = $row;
}else{
	$data['response'] = false;
}
echo json_encode($data);
?>