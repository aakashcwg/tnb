<?php
//edit-tax-rate.php

$trid = $_POST['trid'] ?? null;
$data = array();
if($trid){
	$TR = $ListTypeOBJ->GetTheTaxRate($trid);
	$data['tr'] = $TR->TaxRatePercentage;
	$data['trn'] = $TR->TaxRateName;
	$data['trid'] = $TR->TaxRateID;
}
echo json_encode($data);
