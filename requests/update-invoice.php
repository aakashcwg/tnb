<?php 

$employee_id = currentUser('EmployeeID');

$drawer_id = null;
if(isset($_SESSION['drawer_id'])){
	$drawer_id = $_SESSION['drawer_id'];
}

$salesDetailsIDs 	= explode(',', $_POST['salesDetailsIDs']);
$itemTypes			= explode(',', $_POST['itemTypes']);
$qtys 				= explode(',', $_POST['qtys']);
$items 				= explode(',', $_POST['items']);
$costs 				= explode(',', $_POST['costs']);
$discs 				= explode(',', $_POST['discs']);
$taxs 				= explode(',', $_POST['taxs']);
$ItemNote 			= explode(',', $_POST['ItemNote']);


$pm_name = $_POST['pm_name'] ?? null;

$ctm = 0.00;
if($pm_name == 'Cash'){
	$ctm = $_POST['ctm'];
}


$taxVal = 0;
foreach ($taxs as $tax) {
	$taxVal = $tax+$taxVal;
}

$data = array();
//Data for tblSales
$invoice 			= $_POST['invoice'];
$customer 			= $_POST['customer'];
$date 				= date(DATE_TIME_FORMAT);
$salesAmount 		= trim($_POST['paid_amount']);
$salesTax 			= $taxVal;
$shipingCost 		= 0;
$paymentMethod 		= $_POST['payment_method'] ?? null;
$isPaid 			= $_POST['is_paid'];
$previousSalesID 	= null; //LastSalesID($customer);
$isVoid 			= 0;
$startDate 			= date('m/d/Y H:i:sa', strtotime($SalesOBJ->GetSalesInfo($_POST['salesID'], 'SDOEStart')));
$endDate 			= $date;

$note 				= $_POST['note'] ?? '';

if(empty($salesAmount)){
	$isPaid = 0;
	$isVoid = 1;
}

$purchaseOrder 		= $_POST['purchaseOrder'] ?? null;

$tblSalesData = array($invoice, $customer, $date, $salesAmount, $salesTax, $shipingCost, $paymentMethod, $isPaid, $previousSalesID, $isVoid, $startDate, $endDate, $note, $employee_id, $drawer_id, $purchaseOrder);

$tblSalesUpdate = $SalesOBJ->UpdateInvoice($tblSalesData, array('SalesID', $_POST['salesID']));

if($tblSalesUpdate === true){

	$LoggingOBJ->CustomerCompleteSalesFromQuoteLog(array($customer, $employee_id, $date, CUSTOMER_COMPLETE_SALES_FROM_QUOTE_LOG_NOTE));

	if(isset($_SESSION['manager_auth_for_'.$employee_id])){
		unset($_SESSION['manager_auth_for_'.$employee_id]);
	}
	
	for ($i=0; $i < count($salesDetailsIDs) ; $i++) {

		//Data for tblSalesDetails
		//$OnHandID 			= GetOnHandID($items[$i]);
		//$SDNonInvID			= null;

		$OnHandID			= null;
		$SDNonInvID			= null;
		if($itemTypes[$i] == 'inv'){
			$OnHandID = GetOnHandID($items[$i]);
		}else{
			if($itemTypes[$i] == 'non-inv'){
				$SDNonInvID	= $items[$i];
			}
		}

		$SDPrice 			= $costs[$i];
		$SDPricePaid 		= null;
		$SDQuantity 		= $qtys[$i];
		$SDNotes 			= $ItemNote[$i];
		$SDIsCompanyInstall = 0;
		$SDDOE 				= $date;
		$SDDiscountAmount 	= $discs[$i];
		$SDTaxAmount		= $taxs[$i];

		$tblSalesDtailsData = array($_POST['salesID'], $OnHandID, $SDNonInvID, $SDPrice, $SDPricePaid, $SDQuantity, $SDNotes, $SDIsCompanyInstall, $SDDOE, $SDDiscountAmount, $SDTaxAmount);

		$SalesOBJ->UpdateInvoiceData($tblSalesDtailsData, array('SDID', $salesDetailsIDs[$i]));

		if($isVoid == 0){
			//update onhand quantity
			$NewQuantity = GetOnHandQty($OnHandID) - $SDQuantity;
			$InventoryOBJ->UpdateOnHandItemQuantity(array($NewQuantity), array('IOHID', $OnHandID));
		}

		//Sales log
		if(SALES_LOG === true){
			$ASLNotes = QUOTED_ITEMS_SALES_LOG_NOTE;
			$LogValues = array($_POST['salesID'], $salesDetailsIDs[$i], $employee_id,  $date, $ASLNotes, $OnHandID);
			$SalesOBJ->MakeSalesLog($LogValues);
		}

	}

		$SPCheckNumber = '';
		$SPLast5Digits = '';
		$SPAuthCode = '';
		$SPAuthCode = $_POST['_receipt_total'] ?? null;
		if($pm_name == 'Check'){
			$SPCheckNumber = $_POST['_check_no'] ?? null;
		}
		if($pm_name == 'Visa' && $pm_name == 'Mastercard' && $pm_name == 'Discover' && $pm_name == 'American Express'){
			$SPLast5Digits = $_POST['_5digit'] ?? null;
			$SPAuthCode = $_POST['_auth_code'] ?? null;
		}

		$tblSalesPaymentsData = array(
			'SPSalesID' => $_POST['salesID'], 
			'SPPaymentMethodID' => $paymentMethod,
			'SPAmount' => $SPAuthCode, 
			'SPAuthCode' => $SPAuthCode ?? null, 
			'SPLast5Digits' => $SPLast5Digits ?? null, 
			'SPCheckNumber' => $SPCheckNumber ?? null, 
			'SPEmpID' => $employee_id, 
			'SPDOE' => $date 
		);
		$SDPDO->InsertData(TBL_SALES_PAYMENTS, $tblSalesPaymentsData);



	$SalesID = '';
	if($isVoid == 0){
		$SalesID = $_POST['salesID'];
		$cont = '';
		$cont .= SalesReceipt($SalesID);
		$data['cont'] = $cont;
	}

	$data['response'] = true;
	$data['url'] = SITE_URL.'?destination=sales';
}

$due = 0;
$PaymentLoop = false;
$salesID = $_POST['salesID'];


if($_POST['pm_name'] != 'House Account'){
	if($SalesOBJ->PaidAmt($salesID) < $SalesOBJ->Total($salesID)){
		$PaymentLoop = true;
		$due = $SalesOBJ->Total($salesID) - $SalesOBJ->PaidAmt($salesID);
	}
}
$data['PaymentLoop'] = $PaymentLoop;
$data['SalesID'] = $salesID;
$data['due'] = $due;
$lpmd = '';
$lpmd .= LPMD($salesID);
$lpmd .= '<div class="remaining-panel">Balance Remaining : <span>$'.$due.'</span></div>';
$data['lpmd'] = $lpmd;
echo json_encode($data);


?>