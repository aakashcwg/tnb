<?php
$venID = $_POST['venID'];
$vendorObj = getVendorByID($venID);
$theVendor = $vendorObj[$venID];
$data['vcompany'] = $theVendor->VendorCompanyName;
$data['vaccno'] = $theVendor->VendorAccountNumber;
$data['vfname'] = $theVendor->VCIDFName;
$data['vlname'] = $theVendor->VCIDLname;
$data['vaddrtype'] = $theVendor->VAAddressTypeID;
$data['vaddr1'] = $theVendor->VAAddress1;
$data['vaddr2'] = $theVendor->VAAddress2;
$data['vcity'] = $theVendor->VACity;
$data['vstate'] = str_replace(' ', '-', strtolower($theVendor->VAState));//$theVendor->VAState;
$data['vzip'] = $theVendor->VAZip;
$data['vcontacttype'] = $theVendor->VCCTID;
$data['vcdata'] = $theVendor->VCData;
$data['vpcontactcheck'] = $theVendor->VCIsPrimary;
$data['visactive'] = $theVendor->IsActive;
$data['venID'] = $theVendor->VendorID;
echo json_encode($data);
?>