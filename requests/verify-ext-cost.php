<?php
//verify-ext-cost.php

$data = array();
$itemID = $_POST['itemID'] ?? null;
$ExtPrice = $_POST['ExtPrice'] ?? null;

$LPP = $InventoryOBJ->GetLastPurchasingDetails($itemID);
if($LPP){
	$TotalPurchasingCost = floatval($LPP->IODCost) + floatval($LPP->IODSalesTax) + floatval($LPP->IODShippingCost);

	$TPC_With_Ten_Percent = $TotalPurchasingCost*(1 + 0.1);

	$TPC_With_Ten_Percent = number_format($TPC_With_Ten_Percent, 2);

	if($ExtPrice < $TPC_With_Ten_Percent){
		$data['resp'] = false;
		$data['itemID'] = $itemID;
	}else{
		$data['resp'] = true;
	}
}

echo json_encode($data);