<?php

//modify-sales-discount-over-total.php

$EID = currentUser('EmployeeID');
$RoleID = currentUser('ELRoleID');
$Role = get_the_role($RoleID);

if(!is_admin() && !is_manager()){
	if(isset($_SESSION['manager_auth_for_'.$EID]) && !empty($_SESSION['manager_auth_for_'.$EID])){
		$permission = true;
	}else{
		$permission = false;
	}
}else{
	$permission = true;
}

$Request = $_POST['RequestTo'];

$output = array(
	'permission' => $permission,
	'request' => $Request
);

echo json_encode($output);

?>