<?php
//match-order-item-qty-with-inventory-max-qty.php

//print_r($_POST);

$data = array();
$InputQty = $_POST['InputQty'];
$item = $_POST['SelectedItem'];
$MaxQty = $InventoryOBJ->GetInventoryMaxQty($item);
$OnHandQty = $InventoryOBJ->GetOnHandQtyByInvID($item);

$resp = '';
$msg = '';
$MyQty = '';

if($OnHandQty){
	$NewQty = $InputQty + $OnHandQty;
	if($NewQty > $MaxQty){
		$resp = false;
		$MyQty = intval($MaxQty) - intval($OnHandQty);
		$msg = 'The max quantity allowed is '.$MyQty;
		if($MyQty <= 0){
			$msg = 'Please update your Inventory MaxQty';
		}
	}else{
		$resp = true;
		$MyQty = intval($MaxQty) - intval($OnHandQty);
	}
}else{
	$resp = true;
	$MyQty = $MaxQty;
}

$data['resp'] = $resp;
$data['MyQty'] = $MyQty;
$data['msg'] = $msg;

echo json_encode($data);