<?php

//tblCustomers data
$customer_FirstName = $_POST['cfname'] ?? null;
$customer_LastName = $_POST['clname'] ?? null;
$customer_Company = $_POST['ccompany'] ?? null;
$customer_AccountNumber = $_POST['caccno'] ?? null;
$customer_TaxExempt = $_POST['tax_exempt'] ?? null;
$customer_PLID = $_POST['customer-pl'] ?? null;
$customer_EmployeeID = currentUser('EmployeeID');
$customer_date = date(DATE_TIME_FORMAT);
$customer_IsActive = 0;
if(isset($_POST['cisactivecheck']) && $_POST['cisactivecheck'] == 'on'){
    $customer_IsActive = 1;
}

// tblCustomerAccountType data
$customer_Type = $_POST['customertype'] ?? null;

//tblCustomerContact data
$customer_ContactType = $_POST['ccontacttype'] ?? null;
$customer_ContactData = $_POST['ccdata'] ?? null;
$customer_IsPrimaryContact = 0;
if(isset($_POST['cpcontactcheck']) && $_POST['cpcontactcheck'] == 'on'){
  $customer_IsPrimaryContact = 1;  
}

//tblCustomerAddresses data
$customer_AddressType = $_POST['caddrtype'] ?? null;
$customer_Address1 = $_POST['caddr1'] ?? null;
$customer_Address2 = $_POST['caddr2'] ?? null;
$customer_City = $_POST['ccity'] ?? null;
$customer_State = $_POST['cstate'] ?? null;
$customer_Zip = $_POST['czip'] ?? null;

//tblCustomerTaxData data
$taxID = $_POST['tax-id'] ?? null;
$taxExpireDate = $_POST['expiration-date'] ?? null;
if($customer_TaxExempt == 0){
  $taxID = null;
  $taxExpireDate = null;
}

$returnUrl = '';
$message = '';
$validation_error = '';
$newcustomrDropdown = '';


if(empty($customer_FirstName)){
    $error[] = 'First name is required';
}
if(empty($customer_LastName)){
    $error[] = 'Last name is required';
}
if(empty($customer_AddressType)){
    $error[] = 'Address type is required';
}
/*if(empty($customer_Address1)){
    $error[] = 'Address1 is required';
}
if(empty($customer_City)){
    $error[] = 'City is required';
}
if(empty($customer_State)){
    $error[] = 'State is required';
}*/

if(empty($customer_Zip)){
    $error[] = 'Zip code is required';
}

if(empty($customer_Type)){
    $error[] = 'Customer type is required';
}

if(empty($customer_ContactType)){
    $error[] = 'Contact type is required';
}
if(empty($customer_ContactData)){
    $error[] = 'Contact data is required';
}
if(empty($error)){

    if($SDPDO->CheckExistance(TBL_CUSTOMERS, 'CustomerAccountNumber', $customer_AccountNumber) === true){

        $C_OBJ = $CustomerOBJ->GetTheCustomerByAccountNumber($customer_AccountNumber);
        if($C_OBJ){
            $C_NAME = VC_Name($C_OBJ->CustomerFName, $C_OBJ->CustomerLName, $C_OBJ->CustomerCompanyName);
        }else{
            $C_NAME = 'Anonymous';
        }
        $validation_error = 'You have already used that account number in the system for '.trim($C_NAME).', please enter a different account number';
    }else{

        // 'tblCustomers' data
        $tblCustomers_values = array($customer_FirstName, $customer_LastName, $customer_Company, $customer_AccountNumber, $customer_TaxExempt, $customer_PLID, $customer_EmployeeID, $customer_date, $customer_IsActive);

        $CustomerOBJ = new Customer();

        $ThisCustomerID = $CustomerOBJ->AddNewCustomer($tblCustomers_values);

        if($ThisCustomerID){
            $LoggingOBJ->NewCustomerLog(array($ThisCustomerID, $customer_EmployeeID, $customer_date, NEW_CUSTOMER_LOG_NOTE));

            // 'tblCustomerContact' data
            $tblCustomerContact_values = array($ThisCustomerID, $customer_ContactType, $customer_ContactData, $customer_IsPrimaryContact, $customer_FirstName, $customer_LastName);

            // 'tblCustomerAddresses' data
            $tblCustomerAddresses_values = array($ThisCustomerID, $customer_AddressType, $customer_Address1, $customer_Address2, $customer_City, $customer_State, $customer_Zip);

            // 'tblCustomerAccountType' data
            $tblCustomerAccountType_values = array($ThisCustomerID, $customer_Type);

            // 'tblCustomerTaxData' data
            $tblCustomerTaxData_values = array($ThisCustomerID, $taxID, $taxExpireDate);

            $ContactID = $CustomerOBJ->AddCustomerContactData($tblCustomerContact_values);
            if($ContactID){
                $LoggingOBJ->NewCustomerLog(array($ThisCustomerID, $customer_EmployeeID, $customer_date, 'Customer Contact ID : '.$ContactID.' // Added new customer contact'));
            }

            $AddressID = $CustomerOBJ->AddCustomerAddressData($tblCustomerAddresses_values);
            if($ContactID){
                $LoggingOBJ->NewCustomerLog(array($ThisCustomerID, $customer_EmployeeID, $customer_date, 'Customer Address ID : '.$AddressID.' // Added new customer address'));
            }
            
            $AccountTypeID = $CustomerOBJ->AddCustomerAccountTypeData($tblCustomerAccountType_values);
            if($AccountTypeID){
                $LoggingOBJ->NewCustomerLog(array($ThisCustomerID, $customer_EmployeeID, $customer_date, 'Customer Account Type ID : '.$AccountTypeID.' // Added new customer account type'));
            }
            
            $CustomerTaxDataID = $CustomerOBJ->AddCustomerTaxData($tblCustomerTaxData_values);
            if($CustomerTaxDataID){
                $LoggingOBJ->NewCustomerLog(array($ThisCustomerID, $customer_EmployeeID, $customer_date, 'Customer Tax Data ID : '.$CustomerTaxDataID.' // Added new customer tax data'));
            }


            $message = 'New customer successfully saved';
            $returnUrl =  SITE_URL.'?destination=customers';
            $newcustomrDropdown = '';
            if(isset($_POST['current_page']) && $_POST['current_page'] == 'selling'){
                $selectedClass = '';
                $newcustomrDropdown .= '<select class="sd-select" id="customer" name="customer" data-show-subtext="true" data-live-search="true" class="form-control">';
                    foreach ($CustomerOBJ->GetAllCustomers() as $customer) {

                        if($ThisCustomerID == $customer->CustomerID){
                            $selectedClass = 'selected';
                        }
                        $theCustomer = VC_Name($customer->CustomerFName, $customer->CustomerLName, $customer->CustomerCompanyName);

                       $newcustomrDropdown .= '<option value="'.$customer->CustomerID.'" '.$selectedClass.'>'.$theCustomer.'</option>';
                    }
                $newcustomrDropdown .= '</select>';

               $returnUrl = ''; 
            }
        }

    }

}else{
    $validation_error = implode(", ", $error);
}

$output = array(
    'error' => $validation_error,
    'message' => $message,
    'newcustomrDropdown' => $newcustomrDropdown,
    'returnUrl' => $returnUrl
);

echo json_encode($output);