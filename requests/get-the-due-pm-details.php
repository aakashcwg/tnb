<?php
//get-the-due-pm-details.php

$SalesID = $_POST['SalesID'] ?? null;

if($SalesID){
	$due = $SalesOBJ->Total($SalesID) - $SalesOBJ->PaidAmt($SalesID);
	$lpmd = '';
	$lpmd .= LPMD($SalesID);
	$lpmd .= '<div class="remaining-panel">Balance Remaining : <span>$'.$due.'</span></div>';
	$data['lpmd'] = $lpmd;
	echo json_encode($data);
}else{
	echo 'We can\'t find the Sales ID. Please check your sent request';
}

