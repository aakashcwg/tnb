<?php
//set-on-hand-item-price.php

//print_r($_POST);

$data = array();
$iohpid = $_POST['iohpid'] ?? null;
$price = $_POST['price'] ?? 0;
$ohid = $_POST['ohid'];
$ptid = $_POST['ptid'];

if($iohpid){
	$update = $InventoryOBJ->UpdateListPrice($price, $iohpid);
	if($update){
		$data['price'] 	= $price;
	}
}else{
	$values = array($ohid, $ptid, $price);
	$new_iohpid = $InventoryOBJ->SetItemPriceAndList($values);
	if($new_iohpid){
		$data['iohpid'] = $new_iohpid;
		$data['ohid'] 	= $ohid;
		$data['price'] 	= $price;
	}
}
echo json_encode($data);

