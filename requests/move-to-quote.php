<?php
//move-to-quote.php
//print_r($_POST);
$EmployeeID = currentUser('EmployeeID');
$date 	= date(DATE_TIME_FORMAT);
$RequestTo = $_POST['RequestTo'] ?? null;


$drawer_id = null;
if(isset($_SESSION['drawer_id'])){
	$drawer_id = $_SESSION['drawer_id'];
}


$response = '';
$url = '';
if($RequestTo == 'Quote'){

	$SDIOHID_Array 				= explode(',', $_POST['items']);
	$itemTypes					= explode(',', $_POST['itemTypes']);

	$SDPrice_Array 				= explode(',', $_POST['costs']);
	$SDQuantity_Array 			= explode(',', $_POST['qtys']);
	$SDDiscountAmount_Array 	= explode(',', $_POST['discs']);
	$SDTaxAmount_Array 			= explode(',', $_POST['taxs']);
	$SDNotes_Array 				= explode(',', $_POST['ItemNote']);

	$TotalTax = 0;
	foreach ($SDTaxAmount_Array as $Tax) {
		$TotalTax = $Tax+$TotalTax;
	}

	//tblSales - Data
	$SalesInvoiceNumber 	= time();
	$SCustomerID 			= $_POST['customer'] ?? null;
	$SalesDate 				= $date;
	$SAmount 				= $_POST['sales_amount'] ?? 0;
	$STax 					= $TotalTax;
	$SShippingCost 			= 0;
	$SPaymentMethodID 		= $_POST['payment_method'] ?? null;
	$SIsPaid 				= 0;
	$SPreviousSalesID 		= null; //LastSalesID($SCustomerID);
	$SIsVoid 				= 0;
	$SDOEStart				= $date;
	$SDOEEnd				= null;
	$SNotes					= $_POST['note'] ?? null;
	$SEmpID					= $EmployeeID;
	$SCashDrawerID          = $drawer_id;
	$SPONumber       		= $_POST['purchaseOrder'] ?? null;

	$quote_values = array( $SalesInvoiceNumber, $SCustomerID, $SalesDate, $SAmount, $STax, $SShippingCost, $SPaymentMethodID, $SIsPaid, $SPreviousSalesID, $SIsVoid, $SDOEStart, $SDOEEnd, $SNotes, $SEmpID, $SCashDrawerID, $SPONumber );

	$QuoteID = $SalesOBJ->MovoToQuote($quote_values);

	if($QuoteID){

		$LoggingOBJ->CustomerMakeSaleLog(array($SCustomerID, $SEmpID, $date, CUSTOMER_MAKE_QUOTE_LOG_NOTE));
		
		for ($i=0; $i < count($SDIOHID_Array) ; $i++) { 
			//tblSalesDetails - Data
			$SDSalesID 			= $QuoteID;
			//$SDIOHID 			= GetOnHandID($SDIOHID_Array[$i]);
			$SDIOHID = null;
			$SDNonInvID = null;
			if($itemTypes[$i] == 'inv'){
				$SDIOHID = GetOnHandID($SDIOHID_Array[$i]);
			}else{
				if($itemTypes[$i] == 'non-inv'){
					$SDNonInvID			= $SDIOHID_Array[$i];
				}
			}
			//$SDNonInvID		= null;

			$SDPrice 			= $SDPrice_Array[$i];
			$SDPricePaid 		= null;
			$SDQuantity 		= $SDQuantity_Array[$i];
			$SDNotes 			= $SDNotes_Array[$i];
			$SDIsCompanyInstall = 0;
			$SDDOE 				= $date;
			$SDDiscountAmount 	= $SDDiscountAmount_Array[$i];
			$SDTaxAmount		= $SDTaxAmount_Array[$i];

			$quote_item_values = array( $SDSalesID, $SDIOHID, $SDNonInvID, $SDPrice, $SDPricePaid, $SDQuantity, $SDNotes, $SDIsCompanyInstall, $SDDOE, $SDDiscountAmount, $SDTaxAmount );

			$ItemDetailsID = $SalesOBJ->AddQuoteItemData($quote_item_values);

			
			//Sales log
			if($ItemDetailsID){
				$ASLNotes = SALES_QUOTE_NOTE;
				$LogValues = array($QuoteID, $ItemDetailsID, $SEmpID,  $date, $ASLNotes, $SDIOHID);
				$LoggingOBJ->QuoteMakeLog($LogValues);
			}


		}
		$response = true;
		$url = SITE_URL.'?destination=sales';
	}else{
		$response = false;
	}
	$output = array(
		'response' => $response,
		'url' => $url
	);
	echo json_encode($output);
}



