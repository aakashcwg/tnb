<?php

$employee_id = currentUser('EmployeeID');
$date = date(DATE_TIME_FORMAT);
$SalesID = $_POST['SalesID'] ?? null;
$PMID = $_POST['PMID'] ?? null;

$requestFrom = $_POST['requestFrom'] ?? null;

$SPAmount = $_POST['_receipt_total'] ?? null;

$SPCheckNumber = $_POST['_check_no'] ?? null;

$SPLast5Digits = $_POST['_5digit'] ?? null;
$SPAuthCode = $_POST['_auth_code'] ?? null;

$tblSalesPaymentsData = array(
	'SPSalesID' => $SalesID, 
	'SPPaymentMethodID' => $PMID, 
	'SPAmount' => $SPAmount, 
	'SPAuthCode' => $SPAuthCode, 
	'SPLast5Digits' => $SPLast5Digits, 
	'SPCheckNumber' => $SPCheckNumber, 
	'SPEmpID' => $employee_id, 
	'SPDOE' => $date 
);
$SDPDO->InsertData(TBL_SALES_PAYMENTS, $tblSalesPaymentsData);

$due = 0;
$PMHtml = '';
$PaymentLoop = false;
if($SalesOBJ->PaidAmt($SalesID) < $SalesOBJ->Total($SalesID)){
	$PaymentLoop = true;
	$due = $SalesOBJ->Total($SalesID) - $SalesOBJ->PaidAmt($SalesID);
}
$PMHtml = SalesPMDetails($SalesID);
$data['PaymentLoop'] = $PaymentLoop;
$data['SalesID'] = $SalesID;
$data['due'] = $due;
$data['url'] = SITE_URL.'?destination=sales';
$data['pmhtml'] = $PMHtml;

$lpmd = '';
$lpmd .= LPMD($SalesID);
$lpmd .= '<div class="remaining-panel">Balance Remaining : <span>$'.$due.'</span></div>';
$data['lpmd'] = $lpmd;
$data['requestFrom'] = $requestFrom;

echo json_encode($data);