<?php
//new-list-item.php


$priceList = $_POST['price-list'];
$item = $_POST['item'];
$isPercentage = isset($_POST['is-percentage']) ? 1 : 0;
$plAmount = $_POST['pl-amount'];

$plAmount = str_replace(['%', '$'], '', $plAmount);

$error = '';
$resp = '';
$message = '';
$tr = '';
if(empty($priceList)){
	$error = 'Price List is required';
}elseif(empty($item)){
	$error = 'Item is required';
}

if(empty($error)){
	$Inputs = array(
		'PLIPLID' => $priceList, 
		'PLIINVID' => $item, 
		'PLIIsPercentage' => $isPercentage, 
		'PLIAmount' => $plAmount
	);
	$InsertID = $SDPDO->InsertData(TBL_PRICE_LIST_ITEMS, $Inputs);
	if($InsertID){
		$resp = true;
		$message = 'List Item successfully added';

		$PLI = GetPriceListItem($InsertID);
		$tr .= '<tr id="'.$PLI->PLIID.'">';
			$tr .= '<td>'.PriceListName($PLI->PLIPLID).'</td>';
			$tr .= '<td><a href="'.ItemUrl($PLI->PLIINVID).'">'.getItemWithNumber($PLI->PLIINVID).'</a></td>';
			$PLIIsPercentage = $PLI->PLIIsPercentage == 1 ? 'Yes' : 'NO';
			$tr .= '<td>'.$PLIIsPercentage.'</td>';
			$tr .= '<td>'.number_format($PLI->PLIAmount, 2).'</td>';
			$tr .= '<td><i class="fa fa-edit sd-tootip edit-price-list-item" data-id="'.$PLI->PLIID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editPriceListItem(this)"></i></td>';
		$tr .= '</tr>';

	}else{
		$resp = false;
		$message = 'Error...';
	}
}

$output = array(
	'resp' => $resp,
	'message' => $message,
	'error' => $error,
	'tr' => $tr
);

echo json_encode($output);