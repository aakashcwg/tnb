<?php
$EID = currentUser('EmployeeID');

$url = SITE_URL.'/pages/receiving/item-details.php';
$data = http_build_query( array( 'ODID' => $_POST['ODID'], 'ORDID' => $_POST['ORDID'], 'ORID' => $_POST['ORID'] ) );
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded",
        'method'  => 'POST',
        'content' => $data,
    ),
);
$context = stream_context_create( $options );
$result = file_get_contents( $url, false, $context );

$output = array(
	'content' => $result
);

echo json_encode($output);

?>