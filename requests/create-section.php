<?php 
$fileCode = "";

$lastSectionOrder = getLastSectionID();
$sectionOrder = $lastSectionOrder + 1;

$fileName = strtolower(str_replace([':', '\\', '/', '*', ' '], '-', $_POST['sectionName']));
if(!empty(trim($_POST['sectionName']))){

	if($SDPDO->CheckExistance(TBL_SECTIONS, 'SectionName', $_POST['sectionName'])){
		$data['resp'] = false;
		$data['errorMsg'] = 'Section you entered already exists';
	}else{
		$insertID = $SDPDO->InsertData(TBL_SECTIONS, array('SectionName' => $_POST['sectionName'], 'SectionOrder' => $sectionOrder));

		if($insertID){
			$SDPDO->InsertData(TBL_SECURITY_LEVELS, array('SLSectionID' => $insertID));
			$data['resp'] = true;
			$data['successMsg'] = 'New section successfuly added';
			$sectionDir = BASE_PATH.'/pages/section/';
			$file = $fileName.'.php';
			if(!file_exists($file)){
				$myfile = fopen($sectionDir.$file, "a");
				fwrite($myfile, $fileCode);
				fclose($myfile);
			}

			$row = '';
			$row .= '<tr id="'.$insertID.'" data-order="'.$sectionOrder.'">';
			$row .= '<td>'.$_POST['sectionName'].'</td>';
			$row .= '<td><i class="fa fa-edit sd-tootip edit-section-type" data-action="edit_section_type" data-id="'.$insertID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>';
			$row .= '</tr>';
			$data['row'] = $row;
			$data['SectionID'] = $insertID;

		}else{
			$data['resp'] = false;
			$data['errorMsg'] = 'Error!!';
		}
	}
}else{
	$data['resp'] = false;
	$data['errorMsg'] = 'You can\'t empty the field';
}
echo json_encode($data);
?>