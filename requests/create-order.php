<?php
//create-order.php;

$data = array();
//Repeat data
$Item 			=	explode(',', $_POST['Item']);
$VItem 			=	explode(',', $_POST['VItem']);
$Case 			=	explode(',', $_POST['Case']);
$CaseQty 		=	explode(',', $_POST['CaseQty']);
$QtyPerCase 	=	explode(',', $_POST['QtyPerCase']);
$Cost 			=	explode(',', $_POST['Cost']);
$Tax 			=	explode(',', $_POST['Tax']);
$ShippingCost 	=	explode(',', $_POST['ShippingCost']);
$GL 			=	explode(',', $_POST['GL']);
$Note 			=	explode(',', $_POST['Note']);
$Customer 		=	explode(',', $_POST['Customer']);

//Common data
$VendorID 	= $_POST['VendorID'];
$CustomerID = null;
$OrderPhone = $_POST['OrderPhone'];
$LedgerID 	= $_POST['LedgerID'];
//$OrderNote 	= $_POST['OrderNote'];
$EmpID 		= currentUser('EmployeeID');

//Static data
$OrderStatus = 1;

$date = date(DATE_TIME_FORMAT);


$warehouse 	=	1;
$shelf 		=	3;

$ItemTotalCost = array();
for ($k=0; $k < count($Item); $k++) { 
	$tot = $QtyPerCase[$k] * $Cost[$k];
	array_push($ItemTotalCost, $tot);
}

$NetTotalCost		= array_sum($ItemTotalCost);
$TotalTax			= array_sum($Tax);
$TotalShippingCost	= array_sum($ShippingCost);

//Data for = tblInventoryOrders
$tblInventoryOrders_values = array( $CustomerID, $NetTotalCost, $TotalTax, $TotalShippingCost, $date, $OrderPhone, $EmpID, $LedgerID, $OrderStatus );

//Creating new order here
$OrderID = $OrderOBJ->CreateOrder($tblInventoryOrders_values);

$tblInventoryOrdersReceived_values = array($OrderID, null, 0, null, $EmpID);

//Insert a blank receive order data
$RID = $OrderOBJ->ReceiveOrder($tblInventoryOrdersReceived_values);

if($OrderID){
	for ($i=0; $i < count($Item); $i++) {
		$IODDateShipped 	= null;

		$InventoryID 		= $Item[$i];
		$ThisVItem			= $VItem[$i] == 'null' ? null : $VItem[$i];
		$ThisCost 			= $Cost[$i] == 'null' ? null : $Cost[$i];
		$ThisTax 			= $Tax[$i] == 'null' ? null : $Tax[$i];
		$ThisShippingCost 	= $ShippingCost[$i] == 'null' ? null : $ShippingCost[$i];
		$ThisCase 			= $Case[$i] == 'null' ? null : $Case[$i];
		$ThisCaseQty 		= $CaseQty[$i] == 'null' ? null : $CaseQty[$i];
		$ThisQtyPerCase 	= $QtyPerCase[$i] == 'null' ? null : $QtyPerCase[$i];
		$ThisNote 			= $Note[$i] == 'null' ? null : $Note[$i];
		$ThisCustomer 		= $Customer[$i] == 'null' ? null : $Customer[$i];

		//Data for = tblInventoryOrderDetails
		$tblInventoryOrderDetails_values = array( $OrderID, $InventoryID, $VendorID, $ThisVItem, $ThisCost, $ThisTax, $ThisShippingCost, $ThisCase, $ThisCaseQty, $ThisQtyPerCase, $date, $IODDateShipped, $OrderPhone, $ThisNote, $EmpID, $LedgerID, 0, $ThisCustomer );

		//Insert Order details
		$OrderDetailsID = $OrderOBJ->AddOrderDetails($tblInventoryOrderDetails_values);

		//On Hand Details -- Update if exists/Insert if doesn't exist
		$OnHandID = GetOnHandID($InventoryID);
		$NewQuantity = GetOnHandQty($OnHandID) + $ThisQtyPerCase;
		if($SDPDO->CheckExistance(TBL_INVENTORY_ON_HAND, 'IOHInventoryID', $InventoryID) === true){
			$InventoryOBJ->UpdateOnHandItemQuantity(array($NewQuantity), array('IOHID', $OnHandID));
		}else{
			$IOHBin = null;
			$tblInventoryOnHand_values = array($InventoryID, $OrderDetailsID, $ThisQtyPerCase, $warehouse, $shelf, $IOHBin);
			$SDPDO->InsertOnHandItem($tblInventoryOnHand_values);
		}
		$tblInventoryOrdersReceivedDetails_values = array($RID, $OrderDetailsID, null, 0, 0, null, $EmpID);
		$OrderOBJ->ReceiveOrderItem($tblInventoryOrdersReceivedDetails_values);


		//Logging
		$LogValues = array($EmpID, $OrderID, $OnHandID);
		$LoggingOBJ->InventoryItemOrderLog($LogValues);
			
	}
	$data['resp'] = true;
}else{
	echo 'error..';
}

echo json_encode($data);


?>