<?php
//new-cash-drawer.php

$CashDrawer = $_POST['drawer-name'] ?? null;
$Warehouse = $_POST['warehouse'] ?? null;
$ComputerName = $_POST['computer-name'] ?? null;
$validation_error = '';
$message = '';

if(empty($CashDrawer)){
	$error[] = 'Drawer name is required';
}
if(empty($ComputerName)){
	$error[] = 'Computer name is required';
}
if(empty($error)){
	if($SDPDO->CheckExistance(TBL_CASH_DRAWERS, 'CashDrawerName', $CashDrawer)){
		$validation_error = 'Drawer name is already exist';
	}else{
		$values = array(
			'CashDrawerName' 	=> $CashDrawer,
			'CDWarehouseID' 	=> $Warehouse,
			'CDComputerName'	=> $ComputerName
		);
		$CashDrawerID = $SDPDO->InsertData(TBL_CASH_DRAWERS, $values);
		if($CashDrawerID){
			$message = 'New drawer successfully added';

			$tr = '';
			$obj = GetTheCashDrawer($CashDrawerID);
			$tr .= '<tr id="'.$obj->CashDrawerID.'">';
				$tr .= '<td>'.$obj->CashDrawerName.'</td>';
				$tr .= '<td>'.getWareHouseName($obj->CDWarehouseID).'</td>';
				$tr .= '<td>'.$obj->CDComputerName.'</td>';
				$tr .= '<td><i class="fa fa-edit sd-tootip edit-cash-drawer" data-id="'.$obj->CashDrawerID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="EditCashDrawer(this)"></i></td>';
			$tr .= '</tr>';


		}else{
			$validation_error = 'Error :: data is not saved';
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message,
	'tr' => $tr,
	'cdid' => $CashDrawerID
);
echo json_encode($output);