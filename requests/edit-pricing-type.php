<?php
$typeID = $_POST['id'];
$typeName = getPricingType($typeID);
$data['list_type'] = $typeName;
$data['id'] = $typeID;
$data['request_action'] = str_replace('edit', 'update', $_POST['action']);
$data['modal_title'] = ucwords(str_replace('_', ' ', $_POST['action']));
$data['label_1'] = ucwords(str_replace('_', ' ', str_replace('edit_', '', $_POST['action'])));
echo json_encode($data);
?>