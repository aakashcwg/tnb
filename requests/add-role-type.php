<?php
$typeName = $_POST['typeName'];
$data['action'] = $_POST['action'];


if(!empty($typeName)){
	if($SDPDO->CheckExistance(TBL_EMPLOYEE_ROLES, 'ERRoleName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Role type already exists';
	}else{
		$InsertID = $ListTypeOBJ->AddNewEmployeeRole(array($typeName));
		if($InsertID){

			$id = $InsertID;
			$data['resp'] = true;
			$data['successMsg'] = 'Role type successfully added';
			$data['id'] = $id;
			$data['table_id'] = 'role-type-table';
			$data['typeName'] = $typeName;
			$data['row'] = '<tr id="'.$id.'">
					<td>'.$typeName.'</td>
					<td><i class="fa fa-edit sd-tootip edit-role-type" data-action="edit_role_type" data-id="'.$id.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
				</tr>';
		}
	}
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>