<?php
//get-warehouse-items.php

//print_r($_POST);

$wh = $_POST['fwh'];
$Items = $InventoryOBJ->GetWareHouseItems($wh);

$html = '';
$error = '';
if($Items){
	$html .= '<label for="on-hand-item">On Hand Item<sup>*</sup></label>';
	$html .= '<select id="on-hand-item" class="form-control sd-select" data-show-subtext="true" data-live-search="true" name="on-hand-item">';
	$html .= '<option value="">Select Item</option>';
	foreach ($Items as $Item) {
		$html .= '<option value="'.$Item->IOHInventoryID.'">'.getItemWithNumber($Item->IOHInventoryID).'</option>';
	}
	$html .= '</select>';
}else{
	$error = 'No items available under the warehouse '.$wh;
	$html .= '<label for="on-hand-item">On Hand Item<sup>*</sup></label>';
	$html .= '<select id="on-hand-item" class="form-control sd-select" data-show-subtext="true" data-live-search="true" name="on-hand-item">';
	$html .= '<option value="">Select Item</option>';
	$html .= '</select>';
}

$output = array(
	'cont' => $html,
	'error' => $error
);

echo json_encode($output);