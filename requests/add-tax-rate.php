<?php
//add-tax-rate.php

$taxRate 		= $_POST['tax-rate'] ?? null;
$taxRateName 	= $_POST['tax-rate-name'] ?? null;

$message = '';
$validation_error = '';
$error = array();
if(empty($taxRate)){
	$error[] = 'Tax Rate is requied';
}

if(empty($error)){
	if($SDPDO->CheckExistance(TBL_TAX_RATES, 'TaxRatePercentage', $taxRate)){
		$validation_error = 'Tax Rate is already assigned';
	}else{
		$trid = $SDPDO->InsertData(TBL_TAX_RATES, array('TaxRatePercentage' => $taxRate, 'TaxRateName' => $taxRateName));
		if($trid){
			$message = 'Tax Rate successfully added';
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);

echo json_encode($output);