<?php
//new-non-inv-item.php

//print_r($_POST);

$part_number 		= $_POST['part-number'] ?? null;
$item_name 			= $_POST['item-name'] ?? null;
$item_cost 			= $_POST['item-cost'] ?? null;
$item_desc 			= $_POST['item-desc'] ?? null;

$item_is_active = 0;
if(isset($_POST['item-is-active'])){
	$item_is_active = 1;
}

$ledger_id 			= $_POST['ledger-id'] ?? null;

$item_price_type 	= $_POST['item-price-type'];
$item_price 		= $_POST['item-price'];

$message = '';
$validation_error = '';

if(empty(trim($part_number))){
	$error[] = 'Part Number is required';
}
if(empty(trim($item_name))){
	$error[] = 'Item Name is required';
}

if(empty($error)){
	if($SDPDO->CheckExistance(TBL_NONINVENTORY_ITEMS, 'NonInvPartNumber', $part_number)){
		$validation_error = 'Item with same number already exists';
	}else{
		$values1 = array($part_number, $item_name , $item_cost , $item_desc, $item_is_active, $ledger_id);
		$NewID = $InventoryOBJ->AddNewNonInventoryItem($values1);
		if($NewID){
			$values2 = array($NewID, $item_price_type , $item_price);
			$NewPricingID = $InventoryOBJ->AddNonInventoryItemPriceData($values2);
			if($NewPricingID){
				$message = 'Item successfully added';
			}
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);

echo json_encode($output);