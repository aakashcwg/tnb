<?php
$typeName = $_POST['typeName'];
$id = $_POST['id'];
if(!empty($typeName)){
	if($SDPDO->CheckExistanceExcept(TBL_CUSTOMER_ACCOUNT_TYPE_LIST, 'CATLID', $id, 'CATLName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Customer type already exists';
	}else{
		$update = $ListTypeOBJ->UpdateCustomerAccountType(array($typeName), array('CATLID', $id));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Customer type successfully updated';
			$data['id'] = $id;
			$data['typeName'] = $typeName;
			$data['table_id'] = 'customer-type-table';
		}
	}
}else{
	$data['blank'] = true;
}
echo json_encode($data);
?>