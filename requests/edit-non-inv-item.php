<?php
//edit-non-inv-item.php

$item = $_POST['item'];
$NI = $InventoryOBJ->GetTheNonInventoryItem($item);
$data = array();
if($NI){
	$data['part_number'] = $NI->NonInvPartNumber;
	$data['item_name'] = $NI->NonInvName;
	$data['item_cost'] = number_format($NI->NonInvCost, 2);
	$data['item_desc'] = $NI->NonInvDescription;
	$data['item_price'] = number_format($NI->NIPPrice, 2);

	$IsActive = $NI->IsActive;
	$checked = '';
	if($IsActive == 1){
		$checked = 'checked';
	}else{
		$checked = '';
	}
	$data['isActive'] = '<label for="item-is-active">Is Active?<input type="checkbox" id="item-is-active" class="form-control" name="item-is-active" '.$checked.'></label>';

	$NIGLID = $NI->NIGLID;
	$selected = '';
	$LSelect = '';
	//$LSelect .= '<select id="ledger-id" class="form-control" name="ledger-id">';
        if($ListTypeOBJ->GetAllLedgerNumbers()) {
        foreach ($ListTypeOBJ->GetAllLedgerNumbers() as $num) {
        	if($num->GeneralLedgerID == $NIGLID){
        		$selected = 'selected';
        	}else{
        		$selected = '';
        	}
            $LSelect .= '<option value="'.$num->GeneralLedgerID.'">'.$num->GLNumber.'</option>';
            
        } }
    //$LSelect .= '</select>';

    $data['LSelect'] = $LSelect;

	
	$NIPPriceTypeID = $NI->NIPPriceTypeID;
	$selected = '';
	$NPSelect = '';
	//$NPSelect .= '<select id="item-price-type" class="form-control" name="item-price-type">';
        if($ListTypeOBJ->GetAllPricingTypes()) {
        foreach ($ListTypeOBJ->GetAllPricingTypes() as $PricingType) {
        	if($PricingType->PTID == $NIPPriceTypeID){
        		$selected = 'selected';
        	}else{
        		$selected = '';
        	}
            $NPSelect .= '<option value="'.$PricingType->PTID.'">'.$PricingType->PTName.'</option>';
        } }
    //$NPSelect .= '</select>';

    $data['NPSelect'] = $NPSelect;
    $data['item_id'] = $item;

    echo json_encode($data);

}