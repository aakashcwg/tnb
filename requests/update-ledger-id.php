<?php
$typeName = $_POST['typeName'];
$typeDesc = $_POST['typeDesc'];
$id = $_POST['id'];

if(!empty($id)){
	if($SDPDO->CheckExistanceExcept(TBL_GENERAL_LEDGER_NUMBERS, 'GeneralLedgerID', $id, 'GLNumber', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Ledger id already exists';
	}else{
		$update = $ListTypeOBJ->UpdateLedgerNumber(array($typeName, $typeDesc), array('GeneralLedgerID', $id));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Ledger id successfully updated';
			$data['id'] = $id;
			$data['table_id'] = 'ledger-ids-table';
			$data['typeName'] = $typeName;
			$data['typeDesc'] = $typeDesc;
		}
	}	
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>