<?php
//update-non-inv-item.php

//print_r($_POST);
$item_id = $_POST['non-inv-item-id'];

$part_number 		= $_POST['part-number'] ?? null;
$item_name 			= $_POST['item-name'] ?? null;
$item_cost 			= $_POST['item-cost'] ?? null;
$item_desc 			= $_POST['item-desc'] ?? null;

$item_is_active = 0;
if(isset($_POST['item-is-active'])){
	$item_is_active = 1;
}

$ledger_id 			= $_POST['ledger-id'] ?? null;

$item_price_type 	= $_POST['item-price-type'];
$item_price 		= $_POST['item-price'];

$message = '';
$validation_error = '';
$row = '';

if(empty(trim($part_number))){
	$error[] = 'Part Number is required';
}
if(empty(trim($item_name))){
	$error[] = 'Item Name is required';
}

if(empty($error)){
	if($SDPDO->CheckExistanceExcept(TBL_NONINVENTORY_ITEMS, 'NonInvID', $item_id, 'NonInvPartNumber', $part_number)){
		$validation_error = 'Item with same number already exists';
	}else{
		$values1 = array($part_number, $item_name , $item_cost , $item_desc, $item_is_active, $ledger_id);
		$update1 = $InventoryOBJ->UpdateNonInventoryItem($values1, array('NonInvID', $item_id));
		if($update1){
			$values2 = array($item_price_type , $item_price);
			$update2 = $InventoryOBJ->UpdateNonInventoryItemPriceData($values2, array('NIPNonInvID', $item_id));
			if($update2){
				$message = 'Item successfully updated';

				$NI = $InventoryOBJ->GetTheNonInventoryItem($item_id);
				if($NI){
					$row = '';
					$row .= '<td>'.$NI->NonInvPartNumber.'</td>';
	    	        $row .= '<td>'.$NI->NonInvName.'</td>';
	    	        $row .= '<td>'.number_format($NI->NonInvCost, 2).'</td>';
	    	        $row .= '<td>'.$NI->NonInvDescription.'</td>';
	    	        $row .= '<td>'.$ListTypeOBJ->GetThePricingTypeByID($NI->NIPPriceTypeID).'</td>';
	    	        $row .= '<td>'.number_format($NI->NIPPrice, 2).'</td>';
	    	        $row .= '<td class="action-btns">
	    	            	<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="'.$NI->NonInvID.'" style="font-size:24px;cursor:pointer;" onclick="EditNonInventoryItem(this)"></i></td>';
				}

			}
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message,
	'tr' => $row,
	'item_id' => $item_id
);

echo json_encode($output);