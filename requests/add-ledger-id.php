<?php
$typeName = $_POST['typeName'];
$typeDesc = $_POST['typeDesc'];
$data['action'] = $_POST['action'];


if(!empty($typeName)){
	if($SDPDO->CheckExistance(TBL_GENERAL_LEDGER_NUMBERS, 'GLNumber', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Ledger id already exists';
	}else{
		$InsertID = $ListTypeOBJ->AddNewLedgerNumber(array($typeName, $typeDesc));
		if($InsertID){

			$id = $InsertID;
			$data['resp'] = true;
			$data['successMsg'] = 'Ledger id successfully added';
			$data['id'] = $id;
			$data['table_id'] = 'ledger-ids-table';
			$data['typeName'] = $typeName;
			$data['row'] = '<tr id="'.$id.'">
					<td>'.$typeName.'</td>
					<td>'.$typeDesc.'</td>
					<td><i class="fa fa-edit sd-tootip edit-ledger-id" data-action="edit_ledger_id" data-id="'.$id.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
				</tr>';
		}
	}
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>
