<?php
//update-tax-rate.php
$taxRate 		= $_POST['tax-rate'] ?? null;
$taxRateName 	= $_POST['tax-rate-name'] ?? null;
$trid 			= $_POST['tax-rate-id'] ?? null;

$message = '';
$validation_error = '';
$error = array();
if(empty($taxRate)){
	$error[] = 'Tax Rate is requied';
}

if(empty($error)){
	if($SDPDO->CheckExistanceExcept(TBL_TAX_RATES, 'TaxRateID', $trid, 'TaxRatePercentage', $taxRate)){
		$validation_error = 'Tax Rate is already assigned';
	}else{
		$update = $SDPDO->update(TBL_TAX_RATES, array('TaxRatePercentage', 'TaxRateName'), array($taxRate, $taxRateName), array('TaxRateID', $trid));
		if($update){
			$message = 'Tax Rate successfully updated';
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);

echo json_encode($output);