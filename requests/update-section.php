<?php
$typeName = $_POST['typeName'];
$id = $_POST['id'];
if($SDPDO->CheckExistanceExcept(TBL_SECTIONS, 'SectionID', $id, 'SectionName', $typeName)){
	$data['resp'] = false;
	$data['errorMsg'] = 'Section already exists';
}else{
	$update = $SDPDO->update(TBL_SECTIONS, array('SectionName'), array($typeName), array('SectionID', $id));
	if($update){
		$data['resp'] = true;
		$data['successMsg'] = 'Section successfully updated';
		$data['id'] = $id;
		$data['typeName'] = $typeName;
		$data['table_id'] = 'section-items-table';
	}
}
echo json_encode($data);
?>