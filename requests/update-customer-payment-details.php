<?php
//update-customer-payment-details.php

//print_r($_POST);

$CustomerID = $_POST['CID'] ?? null;
$date = date(DATE_TIME_FORMAT);
$SIDs = explode(",", $_POST['SIDs']);
$Amts = explode(",", $_POST['Amts']);
$CPM = $_POST['CPM'];

$CheckNo = $_POST['CheckNo'] ?? null;
$Last5Digits = $_POST['Last5Digits'] ?? null;
$AuthCode = $_POST['AuthCode'] ?? null;

//$Note = $_POST['Note'] ?? null;

$inserID = '';
for ($i=0; $i < count($SIDs); $i++) { 
	$tblSalesPaymentsData = array(
		'SPSalesID' => $SIDs[$i], 
		'SPPaymentMethodID' => $CPM, 
		'SPAmount' => $Amts[$i], 
		'SPAuthCode' => $AuthCode, 
		'SPLast5Digits' => $Last5Digits, 
		'SPCheckNumber' => $CheckNo, 
		'SPEmpID' => currentUser('EmployeeID'), 
		'SPDOE' => $date 
	);
	$inserID = $SDPDO->InsertData(TBL_SALES_PAYMENTS, $tblSalesPaymentsData);
}

if($inserID){
	$Customer = $CustomerOBJ->GetTheCustomerByID($CustomerID);

	$AllSales = $SalesOBJ->AllSalesByCustomer($CustomerID);

		$html = '';
		$html .= '<div class="customer-details">';
			$html .= '<h2>'.VC_Name($Customer->CustomerFName, $Customer->CustomerLName, $Customer->CustomerCompanyName).'</h2>';

			if($Customer->CAAddress1){
				$html .= '<p>'.$Customer->CAAddress1.'</p>';
			}
			if($Customer->CAAddress2){
				$html .= '<p>'.$Customer->CAAddress2.'</p>';
			}

			if(!empty($Customer->CAState)){
				$CS = strtoupper($Customer->CAState);
				if(array_key_exists($CS, getUSAStates())){
					$CState = getUSAStates()[$CS];
				}
			}else{
				$CState = '';
			}
			if($Customer->CACity){
				$html .= '<i>'.$Customer->CACity.'</i> '.$CState;
			}

		$html .= '</div>';

		$html .= '</br>';

		$html .= '<table class="table-bordered customer-payment-table">';
			$html .= '<thead>';
				$html .= '<th>Invoice</th>';
				$html .= '<th>Amount Due</th>';
				$html .= '<th>Amount being paid</th>';
			$html .= '</thead>';
			$html .= '<tbody>';
			if(!empty($AllSales)){
				foreach ($AllSales as $Each) {
					$STotal = $SalesOBJ->Total($Each->SalesID);
					$SPaid = $SalesOBJ->PaidAmt($Each->SalesID);
					$SDue = $STotal - $SPaid;
					if($SDue > 0){

						$html .= '<tr>';
							$html .= '<td>#'.$Each->SalesInvoiceNumber.'</td>';
							$html .= '<td class="pay-amount">$'.$SDue.'</td>';

							$html .= '<td><input type="text" onkeyup="GetVendorPaymentTotal()" class="price-validation pay-vendor form-control" data-salesid="'.$Each->SalesID.'" /></td>';

						$html .= '</tr>';
					}
					
				}
			}else{
				$html .= '<tr>';
					$html .= '<td colspan="4">No payments are pending.</td>';
				$html .= '</tr>';
			}
			$html .= '</tbody>';
		$html .= '</table>';
		$html .= '</br>';

		if(!empty($AllSales)){
			$html .= '<div class="vendor-extra">';
				// $html .= '<label for="extra-payment">Extra Payment : </label>';
				// $html .= '<input type="text" onkeyup="GetVendorPaymentTotal()" class="form-control price-validation" id="vendor-extra-payment">';
				// $html .= '</br></br>';
				$html .= '<label for="total-payment">Total to Pay : </label>';
				$html .= '<input type="text" class="form-control" id="vendor-total-payment" readonly>';
				$html .= '</br></br>';
				$html .= '<label for="payment-method">Payment Method : </label>';
				$html .= '<select class="vpm form-control" id="vpm" onchange="VpSet(this)">';
				$PMs = $ListTypeOBJ->GetAllPaymentMethods();
				foreach ($PMs as $PM) {
					$html .= '<option value="'.$PM->PMID.'">'.$PM->PaymentMethodName.'</option>';
				}
				$html .= '</select>';
				$html .= '</br></br>';

				$html .= '<div class="check-data" style="display:none;">';
					$html .= '<label for="check-number">Check Number : </label>';
					$html .= '<input type="text" class="form-control" id="vendor-check-number">';
					$html .= '</br></br>';
				$html .= '</div>';

				$html .= '<div class="card-data" style="display:none;">';
					$html .= '<label for="last5digits">Last 5 Digits : </label>';
					$html .= '<input type="text" class="form-control" id="last5digits">';
					$html .= '</br></br>';
					$html .= '<label for="authcode">Authorization Code : </label>';
					$html .= '<input type="text" class="form-control" id="authcode">';
					$html .= '</br></br>';
				$html .= '</div>';

				$html .= '<label for="vp-note">Note : </label>';
				$html .= '<textarea class="form-control" id="vp-note"></textarea>';

				$html .= '</br></br>';

				$html .= '<a href="javascript:void(0)" class="btn theme-default-orange text-right" data-customerid="'.$CustomerID.'" id="vendor-save-payment" onclick="SaveCustomerPayment()">Save Payment</a>';

			$html .= '</div>';
		}
		

	$data['content'] = $html;
	echo json_encode($data);

}


