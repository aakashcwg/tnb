<?php

$Customer_ID = $_POST['Customer_ID'];
$TheCustomer = $CustomerOBJ->GetTheCustomerByID($Customer_ID);

$CPL = $TheCustomer->CustomerPLID;

if($CPL){
	$PL = PriceListName($CPL);
	if($PL){
		$data['pl'] = $PL;
		$data['plcolor'] = GetPriceListColor($CPL);
	}
}

$data['cid'] = $Customer_ID;

$data['taxExempt'] = $TheCustomer->CustomerIsTaxExempt == 1 ? true : false;

$data['customer_name'] = $TheCustomer->CustomerFName.' '.$TheCustomer->CustomerLName;

if($CustomerOBJ->HasQuotes($Customer_ID)){
	$data['Url'] = SITE_URL.'?destination=sales&action=quote-history&customer_id='.$Customer_ID;
}

echo json_encode($data);


?>