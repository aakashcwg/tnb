<?php

$validation_error = '';
$message = '';

$EFName 		= $_POST['efname'] ?? null;
$ELName 		= $_POST['elname'] ?? null;
$EAddress1 		= $_POST['eaddr1'] ?? null;
$EAddress2 		= $_POST['eaddr2'] ?? null;
$ECity 			= $_POST['ecity'] ?? null;
$EState 		= $_POST['estate'] ?? null;
$EZip 			= $_POST['ezip'] ?? null;
$EIsActive 		= 1;

$ELUsername 	= $_POST['euser'] ?? null;
$ELPassword 	= $_POST['epass'] ?? null;
$ELRoleID 		= $_POST['erole'] ?? null;
$ELDateExpire	= null;

$ECCTID 		= $_POST['econtact-type'] ?? null;
$ECData 		= $_POST['econtact-data'] ?? null;
$ECIsPrimary 	= 1;

if(empty($EFName)){
	$error[] = 'First name is required';
}
if(empty($ECity)){
	$error[] = 'City is required';
}
if(empty($EState)){
	$error[] = 'State is required';
}
if(empty($ELRoleID)){
	$error[] = 'Role is required';
}
if(empty($ELUsername)){
	$error[] = 'Username is required';
}
if(empty($ELPassword)){
	$error[] = 'Password is required';
}

if(empty($error)){
	if($SDPDO->CheckExistance(TBL_EMPLOYEE_LOGIN, 'ELUsername', $ELUsername)){
		$validation_error = 'Username already exists';
	}else{
		$EID = $EmployeeOBJ->AddNewEmployee(array($EFName, $ELName, $EAddress1, $EAddress2, $ECity, $EState, $EZip, $EIsActive));
		if($EID){
			$EmployeeOBJ->AddEmployeeLoginData(array($EID, $ELUsername, $ELPassword, $ELRoleID, $ELDateExpire));
			$EmployeeOBJ->AddEmployeeContactData(array($EID, $ECCTID, $ECData, $ECIsPrimary));
			$EmployeeOBJ->AddEmployeeStartDate($EID);
			$message = 'A new employee successfully added';
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);
echo json_encode($output);
?>