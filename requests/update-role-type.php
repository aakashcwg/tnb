<?php
$role = $_POST['typeName'];
$roleID = $_POST['id'];
if(!empty($role)){
	if($SDPDO->CheckExistanceExcept(TBL_EMPLOYEE_ROLES, 'ERID', $roleID, 'ERRoleName', $role)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Role type already exists';
	}else{
		$update = $ListTypeOBJ->UpdateEmployeeRole(array($role), array('ERID', $roleID));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Role type successfully updated';
			$data['id'] = $roleID;
			$data['typeName'] = $role;
			$data['table_id'] = 'role-type-table';
		}
	}
}else{
	$data['blank'] = true;
}
echo json_encode($data);
?>