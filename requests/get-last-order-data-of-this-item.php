<?php
//get-last-order-data-of-this-item.php

$data = array();
$vendor = $_POST['vendor'];
$item = $_POST['item'];

$itemData = $VendorOBJ->GetLastPurchaseDetails($vendor, $item);

if($itemData){
	$data['cost'] = number_format($itemData->IODCost, 2);
	$data['tax'] = number_format($itemData->IODSalesTax, 2);
	$data['scost'] = number_format($itemData->IODShippingCost, 2);
}

echo json_encode($data);

