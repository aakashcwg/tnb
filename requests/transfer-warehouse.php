<?php
//transfer-warehouse.php

//print_r($_POST);

$warehouse_from = $_POST['warehouse-from'];
$warehouse_to 	= $_POST['warehouse-to'];
$warehouse_qty 	= $_POST['warehouse-qty'];
$item 			= $_POST['on-hand-item'];
$message = '';
$validation_error = '';
if(empty($warehouse_from)){
	$error[] = 'FROM Warehouse is required';
}
if(empty($warehouse_to)){
	$error[] = 'TO Warehouse is required';
}
if(empty($warehouse_qty)){
	$error[] = 'QTY is required';
}
if(empty($item)){
	$error[] = 'On Hand Item is required';
}
if(empty($error)){
	if($warehouse_from == $warehouse_to){
		$validation_error = 'FROM and TO can\'t be same warehouse';
	}else{
		$HasItem = $InventoryOBJ->WareHouseHasItem($item, $warehouse_from);
		if($HasItem){

			$NewQty = $HasItem->IOHQuantity - $warehouse_qty;

			if($NewQty > 0){
				$NewOH = $SDPDO->InsertData(TBL_INVENTORY_ON_HAND, array('IOHInventoryID' => $item, 'IOHIORDID' => $HasItem->IOHIORDID, 'IOHQuantity' => $warehouse_qty, 'IOHWarehouseID' => $warehouse_to, 'IOHShelfID' => 1, 'IOHBin' => null));

				$NewQty = $HasItem->IOHQuantity - $warehouse_qty;
				$update = $SDPDO->update(TBL_INVENTORY_ON_HAND, array('IOHQuantity', 'IOHWarehouseID'), array($NewQty, $warehouse_to), array('IOHID', $HasItem->IOHID));
				if($update){
					$message = 'Items are successfully transferred.';
				}
			}else{
				$validation_error = 'You have only '.$HasItem->IOHQuantity.' items in the Warehouse '.getWareHouseName($warehouse_from);
			}
			
		}else{
			$OH_OBJ = $InventoryOBJ->GetOnHandItemByInventoryID($item);
			$NewOH = $SDPDO->InsertData(TBL_INVENTORY_ON_HAND, array('IOHInventoryID' => $item, 'IOHIORDID' => $OH_OBJ->IOHIORDID, 'IOHQuantity' => $warehouse_qty, 'IOHWarehouseID' => $warehouse_to, 'IOHShelfID' => 1, 'IOHBin' => null));
			if($NewOH){
				$message = 'Items are successfully transferred.';
			}
		}
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message
);

echo json_encode($output);