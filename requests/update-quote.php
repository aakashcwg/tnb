<?php
//update-quote.php

//print_r($_POST);

$EmployeeID = currentUser('EmployeeID');

$drawer_id = null;
if(isset($_SESSION['drawer_id'])){
	$drawer_id = $_SESSION['drawer_id'];
}

$date 	= date(DATE_TIME_FORMAT);
$RequestTo = $_POST['RequestTo'] ?? null;
$response = '';
$url = '';
if($RequestTo == 'UpdateQuote'){

	$SDID_Array 				= explode(',', $_POST['salesDetailsIDs']);
	$itemTypes					= explode(',', $_POST['itemTypes']);
	$SDIOHID_Array 				= explode(',', $_POST['items']);
	$SDPrice_Array 				= explode(',', $_POST['costs']);
	$SDQuantity_Array 			= explode(',', $_POST['qtys']);
	$SDDiscountAmount_Array 	= explode(',', $_POST['discs']);
	$SDTaxAmount_Array 			= explode(',', $_POST['taxs']);
	$SDNotes_Array 				= explode(',', $_POST['ItemNote']);

	$TotalTax = 0;
	foreach ($SDTaxAmount_Array as $Tax) {
		$TotalTax = $Tax+$TotalTax;
	}

	$SalesID = $_POST['SalesID'];
	//tblSales - Data
	$SalesInvoiceNumber 	= $_POST['InvoiceNum'];
	$SCustomerID 			= $_POST['customer'] ?? null;
	$SalesDate 				= $date;
	$SAmount 				= $_POST['sales_amount'] ?? 0;
	$STax 					= $TotalTax;
	$SShippingCost 			= 0;
	$SPaymentMethodID 		= $_POST['payment_method'] ?? null;
	$SIsPaid 				= 0;
	$SPreviousSalesID 		= null; //LastSalesID($SCustomerID);
	$SIsVoid 				= 0;
	$SDOEStart				= $date;
	$SDOEEnd				= null;
	$SNotes					= $_POST['note'] ?? null;
	$SEmpID					= $EmployeeID;
	$SCashDrawerID          = $drawer_id;
	$SPONumber       		= $_POST['purchaseOrder'] ?? null;

	$update = $SDPDO->update(TBL_SALES, array('SalesInvoiceNumber', 'SCustomerID', 'SAmount', 'STax', 'SShippingCost', 'SCashDrawerID', 'SPONumber'), array($SalesInvoiceNumber, $SCustomerID, trim($SAmount), $STax, $SShippingCost, $SCashDrawerID, $SPONumber), array('SalesID', $SalesID));
	if($update){

		$LoggingOBJ->CustomerQuoteUpdateLog(array($SCustomerID, $SEmpID, $date, CUSTOMER_QUOTE_UPDATE_LOG_NOTE));
		
		for ($i=0; $i < count($SDIOHID_Array) ; $i++) { 


			if(isset($SDID_Array[$i])){

				//tblSalesDetails - Data
				$SDID 				= $SDID_Array[$i] ?? null;
				$SDSalesID 			= $SalesID;
				//$SDIOHID 			= GetOnHandID($SDIOHID_Array[$i]);
				$SDIOHID			= null;
				$SDNonInvID			= null;
				if($itemTypes[$i] == 'inv'){
					$SDIOHID = GetOnHandID($SDIOHID_Array[$i]);
				}else{
					if($itemTypes[$i] == 'non-inv'){
						$SDNonInvID			= $SDIOHID_Array[$i];
					}
				}

				//$SDNonInvID			= null;
				$SDPrice 			= $SDPrice_Array[$i];
				$SDPricePaid 		= null;
				$SDQuantity 		= $SDQuantity_Array[$i];
				$SDNotes 			= $SDNotes_Array[$i];
				$SDIsCompanyInstall = 0;
				$SDDOE 				= $date;
				$SDDiscountAmount 	= $SDDiscountAmount_Array[$i];
				$SDTaxAmount		= $SDTaxAmount_Array[$i];


				$SDPDO->update(TBL_SALES_DETAILS, array('SDIOHID', 'SDPrice', 'SDQuantity', 'SDNotes', 'SDDOE', 'SDDiscountAmount', 'SDTaxAmount'), array($SDIOHID, $SDPrice, $SDQuantity, $SDNotes, $SDDOE, $SDDiscountAmount, $SDTaxAmount), array('SDID', $SDID));

			}else{

				$SDSalesID 			= $SalesID;
				//$SDIOHID 			= GetOnHandID($SDIOHID_Array[$i]);

				$SDIOHID			= null;
				$SDNonInvID			= null;
				if($itemTypes[$i] == 'inv'){
					$SDIOHID = GetOnHandID($SDIOHID_Array[$i]);
				}else{
					if($itemTypes[$i] == 'non-inv'){
						$SDNonInvID			= $SDIOHID_Array[$i];
					}
				}

				//$SDNonInvID			= null;
				$SDPrice 			= $SDPrice_Array[$i];
				$SDPricePaid 		= null;
				$SDQuantity 		= $SDQuantity_Array[$i];
				$SDNotes 			= $SDNotes_Array[$i];
				$SDIsCompanyInstall = 0;
				$SDDOE 				= $date;
				$SDDiscountAmount 	= $SDDiscountAmount_Array[$i];
				$SDTaxAmount		= $SDTaxAmount_Array[$i];


				$tblSalesDetails_values = array( $SDSalesID, $SDIOHID, $SDNonInvID,  $SDPrice, $SDPricePaid, $SDQuantity, $SDNotes, $SDIsCompanyInstall, $SDDOE, $SDDiscountAmount, $SDTaxAmount );

				$SalesDetailID = $SalesOBJ->AddSalesData($tblSalesDetails_values);

			//Sales log
			$ASLNotes = SALES_QUOTE_UPDATE_NOTE;
			$LogValues = array($SalesID, $SalesDetailID, $EmployeeID,  $date, $ASLNotes, $SDIOHID);
			$LoggingOBJ->QuoteUpdateLog($LogValues);

			}

			
			
			
		}
		$response = true;
		$url = SITE_URL.'?destination=sales';
	}else{
		$response = false;
	}

	$output = array(
		'response' => $response,
		'url' => $url
	);
	echo json_encode($output);
}



