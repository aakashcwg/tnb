<?php

//print_r($_POST); die;

$validation_error = '';
$message = '';

$EID 			= $_POST['eid'];

$EFName 		= $_POST['efname'] ?? null;
$ELName 		= $_POST['elname'] ?? null;
$EAddress1 		= $_POST['eaddr1'] ?? null;
$EAddress2 		= $_POST['eaddr2'] ?? null;
$ECity 			= $_POST['ecity'] ?? null;
$EState 		= $_POST['estate'] ?? null;
$EZip 			= $_POST['ezip'] ?? null;
$EIsActive 		= 1;

$ELUsername 	= $_POST['euser'] ?? null;
$ELPassword 	= $_POST['epass'] ?? null;
$ELRoleID 		= $_POST['erole'] ?? 0;
$ELDateExpire	= null;

$ECCTID 		= $_POST['econtact-type'] ?? null;
$ECData 		= $_POST['econtact-data'] ?? null;
$ECIsPrimary 	= 1;

if(empty($EFName)){
	$error[] = 'First name is required';
}
if(empty($ECity)){
	$error[] = 'City is required';
}
if(empty($EState)){
	$error[] = 'State is required';
}
if(empty($ELRoleID)){
	$error[] = 'Role is required';
}
if(empty($ELUsername)){
	$error[] = 'Username is required';
}
if(empty($ELPassword)){
	$error[] = 'Password is required';
}

if(empty($error)){
	if($SDPDO->CheckExistanceExcept(TBL_EMPLOYEE_LOGIN, 'ELEID', $EID, 'ELUsername', $ELUsername)){
		$validation_error = 'Username already exists';
	}else{
		$update1 = $SDPDO->update(TBL_EMPLOYEES, array('EFName', 'ELName', 'EAddress1', 'EAddress2', 'ECity', 'EState', 'EZip', 'IsActive'), array($EFName, $ELName, $EAddress1, $EAddress2, $ECity, $EState, $EZip, $EIsActive), array('EmployeeID', $EID));

		$update2 = $SDPDO->update(TBL_EMPLOYEE_LOGIN, array('ELUsername', 'ELPassword', 'ELRoleID', 'ELDateExpire'), array($ELUsername, $ELPassword, $ELRoleID, $ELDateExpire), array('ELEID', $EID));

		if($SDPDO->CheckExistance(TBL_EMPLOYEE_CONTACT, 'ECIDEmployeeID', $EID)){
			$update3 = $SDPDO->update(TBL_EMPLOYEE_CONTACT, array('ECCTID', 'ECData', 'ECIsPrimary'), array($ECCTID, $ECData, $ECIsPrimary), array('ECIDEmployeeID', $EID));
		}else{
			$EmployeeOBJ->AddEmployeeContactData(array($EID, $ECCTID, $ECData, $ECIsPrimary));
		}
		
		if($update1 && $update2){
			$message = 'Profile successfully updated';
		}
		
	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' => $validation_error,
	'message' => $message,
	'redirect_url' => SITE_URL.'?destination=employees&action=employee-profile&eid='.base64_encode($EID)
);
echo json_encode($output);

?>