<?php
$ListID = $_POST['ListID'];
$theList = getPriceList($ListID);
$data['PLName'] = $theList->PLName;
$data['PLIsPercentage'] = $theList->PLIsPercentage;

$PLAmount = number_format($theList->PLAmount, 2);
if($theList->PLIsPercentage == 1){
	$PLAmount = $PLAmount.'%';
}
if($theList->PLIsPercentage == 0){
	$PLAmount = '$'.$PLAmount;
}

$data['PLAmount'] = $PLAmount;
$data['PLHasItems'] = $theList->PLHasItems;
$data['PLColor'] = $theList->PLColor;

// $SelectedClass = '';
// $InventoryDropdown = '';
// $InventoryDropdown .= '<label for="inventory">Inventory Item</label>';
// $InventoryDropdown .= '<select class="form-control sd-select item" id="inventory" name="inventory" data-show-subtext="true" data-live-search="true" class="form-control">
//         <option value="">Select Item</option>';
//         foreach (getInventories() as $inventory) {
//         	if($inventory->INVID == $theList->PLINVID){
// 				$SelectedClass = 'selected';
// 			}else{
// 				$SelectedClass = '';
// 			}
//             $InventoryDropdown .= '<option value="'.$inventory->INVID.'" '.$SelectedClass.'>'.getItemWithNumber($inventory->INVID).'</option>';
//         }
//     $InventoryDropdown .= '</select>';


// $data['PLINV_List'] = $InventoryDropdown;

$data['PLID'] = $ListID;

echo json_encode($data);
?>