<?php

//update-inventory-item.php

$itemname = $_POST['itemname'] ?? null;
$itemnumber = $_POST['itemnumber'] ?? null;
$itembarcode = $_POST['itembarcode'] ?? null;
$itemserialnumber = $_POST['itemserialnumber'] ?? null;
$itemdescription = $_POST['itemdescription'] ?? null;

$itemwarrenty = $_POST['itemwarrenty'] ?? 0;
$itemisactive = 0;
if(isset($_POST['itemisactive'])){
	$itemisactive = 1;
}

$minqty = $_POST['minqty'] ?? 1;
$maxqty = $_POST['maxqty'] ?? 10;

$Values = array( $itemname, $itemnumber, $itembarcode, $itemserialnumber, $itemdescription, $itemwarrenty, $itemisactive, $maxqty, $minqty );

$message = null;
$validation_error = null;
if(trim($itemname) == ''){
	$error[] = 'Item name is required';
}
if(trim($itemnumber) == ''){
	$error[] = 'Item number is required';
}
if(trim($minqty) == ''){
	$error[] = 'MinQty is required';
}
if(trim($maxqty) == ''){
	$error[] = 'MaxQty is required';
}

if(trim($minqty) != ''){
	if($minqty <= 0){
		$error[] = 'MinQty value should be atleast 1';
	}
}

if(trim($maxqty) != ''){
	if($maxqty <= $minqty){
		$error[] = 'MaxQty value should be greater than MinQty';
	}
}
if(empty($error)){
	//if( $SDPDO->CheckExistance(TBL_INVENTORY, 'INVItemNumber', $_POST['itemnumber']) === false ){
		$InventoryOBJ->UpdateInventory($Values, array('INVID', $_POST['invid']));
		$theInventory = $InventoryOBJ->GetTheInventoryByID($_POST['invid']);
		$invID = $theInventory[0]->INVID;
		$data['resp'] = true;
		$data['invid'] = $invID;
		$data['message'] = 'Item : '.$theInventory[0]->INVItemNumber.', successfully updated.';

		$isActive = $theInventory[0]->IsActive==1?'<img src="dist/images/active.png" class="active-inv-item sd-tootip isactive" title="Active" onclick="invIsActive(this)" data-status="active">':'<img src="dist/images/disabled.png" class="inactive-inv-item sd-tootip isactive" title="Inactive" onclick="invIsActive(this)" data-status="inactive">';

		$row = '
			<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=inventory&action=item-details&item-id='.$invID.'" style="cursor: pointer;">';
			$row .= $theInventory[0]->INVItemName;
		$row .= '</td>';

		$row .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=inventory&action=item-details&item-id='.$invID.'" style="cursor: pointer;">';
			$row .= $theInventory[0]->INVItemNumber;
		$row .= '</td>';
		$row .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=inventory&action=item-details&item-id='.$invID.'" style="cursor: pointer;">';
			$row .= $theInventory[0]->INVDescription;
		$row .= '</td>';
		$row .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=inventory&action=item-details&item-id='.$invID.'">';
			$row .= $theInventory[0]->INVIsWarrantyItem==1?'Yes':'No';
		$row .= '</td>';


		$row .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='.SITE_URL.'?destination=inventory&action=item-details&item-id='.$invID.'">';
            $row .= $InventoryOBJ->GetOnHandQtyByInvID($invID);
        $row .= '</td>';

		$row .= '<td class="isactive">';
			$row .= $isActive;
		$row .= '</td>';
		$row .= '<td class="action-btns">';
		$row .= '<i class="fa fa-edit sd-tootip" title="" data-id="'.$invID.'" style="font-size:24px;cursor:pointer;" onclick="editInventory(this)" data-original-title="Quick Edit"></i>';
		$row .= '<a style="margin-left: 10px;" href="'.SITE_URL.'?destination=orders&action=add-order&itemID='.$invID.'"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="" data-original-title="Order this item"></i></a>';
		$row .= '</td>';

    $data['row'] = $row;

    	//Logging
		$EmpID = currentUser('EmployeeID');
		$AIIOID = null;
		$AIIOHID = null;
		$LogValues = array( $EmpID, $AIIOID, $AIIOHID );
		$LoggingOBJ->UpdateInventoryItemLog($LogValues);

	/*}else{
		$validation_error = 'Item number already exists!';
	}*/
}else{
	$validation_error = implode(", ", $error);
}
$data['rf'] = $_POST['request-from'];
$data['error'] = $validation_error;

echo json_encode($data);