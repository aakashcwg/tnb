<?php
//drawer-cashout.php

//print_r($_POST);

$data = array();

$StartingCash = $_POST['DSC'];

$COCashDrawerID = $_SESSION['drawer_id'];
$COEmployeeID = currentUser('EmployeeID');
$COStartDOE = date('Y-m-d H:i:s', strtotime($_SESSION['time_start_login']));
$COEndDOE = date(DATE_TIME_FORMAT);
$COSystemValue = $_POST['SC'];
$COCountedValue = $_POST['DC'];
$COAdjustedValue = $_POST['AV'] ?? 0;
$COManagerID = $_SESSION['auth_manager'] ?? null;
$COIsClosed = 1;
$COAdjustedValueReason = $_POST['AV_Reason'] ?? null;

$CO_Values = array($COCashDrawerID, $COEmployeeID, $COStartDOE, $COEndDOE, $COSystemValue, $COCountedValue, $COAdjustedValue, $COManagerID, $COIsClosed, $COAdjustedValueReason);

$COID = $SalesOBJ->CashOut($CO_Values);

if($COID){
	$CODPaymentMethodID = $_POST['COCountedValue'] ?? 1;
	$COD_Values = array($COID, $CODPaymentMethodID, $COCountedValue);
	$SalesOBJ->CashOutDetails($COD_Values);
	$resp = true;
}else{
	$resp = false;
}
$CDA_Values = array($COCashDrawerID, $COEndDOE, $COEmployeeID, $StartingCash, $COCountedValue, $COEndDOE, $COEmployeeID);
$SalesOBJ->CashDrawerActivity($CDA_Values);

$data['resp'] = $resp;

echo json_encode($data);