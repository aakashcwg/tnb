<?php
//edit_cash_drawer.php

//print_r($_POST);
$CDID = $_POST['CDID'];
$CD = GetTheCashDrawer($CDID);
$data['CD_Name'] = $CD->CashDrawerName;
$data['CD_Computer'] = $CD->CDComputerName;

$dropdown = '';
$dropdown .= '<label for="warehouse">Warehouse</label>';
$dropdown .= '<select class="form-control" name="warehouse" id="warehouse">';
$dropdown .= '<option value="">Select Warehouse</option>';
$selected = '';
foreach (getWareHouses() as $WH) {
	if($WH->WarehouseID == $CD->CDWarehouseID){
		$selected = 'selected';
	}else{
		$selected = '';
	}
	$dropdown .= '<option value="'.$WH->WarehouseID.'" '.$selected.'>'.$WH->WName.'</option>';
}
$dropdown .= '</select>';

$data['CD_dd'] = $dropdown;
$data['CDID'] = $CDID;
echo json_encode($data);