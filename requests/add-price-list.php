<?php

$pl_name = $_POST['pl_name'];
$pl_amount = $_POST['pl_amount'] ?? null;
$pl_color = $_POST['pl_color'] ?? null;
$pl_is_percentage = isset($_POST['pl_is_percentage']) ? 1 : 0;
$has_items = isset($_POST['has_items']) ? 1 : 0;

$pl_amount = str_replace(['%', '$'], '', $pl_amount);
$plDropDown = '';
$error = '';
$InserID = '';
$resp = '';
$msg = '';
$tr = '';
if(empty($pl_name)){
	$error = 'Name is required';
}

if(empty($error)){
	if($SDPDO->CheckExistance(TBL_PRICE_LISTS, 'PLName', $pl_name)){
		$data['exist'] = true;
		$data['return_msg'] = 'There already exists a Price List with same name';
	}else{
		$InserID = $SDPDO->InsertData(TBL_PRICE_LISTS, array('PLName' => $pl_name, 'PLIsPercentage' => $pl_is_percentage, 'PLAmount' => $pl_amount, 'PLHasItems' => $has_items, 'PLColor' => $pl_color));

		if($InserID){
			$resp = true;
			$msg = 'New price list succesfully added';
			$obj = getPriceList($InserID);
			
			$tr .= '<tr id="'.$InserID.'" style="background-color:'.$obj->PLColor.'">';
			
			$tr .= '<td>'.$obj->PLName.'</td>';
			$PLIsPercentage = $obj->PLIsPercentage==1 ? 'Yes' : 'No';
			$tr .= '<td>'.$PLIsPercentage.'</td>';
			$tr .= '<td>'.number_format($obj->PLAmount, 2).'</td>';

			$PLHasItems = $obj->PLHasItems==1 ? 'Yes' : 'No';
			$tr .= '<td>'.$PLHasItems.'</td>';
			//$tr .= '<td>'.$obj->PLColor.'</td>';
			//$tr .= '<td><i class="fa fa-edit sd-tootip edit-cash-drawer" data-id="'.$obj->PLID.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editPriceList(this)"></i></td>';

			$tr .= '<td><img src="'.SITE_URL.'/dist/images/tnbpos-multi-edit-pic.png" style="cursor:pointer;" data-id="'.$obj->PLID.'" data-original-title="Edit" onclick="editPriceList(this)"></td>';


			$tr .= '</tr>';


			//Price list dropdown
			$plDropDown = '';
			$plDropDown .= '<label for="price-list">Price List <sup>*</sup></label>';
		    $plDropDown .= '<select class="form-control sd-select" id="price-list" name="price-list" data-live-search="true">';
		    $plDropDown .= '<option value="">Select Price List</option>';
		    $selected = '';
		    foreach (getPriceLists() as $PL) {
		    	if($PL->PLID == $InserID){
		    		$selected = 'selected';
		    	}else{
		    		$selected = '';
		    	}
		        $plDropDown .= '<option value="'.$PL->PLID.'" '.$selected.'>'.$PL->PLName.'</option>';
		    }
		    $plDropDown .= '</select>';



		}else{
			$resp = false;
			$msg = 'error...';
		}
	}
}

$data['resp'] = $resp;
$data['msg'] = $msg;
$data['error'] = $error;
$data['tr'] = $tr;
$data['plid'] = $InserID;
$data['has_items'] = $has_items;
$data['plDropDown'] = $plDropDown;
echo json_encode($data);

?>