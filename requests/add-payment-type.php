<?php
$typeName = $_POST['typeName'];
$typeDesc = $_POST['typeDesc'];
$data['action'] = $_POST['action'];

if(!empty($typeName)){
	if($SDPDO->CheckExistance(TBL_PAYMENT_METHODS, 'PaymentMethodName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Payment type already exists';
	}else{
		$InsertID = $ListTypeOBJ->AddNewPaymentMethod(array($typeName, $typeDesc));
		if($InsertID){

			$id = $InsertID;
			$data['resp'] = true;
			$data['successMsg'] = 'Payment type successfully added';
			$data['id'] = $id;
			$data['table_id'] = 'payment-type-table';
			$data['typeName'] = $typeName;
			$data['row'] = '<tr id="'.$id.'">
					<td>'.$typeName.'</td>
					<td>'.$typeDesc.'</td>
					<td><i class="fa fa-edit sd-tootip edit-payment-type" data-action="edit_payment_type" data-id="'.$id.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
				</tr>';
		}
	}
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>