<?php

//print_r($_POST); die;

$OIDs = explode(",", $_POST['OIDs']);
$Amts = explode(",", $_POST['Amts']);

$VendorID = $_POST['VID'];
$ExtraAmt = $_POST['ExtraAmt'] ?? 0;

$VPM = $_POST['VPM'];
$CheckNo = $_POST['CheckNo'] ?? null;
$Last5Digits = $_POST['Last5Digits'] ?? null;
$AuthCode = $_POST['AuthCode'] ?? null;

$Note = $_POST['Note'] ?? null;

$date = date(DATE_TIME_FORMAT);
$EmployeeID = $_POST['EmployeeID'];

$tblVendorPaymentsFields = array('VPVendorID', 'VPIsCredit', 'VPIOID', 'VPDatePaid', 'VPPaymentMethodID', 'VPReferenceNumber', 'VPAmount', 'VPNotes', 'VPEmployeeID', 'VPGLID');


if($ExtraAmt && $ExtraAmt != 0){
	$VPID_for_ExtraPay = $SDPDO->InsertData(TBL_VENDOR_PAYMENTS, array('VPVendorID' => $VendorID, 'VPIsCredit' => 0, 'VPDatePaid' => $date, 'VPPaymentMethodID' => $VPM, 'VPAmount' => $ExtraAmt, 'VPNotes' => $Note, 'VPEmployeeID' => $EmployeeID));
}

for ($i=0; $i < count($OIDs) ; $i++) { 

	$tblVendorPaymentsData = array(
		'VPVendorID' => $VendorID, 
		'VPIsCredit' => 0, 
		'VPIOID' => $OIDs[$i], 
		'VPDatePaid' => $date, 
		'VPPaymentMethodID' => $VPM, 
		'VPReferenceNumber' => null, 
		'VPAmount' => $Amts[$i], 
		'VPNotes' => $Note, 
		'VPEmployeeID' => $EmployeeID, 
		'VPGLID' => 0
	);

	$VPID = $SDPDO->InsertData(TBL_VENDOR_PAYMENTS, $tblVendorPaymentsData);

	if($VPID){
		$VPDAuthCode = $AuthCode;
		$VPDLast5Digits = $Last5Digits;
		$VPDCheckNumber = $CheckNo;
		$tblVendorPaymentDetailsData = array(
			'VPDVPID' => $VPID, 
			'VPIOID' => $OIDs[$i], 
			'VPDPaymentMethodID' => $VPM, 
			'VPDAmount' => $Amts[$i], 
			'VPDAuthCode' => $VPDAuthCode, 
			'VPDLast5Digits' => $VPDLast5Digits, 
			'VPDCheckNumber' => $VPDCheckNumber, 
			'VPDEmpID' => $EmployeeID, 
			'VPDDOE' => $date
		);
		$SDPDO->InsertData(TBL_VENDOR_PAYMENT_DETAILS, $tblVendorPaymentDetailsData);
	}
	
}


$VPs = $VendorOBJ->GetVendorPayableOrderIds($VendorID);

$TheVendor = $VendorOBJ->GetTheVendorByID($VendorID);

$html = '';

	$html .= '<div class="vendor-details">';
		$html .= '<h2>'.VC_Name($TheVendor->VCIDFName, $TheVendor->VCIDLname, $TheVendor->VendorCompanyName).'</h2>';
		if($TheVendor->VAAddress1 || $TheVendor->VAAddress2){
			$html .= '<p>'.$TheVendor->VAAddress1.' , '.$TheVendor->VAAddress2.'</p>';
		}

		if(!empty($TheVendor->VAState)){
			$VState = getUSAStates()[strtoupper($TheVendor->VAState)];
		}else{
			$VState = '';
		}

		$html .= '<i>'.$TheVendor->VACity.'</i> , <b>'.$VState.'</b>';
	$html .= '</div>';

	$html .= '</br>';

	$html .= '<table class="table-bordered vendor-payment-table">';
	$html .= '<thead>';
		$html .= '<th>Order ID</th>';
		$html .= '<th>Amount Due</th>';
		$html .= '<th>Amount being paid</th>';
	$html .= '</thead>';
	$html .= '<tbody>';
	if(!empty($VPs)){
		foreach ($VPs as $VP) {
			$DueAmt = $OrderOBJ->GetOrderDueAmount($VP);
			if($DueAmt > 0){

				$html .= '<tr>';
					$html .= '<td>#'.$VP.'</td>';
					$html .= '<td class="pay-amount">$'.$DueAmt.'</td>';
					$html .= '<td><input type="text" onkeyup="GetVendorPaymentTotal()" class="price-validation pay-vendor form-control" data-orderid="'.$VP.'" /></td>';
				$html .= '</tr>';
			}
			
		}
	}else{
		$html .= '<tr>';
			$html .= '<td colspan="4">No payments are pending.</td>';
		$html .= '</tr>';
	}
	$html .= '</tbody>';
$html .= '</table>';

$html .= '</br>';

$html .= '<div class="vendor-extra">';

	$html .= '<label for="extra-payment">Extra Payment : </label>';
	$html .= '<input type="text" onkeyup="GetVendorPaymentTotal()" class="form-control price-validation" id="vendor-extra-payment">';
	$html .= '</br></br>';
	$html .= '<label for="total-payment">Total to Pay : </label>';
	$html .= '<input type="text" class="form-control" id="vendor-total-payment" readonly>';
	$html .= '</br></br>';
	$html .= '<label for="payment-method">Payment Method : </label>';
	$html .= '<select class="vpm form-control" id="vpm" onchange="VpSet(this)">';
	$PMs = $ListTypeOBJ->GetAllPaymentMethods();
	foreach ($PMs as $PM) {
		$html .= '<option value="'.$PM->PMID.'">'.$PM->PaymentMethodName.'</option>';
	}
	$html .= '</select>';
	$html .= '</br></br>';

	$html .= '<div class="check-data" style="display:none;">';
		$html .= '<label for="check-number">Check Number : </label>';
		$html .= '<input type="text" class="form-control" id="vendor-check-number">';
		$html .= '</br></br>';
	$html .= '</div>';

	$html .= '<div class="card-data" style="display:none;">';
		$html .= '<label for="last5digits">Last 5 Digits : </label>';
		$html .= '<input type="text" class="form-control" id="last5digits">';
		$html .= '</br></br>';
		$html .= '<label for="authcode">Authorization Code : </label>';
		$html .= '<input type="text" class="form-control" id="authcode">';
		$html .= '</br></br>';
	$html .= '</div>';

	$html .= '<label for="vp-note">Note : </label>';
	$html .= '<textarea class="form-control" id="vp-note"></textarea>';

	$html .= '</br></br>';

	$html .= '<a href="javascript:void(0)" class="btn theme-default-orange text-right" data-vendorid="'.$VendorID.'" id="vendor-save-payment" onclick="SavePayment()">Save Payment</a>';

$html .= '</div>';

$data['content'] = $html;


$slip = '';

$slip .= '<h2>'.VC_Name($TheVendor->VCIDFName, $TheVendor->VCIDLname, $TheVendor->VendorCompanyName).'</h2>';
	if($TheVendor->VAAddress1 || $TheVendor->VAAddress2){
		$slip .= '<p>'.$TheVendor->VAAddress1.' , '.$TheVendor->VAAddress2.'</p>';
	}

	if(!empty($TheVendor->VAState)){
		$VState = getUSAStates()[strtoupper($TheVendor->VAState)];
	}else{
		$VState = '';
	}
	$slip .= '<i>'.$TheVendor->VACity.'</i> , <b>'.$VState.'</b>';


$slip .= '<h4>'.$date.'</h4>';

$slip .= '<table border="1">';
$slip .= '<thead>';
	$slip .= '<th>Order ID</th>';
	$slip .= '<th>Amount Paid</th>';
	$slip .= '<th>Total Paid</th>';
	$slip .= '<th>Amount Due</th>';
$slip .= '</thead>';

$slip .= '<tbody>';
for ($i=0; $i < count($Amts) ; $i++) { 

	$TotalPaid = $OrderOBJ->GetOrderPaidAmount($OIDs[$i]);
	$slip_due = $OrderOBJ->GetOrderDueAmount($OIDs[$i]);
	
	$slip .= '<tr>';
		$slip .= '<td>#'.$OIDs[$i].'</td>';
		$slip .= '<td>'.number_format($Amts[$i], 2).'</td>';
		$slip .= '<td>'.number_format($TotalPaid, 2).'</td>';
		if($slip_due > 0){
			$slip .= '<td>'.$slip_due.'</td>';
		}else{
			$slip .= '<td>None</td>';
		}
	$slip .= '</tr>';
}

$slip .= '</tbody>';

$slip .= '</table>';

$data['slip'] = $slip;

echo json_encode($data);

?>