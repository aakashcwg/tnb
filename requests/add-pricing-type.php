<?php
$typeName = $_POST['typeName'];
$data['action'] = $_POST['action'];


if(!empty($typeName)){
	if($SDPDO->CheckExistance(TBL_PRICING_TYPES, 'PTName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Pricing type already exists';
	}else{
		$InsertID = $ListTypeOBJ->AddNewPricingType(array($typeName));
		if($InsertID){

			$id = $InsertID;
			$data['resp'] = true;
			$data['successMsg'] = 'Pricing type successfully added';
			$data['id'] = $id;
			$data['table_id'] = 'pricing-type-table';
			$data['typeName'] = $typeName;
			$data['row'] = '<tr id="'.$id.'">
					<td>'.$typeName.'</td>
					<td><i class="fa fa-edit sd-tootip edit-pricing-type" data-action="edit_pricing_type" data-id="'.$id.'" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
				</tr>';
		}
	}
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>
