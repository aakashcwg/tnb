<?php
session_start();
require_once ( '../functions.php' );
$ActiveEmployee = currentUser('EmployeeID');
$action = $_POST['action'];
$file = str_replace('_', '-', $action).'.php';
$actionDir = BASE_PATH.'/requests/';
if(file_exists($actionDir.$file)){
	require_once ( $file );
}
?>