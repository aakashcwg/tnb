<?php
//receive-item.php

//print_r($_POST);

$validation_error = '';
$error = array();
$result = '';

$EID = currentUser('EmployeeID');

$ODID 	= $_POST['ODID'];
$ORDID 	= $_POST['ORDID'];
$ORID 	= $_POST['ORID'];
$RQty 	= $_POST['RQty'];
$RNote 	= $_POST['RNote'];

$date = date(DATE_TIME_FORMAT);

if(empty($RQty)){
	$error[] = 'Received quantity is required';
}

if(empty($error)){
	// Update Table - tblInventoryOrdersReceivedDetails
	$Return = $SDPDO->update(TBL_INVENTORY_ORDERS_RECEIVED_DETAILS, array('IORDDateReceived', 'IORDQuantityReceived', 'IORDIsComplete', 'IORDNotes', 'IORDEID'), array($date, $RQty, 1, $RNote, $EID), array('IORDID', $ORDID));

	// Update Table - tblInventoryOrderDetails
	$Return = $SDPDO->update(TBL_INVENTORY_ORDER_DETAILS, array('IODDateShipped'), array($date), array('IODID', $ODID));

	if($Return){
		$url = SITE_URL.'/pages/receiving/receiving.php';
		$data = http_build_query( array( 'RID' => $ORID, 'EID' => $EID ) );
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded",
		        'method'  => 'POST',
		        'content' => $data,
		    ),
		);
		$context = stream_context_create( $options );
		$result = file_get_contents( $url, false, $context );

		if($OrderOBJ->AllItemsReceived($ORID) === true){
			$SDPDO->update(TBL_INVENTORY_ORDERS_RECEIVED, array('IORDateReceived', 'IORIsComplete'), array($date, 1), array('IORID', $ORID));
		}

	}
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
		'error' => $error,
		'content' => $result
	);
echo json_encode($output);
