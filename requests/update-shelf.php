<?php
$Shelf_Name = $_POST['Shelf_Name'];
$Shelf_Location = $_POST['Shelf_Location'];
$Shelf_Warehouse = $_POST['Shelf_Warehouse'];
$Shelf_ID = $_POST['Shelf_ID'];

$sql = ' UPDATE tblShelves SET SWarehouseID = ?, ShelfName = ?, ShelfLocation = ? WHERE ShelfID = ? ';
$stmt = $pdo->prepare($sql);
$stmt->execute([$Shelf_Warehouse, $Shelf_Name, $Shelf_Location, $Shelf_ID]);
if($stmt->rowCount() > 0){
	$data['response'] = true;

	$Shelf = getShelf($Shelf_ID);

	$row = '';
	$row .= '<td>'.$Shelf->ShelfName.'</td>';
	$row .= '<td>'.$Shelf->ShelfLocation.'</td>';
	$row .= '<td>'.getWareHouseName($Shelf->SWarehouseID).'</td>';
	$row .= '<td><i class="fa fa-edit sd-tootip" title="" data-id="'.$Shelf_ID.'" style="font-size:24px;cursor:pointer;" onclick="editShelf(this)" data-original-title="Quick Edit"></i></td>';
	$data['row'] = $row;
	$data['Shelf_ID'] = $Shelf_ID;
}else{
	$data['response'] = false;
}
echo json_encode($data);
?>