<?php

//add-vendor.php

$message = null;
$validation_error = null;
$date = date(DATE_TIME_FORMAT);

$vfname = $_POST['vfname'] ?? null; 
$vlname = $_POST['vlname'] ?? null;

$vcompany = $_POST['vcompany'] ?? null;
$vaccno = $_POST['vaccno'] ?? null;
$empID = currentUser('EmployeeID');
$visactivecheck = $_POST['visactivecheck'] ?? 0;


$vcontacttype = $_POST['vcontacttype'] ?? null;
$vcdata = $_POST['vcdata'] ?? null;
$vpcontactcheck = $_POST['vpcontactcheck'] ?? 0;

$vaddrtype = $_POST['vaddrtype'] ?? null;
$vaddr1 = $_POST['vaddr1'] ?? null;
$vaddr2 = $_POST['vaddr2'] ?? null;
$vcity = $_POST['vcity'] ?? null;
$vstate = $_POST['vstate'] ?? null;
$vzip = $_POST['vzip'] ?? null;




//data for - tblVendors
$tblVendors_values = array( $vcompany, $vaccno, $empID, $date, $visactivecheck );

if(trim($vcompany) == ''){
	$error[] = 'Company name is required';
}
if(trim($vaccno) == ''){
	$error[] = 'Account number is required';
}

if(empty($error)){
	if($SDPDO->CheckExistance(TBL_VENDORS, 'VendorCompanyName', $vcompany) === true){
		$validation_error = 'Company already exists!';
	}else if($SDPDO->CheckExistance(TBL_VENDORS, 'VendorAccountNumber', $vaccno) === true){
		$validation_error = 'Account number already exists!';
	}else{
		$VendorID = $VendorOBJ->AddNewVendor($tblVendors_values);
		if($VendorID){
			//Logging
			$LoggingOBJ->VendorLog(array($VendorID, $empID, $date, NEW_VENDOR_LOG_NOTE));

			//data for - tblVendorContact
			$tblVendorContact_values = array( $VendorID, $vcontacttype, $vcdata, $vpcontactcheck, $vfname, $vlname );
			$VContactID = $VendorOBJ->AddVendorContactData($tblVendorContact_values);
			//Logging
			if($VContactID){
				$LoggingOBJ->VendorLog(array($VendorID, $empID, $date, 'Vendor Contact ID : '.$VContactID.' // Added new vendor contact'));
			}

			//data for - tblVendorAddresses
			$tblVendorAddresses_values = array( $VendorID, $vaddrtype, $vaddr1, $vaddr2, $vcity, $vstate, $vzip );
			$VAddressID = $VendorOBJ->AddVendorAddressData($tblVendorAddresses_values);
			//Logging
			if($VAddressID){
				$LoggingOBJ->VendorLog(array($VendorID, $empID, $date, 'Vendor Address ID : '.$VAddressID.' // Added new vendor address'));
			}

			$message = 'Vendor successfully added.';
		}
	}
	
}else{
	$validation_error = implode(", ", $error);
}

$output = array(
	'error' 	=> $validation_error,
	'message' 	=> $message,
	'returnUrl' => SITE_URL.'/?destination=vendors'
);

echo json_encode($output);

?>
