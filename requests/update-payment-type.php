<?php
$typeName = $_POST['typeName'];
$typeDesc = $_POST['typeDesc'];
$id = $_POST['id'];

if(!empty($typeName)){
	if($SDPDO->CheckExistanceExcept(TBL_PAYMENT_METHODS, 'PMID', $id, 'PaymentMethodName', $typeName)){
		$data['resp'] = false;
		$data['errorMsg'] = 'Payment type already exists';
	}else{
		$update = $ListTypeOBJ->UpdatePaymentMethod(array($typeName, $typeDesc), array('PMID', $id));
		if($update){
			$data['resp'] = true;
			$data['successMsg'] = 'Payment type successfully updated';
			$data['id'] = $id;
			$data['table_id'] = 'payment-type-table';
			$data['typeName'] = $typeName;
			$data['typeDesc'] = $typeDesc;
		}
	}	
}else{
	$data['blank'] = true;
}

echo json_encode($data);
?>