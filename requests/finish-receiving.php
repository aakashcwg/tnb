<?php
//finish-receiving.php
/*echo '<pre>';
print_r($_POST);*/



$EID = currentUser('EmployeeID');

$ODID 	= $_POST['ODID'];
$ORDID 	= $_POST['ORDID'];
$ORID 	= $_POST['ORID'];
$RQty 	= $_POST['RQty'];
$RNote 	= $_POST['RNote'];
$Item   = $_POST['Item'];

$date = date(DATE_TIME_FORMAT);

if(empty($RQty)){
	$error = 'Received quantity is required';
}
$error = '';
$message = '';
if(empty($error)){

	for ($i=0; $i < count($RQty); $i++) {

		// Update Table - tblInventoryOrdersReceivedDetails

		if($RQty[$i] > 0){
			$update1 = $SDPDO->update(TBL_INVENTORY_ORDERS_RECEIVED_DETAILS, array('IORDDateReceived', 'IORDQuantityReceived', 'IORDIsComplete', 'IORDNotes', 'IORDEID'), array($date, $RQty[$i], 1, $RNote[$i], $EID), array('IORDID', $ORDID[$i]));


			if($update1){
				// Update Table - tblInventoryOrderDetails
				$update2 = $SDPDO->update(TBL_INVENTORY_ORDER_DETAILS, array('IODQtyPerCase', 'IODDateShipped'), array($RQty[$i], $date), array('IODID', $ODID[$i]));

				if($OrderOBJ->AllItemsReceived($ORID[0]) === true){
					$SDPDO->update(TBL_INVENTORY_ORDERS_RECEIVED, array('IORDateReceived', 'IORIsComplete'), array($date, 1), array('IORID', $ORID[0]));
				}
				$ItemID = str_replace('#', '', $Item[$i]);
				$OnHandID = GetOnHandID($ItemID);
				$NewQuantity = GetOnHandQty($OnHandID) + $RQty[$i];
				if($SDPDO->CheckExistance(TBL_INVENTORY_ON_HAND, 'IOHInventoryID', $ItemID) === true){
					$InventoryOBJ->UpdateOnHandItemQuantity(array($NewQuantity), array('IOHID', $OnHandID));
				}

				$message = 'Item received successfully';
			}



		}
		
	}

}
$output = array(
	'error' => $error,
	'message' => $message
);
echo json_encode($output);