<?php

$itemIDs = explode(',', $_POST['myitems']);
$num = $_POST['btnid'];
$newNum = $num+1;

$selectedItems = array();
foreach ($itemIDs as $itemID) {
	$selectedItems[] = $itemID;
}

$AllItems = $InventoryOBJ->GetAllIds();
$resaultatIds = array_diff($AllItems, $selectedItems);


$html = '';

$html .= '<div class="item-block" data-item="'.$newNum.'">';

	//Item
	$html .= '<div class="col-md-2">';
		$html .= '<div class="form-group">';
			$html .= '<label for="item'.$newNum.'">Item</label>';
			$html .= '<select class="form-control sd-select item" id="item'.$newNum.'" name="item" data-show-subtext="true" data-live-search="true" class="form-control">';
				$html .= '<option value="">Select Item</option>';
				foreach ($resaultatIds as $item) {
					$html .= '<option value="'.$item.'">'.getItemWithNumber($item).'</option>';
				}

			$html .= '</select>';
		$html .= '</div>';
	$html .= '</div>';

	//Vendor Item#
	$html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="vitem">Vendor Item#</label>';
            $html .= '<input type="vitem" name="vitem[]" id="vitem" class="form-control vitem">';
        $html .= '</div>';
    $html .= '</div>';

	//Case
	$html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="case">Is Case</label>';
            $html .= '<input type="checkbox" name="case[]" id="case" class="form-control case">';
        $html .= '</div>';
    $html .= '</div>';

    //Case Qty
    $html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="case-qty">How many cases?<sup>*</sup></label>';
            $html .= '<input type="number" name="case-qty[]" min="1" value="1" id="case-qty" class="form-control case-qty qty">';
        $html .= '</div>';
    $html .= '</div>';

    //Qty Per Case
    $html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="qty-per-case">Qty Per Case<sup>*</sup></label>';
            $html .= '<input type="text" name="qty-per-case[]" min="1" value="1" id="qty-per-case" class="form-control qty-per-case qty" onblur="MatchWithInventoryMaxQty(this)">';
        $html .= '</div>';
    $html .= '</div>';

	//Cost
	$html .= '<div class="col-md-2">';
		$html .= '<div class="form-group">';
			$html .= '<label for="cost'.$newNum.'">Cost per Item<sup>*</sup></label>';
			$html .= '<input type="text" name="cost" id="cost'.$newNum.'" class="form-control cost price-validation">';
		$html .= '</div>';
	$html .= '</div>';


	//Sales Tax
	$html .= '<div class="col-md-2">';
		$html .= '<div class="form-group">';
			$html .= '<label for="tax'.$newNum.'">Sales Tax<sup>*</sup></label>';
			$html .= '<input type="text" name="tax" id="tax'.$newNum.'" class="form-control tax price-validation">';
		$html .= '</div>';
	$html .= '</div>';

	//Shipping Cost
	$html .= '<div class="col-md-2">';
		$html .= '<div class="form-group">';
			$html .= '<label for="shipping-cost'.$newNum.'">Shipping Cost<sup>*</sup></label>';
			$html .= '<input type="text" name="shipping_cost" id="shipping-cost'.$newNum.'" class="form-control shipping-cost price-validation">';
		$html .= '</div>';
	$html .= '</div>';

	//GL#
	$html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="gl'.$newNum.'">GL#<sup>*</sup></label>';
            $html .= '<select name="gl[]" id="gl'.$newNum.'" class="form-control gl">';
            foreach ($ListTypeOBJ->GetAllLedgerNumbers() as $ListType) {
                $html .= '<option value="'.$ListType->GeneralLedgerID.'">'.$ListType->GLNumber.'</option>';
            }
            $html .= '</select>';
        $html .= '</div>';
    $html .= '</div>';

    //Note
    $html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="note'.$newNum.'">Note</label>';
            $html .= '<input type="text" name="note" id="note'.$newNum.'" class="form-control note">';
        $html .= '</div>';
    $html .= '</div>';

    $html .= '<div class="col-md-2">';
        $html .= '<div class="form-group">';
            $html .= '<label for="customer'.$newNum.'">Customer</label>';
            $html .= '<select class="form-control customer selectpicker sd-select" id="customer'.$newNum.'" name="customer" data-show-subtext="true" data-live-search="true">';
                $html .= '<option value="NULL">Select Customer</option>';
                foreach ($CustomerOBJ->GetAllCustomers() as $customer) {
                    $theCustomer = VC_Name($customer->CustomerFName, $customer->CustomerLName, $customer->CustomerCompanyName);
                    $html .= '<option value="'.$customer->CustomerID.'">'.$theCustomer.'</option>';
                }
            $html .= '</select>';
        $html .= '</div>';
    $html .= '</div>';


	$html .= '<div class="col-md-2">';
		$html .= '<a href="javascript:void(0)" class="add-remove-btn remove-selling-item" style="background:red;margin-top:24px;" onclick="RemoveOrderItem(this)" data-btnid="'.$newNum.'">-</a><a href="javascript:void(0)" class="add-remove-btn add-selling-item" style="margin-top:24px;" onclick="AddOrderItem(this)" data-btnid="'.$newNum.'">+</a>';
	$html .= '</div>';

	$html .= '<div class="clearfix"></div>';

$html .= '</div>';


$data['htmlContent'] = $html;

echo json_encode($data);



?>