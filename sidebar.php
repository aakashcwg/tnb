<?php

//sidebar.php

if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false){
    die("<h2>You are unable to access this page.</h2>");
}

?>
<nav id="sidebar">
    <!-- Sidebar Header -->
    <div class="sidebar-header">
    	<h3><?=currentUser('ELUsername');?></h3>
        <h4><a href="<?=SITE_URL;?>">Dashboard</a></h4>
    </div>
    <!-- Sidebar Links -->
    <?php 
    if(isset($_GET['destination']) && !empty($_GET['destination'])){
    	$activeMenu = $_GET['destination'];
    }else{
    	$activeMenu='';
    }
    $menuItems = getMenus();
    ?>
    <ul class="list-unstyled components">
    	<?php
    	$count=1;
    	foreach ($menuItems as $menu) {
            $item = $menu->SectionName;
    		?>
            <?php if($item=='Sales' || $item=='sales') : ?>
                <li class="<?php echo (!empty($activeMenu) && $activeMenu==strtolower(str_replace([':', '\\', '/', '*', ' '], '-', $item)))?'active':''; ?>"><a href="javascript:void(0)" data-url="<?php echo SITE_URL.'?destination='.strtolower(str_replace([':', '\\', '/', '*', ' '], '-', $item)); ?>"><?=$item;?></a></li>
            <?php else: ?>
    		<li class="<?php echo (!empty($activeMenu) && $activeMenu==strtolower(str_replace([':', '\\', '/', '*', ' '], '-', $item)))?'active':''; ?>"><a href="<?php echo SITE_URL.'?destination='.strtolower(str_replace([':', '\\', '/', '*', ' '], '-', $item)); ?>"><?=$item;?></a></li>
            <?php endif; ?>
    		<?php
    	$count++; } ?>
        <li><a href="<?php echo SITE_URL; ?>?destination=permissions">Permissions</a></li>
    </ul>
</nav>