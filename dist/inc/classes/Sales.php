<?php

/*if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");*/

//Claas Sales.php

Class Sales extends SDPDO {

	const TBL_SALES_FIELDS = array('SalesInvoiceNumber', 'SCustomerID', 'SalesDate', 'SAmount', 'STax', 'SShippingCost', 'SPaymentMethodID', 'SIsPaid', 'SPreviousSalesID', 'SIsVoid', 'SDOEStart', 'SDOEEnd', 'SNotes', 'SEmpID', 'SCashDrawerID', 'SPONumber');

	const TBL_SALES_DETAILS_FIELDS = array('SDSalesID','SDIOHID','SDNonInvID','SDPrice','SDPricePaid','SDQuantity', 'SDNotes', 'SDIsCompanyInstall', 'SDDOE', 'SDDiscountAmount', 'SDTaxAmount');

	const TBL_AUDIT_SALES_LOG_FIELDS = array('ASLSalesID','ASLSalesDetailID','ASLEmployeeID','ASLDOE','ASLNotes', 'ASLIOHID');


	public function MakeSale($tblSales_values){
		$tblSales_data = array_combine(self::TBL_SALES_FIELDS, $tblSales_values);
		$NewSalesID = $this->InsertData(TBL_SALES, $tblSales_data);
		return $NewSalesID;
	}

	public function AddSalesData($tblSalesDetails_values){
		$tblSalesDetails_data = array_combine(self::TBL_SALES_DETAILS_FIELDS, $tblSalesDetails_values);
		$tblSalesDetailsID = $this->InsertData(TBL_SALES_DETAILS, $tblSalesDetails_data);
		return $tblSalesDetailsID;
	}

	public function GetAllInvoices(){
		return $this->GetAll(TBL_SALES);
	}

	public function GetTheItemsBySalesID($SalesID){
		return $this->GetAll(TBL_SALES_DETAILS, array('SDSalesID' => $SalesID));
	}

	public function UpdateInvoice($values, $where){
		$update = $this->update(TBL_SALES, self::TBL_SALES_FIELDS, $values, $where);
		return $update;
	}

	public function UpdateInvoiceData($values, $where){
		$update = $this->update(TBL_SALES_DETAILS, self::TBL_SALES_DETAILS_FIELDS, $values, $where);
		return $update;
	}

	public function GetSalesInfo($SalesID, $field){
		$SalesInfo = $this->GetAll(TBL_SALES, array('SalesID' => $SalesID));
		return $SalesInfo[0]->$field;
	}

	public function MakeSalesLog($tblAudit_SalesLog_values){
		$tblAudit_SalesLog_data = array_combine(self::TBL_AUDIT_SALES_LOG_FIELDS, $tblAudit_SalesLog_values);
		$NewSalesID = $this->InsertData(TBL_AUDIT_SALES_LOG, $tblAudit_SalesLog_data);
		return $NewSalesID;
	}

	public function Total($SalesID){
		$data = $this->MyQuery( ' SELECT SAmount AS SA FROM '.TBL_SALES.' WHERE SalesID = '.$SalesID,  true );
		if($data){
			return $data->SA;
		}
	}

	public function PaidAmt($SalesID){
		$data = $this->MyQuery( ' SELECT SUM(SPAmount) AS SPA FROM '.TBL_SALES_PAYMENTS.' WHERE SPSalesID = '.$SalesID,  true );
		if($data){
			return $data->SPA;
		}
	}

	public function PMs($SalesID){
		$data = $this->GetAll( TBL_SALES_PAYMENTS, array('SPSalesID' => $SalesID) );
		if($data){
			return $data;
		}
	}

	//Move to Quote
	public function MovoToQuote($quote_values){
		$QuoteID = $this->MakeSale($quote_values);
		return $QuoteID;
	}

	//Add Quote Item Data
	public function AddQuoteItemData($quote_item_values){
		$InsertID = $this->AddSalesData($quote_item_values);
		return $InsertID;
	}

	//Get Quotes
	public function GetAllQuotes(){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES.' WHERE SIsPaid = 0 AND SIsVoid = 0');
		//$data = $this->GetAll(TBL_SALES, array('SIsPaid' => 0, 'SIsVoid' => 0));
		if($data){
			return $data;
		}
	}

	//Get Quotes By Customer ID
	public function GetQuotesByCustomerID($CustomerID){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES.' WHERE SIsPaid = 0 AND SCustomerID = '.$CustomerID.' AND SIsVoid = 0');
		if($data){
			return $data;
		}
	}

	//Get Quotes By Sales ID
	public function GetQuotesBySalesID($SalesID){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES.' WHERE SIsPaid = 0 AND SalesID = '.$SalesID);
		if($data){
			return $data[0];
		}
	}

	//Get Quotes By Customer ID & Sales ID
	public function GetQuotesByCustomerIDAndSalesID($CustomerID, $SalesID){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES.' WHERE SIsPaid = 0 AND SCustomerID = '.$CustomerID.' AND SalesID = '.$SalesID);
		if($data){
			return $data[0];
		}
	}

	//Cashout
	public function SalesCashByDate($startDT, $endDT){
		/*$sdate = $startDT;
		if($startDT && $startDT == 'login_dt'){
			$sdate = date('Y-m-d H:i:s', strtotime($_SESSION['time_start_login']));
		}*/
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES_PAYMENTS.' WHERE SPDOE <= '.$endDT);

		if($data){
			return $data;
		}else{
			return 'No Sales Cash Available';
		}
	}


	public function CashOut($tblCashOut_values){
		$fields = array('COCashDrawerID', 'COEmployeeID', 'COStartDOE', 'COEndDOE', 'COSystemValue', 'COCountedValue', 'COAdjustedValue', 'COManagerID', 'COIsClosed', 'COAdjustedValueReason');
		$tblCashOut_data = array_combine($fields, $tblCashOut_values);
		$COID = $this->InsertData(TBL_CASH_OUT, $tblCashOut_data);
		return $COID;
	}

	public function CashOutDetails($tblCashOutDetails_values){
		$fields = array('CODCOID', 'CODPaymentMethodID', 'COAmount');
		$tblCashOutDetails_data = array_combine($fields, $tblCashOutDetails_values);
		$CODID = $this->InsertData(TBL_CASH_OUT_DETAILS, $tblCashOutDetails_data);
		return $CODID;
	}


	public function AllPayments(){
		$data = $this->MyQuery('SELECT SPSalesID, SUM(SPAmount) AS Amount FROM '.TBL_SALES_PAYMENTS.' GROUP BY SPSalesID');
		if($data){
			return $data;
		}
	}

	public function SalesDetailsBySalesID($SalesID){
		$data = $this->GetAll(TBL_SALES_DETAILS, array('SDSalesID' => $SalesID));
		if($data){
			return $data;
		}
	}

	public function PaymentsByDate($date1, $date2 = null){
		if($date2){
			$stmt = $this->pdo->prepare('SELECT * FROM '.TBL_SALES_PAYMENTS.' WHERE SPDOE >= ? AND SPDOE <= ?');
			$stmt->execute([$date1, $date2]);
			$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		}else{
			$stmt = $this->pdo->prepare('SELECT * FROM '.TBL_SALES_PAYMENTS.' WHERE SPDOE >= ? AND SPDOE <= GETDATE()');
			$stmt->execute([$date1]);
			$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		}
		if($data){
			return $data;
		}
	}

	public function EmployeeSalesValue($EmpID){
		$UserLoginTime = isset($_SESSION['time_start_login']) ? $_SESSION['time_start_login'] : null;
		if($UserLoginTime){
			$PMs = $this->PaymentsByDate($UserLoginTime);
			foreach ($PMs as $PM) {
				if($PM->SPEmpID == $EmpID){
					$data[] = $PM->SPAmount;
				}
			}
			$Total = array_sum($data);
			return number_format($Total, 2);
		}
	}


	public function MakeVoid($SalesID){
		$update = $this->update(TBL_SALES, array('SIsVoid'), array(1), array('SalesID', $SalesID));
		if($update){
			return true;
		}
	}

	public function IsVoid($SalesID){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES.' WHERE SalesID = '.$SalesID.' AND SIsVoid = 1', true);
		if($data){
			if($data->SIsVoid == 1){
				return true;
			}
		}
	}

	public function AllSalesByCustomer($cid){
		$data = $this->GetAll(TBL_SALES, array('SCustomerID' => $cid));
		if($data){
			return $data;
		}
	}

	public function AllSales(){
		$data = $this->GetAll(TBL_SALES);
		if($data){
			return $data;
		}
	}
	/*public function CO_Complete(){
		if(isset($_SESSION['time_start_login'])){
			$LT = $_SESSION['time_start_login'];
			$data = $this->MyQuery('SELECT * FROM '.TBL_CASH_OUT.' WHERE COStartDOE = '.$LT);
			if()
			return $data;
		}else{
			return false;
		}
	}*/

}
