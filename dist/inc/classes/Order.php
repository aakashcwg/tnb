<?php

// if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
// 	die("<h2 align='center'>You are unable to access this page.</h2>");

//Claas Order.php

Class Order extends SDPDO {

	const TBL_INVENTORY_ORDERS_FIELDS 					= array('IOCustomerID', 'IOTotalCost', 'IOTotalSalesTax', 'IOTotalShippingCost', 'IODateOrdered', 'IOOrderPONumber', 'IOEID', 'IOGLID', 'IOIsComplete');

	const TBL_INVENTORY_ORDER_DETAILS_FIELDS 			= array('IODIOID','IODINVID','IODVendorID','IODVendorItemNumber','IODCost','IODSalesTax', 'IODShippingCost', 'IODIsCase', 'IODQtyOfCases', 'IODQtyPerCase', 'IODDateOrdered', 'IODDateShipped', 'IODOrderPONumber', 'IODNotes', 'IODEID', 'IODGLID', 'IODPaidAmount', 'IODCustomerID');

	const TBL_INVENTORY_ORDERS_RECEIVED_FIELDS 			= array('IORIOID','IORDateReceived','IORIsComplete','IORNotes','IOREID');

	const TBL_INVENTORY_ORDERS_RECEIVED_DETAILS_FIELDS 	= array('IORDIORID','IODID','IORDDateReceived','IORDQuantityReceived','IORDIsComplete', 'IORDNotes', 'IORDEID');


	//Create new order
	public function CreateOrder($tblInventoryOrders_values){
		$tblInventoryOrders_data = array_combine(self::TBL_INVENTORY_ORDERS_FIELDS, $tblInventoryOrders_values);
		$NewOrderID = $this->InsertData(TBL_INVENTORY_ORDERS, $tblInventoryOrders_data);
		return $NewOrderID;
	}

	//Add order details
	public function AddOrderDetails($tblInventoryOrderDetails_values){
		$tblInventoryOrderDetails_data = array_combine(self::TBL_INVENTORY_ORDER_DETAILS_FIELDS, $tblInventoryOrderDetails_values);
		$OrderDetailsID = $this->InsertData(TBL_INVENTORY_ORDER_DETAILS, $tblInventoryOrderDetails_data);
		return $OrderDetailsID;
	}

	//Receive Order
	public function ReceiveOrder($tblInventoryOrdersReceived_values){
		$tblInventoryOrdersReceived_data = array_combine(self::TBL_INVENTORY_ORDERS_RECEIVED_FIELDS, $tblInventoryOrdersReceived_values);
		$OrderReceivedID = $this->InsertData(TBL_INVENTORY_ORDERS_RECEIVED, $tblInventoryOrdersReceived_data);
		return $OrderReceivedID;
	}

	//Update Received Order
	public function UpdateReceivedOrder($values, $where){
		$update = $this->update(TBL_INVENTORY_ORDERS_RECEIVED, self::TBL_INVENTORY_ORDERS_RECEIVED_FIELDS, $values, $where);
		return $update;
	}

	//Receive Order Item
	public function ReceiveOrderItem($tblInventoryOrdersReceivedDetails_values){
		$tblInventoryOrdersReceivedDetails_data = array_combine(self::TBL_INVENTORY_ORDERS_RECEIVED_DETAILS_FIELDS, $tblInventoryOrdersReceivedDetails_values);
		$OrderReceivedDetailsID = $this->InsertData(TBL_INVENTORY_ORDERS_RECEIVED_DETAILS, $tblInventoryOrdersReceivedDetails_data);
		return $OrderReceivedDetailsID;
	}

	//Update Received Order Item
	public function UpdateReceivedOrderItem($values, $where){
		$update = $this->update(TBL_INVENTORY_ORDERS_RECEIVED_DETAILS, self::TBL_INVENTORY_ORDERS_RECEIVED_DETAILS_FIELDS, $values, $where);
		return $update;
	}

	//Get Order Status By Order ID
	public function GetOrderStatusByOrderID($OrderID){
		$data = $this->MyQuery( 'SELECT IOIsComplete FROM '.TBL_INVENTORY_ORDERS.' WHERE IOID = '.$OrderID, true );
		if($data->IOIsComplete == 1){
			$status = 'Complete';
		}else{
			$status = 'Pending';
		}
		return $status;
	}

	//Get Order Total Amount
	public function GetOrderTotalAmount($OrderID){
		$data = $this->MyQuery( 'SELECT (IOTotalCost+IOTotalSalesTax+IOTotalShippingCost) AS Total FROM '.TBL_INVENTORY_ORDERS.' WHERE IOID = '.$OrderID, true );
		return $data;
	}

	//Get Order Details Item Paid Amount
	public function GetPaidAmountOverItem($OrderID){
		$data = $this->MyQuery( 'SELECT SUM(IODPaidAmount) AS PaidAmount FROM '.TBL_INVENTORY_ORDER_DETAILS.' WHERE IODIOID = '.$OrderID, true );
		return $data;
	}

	//Get Order Paid Amount
	public function GetOrderPaidAmount($OrderID){
		$data = $this->MyQuery( 'SELECT SUM(VPAmount) AS PaidAmount FROM '.TBL_VENDOR_PAYMENTS.' WHERE VPIOID = '.$OrderID, true );
		return $data->PaidAmount;
	}

	//Get Order Due Amount
	public function GetOrderDueAmount($OrderID){
		$TOT = floatval($this->GetOrderTotalAmount($OrderID)->Total);
		$Paid = floatval($this->GetOrderPaidAmount($OrderID));
		$Due = $TOT - $Paid;
		return number_format($Due, 2);
	}

	//
	public function GetOrdersReadyToReceive(){
		$data = $this->MyQuery( ' SELECT * FROM '.TBL_INVENTORY_ORDERS_RECEIVED );
		return $data;
	}

	//
	public function GetOrderItemsReadyToReceive($ORID){
		$data = $this->MyQuery( ' SELECT * FROM '.TBL_INVENTORY_ORDERS_RECEIVED.' INNER JOIN '.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.' ON '.TBL_INVENTORY_ORDERS_RECEIVED.'.IORID='.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.'.IORDIORID WHERE '.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.'.IORDIORID ='.$ORID);
		return $data;
	}

	//
	public function GetAllOrdersWithReceive(){
		$data = $this->MyQuery( ' SELECT * FROM '.TBL_INVENTORY_ORDERS.' INNER JOIN '.TBL_INVENTORY_ORDERS_RECEIVED.' ON '.TBL_INVENTORY_ORDERS.'.IOID='.TBL_INVENTORY_ORDERS_RECEIVED.'.IORIOID ' );
		return $data;
	}

	//
	public function GetAllOrderItemsByReceiveID($ReceiveID){
		$data = $this->MyQuery( ' SELECT * FROM '.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.' INNER JOIN '.TBL_INVENTORY_ORDER_DETAILS.' ON '.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.'.IODID='.TBL_INVENTORY_ORDER_DETAILS.'.IODID WHERE '.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.'.IORDIORID = '.$ReceiveID );
		return $data;
	}

	//
	public function GetItemQuantityByOrderDetailsID($ODID){
		$data = $this->MyQuery( ' SELECT IODQtyPerCase AS QTY FROM '.TBL_INVENTORY_ORDER_DETAILS.' WHERE IODID = '.$ODID );
		if($data){
			return $data[0]->QTY;
		}
	}

	//
	public function AllItemsReceived($ORID){
		$data = $this->MyQuery( ' SELECT * FROM '.TBL_INVENTORY_ORDERS_RECEIVED_DETAILS.' WHERE IORDIsComplete = 0 AND IORDIORID = '.$ORID );
		if(empty($data)){
			return true;
		}else{
			return false;
		}
	}

	public function GetTheOrder($OrderID){
		$data = $this->GetAll(TBL_INVENTORY_ORDERS, array('IOID' => $OrderID));
		if($data){
			return $data[0];
		}
	}

	public function GetOrderDate($OrderID){
		$data = $this->MyQuery(' SELECT IODateOrdered AS OD FROM '.TBL_INVENTORY_ORDERS.' WHERE IOID = '.$OrderID, true );
		if($data){
			return $data->OD;
		}
	}

	public function LastOrderID(){
		$data = $this->MyQuery('SELECT * FROM '.TBL_INVENTORY_ORDERS.' ORDER BY IOID DESC', true );
		if($data){
			return $data->IOID;
		}
	}

}