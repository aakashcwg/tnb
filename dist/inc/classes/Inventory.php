<?php

/*if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");*/

//Claas Inventory.php

Class Inventory extends SDPDO {

	const TBL_INVENTORY_FIELDS = array('INVItemName', 'INVItemNumber', 'INVBarcodeNumber', 'INVSerialNumber', 'INVDescription', 'INVIsWarrantyItem', 'IsActive', 'INVMaxQty', 'INVMinQty');

	const TBL_INVENTORY_ON_HAND_FIELDS = array('IOHInventoryID','IOHIORDID','IOHQuantity','IOHWarehouseID','IOHShelfID','IOHBin');

	const TBL_INVENTORY_ON_HAND_PRICING_FIELDS = array('IOHPIOHID','IOHPPriceTypeID','IOHPPrice');

	const TBL_NONINVENTORY_ITEMS_FIELDS = array('NonInvPartNumber', 'NonInvName', 'NonInvCost', 'NonInvDescription', 'IsActive', 'NIGLID');

	const TBL_NON_INVENTORY_PRICING_FIELDS = array('NIPNonInvID', 'NIPPriceTypeID', 'NIPPrice');

	//Insert new inventory
	public function AddNewInventory($tblInventory_values){
		$tblInventory_data = array_combine(self::TBL_INVENTORY_FIELDS, $tblInventory_values);
		$NewInventoryID = $this->InsertData(TBL_INVENTORY, $tblInventory_data);
		return $NewInventoryID;
	}

	//Get all inventories
	public function GetAllInventories(){
		$data = $this->GetAll(TBL_INVENTORY);
		if($data){
			return $data;
		}
	}

	//Get All Ids
	public function GetAllIds(){
		$data = $this->MyQuery(" SELECT INVID AS id FROM ".TBL_INVENTORY." ORDER BY id DESC ");
		$ids = array();
		foreach ($data as $value) {
			$ids[] = $value->id;
		}
		return $ids;
	}

	//Get Inventory by Inventory ID
	public function GetTheInventoryByID($InventoryID){
		return $this->GetAll(TBL_INVENTORY, array('INVID' => $InventoryID));
	}

	//Update Inventory
	public function UpdateInventory($values, $where){
		$update = $this->update(TBL_INVENTORY, self::TBL_INVENTORY_FIELDS, $values, $where);
		return $update;
	}

	//Get all ON Hand Items
	public function GetOnHandItems(){
		$data = $this->GetAll(TBL_INVENTORY_ON_HAND);
		if($data){
			return $data;
		}
	}

	//Get ON Hand Item by On Hand ID
	public function GetOnHandItemByID($OnHandID){
		return $this->GetAll(TBL_INVENTORY_ON_HAND, array('IOHID' => $OnHandID));
	}

	//Get On Hand Item by Invrntory ID
	public function GetOnHandItemByInventoryID($InventoryID){
		$data = $this->GetAll(TBL_INVENTORY_ON_HAND, array('IOHInventoryID' => $InventoryID));
		if($data){
			return $data[0];
		}
	}


	public function GetItemPriceByListID($OnHandID, $List){
		$data = $this->MyQuery('SELECT * FROM '.TBL_INVENTORY_ON_HAND_PRICING.' WHERE IOHPIOHID = '.$OnHandID.' AND IOHPPriceTypeID = '.$List);
		if($data){
			return $data[0];
		}
	}

	public function SetItemPriceAndList($values){
		$fields = array('IOHPIOHID', 'IOHPPriceTypeID', 'IOHPPrice');
		$newPriceData = array_combine($fields, $values);
		$InsertID = $this->InsertData(TBL_INVENTORY_ON_HAND_PRICING, $newPriceData);
		return $InsertID;
	}

	public function UpdateListPrice($value, $iohpid){
		$update = $this->update(TBL_INVENTORY_ON_HAND_PRICING, array('IOHPPrice'), array($value), array('IOHPID', $iohpid));
		if($update){
			return true;
		}
	}


	//Get item price by Inventory ID
	public function GetItemPriceByInventoryID($InventoryID){
		$data = $this->MyQuery(' SELECT IOHPPrice FROM ( '.TBL_INVENTORY_ON_HAND.' INNER JOIN '.TBL_INVENTORY_ON_HAND_PRICING.' ON '.TBL_INVENTORY_ON_HAND.'.IOHID = '.TBL_INVENTORY_ON_HAND_PRICING.'.IOHPIOHID ) WHERE IOHInventoryID = '.$InventoryID );
		if($data){
			return number_format($data[0]->IOHPPrice, 2);
		}
	}

	//Get item price by On Hand ID
	public function GetItemPriceByOnHandID($OnHandID){
		$data = $this->MyQuery(' SELECT IOHPPrice FROM ( '.TBL_INVENTORY_ON_HAND.' INNER JOIN '.TBL_INVENTORY_ON_HAND_PRICING.' ON '.TBL_INVENTORY_ON_HAND.'.IOHID = '.TBL_INVENTORY_ON_HAND_PRICING.'.IOHPIOHID ) WHERE IOHID = '.$OnHandID, true );
		return number_format($data->IOHPPrice, 2);
	}

	//Insert On Hand Item
	public function InsertOnHandItem($tblInventoryOnHand_values){
		$tblInventoryOnHand_data = array_combine(self::TBL_INVENTORY_ON_HAND_FIELDS, $tblInventoryOnHand_values);
		$NewOnHandID = $this->InsertData(TBL_INVENTORY_ON_HAND, $tblInventoryOnHand_data);
		return $NewOnHandID;
	}

	//Insert On Hand Price Data
	public function InsertOnHandItemPriceData($tblInventoryOnHandPricing_values){
		$tblInventoryOnHandPricing_data = array_combine(self::TBL_INVENTORY_ON_HAND_PRICING_FIELDS, $tblInventoryOnHandPricing_values);
		$NewOnHandPricingID = $this->InsertData(TBL_INVENTORY_ON_HAND_PRICING, $tblInventoryOnHandPricing_data);
		return $NewOnHandPricingID;
	}

	//Update On Hand Item
	public function UpdateOnHandItem($values, $where){
		$update = $this->update(TBL_INVENTORY_ON_HAND, self::TBL_INVENTORY_ON_HAND_FIELDS, $values, $where);
		return $update;
	}

	//Update On Hand Item Quantity
	public function UpdateOnHandItemQuantity($values, $where){
		$update = $this->update(TBL_INVENTORY_ON_HAND, array('IOHQuantity'), $values, $where);
		return $update;
	}

	//Get OnHand Quantity By Inventory ID
	public function GetOnHandQtyByInvID($InventoryID){
		$data = $this->MyQuery( 'SELECT IOHQuantity AS Qty FROM '.TBL_INVENTORY_ON_HAND.' WHERE IOHInventoryID = '.$InventoryID, true );
		if($data){
			return $data->Qty;
		}
	}

	//Update On Hand Price Data
	public function UpdateOnHandItemPriceData($values, $where){
		$update = $this->update(TBL_INVENTORY_ON_HAND_PRICING, self::TBL_INVENTORY_ON_HAND_PRICING_FIELDS, $values, $where);
		return $update;
	}

	//Get Most times ordered item
	public function GetMostOrderedItem(){
		$data = $this->MyQuery(' SELECT TOP 1 IODINVID AS ItemID, COUNT(IODINVID) AS ItemCount FROM '.TBL_INVENTORY_ORDER_DETAILS.' GROUP BY IODINVID ORDER BY ItemCount DESC ', true);
		if($data){
			return $data;
		}
	}

	//Get Most selling item - by quantity
	public function GetMostSellingItem(){
		$data = $this->MyQuery(' SELECT TOP 1 SDIOHID AS ItemID, SUM(SDQuantity) AS ItemCount FROM '.TBL_SALES_DETAILS.' GROUP BY SDIOHID ORDER BY ItemCount DESC ', true);
		if($data){
			return $data;
		}
	}

	//Get sild items quantity by their ID 
	public function GetItemSoldQty($InventoryID){
		$data = $this->MyQuery(' SELECT SUM(SDQuantity) AS ItemCount FROM '.TBL_SALES_DETAILS.' WHERE SDIOHID = '.$InventoryID.' GROUP BY SDIOHID ORDER BY ItemCount DESC ', true);
		if($data){
			return $data->ItemCount;
		}
	}

	public function DoActive($ItemID){
		$update = $this->update(TBL_INVENTORY, array('IsActive'), array(1), array('INVID',$ItemID));
		return $update;
	}

	public function DoInActive($ItemID){
		$update = $this->update(TBL_INVENTORY, array('IsActive'), array(0), array('INVID',$ItemID));
		return $update;
	}

	public function GetSearchResults($SearchString){
		$stmt = $this->pdo->prepare(' SELECT * FROM '.TBL_INVENTORY.' WHERE INVItemName LIKE ? OR INVItemNumber LIKE ? OR INVDescription LIKE ? ');
		$stmt->execute(['%'.$SearchString.'%', '%'.$SearchString.'%', '%'.$SearchString.'%']);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $result;
	}

	public function GetLowStockItems(){
		$data = $this->MyQuery('SELECT IOHInventoryID, IOHQuantity FROM '.TBL_INVENTORY_ON_HAND.' WHERE IOHQuantity <= 1');
		if($data){
			return $data;
		}
	}

	public function GetAllUniqueVendors($ItemID){
		$data = $this->MyQuery('SELECT DISTINCT(IODVendorID) FROM '.TBL_INVENTORY_ORDER_DETAILS.' WHERE IODINVID = '.$ItemID);
		if($data){
			return $data;
		}
	}

	public function GetLastPurchasingDetails($Item){
		$data = $this->MyQuery('SELECT TOP 1 * FROM '.TBL_INVENTORY_ORDER_DETAILS.' WHERE IODINVID = '.$Item.' ORDER BY IODID DESC');
		if($data){
			return $data[0];
		}else{
			return false;
		}
	}

	public function GetInventoryMaxQty($ItemID){
		$data = $this->MyQuery('SELECT INVMaxQty AS MaxQty FROM '.TBL_INVENTORY.' WHERE INVID = '.$ItemID, true);
		if($data){
			return $data->MaxQty;
		}
	}

	//Add new non-inventory item
	public function AddNewNonInventoryItem($tblNonInventoryItems_values){
		$tblNonInventoryItems_data = array_combine(self::TBL_NONINVENTORY_ITEMS_FIELDS, $tblNonInventoryItems_values);
		$NewNonInventoryItemID = $this->InsertData(TBL_NONINVENTORY_ITEMS, $tblNonInventoryItems_data);
		return $NewNonInventoryItemID;
	}
	//Add non inventory price data
	public function AddNonInventoryItemPriceData($tblNonInventoryPricing_values){
		$tblNonInventoryPricing_data = array_combine(self::TBL_NON_INVENTORY_PRICING_FIELDS, $tblNonInventoryPricing_values);
		$NonInventoryItemPricingID = $this->InsertData(TBL_NON_INVENTORY_PRICING, $tblNonInventoryPricing_data);
		return $NonInventoryItemPricingID;
	}

	public function GetAllNonInventoryItems(){
		$data = $this->MyQuery('SELECT * FROM '.TBL_NONINVENTORY_ITEMS.' AS TNI INNER JOIN '.TBL_NON_INVENTORY_PRICING.' AS TNIP ON TNI.NonInvID = TNIP.NIPNonInvID');
		if($data){
			return $data;
		}
	}

	public function GetTheNonInventoryItem($non_inv_id){
		$data = $this->MyQuery('SELECT * FROM '.TBL_NONINVENTORY_ITEMS.' AS TNI INNER JOIN '.TBL_NON_INVENTORY_PRICING.' AS TNIP ON TNI.NonInvID = TNIP.NIPNonInvID WHERE TNI.NonInvID = '.$non_inv_id, true);
		if($data){
			return $data;
		}
	}

	public function UpdateNonInventoryItem($values, $where){
		$update = $this->update(TBL_NONINVENTORY_ITEMS, self::TBL_NONINVENTORY_ITEMS_FIELDS, $values, $where);
		return $update;
	}

	public function UpdateNonInventoryItemPriceData($values, $where){
		$update = $this->update(TBL_NON_INVENTORY_PRICING, array('NIPPriceTypeID', 'NIPPrice'), $values, $where);
		return $update;
	}

	public function GetNonInvPrice($id){
		$data = $this->MyQuery('SELECT NIPPrice AS price FROM '.TBL_NON_INVENTORY_PRICING.' WHERE NIPNonInvID = '.$id, true);
		if($data){
			return $data->price;
		}
	}

	public function GetInvItemIDByOnHandID($OnhandID){
		$data = $this->MyQuery('SELECT IOHInventoryID AS InvID FROM '.TBL_INVENTORY_ON_HAND.' WHERE IOHID = '.$OnhandID, true);
		if($data){
			return $data->InvID;
		}
	}

	public function WareHouseHasItem($item, $wh){
		$data = $this->GetAll(TBL_INVENTORY_ON_HAND, array('IOHInventoryID' => $item, 'IOHWarehouseID' => $wh));
		if($data){
			return true;
		}
	}

}