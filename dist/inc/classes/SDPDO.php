<?php

//Claas SDPDO.php

class SDPDO {

    const SD_DRIVER = DRIVER;
    const SD_DB_SERVER = SERVER_NAME;
    const SD_DB_NAME = DB_NAME;
    const SD_DB_USER = USER;
    const SD_DB_PASSWORD = PASS;

    public function __construct() {
        $conStr = sprintf("%s:server=%s;Database=%s", self::SD_DRIVER, self::SD_DB_SERVER, self::SD_DB_NAME);
        try {
            $this->pdo = new PDO($conStr, self::SD_DB_USER, self::SD_DB_PASSWORD);
        }  catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    //Custom Query
    public function MyQuery($sql, $bool=false){
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        if($bool==true){
            return $stmt->fetch(PDO::FETCH_OBJ);
        }else{
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
    }

    //Get All Data
    public function GetAll($table, $where=null){
        $sql =  'SELECT * FROM "'.$table.'" ';
        if($where){
            $whereStr = '';
            foreach ($where as $key => $value) {
                $whereStr .= $key.'='. "'$value'" .' AND ';
            }
            $sql = ' SELECT * FROM '.$table.' WHERE '.rtrim($whereStr, ' AND') ;
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    //Insert Data
    public function InsertData($table, $data){
        $keys = array_keys($data);
        $values = array_values($data);
        $params = rtrim(str_repeat("?, ", count($values)), ', ');
        try {
            $stmt = $this->pdo->prepare("INSERT INTO $table (".implode(",", $keys).") VALUES ($params)");
            $stmt->execute($values);
            return $this->pdo->lastInsertId();
        } catch(PDOException $e) {
            echo "Oops... {$e->getMessage()}";
        }
    }

    //Update
    public function update($table, $columns, $data, $where) {

        $update = '';
        foreach ( $columns as $column ) {
            $update .= " " . $column . " = :" . $column . ", ";
        }
        $update = trim($update, ", ");
        $where_prepare = " " . $where[0] . " = :" . $where[0];
        $prepare = "UPDATE {$table} SET {$update} WHERE {$where_prepare}";

        $counter = 0;
        $execute_data = array();
        foreach ( $columns as $column ) {
            $execute_data[":{$column}"] = $data[$counter];
            $counter++;
        }
        $where_0 = $where[0];
        $execute_data[":{$where_0}"] = $where[1];

        $stmt = $this->pdo->prepare($prepare);
        $stmt->execute($execute_data);
        $affected_rows = $stmt->rowCount();
        if ( $affected_rows < 1 ) {
            return false;
        } else {
            return true;
        }
    }

    //Check Field Existance
    public function CheckExistance($table, $field, $value){
        $data = $this->GetAll($table, array($field => $value));
        if($data){
            if($data[0]->$field){
                return true;
            }
        }else{
            return false;
        }
    }

    //Check Field Existance Except Current One
    public function CheckExistanceExcept($table, $except_key, $except_val, $match_key, $match_val){
        $data = $this->MyQuery(' SELECT * FROM '.$table.' WHERE '.$except_key.' != '.$except_val );
        if($data){
            foreach ($data as $obj) {
                if($obj->$match_key){
                    if($obj->$match_key == $match_val){
                        return true;
                    }
                }
            }
        }
    }

    public function GetPageContent($Page, $data=null){
        $context = null;
        if($data){
            $query = http_build_query($data);
            $options = array('http' => array(
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                            "Content-Length: ".strlen($query)."\r\n".
                            "User-Agent:MyAgent/1.0\r\n",
                'method'  => 'POST',
                'content' => $query
            ));
            $context  = stream_context_create($options);
        }
        return file_get_contents(SITE_URL.'/pages/'.$Page.'.php', false, $context);
    }


    public function __destruct() {
        $this->pdo = null;
    }

}
