var $ = jQuery.noConflict();

//Page Lader
$(window).on('load', function() {
    $(".page-loader").fadeOut(2000);
});

function site_url(){
	var protocol =  window.location.protocol;
	var hostname =  window.location.hostname;
	var pathname =  window.location.pathname;
	var url = protocol+'//'+hostname+pathname;
	return url;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;

    return true;
}

var params = {};
if (location.search) {
    var parts = location.search.substring(1).split('&');

    for (var i = 0; i < parts.length; i++) {
        var nv = parts[i].split('=');
        if (!nv[0]) continue;
        params[nv[0]] = nv[1] || true;
    }
}
$(".components li a").click(function(e){
	var $url = $(this).attr('data-url');
	if(params.destination == 'sales' && !params.action){
		var itemsArr = [];
		$("#customer-selling-form .item").each(function(){
			if($(this).val() != ''){
				itemsArr.push($(this).val());
			}
		});
		if(itemsArr.length > 0){
			var leave = confirm("Are you sure you want to leave the page? You have unsaved data in the page.");
				if(leave === false){
					e.preventDefault();
				}else{
					if($url != 'undefined' && $url != ''){
						window.location = $url;
					}
				}
		}
	}else{
		if($url){
			window.location.href = $url;
		}
	}
	
});

//Print all wharever exist into thr Div
function printDiv(div){
  var divToPrint=document.getElementById(div);
  var newWin=window.open('','Print-Window');
  newWin.document.open();
  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
  newWin.document.close();
  setTimeout(function(){newWin.close();},10);
}

//Print Invoice
$("#print-invoice").on('click', function(){
	printDiv('print-sd');
});

//Get each from '$from' and store into '$to'
function SetEachOne($from, $to){
	$($from).each(function(){
		var $thisVal = $(this).val();
		$thisVal = $thisVal == '' ? 'null' : $thisVal;
		$to.push($thisVal);
	});
}

//Get each from '$from' and store into '$to' for amount
function SetEachAmt($from, $to){
	$($from).each(function(){
		var $thisVal = $(this).val();
		$thisVal = $thisVal == '' ? 0 : $thisVal;
		$to.push($thisVal);
	});
}

//Check input page number
function ValidInputPageNum(e){
	var maxVal = e.getAttribute('max');
	var minVal = e.getAttribute('min');
	if(parseInt(e.value) > parseInt(maxVal)){
		e.value = maxVal;
	}
	if(parseInt(e.value) < parseInt(minVal)){
		e.value = minVal;
	}
}

//Close Alert Message
function CloseAlert(){
	$(".close-alert").each(function(){
		$(this).on('click', function(){
			$(this).closest('.alert').hide();
		});
	});
}
CloseAlert();

//Dropdown select with live search
function SdSelection(){
	$('.sd-select').each(function(){
		$(this).selectpicker({
			showTick: true,
		});
	});
}
SdSelection();

//Reset login form
function clearForm(){
    $('#login-form input[type=text], #login-form input[type=password]').css({'border' : '1px solid #f48b1b'});
    $('#login-form input[type=text], #login-form input[type=password]').val('');
    $('#login-form #remember').prop('checked', false);
}

/*=======make first letter of a word in uppercase=======*/
function sdUcWords(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   return splitStr.join(' '); 
}

//Username format
function sdMakeUsername(str) {
   var splitStr = str.replace(/\s+/g, '_').toLowerCase();
   return splitStr;
}

//Ledger Id format
function sdMakeLedgerId(str) {
   var splitStr = str.replace(/\s+/g, '-').toUpperCase();
   return splitStr;
}

//Number Validation
function NumberValidation(){
	var elem = document.getElementsByClassName("number-validation");
	for (i = 0; i < elem.length; i++) {
	    elem[i].addEventListener('keypress', function(event){
	        var keys = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0];
	        var validIndex = keys.indexOf(event.charCode);
	        if(validIndex == -1){
	            event.preventDefault();
	        }
	    });
	}
}
NumberValidation();

//Quantity Validation
function QuantityValidation(){
	var elem = document.getElementsByClassName("qty");
	for (i = 0; i < elem.length; i++) {
	    elem[i].addEventListener('keypress', function(event){
	        var keys = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0];
	        var validIndex = keys.indexOf(event.charCode);
	        if(validIndex == -1){
	            event.preventDefault();
	        }
	    });
	}
}
QuantityValidation();

//Price Validation
function PriceValidation(){
	var elem = document.getElementsByClassName("price-validation");
	for (i = 0; i < elem.length; i++) {
	    elem[i].addEventListener('keypress', function(event){
	        var keys = [46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0];
	        var validIndex = keys.indexOf(event.charCode);
	        if(validIndex == -1){
	            event.preventDefault();
	        }
	    });
	}
}
PriceValidation();

function GoToItemDetailsPage(e){
	var ItemLink = $(e).data('link');
	window.location = ItemLink;
}

//Tooltip
function SetTooltip(){
	$(".sd-tootip").each(function(){
    	$(this).tooltip();
    });
}
SetTooltip();
/*==============================================================*/
			/*=====Bootstrap Tables START====*/
/*==============================================================*/
$(document).ready(function() {
	$(".sd-table").each(function(){
		$(this).DataTable();
	});
});

function SdTable(){
	$(".sd-table").each(function(){
		$(this).DataTable();
	});
}

function ReceiveOrderItemTable(){
	$("#receive-order-item-table").DataTable({
						language : {
					        sLengthMenu: "Show _MENU_ Items",
					    }
					});
}

$("#on-hand-table, #non-inv-items-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ items",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: 'Search item'
		    }
		});
$("#vendor-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ vendors",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: 'Search vendors'
		    }
		});
$("#employee-table, #employees-history-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ employees",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: 'Search employee'
		    }
		});
$("#customer-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ customers",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: 'Search customers'
		    }
		});
$("#orders-table, #pending-orders-table, #received-orders-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ orders",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: '--Search--'
		    }
		});
$("#warehouse-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ warehouse",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: '--Search--'
		    }
		});
$("#shelf-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ shelf",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: '--Search--'
		    }
		});
$("#invoice-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ invoice",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: 'Search invoices'
		    },
		    aaSorting: [[5, 'desc']]
		});
$("#sales-quote-table").DataTable({
			language : {
		        sLengthMenu: "Show _MENU_ quotes",
		        info: "Showing page _PAGE_ of _PAGES_",
		        searchPlaceholder: 'Search Quotes'
		    },
		    aaSorting: [[5, 'desc']]
		});

$("#sp-table").DataTable({
	language : {
        sLengthMenu: "Show _MENU_ payments",
    }
});

/*==============================================================*/
			/*=====Bootstrap Tables END====*/
/*==============================================================*/

/*==============================================================*/
				/*===Employee Login START===*/
/*==============================================================*/
$("#login-form #login").on('click', function(e){
	e.preventDefault();
	var username 	= $("#login-form input[name=username]").val();
	var password 	= $("#login-form input[name=password]").val();
	var rememberOBJ = $("#login-form input[name=remember]");
	var remember 	= 0;
	if(rememberOBJ.prop('checked', true)){
		remember 	= 1
	}
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: { 'action' : 'login', 'username' : username, 'password' : password, 'remember' : remember },
        beforeSend: function() {

        },
        success: function(data){
        	$("#login-alert").show();
        	if(data.error){
        		$("#login-alert").removeClass('alert-success');
        		$("#login-alert").addClass('alert-danger');
        		$("#login-alert #login-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#login-alert").removeClass('alert-danger');
        		$("#login-alert").addClass('alert-success');
        		$("#login-alert #login-alert-message").text(data.message);
        		$("#login-form")[0].reset();
        		location.reload();
        	}
        },
	});
});
/*==============================================================*/
				/*===Employee Login END===*/
/*==============================================================*/


/*==============================================================*/
				/*===Sort Sections START===*/
/*==============================================================*/
$("#section-items-table tbody").sortable({
	placeholder : "ui-state-highlight",
	axis : 'y',
	cursor: "move",
	update  : function(event, ui){
		var section_id_array = new Array();
	   	$("#section-items-table tbody tr").each(function(){
	    	section_id_array.push($(this).attr("id"));
	   	});
	   	var SectionData = new FormData();
	   	SectionData.append('section_id_array', section_id_array);
	   	SectionData.append('action', 'update_section_order');
	   	$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'contentType'	: false,
	        'processData'	: false,
	        'data'			: SectionData,
	        beforeSend: function() {

	        },
	        success: function(data){
	        	if(data.msg){
	        		$.alert({
				        content: data.msg,
				    });
	        	}
	        },
	    });
	}
});
/*==============================================================*/
				/*===Sort Sections END===*/
/*==============================================================*/

//List item hover show/hide - edit icon
function listItemHoverEffect(){
	$(".scroll-list-item").hover(function(){
		$(this).children('i').show();
	}, function(){
		$(this).children('i').hide();
	});
}
listItemHoverEffect();


function RowOverEffect(e){
	$(e).parent('tr').css({'background-color' : '#749beb'});
	$(e).css({'cursor' : 'pointer'});
}
function RowLeaveEffect(e){
	$(e).parent('tr').css({'background-color' : '#fff'});
}

/*====================================*/
		/*List Types START*/
/*====================================*/
//Add List type
function addListsTypes(e){
	var typeName = $(e).closest('form').find('#list-type').val();
	var ListsTypesData = new FormData();
	ListsTypesData.append('typeName', typeName);
	if($(e).closest('form').find('textarea').length > 0){
		var typeDesc = $(e).closest('form').find('textarea').val();
		ListsTypesData.append('typeDesc', typeDesc);
	}
	var action = $(e).parents('.modal').find('.modal-title').text();
	action = action.replace(/ /g, '_');
	action = action.toLowerCase();
	ListsTypesData.append('action', action);
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: ListsTypesData,
        beforeSend: function() {
        },
        success: function(data){
        	if(data.blank && data.blank === true){
        		$.alert({
        			title : 'Alert!',
        			content : 'You can\'t empty the field'
        		});
        		$(e).closest('form').find('#list-type').focus();
        	}else{
        		switch(data.action){
	        		case 'add_address_type':
	        		case 'add_contact_type':
	        		case 'add_role_type':
	        		case 'add_customer_type':
	        		case 'add_pricing_type':
	    				switch(data.resp){
	        				case true:
		        				$("#add-list-type").modal('hide');
		        				$.alert({
				        			title : 'Alert!',
				        			content : data.successMsg
				        		});
		        				//alert(data.successMsg);
		        				$('#'+data.table_id+' tbody').append(data.row);
		        				break;
		        			case false:
		        				$.alert({
				        			title : 'Alert!',
				        			content : data.errorMsg
				        		});
		        				//alert(data.errorMsg);
	        					$("#add-list-type form #list-type").focus();
	        					break;
	        			}
	        			break;
	        		case 'add_payment_type':
	        		case 'add_ledger_id':
	        			switch(data.resp){
	        				case true:
		        				$("#add-list-type-with-description").modal('hide');
		        				$.alert({
				        			title : 'Alert!',
				        			content : data.successMsg
				        		});
		        				//alert(data.successMsg);
		        				$('#'+data.table_id+' tbody').append(data.row);
		        				break;
		        			case false:
		        				$.alert({
				        			title : 'Alert!',
				        			content : data.errorMsg
				        		});
		        				//alert(data.errorMsg);
	        					$("#add-list-type-with-description form #list-type").focus();
	        					break;
	        			}
	        			break;
	        	}
        	}
        },
    });
}

//Edit Lists Types
function editListsTypes(e){
	$("#edit-list-type #textarea-field").html('');
	var id = $(e).data('id');
	var action = $(e).data('action');
	var ListsTypesData = new FormData();
	ListsTypesData.append('action', action);
	ListsTypesData.append('id', id);
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: ListsTypesData,
        beforeSend: function() {
        },
        success: function(data){
        	switch(data.request_action){
        		case 'update_address_type':
        		case 'update_contact_type':
        		case 'update_role_type':
        		case 'update_customer_type':
        		case 'update_pricing_type':
        			$("#edit-list-type .modal-header .modal-title").text(data.modal_title);
        			$("#edit-list-type form label:eq(0)").html(data.label_1+'<sup>*</sup>');
        			$("#edit-list-type form #list-type").val(data.list_type);
        			$("#edit-list-type form #hidden-id").val(data.id);
        			$("#edit-list-type form #hidden-action").val(data.request_action);
        			$("#edit-list-type").modal('show');
        			break;
        		case 'update_payment_type':
        		case 'update_ledger_id':
        			$("#edit-list-type-with-description .modal-header .modal-title").text(data.modal_title);
        			$("#edit-list-type-with-description form label:eq(0)").html(data.label_1+'<sup>*</sup>');
        			$("#edit-list-type-with-description form #list-type").val(data.list_type);
        			$("#edit-list-type-with-description form #list-type-desc").val(data.list_type_desc);
        			$("#edit-list-type-with-description form #hidden-id").val(data.id);
        			$("#edit-list-type-with-description form #hidden-action").val(data.request_action);
        			$("#edit-list-type-with-description").modal('show');
        			break;
        		case 'update_section':
        			$("#edit-list-type .modal-header .modal-title").text(data.modal_title);
        			$("#edit-list-type form label:eq(0)").html(data.label_1+'<sup>*</sup>');
        			$("#edit-list-type form #list-type").val(data.section);
        			$("#edit-list-type form #hidden-id").val(data.id);
        			$("#edit-list-type form #hidden-action").val(data.request_action);
        			$("#edit-list-type").modal('show');
        			break;
        	}
        },
    });
}

//Update Lists Types
function updateListsTypes(e){
	var id = $(e).closest('form').find('#hidden-id').val();
	var typeName = $(e).closest('form').find('#list-type').val();
	var action = $(e).closest('form').find('#hidden-action').val();
	var ListsTypesData = new FormData();
	ListsTypesData.append('action', action);
	ListsTypesData.append('id', id);
	ListsTypesData.append('typeName', typeName);
	if($(e).closest('form').find('textarea').length > 0){
		var typeDesc = $(e).closest('form').find('textarea').val();
		ListsTypesData.append('typeDesc', typeDesc);
	}
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: ListsTypesData,
        beforeSend: function() {
        },
        success: function(data){
        	if(data.blank === true){
        		$.alert({
        			title : 'Alert!',
        			content : 'You can\'t empty the field'
        		});
        		$(e).closest('form').find('#list-type').focus();
        	}else{
        		switch(data.resp){
	        		case true:
	        			$('#edit-list-type, #edit-list-type-with-description').modal('hide');
	        			$('#'+data.table_id+' tr#'+data.id+' td:eq(0)').html(data.typeName);
	        			if(data.typeDesc){
	        				$('#'+data.table_id+' tr#'+data.id+' td:eq(1)').html(data.typeDesc);
	        			}
	        			$.alert({
		        			title : 'Alert!',
		        			content : data.successMsg
		        		});
	        			break;
	        		case false:
	        			$.alert({
		        			title : 'Alert!',
		        			content : data.errorMsg
		        		});
	        			$(e).closest('form').find('#list-type').focus();
	        			break;
	        	}
        	}
        },
    });
}
/*====================================*/
		/*List Types END*/
/*====================================*/

/*==========Create Menu==========*/
$("#create-section-btn").on('click', function(e){
	e.preventDefault();
	var sectionName = $("#create-section-form #section").val();
	var sectionData = new FormData();
	sectionData.append('sectionName', sectionName);
	sectionData.append('action', 'create_section');
	if(sectionName != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'contentType'	: false,
	        'processData'	: false,
	        'data'			: sectionData,
	        beforeSend: function() {

	        },
	        success: function(data){
	        	switch(data.resp){
	        		case true:
	        			alert(data.successMsg);
	        			$("#create-section-form")[0].reset();
	        			$("#section-items-table tbody").append(data.row);
	        			break;

	        		case false:
	        			alert(data.errorMsg);
	        			$("#create-section-form #section").focus();
	        			break;

	        		default:
	        			alert('Internal error....');
	        			break;
	        	}
	        },
		});
	}else{
		alert('Please enter a section name');
		$("#create-section-form #section").focus();
	}
});

/*======================================================*/
			/*===Add New Employee START===*/
/*======================================================*/
$("#add-employee-btn").on('click', function(e){
	e.preventDefault();
	var NewEmployeeData = $("#new-employee-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: NewEmployeeData+'&'+$.param({ 'action': 'new_employee' }),
        beforeSend: function() {
        },
        success: function(data){
        	$("#new-employee-alert").show();
        	if(data.error){
        		$("#new-employee-alert").removeClass('alert-success');
        		$("#new-employee-alert").addClass('alert-danger');
        		$("#new-employee-alert #new-employee-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#new-employee-alert").removeClass('alert-danger');
        		$("#new-employee-alert").addClass('alert-success');
        		$("#new-employee-alert #new-employee-alert-message").text(data.message);
        		$("#new-employee-form")[0].reset();
        	}
        },
	});
});

/*======================================================*/
			/*===Add New Employee END===*/
/*======================================================*/

/*==========Edit Employee modal open==========*/
function editEmployee(obj){
	var eId = $(obj).attr('data-id');
	var eData  = new FormData();
	eData.append('eId', eId);
	eData.append('action', 'edit_employee');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: eData,
        beforeSend: function() {
			
        },
        success: function(data){

        	if(data.content){
        		$("#edit-employee-body").html(data.content);
        		$("#edit-employee").modal('show');
        	}
        },
	});
}

/*==========Upadate Employee==========*/

function UpdateEmployee(){
	var EmployeeData = $("#edit-employee-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: EmployeeData+'&'+$.param({'action' : 'update_employee'}),
        beforeSend: function() {
        	$("#update-employee-btn").text('Updating...');
        },
        success: function(data){
    		$("#edit-employee-alert").show();
        	if(data.error){
        		$("#edit-employee-alert").removeClass('alert-success');
        		$("#edit-employee-alert").addClass('alert-danger');
        		$("#edit-employee-alert #edit-employee-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#edit-employee-alert").removeClass('alert-danger');
        		$("#edit-employee-alert").addClass('alert-success');
        		$("#edit-employee-alert #edit-employee-alert-message").text(data.message);
        		$("#edit-employee-form")[0].reset();
        		setTimeout(function(){
        			$("#edit-employee").hide('show');
        		},2000);
        		window.location.href = data.redirect_url;
        	}
        	$("#update-employee-btn").text('Update');
        	CloseAlert();
        },
	});
}

/*==============Reset Password==============*/
function ResetPassword(){
	var ResetData = $("#reset-password-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: ResetData+'&'+$.param({ 'action' : 'reset_password' }),
        beforeSend: function() {
        },
        success: function(data){
        	$("#reset-employee-password-alert").show();
        	if(data.error){
        		$("#reset-employee-password-alert").removeClass('alert-success');
        		$("#reset-employee-password-alert").addClass('alert-danger');
        		$("#reset-employee-password-alert #reset-employee-password-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#reset-employee-password-alert").removeClass('alert-danger');
        		$("#reset-employee-password-alert").addClass('alert-success');
        		$("#reset-employee-password-alert #reset-employee-password-alert-message").text(data.message);
        		$("#reset-password-form")[0].reset();
        		setTimeout(function(){
        			$("#reset-employee-password-alert").hide('show');
        		},2000);
        	}
        	CloseAlert();
        },
	});
}

/*==========Delete Employee==========*/
function delEmployee(obj){
	var eId = $(obj).attr('data-id');
	var eData  = new FormData();
	eData.append('eId', eId);
	eData.append('action', 'delete_employee');
	var check = confirm("Want to remove this employee?");
	if(check === true){
		var LeaveNote = prompt('Note for the leave');
		eData.append('LeaveNote', LeaveNote);
		if($.trim(LeaveNote) != ''){
			$.ajax({
				'url'			: 'requests/actions.php',
		        'type'			: 'post',
		        'dataType'		: 'JSON',
		        'cache'			: false,
		        'contentType'	: false,
		        'processData'	: false,
		        'data'			: eData,
		        beforeSend: function() {

		        },
		        success: function(data){
		        	if(data.message){
		        		$('#employee-table tbody #row-'+data.eid).fadeOut('slow');
		        		$('#employee-table').DataTable();
		        		$.alert({
		        			title : 'Alert!',
		        			content : data.message
		        		});
		        	}
		        },
			});
		}
	}
}


/*==========Add inventory item==========*/
$("#add-inventory-btn").on('click', function(e){
	e.preventDefault();
	var invData = $("#add-inv-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: invData+'&'+$.param({ 'action' : 'add_inventory_item' }),
        beforeSend: function() {
        },
        success: function(data){
        	$("#add-inv-alert").show();
        	if(data.error){
        		$("#add-inv-alert").removeClass('alert-success');
        		$("#add-inv-alert").addClass('alert-danger');
        		$("#add-inv-alert #add-inv-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#add-inv-alert").removeClass('alert-danger');
        		$("#add-inv-alert").addClass('alert-success');
        		$("#add-inv-alert #add-inv-alert-message").text(data.message);
        		$("#add-inv-form")[0].reset();
        	}
        },
	});
});

/*==========Edit Inventory Modal Open==========*/
function editInventory(obj){
	var invID = $(obj).attr('data-id');
	var invData  = new FormData();
	invData.append('invID', invID);
	invData.append('action', 'edit_inventory_item');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: invData,
        beforeSend: function() {
        },
        success: function(data){
        	$("#edit-inventory-form input[name=itemname]").val(data.itemName);
        	$("#edit-inventory-form input[name=itemnumber]").val(data.itemNumber);
        	$("#edit-inventory-form input[name=itembarcode]").val(data.itemBarcode);
        	$("#edit-inventory-form input[name=itemserialnumber]").val(data.itemSerialNumber);
        	$("#edit-inventory-form input[name=itemdescription]").val(data.itemDescription);
			$("#edit-inventory-form select[name=itemwarrenty] option").removeAttr('selected');
        	$("#edit-inventory-form select[name=itemwarrenty] option#opt_"+data.itemWarrenty).attr('selected', true);
        	$("#edit-inventory-form #itemisactive").removeAttr('checked');
        	if(data.itemisactive == 1){
        		$("#edit-inventory-form #itemisactive").trigger('click');
        	}
        	$("#edit-inventory-form #update-inventory-btn").attr('data-invid', data.itemID);
			$("#edit-inventory-form input[name=maxqty]").val(data.maxqty);
        	$("#edit-inventory-form input[name=minqty]").val(data.minqty);
        	$("#edit-inventory-form input[name=invid]").val(data.itemID);
        	$("#edit-inventory-form #update-inventory-btn").text('Update');
        	$("#edit-inventory").modal('show');

        },
	});
}

/*==========Update Inventory==========*/
$("#update-inventory-btn").on('click', function(e){
	e.preventDefault();
	var invData = $("#edit-inventory-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: invData+'&'+$.param({ 'action' : 'update_inventory_item' }),
        beforeSend: function() {
        	$('#update-inventory-btn').text('Updating...');
        },
        success: function(data){
        	if(data.error){
        		$("#modal-inv-alert").show();
        		$("#modal-inv-alert").addClass('alert-danger');
        		$("#modal-inv-alert #modal-inv-alert-message").text(data.error);
        		$("#edit-inventory-form #update-inventory-btn").text('Update');
        	}
    		if(data.resp === true){
        		$("#edit-inventory button.close").click();
        		$("#inventory-table tbody tr#row-"+data.invid).html(data.row);
        		SetTooltip();
        		$("#inv-alert").show();
        		$("#inv-alert").removeClass('alert-danger');
	    		$("#inv-alert").addClass('alert-success');
	    		$("#inv-alert #inv-alert-message").html(data.message);
	    		if(data.rf =='item-details'){
	    			location.reload();
	    		}
        	}
        },
	});
});

/*==========Inventory Item IsActive==========*/
function invIsActive(e){
	var status = $(e).data('status');
	var tr = $(e).closest('tr');
	var item_id = tr.attr('id').replace('row-','');
	var IsActiveData = new FormData();
	IsActiveData.append('item_id', item_id);
	IsActiveData.append('status', status);
	IsActiveData.append('action', 'inventory_item_is_active');
	switch(status){
		case 'active' :
			need = confirm('Want to Inactive this item?');
			break;
		case 'inactive' :
			need = confirm('Want to Active this item?');
			break;
	}
	if(need === true){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'contentType'	: false,
	        'processData'	: false,
	        'data'			: IsActiveData,
	        beforeSend: function() {
	        },
	        success: function(data){
	        	$("#inv-alert").show();
	    		switch(data.resp){
	    			case true:
	    				$('#inventory-table tr#row-'+data.item_id+' td.isactive').html(data.tr);
	    				SetTooltip();
	    				$("#inv-alert").removeClass('alert-danger');
	    				$("#inv-alert").addClass('alert-success');
	    				$("#inv-alert #inv-alert-message").html(data.message);
	    				break;
	    			case false:
	    				a$("#inv-alert").removeClass('alert-success');
	    				$("#inv-alert").addClass('alert-danger');
	    				$("#inv-alert #inv-alert-message").html(data.errorMsg);
	    				break;
	    		}
	        },
		});
	}
}

//Edit On Hand Item
function EditOnHandItem($this){
	var OnHandID = $($this).attr('data-id');
	//alert(OnHandID);
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: { 'action' : 'edit_on_hand_item', 'OnHandID' : OnHandID },
        beforeSend : function() {
        },
        success : 	function(){
        	$("#edit-on-hand-item").modal('show');
        }
	});
}


/*==========Add Vendor==========*/
$("#add-vendor-btn").click(function(e){
	e.preventDefault();
	var vcompany = $("#add-vendor-form #vcompany").val();
	var vaccno = $("#add-vendor-form #vaccno").val();
	var vfname = $("#add-vendor-form #vfname").val();
	var vlname = $("#add-vendor-form #vlname").val();
	var vaddrtype = $("#add-vendor-form #vaddrtype option:selected").attr('data-id');
	var vaddr1 = $("#add-vendor-form #vaddr1").val();
	var vaddr2 = $("#add-vendor-form #vaddr2").val();
	var vcity = $("#add-vendor-form #vcity").val();
	var vstate = $("#add-vendor-form #vstate option:selected").val();
	var vzip = $("#add-vendor-form #vzip").val();
	var vcontacttype = $("#add-vendor-form #vcontacttype option:selected").attr('data-id');
	var vcdata = $("#add-vendor-form #vcdata").val();

	var vpcontactcheck = 0;
	var visactivecheck = 0

	if($("#add-vendor-form #vpcontactcheck").prop('checked') == true){
		vpcontactcheck = 1;
	}
	if($("#add-vendor-form #visactivecheck").prop('checked') == true){
		visactivecheck = 1;
	}

	var empID = $("#add-vendor-form #current-emp-id").val();

	var newVendor  = new FormData();
	newVendor.append('vcompany', vcompany);
	newVendor.append('vaccno', vaccno);
	newVendor.append('vfname', vfname);
	newVendor.append('vlname', vlname);
	newVendor.append('vaddrtype', vaddrtype);
	newVendor.append('vaddr1', vaddr1);
	newVendor.append('vaddr2', vaddr2);
	newVendor.append('vcity', vcity);
	newVendor.append('vstate', vstate);
	newVendor.append('vzip', vzip);
	newVendor.append('vcontacttype', vcontacttype);
	newVendor.append('vcdata', vcdata);
	newVendor.append('vpcontactcheck', vpcontactcheck);
	newVendor.append('visactivecheck', visactivecheck);
	newVendor.append('empID', empID);
	newVendor.append('action', 'add_vendor');

	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: newVendor,
        beforeSend: function() {

        },
        success: function(data){
        	$("#add-vendor-alert").show();
        	if(data.message){
        		$("#add-vendor-alert").removeClass('alert-danger');
        		$("#add-vendor-alert").addClass('alert-success');
        		$("#add-vendor-alert #add-vendor-alert-message").text(data.message);
        		$("#add-vendor-form")[0].reset();
        		window.location = data.returnUrl;
        	}
        	if(data.error){
        		$("#add-vendor-alert").removeClass('alert-success');
        		$("#add-vendor-alert").addClass('alert-danger');
        		$("#add-vendor-alert #add-vendor-alert-message").text(data.error);
        	}
        },
	});
});


/*==========Edit vendor modal open==========*/
function editVendor(obj){
	var venID = $(obj).attr('data-id');
	var venData  = new FormData();
	venData.append('venID', venID);
	venData.append('action', 'edit_vendor');
	$("#hidden-vendor-id").val(venID);
	$("#edit-vendor").modal('show');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: venData,
        beforeSend: function() {
			$("#edit-vendor #edit-vendor-form")[0].reset();
        },
        success: function(data){
        	//alert(data.venID);
        	$("#edit-vendor #edit-vendor-form #vcompany").val(data.vcompany);
        	$("#edit-vendor #edit-vendor-form #vaccno").val(data.vaccno);
        	$("#edit-vendor #edit-vendor-form #vfname").val(data.vfname);
        	$("#edit-vendor #edit-vendor-form #vlname").val(data.vlname);
        	$("#edit-vendor #edit-vendor-form #vaddrtype option").each(function (){
               	$(this).removeAttr('selected'); 
            });
        	$("#edit-vendor #edit-vendor-form #vaddrtype option#opt_"+data.vaddrtype).attr('selected', true);
        	$("#edit-vendor #edit-vendor-form #vaddr1").val(data.vaddr1);
        	$("#edit-vendor #edit-vendor-form #vaddr2").val(data.vaddr2);
			$("#edit-vendor #edit-vendor-form #vcity").val(data.vcity);

			$("#edit-vendor #edit-vendor-form #vstate option").each(function () {
               	$(this).removeAttr('selected'); 
            });
			$("#edit-vendor #edit-vendor-form #vstate option#"+data.vstate).attr('selected', true);
			$("#edit-vendor #edit-vendor-form #vzip").val(data.vzip);
			$("#edit-vendor #edit-vendor-form #vcontacttype option").each(function () {
               	$(this).removeAttr('selected'); 
            });
			$("#edit-vendor #edit-vendor-form #vcontacttype option#opt_"+data.vcontacttype).attr('selected', true);
			$("#edit-vendor #edit-vendor-form #vcdata").val(data.vcdata);
        	$("#edit-vendor-form #hidden-vendor-id").val(data.venID);
        	$("#edit-vendor #edit-vendor-form #update-vendor-btn").text('Update');
        	$("#edit-vendor #edit-vendor-form #vpcontact").removeAttr('checked');
        	$("#edit-vendor #edit-vendor-form #visactivecheck").removeAttr('checked');
        	if(data.vpcontactcheck == 1){
        		$("#edit-vendor #edit-vendor-form #vpcontact").trigger('click');
        	}
        	if(data.visactive == 1){
        		$("#edit-vendor #edit-vendor-form #visactivecheck").trigger('click');
        	}
        	$("#edit-vendor").modal('show');
        },
	});
}

/*==========Update vendor==========*/
function updateVandor(e){
	var venID = $(e).closest('form').find('#hidden-vendor-id').val();
	var vcompany = $("#edit-vendor-form #vcompany").val();
	var vaccno = $("#edit-vendor-form #vaccno").val();
	var vfname = $("#edit-vendor-form #vfname").val();
	var vlname = $("#edit-vendor-form #vlname").val();
	var vaddrtype = $("#edit-vendor-form #vaddrtype option:selected").attr('data-id');
	var vaddr1 = $("#edit-vendor-form #vaddr1").val();
	var vaddr2 = $("#edit-vendor-form #vaddr2").val();
	var vcity = $("#edit-vendor-form #vcity").val();
	var vstate = $("#edit-vendor-form #vstate option:selected").val();
	var vzip = $("#edit-vendor-form #vzip").val();
	var vcontacttype = $("#edit-vendor-form #vcontacttype option:selected").attr('data-id');
	var vcdata = $("#edit-vendor-form #vcdata").val();
	var vpcontactcheck = 0;
	var visactivecheck =0;
	if($("#edit-vendor-form #vpcontact").prop("checked") === true){
		vpcontactcheck = 1;
	}
	if($("#edit-vendor-form #visactivecheck").prop("checked") === true){
		visactivecheck =1;
	}

	var request_from = $("#edit-vendor-form #request-from").val();

	var vendorData  = new FormData();
	vendorData.append('venID', venID);
	vendorData.append('vcompany', vcompany);
	vendorData.append('vaccno', vaccno);
	vendorData.append('vfname', vfname);
	vendorData.append('vlname', vlname);
	vendorData.append('vaddrtype', vaddrtype);
	vendorData.append('vaddr1', vaddr1);
	vendorData.append('vaddr2', vaddr2);
	vendorData.append('vcity', vcity);
	vendorData.append('vstate', vstate);
	vendorData.append('vzip', vzip);
	vendorData.append('vcontacttype', vcontacttype);
	vendorData.append('vcdata', vcdata);
	vendorData.append('vpcontactcheck', vpcontactcheck);
	vendorData.append('visactivecheck', visactivecheck);
	vendorData.append('action', 'update_vendor');
	vendorData.append('request_from', request_from);

	if($.trim(vcompany) != '' && $.trim(vaccno) != '' ){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'contentType'	: false,
	        'processData'	: false,
	        'data'			: vendorData,
	        beforeSend: function() {
	        	$('#update-vendor-btn').text('Updating...');
	        },
	        success: function(data){
	        	$("#update-vendor-btn").text('Update');
	        	switch(data.request_from){
	        		case 'modal':
	        			$("#edit-vendor button.close").click();
	        			$.alert({
							title : 'Alert!',
							content : 'Details successfully updated'
						});
						$("#vendor-table tbody tr#row-"+data.venID).html(data.row);
						$('#vendor-table').DataTable();
						break;

					case 'profile':
						$.alert({
							title : 'Alert!',
							content : 'Profile successfully updated'
						});
						window.location.href = data.profile_url;
						break;
	        	}
	        	
	        },
		});
	}else{
		if($.trim(vcompany) == ''){
			$.alert({
				title : 'Alert!',
				content : 'Please enter company name'
			});
			$("#edit-vendor #edit-vendor-form #vcompany").focus();
		}else if($.trim(vaccno) == ''){
			$.alert({
				title : 'Alert!',
				content : 'Please enter an account number'
			});
			$("#edit-vendor #edit-vendor-form #vaccno").focus();
		}
	}
}

/*==========Order item - repeater==========*/
function AddOrderItem(e){
	var itemRow = $("#items-wrapper");
	var myitems = [];
	$("#order-form .item").each(function(){
		if($(this).val() != ''){
			myitems.push($(this).val());
		}
	});
	var itemID = $("#order-form select[name=item] :selected").val();
	var btnid = $(e).data('btnid');
	var orderItemData = new FormData();
	orderItemData.append('itemID', itemID);
	orderItemData.append('btnid', btnid);
	orderItemData.append('myitems', myitems);
	orderItemData.append('action', 'add_order_item_row');

	var ThisRow = $(e).closest('.item-block').find('select.item').val();
	var ThisRowCost = $(e).closest('.item-block').find('.cost').val();
	var ThisRowTax = $(e).closest('.item-block').find('.tax').val();
	var ThisRowShippinCost = $(e).closest('.item-block').find('.shipping-cost').val();

	if(ThisRow == ''){
		$.alert({
			title : 'Alert!',
			content : 'Please Select an Item'
		});
	}else if(ThisRowCost == ''){
		$.alert({
			title : 'Alert!',
			content : 'Please Enter Item Cost'
		});
		$(e).closest('.item-block').find('.cost').focus();
	}else if(ThisRowTax == ''){
		$.alert({
			title : 'Alert!',
			content : 'Please Enter Sales Tax'
		});
		$(e).closest('.item-block').find('.tax').focus();
	}else if(ThisRowShippinCost == ''){
		$.alert({
			title : 'Alert!',
			content : 'Please Enter Shipping Cost'
		});
		$(e).closest('.item-block').find('.shipping-cost').focus();
	}else{
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'contentType'	: false,
	        'processData'	: false,
	        'data'			: orderItemData,
	        beforeSend: function() {
	        	$(e).text('...');
	        },
	        success: function(data){
	        	itemRow.append(data.htmlContent);
	        	$(e).text('+');
	        	SdSelection();
	        	QuantityValidation();
	        	PriceValidation();
	        },
		});
	}
}

function RemoveOrderItem(thisItem){
	thisItem.closest('.item-block').remove();
	item--;
	if(item == 2){
		$("#items-wrapper .item-block a:eq(0)").show();
	}
}

$("#create-order-btn").on('click', function(e){
	e.preventDefault();
	var Item 			= [];
	var VItem 			= [];
	var Case 			= [];
	var CaseQty 		= [];
	var QtyPerCase 		= [];
	var Cost 			= [];
	var Tax 			= [];
	var ShippingCost 	= [];
	var GL 				= [];
	var Note 			= [];
	var Customer 		= [];

	//Item
	$("#order-form .item").each(function(){
		var ItemVal = $(this).val();
		if(ItemVal != ''){
			Item.push(ItemVal);
		}
	});

	//Vendor Item
	SetEachOne("#order-form .vitem", VItem);
	//Case
	$("#order-form .case").each(function(){
		var CaseVal = $(this).is(':checked') ? '1' : '0';
		if(CaseVal != ''){
			Case.push(CaseVal);
		}
	});
	//Case Qty
	SetEachOne("#order-form .case-qty", CaseQty);
	//Qty Per Case
	SetEachOne("#order-form .qty-per-case", QtyPerCase);
	//Cost
	SetEachOne("#order-form .cost", Cost);
	//Tax
	SetEachOne("#order-form .tax", Tax);
	//Shipping Cost
	SetEachOne("#order-form .shipping-cost", ShippingCost);
	//GL
	SetEachOne("#order-form .gl", GL);
	//Note
	SetEachOne("#order-form .note", Note);
	//Customer
	$("#order-form .customer").each(function(){
		if($(this).val() != ''){
			Customer.push($(this).val());
		}
	});

	var VendorID 	= $("#order-form #vendor").val();
	var OrderPhone 	= $("#order-form #order-phone").val();
	var LedgerID 	= $("#order-form #ledger-id").val();
	var EmpID 		= $("#order-form #current-emp").val();

	var OrderData = new FormData();
	OrderData.append('Item', Item);
	OrderData.append('VItem', VItem);
	OrderData.append('Case', Case);
	OrderData.append('CaseQty', CaseQty);
	OrderData.append('QtyPerCase', QtyPerCase);
	OrderData.append('Cost', Cost);
	OrderData.append('Tax', Tax);
	OrderData.append('ShippingCost', ShippingCost);
	OrderData.append('GL', GL);
	OrderData.append('Note', Note);
	OrderData.append('Customer', Customer);

	OrderData.append('VendorID', VendorID);
	OrderData.append('CustomerID', null);
	OrderData.append('OrderPhone', OrderPhone);
	OrderData.append('LedgerID', LedgerID);
	//OrderData.append('OrderNote', OrderNote);
	OrderData.append('EmpID', EmpID);

	OrderData.append('action', 'create_order');
	if(VendorID != ''){
		if(Item.length > 0){
			$.ajax({
				'url'			: 'requests/actions.php',
		        'type'			: 'post',
		        'dataType'		: 'JSON',
		        'cache'			: false,
		        'contentType'	: false,
		        'processData'	: false,
		        'data'			: OrderData,
		        beforeSend: function() {
		        },
		        success: function(data){
		        	if(data.resp === true){
		        		$.alert({
					        title: 'Alert!',
					        content: 'Successfully Ordered!',
					    });
		        		$("#order-form")[0].reset();
		        		location.reload();
		        	}
		        },
			});
		}else{
			$.alert({
				title : 'Alert!',
				content : 'Please select an item'
			});
		}
	}else{
		$.alert({
			title : 'Alert!',
			content : 'Please select a vendor'
		});
	}
});


/*==========Add Customer==========*/
function CustomerTaxDetails(){
	$("#add-customer-form #tax-exempt, #edit-customer-form #tax-exempt").on('change', function(){
		if($(this).val() == 1){
			$("#add-customer-tax-details").modal('show');
			var tid = $("#edit-customer-form #tax-id").val();
			var expd = $("#edit-customer-form #expiration-date").val();
			$("#add-customer-tax-details #tax-id-number").val(tid);
			$("#add-customer-tax-details #expire-date").val(expd);
		}
	});
}
CustomerTaxDetails();

$('#add-customer-tax-details').on('hidden.bs.modal', function (e) {
  var taxid = $("#add-customer-tax-details #tax-id-number").val();
  var expdate = $("#add-customer-tax-details #expire-date").val();
  if($.trim(taxid) == '' && $.trim(expdate) == ''){
  	$("#add-customer-form #tax-exempt, #edit-customer-form #tax-exempt").each(function(){
  		$(this).find('option').removeAttr('selected');
  		$(this).find('option:eq(0)').attr('selected');
  	});
  }
});

$("#add-customer-btn").click(function(e){
	e.preventDefault();
	var NewCustomer = $("#add-customer-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: NewCustomer+'&'+$.param({ 'action' : 'new_customer' }),
        beforeSend: function() {

        },
        success: function(data){
	        $("#add-customer-alert").show();
        	if(data.error){
        		$("#add-customer-alert").removeClass('alert-success');
        		$("#add-customer-alert").addClass('alert-danger');
        		$("#add-customer-alert #add-customer-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#add-customer-alert").removeClass('alert-danger');
        		$("#add-customer-alert").addClass('alert-success');
        		$("#add-customer-alert #add-customer-alert-message").text(data.message);
        		$("#add-customer-form")[0].reset();
        		if(data.returnUrl){
        			window.location.href = data.returnUrl;
        		}
        	}
        	if(data.newcustomrDropdown){
        		$("#customer-selling-form #selling-customer").html(data.newcustomrDropdown);
        		SdSelection();
        		$("#add-customer").modal('hide');
        		$("#add-customer-alert").hide();
        	}
        },
	});
});

/*============Edit Customer Modal Open============*/
function editCustomer(obj){
	var customerID = $(obj).attr('data-id');
	var customerData  = new FormData();
	customerData.append('customerID', customerID);
	customerData.append('action', 'edit_customer');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: customerData,
        beforeSend: function() {

        },
        success: function(data){

        	if(data.content){
        		$("#edit-customer-body").html(data.content);
        		$("#edit-customer").modal('show');
        	}
        	CustomerTaxDetails();
        },
	});
}

function UpdateCustomer(){
	var CustomerData = $("#edit-customer-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: CustomerData+'&'+$.param({ 'action' : 'update_customer' }),
        beforeSend: function() {
        	$('#update-customer-btn').text('Updating...');
        },
        success: function(data){
        	$("#edit-customer-alert").show();
        	if(data.error){
        		$("#edit-customer-alert").removeClass('alert-success');
        		$("#edit-customer-alert").addClass('alert-danger');
        		$("#edit-customer-alert #edit-customer-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#edit-customer-alert").removeClass('alert-danger');
        		$("#edit-customer-alert").addClass('alert-success');
        		$("#edit-customer-alert #edit-customer-alert-message").text(data.message);
        		window.location.href = data.redirect_url;
        	}
        	$('#update-customer-btn').text('Update');
        },
	});
}

$(".order-status").on('click', function(){
	var activeClass = 'theme-default-orange';
	var orderID = $(this).data('orderid');
	var status = $(this).data('status');
	if(!$(this).hasClass(activeClass)){
		var confirmation = 'Want to update order status?';
		if(status == 'pending'){
			confirmation = confirm('Order Pending?');
		}
		if(status == 'completed'){
			confirmation = confirm('Order Completed?');
		}
		if(confirmation === true){
			$(this).addClass(activeClass);
			$("#order-pending").removeClass(activeClass);
			$data = new FormData();
			$data.append('order_id', orderID);
			$data.append('action', 'update_order_status');
			$data.append('status', status);
			$.ajax({
				'url'			: 'requests/actions.php',
		        'type'			: 'post',
		        'dataType'		: 'JSON',
		        'cache'			: false,
		        'contentType'	: false,
		        'processData'	: false,
		        'data'			: $data,
		        beforeSend: function() {
		        },
		        success: function(data){
		        	if(data.resp === true){
		        		$("#tbl-orderitemdetals .shipping-date-td").each(function(){
		        			$(this).text(data.date?data.date:'Null');
		        		});
		        		if(data.status == 'C'){
		        			$("#tbl-orderitemdetals .cs").each(function(){
			        			$(this).attr('checked', true);
			        		});
		        		}
		        		if(data.status == 'P'){
		        			$("#tbl-orderitemdetals .ps").each(function(){
			        			$(this).attr('checked', true);
			        		});
		        		}
		        		$.alert(data.msg);
		        	}
		        },
			});
		}
	}
});

$(".status-check").on('click', function(){
	var itemID = $(this).data('itemid');
	var status = $(this).data('status');

	var $checkData = new FormData();
	$checkData.append('itemID', itemID);
	$checkData.append('action', 'order status');
	$checkData.append('status', status);
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: $checkData,
        beforeSend: function() {
      
        },
        success: function(data){
        	if(data.resp === true){
        		
        		$("#tbl-orderitemdetals #shiptd-"+data.item).text(data.date?data.date:'Null');
        		alert(data.msg);
        	}
        },
	});
});

/*================ SELLING Section - START ================*/

function DiscountFormat(){
	$(".disc-format").each(function(){
		$(this).on('blur', function(event){
			var $thisLastVal = $(this).val().slice(-1);
			if($thisLastVal.match(/[a-zA-Z!@#^&*()_+\-=\[\]{};':"\\|,<>\/?]/g)){
				$(this).val($(this).val().substring(0, $(this).val().length - 1));
			}
			spattr = $(this).attr('data-sp');
			if(typeof spattr !== typeof undefined && spattr !== false){
				var Customer = $("#customer-selling-form #customer-name").val();
				if(parseFloat($(this).val().replace('$','').replace('%','')) > 0){
					var sp = confirm('This customer ( '+Customer+' ) already has special pricing. Are you sure you want to allow an additional discount more than the current special price?');
					if(sp === false){
						$(this).val('');
						getItemNetPrice(this);
					}
				}
			}
		});
	});
}
DiscountFormat();

function DiscountAddNegativeSign(){
	$(".disc-format").each(function(){
		$(this).on('blur', function(event){
			var v = $(this).val().replace('-', '');
			if(v != ''){
				$(this).val('-'+v);
			}
		});
	});
}


function RestrictCCPD(){
	$(".disc-format, .qty, .pay-vendor").each(function(){
	    $(this).bind("cut copy paste drop",function(e) {
	        e.preventDefault();
	    });
	});
}
RestrictCCPD();


function addSellingItem(e){
	var itemRow = $("#item-row");
	var myitems = [];
	var myItemTypes = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			myitems.push($(this).val());
			myItemTypes.push($(this).find('option:selected').attr('data-type'));
		}
		
	});

	var customer = $("#selling-customer #customer option:selected").val();
	var itemID = $(e).closest('.item-block').find('.item option:selected').val();
	var btnid = $(e).data('btnid');
	var sellingData = new FormData();
	sellingData.append('itemID', itemID);
	sellingData.append('btnid', btnid);
	sellingData.append('myitems', myitems);
	sellingData.append('myItemTypes', myItemTypes);
	sellingData.append('action', 'add_selling_item_row');

	var $this = $(e).closest('.item-block');

	var $thisQty = $this.find('.qty');
	var $thisCost = $this.find('.cost');
	var $thisDisc = $this.find('.disc');
	if(customer != '' && $thisQty.val() != '' && $thisCost.val() != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'contentType'	: false,
	        'processData'	: false,
	        'data'			: sellingData,
	        beforeSend: function() {
	        	$(e).text('...');
	        },
	        success: function(data){
	        	itemRow.append(data.htmlContent);
	        	$(e).text('+');
	        	SdSelection();
	        	QuantityValidation();
	        	PriceValidation();
	        	DiscountFormat();
	        	RestrictCCPD();
	        },
		});
	}else{
		if(customer == ''){
		    $.alert({
		        title: 'Alert!',
		        content: 'Please select a customer first',
		    });
		}else if($thisQty.val() == ''){
			$.alert({
		        title: 'Alert!',
		        content: 'You must enter a quantity',
		    });
			$thisQty.focus();
		}else if($thisCost.val() == ''){
			$.alert({
		        title: 'Alert!',
		        content: 'You must select an item',
		    });
			$thisItem.focus();
		}
	}
}

function RemoveSellingItem(thisItem){
	$.confirm({
        title : 'Confirmation',
        content : 'Delete line item?',
        buttons: {
            Ok : {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function(){
                           thisItem.closest('.item-block').remove();
							getItemNetPrice(thisItem);        
                    },     
            },
            Cancel : {
                    text: 'Cancel',
                    btnClass: 'btn-red',
            }
        }
    });
}


$(".selling-payment-method").each(function(){
	$(this).on('click', function(){
		var PayBlock = $(this).closest('li').find('.pay-block')
		$(this).toggleClass("active");
		if($(this).hasClass("active")){
			PayBlock.show();
		}else{
			PayBlock.hide();
		}
	});
});

//On select selling payment methods
$(".selling-payment-method").each(function(){
	$(this).on('click', function(){
		$(".selling-payment-method").removeClass('active');
		$(this).addClass('active');
		var dataCard = $(this).attr('data-card');
		$("._p_m input").val('');
		$("#selling-payment #ctm").text('');
		$("._p_m").hide();
		$("."+dataCard).show();
		if(dataCard == '_card'){
			$("#invoice-block").hide();
		}else{
			$("#invoice-block").show();
		}
	});
});

//Calculate sales data
function getItemPrice(e){
	var itemID = $(e).val();
	var itemType = $(e).find('option:selected').attr('data-type');
	var SellingItemPrice = new FormData();
	var Customer = $("#customer-selling-form #selling-customer #customer").val();
	SellingItemPrice.append('itemID', itemID);
	SellingItemPrice.append('itemType', itemType);
	SellingItemPrice.append('Customer', Customer);
	SellingItemPrice.append('action', 'get_selling_item_price');

	if(Customer != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: SellingItemPrice,
			beforeSend		: function(){
			},
			success 		: function(data){
							if(data.price){
								$(e).closest('.item-block').find('.cost').val(data.price);
								$(e).closest('.item-block').find('.tax').val(data.tax);
								$(e).closest('.item-block').find('.unit-tax').val(data.tax);
								$(e).closest('.item-block').find('.qty').attr('data-qty',data.qty);
								$(e).closest('.item-block').find('.disc').val('');
								$(e).closest('.item-block').find('.disc').removeAttr('data-sp');
								$(e).closest('.item-block').find('.ext-cost').removeAttr('style');
								if(data.sp){
									$(e).closest('.item-block').find('.disc').attr('data-sp',data.sp);
								}
								if(parseInt($(e).closest('.item-block').find('.qty').val()) > data.qty){
									$(e).closest('.item-block').find('.qty').val(data.qty);
								}
							}else{
								$(e).closest('.item-block').find('.cost').val('');
								$(e).closest('.item-block').find('.tax').val('');
								$(e).closest('.item-block').find('.unit-tax').val('');
								$(e).closest('.item-block').find('.qty').removeAttr('data-qty');
								$(e).closest('.item-block').find('.qty').val(1);

								$("#selling-price").text('$ 0.00');
								$("#selling-disc").text('$ 0.00');
								$("#selling-total").text('$ 0.00');
							}
							getItemNetPrice(e);
			},

			complete 		: function(){
							var FinalExtCost = $(e).closest('.item-block').find('.ext-cost').val();
							$.ajax({
								'url'			: 'requests/actions.php',
								'type'			: 'post',
								'dataType'		: 'json',
								'cache'			: false,
								'data'			: {'action' : 'verify_ext_cost', 'itemID' : itemID, 'FinalExtCost' : FinalExtCost },
								beforeSend		: function(){
								},
								success 		: function(Response){
												if(Response.false){
													$.alert({
														title : 'Alert!',
														content : 'Price becomes less than Purchasing Cost. Please manage item price'
													});
												}
								}
							});
			}
		});
	}else{
		$.alert({
	        title: 'Alert!',
	        content: 'Please select a customer first',
	    });
		$(e).selectpicker('val', '');
		return false;
	}
}

//Calculate sales data
function getItemNetPrice(e){


	var $thisBlock = $(e).closest('.item-block');
	var qty = $thisBlock.find('.qty').val();

	var tax = $thisBlock.find('.tax').val();
	var unitTax = $thisBlock.find('.unit-tax').val();
	$thisBlock.find('.tax').val(qty * unitTax);
	tax = $thisBlock.find('.tax').val();

	var cost = $thisBlock.find('.cost').val();
	var disc = $thisBlock.find('.disc').val();
	var ext = $thisBlock.find('.ext-cost').val();
	var SelectedCustomer = $("#customer-selling-form #customer").val();
	var ThisItem = $thisBlock.find('.item option:selected').val();
	var ThisItemName = $thisBlock.find('.item option:selected').text();
	var IstaxExempt = $("#tax-exempt-word").text(); //IstaxExempt

	if($thisBlock.find('.qty').attr('data-is_spo') != 1){
		if(parseInt(qty) > parseInt($thisBlock.find('.qty').attr('data-qty'))){
			$thisBlock.find('.qty').val($thisBlock.find('.qty').attr('data-qty'));
			$.confirm({
		        title : 'Confirmation',
		        content : 'Quantity for item '+ThisItemName+' is more than we have on hand.  You cannot sell below the items on hand. Please adjust quantity.',
		        buttons: {
	                Ok : {
                        text: 'Ok',
                        btnClass: 'btn-blue',
                        action: function(){
          					$.confirm({
						        title : 'Confirmation',
						        content : 'Want to special order this for the customer?',
						        buttons: {
					                Ok : {
					                        text: 'Ok',
					                        btnClass: 'btn-blue',
					                        action: function(){
					                            window.open(site_url()+'?destination=purchasing&itemID='+ThisItem+'&cid='+SelectedCustomer, '_blank');     
					                        },     
					                },
					                Cancel : {
					                        text: 'Cancel',
					                        btnClass: 'btn-red',
					                        action : function(){
					                        }
					                }
						        }
						    }); 
                        },     
	                },
	                Cancel : {
                        text: 'Cancel',
                        btnClass: 'btn-red',
                        action : function(){
                        }
	                }
		        }
		    });
		}
	}
	if(parseInt(qty) == 0){
		var countItemBlock = $("#customer-selling-form .item-block").length;
		$.confirm({
	        title : 'Confirmation',
	        content : 'Delete line item?',
	        buttons: {
                Ok : {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function(){
                    	$thisBlock.find('.item').selectpicker('val', '');
                    	$thisBlock.find('.qty').val('1');
                    	$thisBlock.find('.cost').val('');
                    	$thisBlock.find('.disc').val('');
                    	$thisBlock.find('.tax').val('');
                    	$thisBlock.find('.ext-cost').val('');
                    },     
                },
                Cancel : {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                    action : function(){
                    	$.confirm({
					        title : 'Confirmation',
					        content : 'This item is Out of Stock! Do you want to Special order it?',
					        buttons: {
				                Ok : {
			                        text: 'Ok',
			                        btnClass: 'btn-blue',
			                        action: function(){
			                            $thisBlock.find('.qty').val($thisBlock.find('.qty').attr('min'));
			                            $thisBlock.find('.qty').attr('data-is_spo', 1);    
			                        },     
				                },
				                Cancel : {
			                        text: 'Cancel',
			                        btnClass: 'btn-red',
			                        action : function(){
			                        	if(countItemBlock > 1){
											$thisBlock.remove();
										}
			                        }
				                }
					        }
					    });
                    }
                }
	        }
	    });
	}
	qty 	= qty?qty:0;
	cost 	= cost?cost:0;
	disc 	= disc?disc:0;
	tax 	= tax?tax:0;
	ext 	= ext?ext:0;
	tax = IstaxExempt.length > 0 ? 0 : tax;
	var Ext_Cost = 0;
	var DiscPriceVal = 0;
	if(disc!=''){
		disc = disc.replace('-', '');
		if(disc.indexOf('.') > -1 && disc.indexOf('$') > -1){
			disc = disc.replace('$', '');
			Ext_Cost = (parseInt(qty)*parseFloat(cost)) - parseFloat(disc) + parseFloat(tax);
			DiscPriceVal = disc;
		}else if(disc.indexOf('$') > -1){
			disc = disc.replace('$', '');
			Ext_Cost = (parseInt(qty)*parseFloat(cost)) - parseFloat(disc) + parseFloat(tax);
			DiscPriceVal = disc;
		}else if(disc.indexOf('.') > -1 && disc.indexOf('%') > -1){
			disc = disc.replace('%', '');
			Ext_Cost = (parseInt(qty)*parseFloat(cost))*(1-(parseFloat(disc) / 100)) + parseFloat(tax);
			DiscPriceVal = (parseInt(qty)*parseFloat(cost))*(parseFloat(disc) / 100);
		}else if(disc.indexOf('%') > -1){
			disc = disc.replace('%', '');
			Ext_Cost = (parseInt(qty)*parseFloat(cost))*(1-(parseFloat(disc) / 100)) + parseFloat(tax);
			DiscPriceVal = (parseInt(qty)*parseFloat(cost))*(parseFloat(disc) / 100);
		}else if(disc.indexOf('.') > -1){
			Ext_Cost = (parseInt(qty)*parseFloat(cost))*(1-parseFloat(disc)) + parseFloat(tax);
			DiscPriceVal = disc;
		}else{
			Ext_Cost = (parseInt(qty)*parseFloat(cost)) - parseFloat(disc) + parseFloat(tax);
			DiscPriceVal = disc;
		}
	}else{
		Ext_Cost = (parseInt(qty)*parseFloat(cost)) - 0 + parseFloat(tax);
	}
	if(parseFloat(DiscPriceVal) > parseFloat(ext)){
		$.alert({
	        title: 'Alert!',
	        content: 'Discount can\'t be greater than Ext Cost',
	    });
		$thisBlock.find('.disc').val(0);
		return false;
	}
	if(parseFloat(cost)*(1+10/100) < parseFloat(DiscPriceVal)){
		$thisBlock.find('.disc').val(0);
		return false;
	}
	$thisBlock.find('.ext-cost').val(Ext_Cost.toFixed(2));
	//Cost
	var CostArray = new Array();
	$(".cost").each(function(){
		if($(this).val() != ''){
			var $thisQty = $(this).closest('.item-block').find('.qty').val();
			CostArray.push(parseFloat($(this).val())*$thisQty);
		}
	});
	var SumOfCost = CostArray.reduce(function (accumulator, currentValue) {
		return parseFloat(accumulator) + parseFloat(currentValue);
	}, 0);
	$("#selling-price").text('$ '+SumOfCost.toFixed(2));

	//Tax
	var TaxArray = new Array();
	$(".tax").each(function(){
		if($(this).val() != ''){
			//AVD start - 12/30/18
			//TaxArray.push((parseFloat($(this).val())/100)*parseFloat($(this).closest('.item-block').find('.cost').val()));
			var itemcost = parseFloat($(this).closest('.item-block').find('.cost').val());
			var itemqty = parseFloat($(this).closest('.item-block').find('.qty').val());
			//TaxArray.push((parseFloat($(this).val())/100)*(itemcost * itemqty)); //PREVIOUS BLOCK BEFORE FAWSZAN MODIFIED THIS
			TaxArray.push(parseFloat($(this).val()));
			//AVD end - 12/30/18
		}
	});
	var SumOfTax = TaxArray.reduce(function (accumulator, currentValue) {
		return parseFloat(accumulator) + parseFloat(currentValue);
	}, 0);

	SumOfTax = IstaxExempt.length > 0 ? 0 : SumOfTax;
	$("#selling-tax").text('$ '+SumOfTax.toFixed(2));

	//Ext Cost
	var ExtArray = new Array();
	$(".ext-cost").each(function(){
		if($(this).val() != ''){
			ExtArray.push($(this).val());
		}
	});
	var SumOfExt = ExtArray.reduce(function (accumulator, currentValue) {
		return parseFloat(accumulator) + parseFloat(currentValue);
	}, 0);


	$("#selling-total").text('$ '+SumOfExt.toFixed(2));

	$thisBlock.find('.discvalue').val(DiscPriceVal);

	//var calculatedTax = (parseFloat(tax)/100)*parseFloat(cost);  original
	$thisBlock.find('.taxvalue').val(tax);

	//Disc
	var TotalDisc = CalculateDisc(); //SumOfCost - SumOfExt
	$("#selling-disc").text('$ '+TotalDisc.toFixed(2));

	$(".disc-percentage").each(function(){
		if($(this).hasClass('active')){
			$(this).trigger('click');
		}
	});
	if(disc != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'data'			: { 'action' : 'check_employee_role_while_selling', 'disc' : disc },
			beforeSend		: function(){
			},
			success 		: function(data){
							switch(data.auth){
								case true:
									break;
								case false:
									if(data.role == 'Cashier' && data.role != 'Administrator'){
										$thisBlock.find('.disc').val('');
										$("#manager-approval-popup").modal('show');
									}
								break;	
							}
			}
		});
	}
}

//Manager Approval
function SaveManagerApproval(){
	var ManagerUsername = $("#manager-approval-form  #manager-username").val();
	var ManagerPassword = $("#manager-approval-form  #manager-password").val();
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: { 'action' : 'manager_authentication', 'ManagerUsername' : ManagerUsername, 'ManagerPassword' : ManagerPassword },
		beforeSend		: function(){
		},
		success 		: function(data){
						$("#manager-auth-alert").show();
			        	if(data.error){
			        		$("#manager-auth-alert").removeClass('alert-success');
			        		$("#manager-auth-alert").addClass('alert-danger');
			        		$("#manager-auth-alert #manager-auth-alert-message").text(data.error);
			        	}
			        	if(data.message){
			        		$("#manager-auth-alert").removeClass('alert-danger');
			        		$("#manager-auth-alert").addClass('alert-success');
			        		$("#manager-auth-alert #manager-auth-alert-message").text(data.message);
			        		$("#manager-approval-form")[0].reset();
			        		setTimeout(function(){
			        			$("#manager-approval-popup").modal('hide');
			        		}, 2000);
			        	}
		}
	});
}

//Calculating discount
function CalculateDisc(){
	var  DiscTotalArr = [];
	$("#customer-selling-form .discvalue").each(function(){
		if($(this).val() != ''){
			DiscTotalArr.push($(this).val());
		}
	});
	var DiscTotalVal = DiscTotalArr.reduce(function (accumulator, currentValue) {
		return parseFloat(accumulator) + parseFloat(currentValue);
	}, 0);
	return DiscTotalVal; 
}


//Sales Items move to Quote except CustomerID = 1
$("#customer-selling-form #invoice").on('click', function(){
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var itemTypes = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			itemTypes.push($(this).find('option:selected').attr('data-type'));
		}
	});

	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	var costs = [];
	$("#customer-selling-form .cost").each(function(){
		if($(this).val() != ''){
			costs.push($(this).val());
		}
	});
	var discs = [];
	$("#customer-selling-form .disc").each(function(){
		if($(this).val() != ''){
			discs.push($(this).val());
		}else{
			discs.push('0');
		}
	});
	var taxs = [];
	$("#customer-selling-form .tax").each(function(){
		if($(this).val() != ''){
			taxs.push($(this).val());
		}else{
			taxs.push('0');
		}
	});
	var ItemNote = [];
	$("#customer-selling-form .note").each(function(){
		var noteVal = $(this).val();
		noteVal = noteVal ? noteVal : 'NULL';
		ItemNote.push(noteVal);
	});
	var purchaseOrder = $("#customer-selling-form #purchase-order").val();
	var customer = $("#customer-selling-form #customer").val();
	if(customer == 1){
		$.alert({
	        title: 'Alert!',
	        content: 'When creating a quote you must have a valid customer, it cannot be a Walk-In. Please choose a valid customer.',
	    });
	}else{
		var sales_amount = $("#customer-selling-form #selling-total").text();
		sales_amount = sales_amount.replace('$', '');
		if(customer == ''){
			$.alert({
		        title: 'Alert!',
		        content: 'Please select a customer first',
		    });
		}else if(items.length <= 0){
			$.alert({
		        title: 'Alert!',
		        content: 'Please select an item',
		    });
		}else if(qtys.length <= 0){
			$.alert({
		        title: 'Alert!',
		        content: 'Please input some quantity',
		    });
		}else{
			$.confirm({
		        title : 'Confirmation',
		        content : 'Quote has been saved, clear screen?',
		        buttons: {
	                Ok : {
                        text: 'Ok',
                        btnClass: 'btn-blue',
                        action: function(){
                            var sellingData = new FormData();
							sellingData.append('items', items);
							sellingData.append('itemTypes', itemTypes);
							sellingData.append('qtys', qtys);
							sellingData.append('costs', costs);
							sellingData.append('discs', discs);
							sellingData.append('taxs', taxs);
							sellingData.append('ItemNote', ItemNote);
							sellingData.append('sales_amount', sales_amount);
							sellingData.append('customer', customer);
							sellingData.append('purchaseOrder', purchaseOrder);
							sellingData.append('RequestTo', 'Quote');
							sellingData.append('action', 'move_to_quote');
							$.ajax({
								'url'			: 'requests/actions.php',
						        'type'			: 'post',
						        'dataType'		: 'JSON',
						        'cache'			: false,
						        'contentType'	: false,
						        'processData'	: false,
						        'data'			: sellingData,
						        beforeSend: function() {
						        },
						        success: function(data){
						        	if(data.response === true){
					        			window.location.href = data.url;
					        		}
						        },
							});        
                        },     
	                },
	                Cancel : {
                        text: 'Cancel',
                        btnClass: 'btn-red',
	                }
		        }
			});
		}
	}
});

//Sales Items move to for All Customers
$("#customer-selling-form #park").on('click', function(){
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	var costs = [];
	$("#customer-selling-form .cost").each(function(){
		if($(this).val() != ''){
			costs.push($(this).val());
		}
	});
	var discs = [];
	$("#customer-selling-form .disc").each(function(){
		if($(this).val() != ''){
			discs.push($(this).val());
		}else{
			discs.push('0');
		}
	});
	var taxs = [];
	$("#customer-selling-form .tax").each(function(){
		if($(this).val() != ''){
			taxs.push($(this).val());
		}else{
			taxs.push('0');
		}
	});
	var ItemNote = [];
	$("#customer-selling-form .note").each(function(){
		var noteVal = $(this).val();
		noteVal = noteVal ? noteVal : 'NULL';
		ItemNote.push(noteVal);
	});
	var purchaseOrder = $("#customer-selling-form #purchase-order").val();
	var customer = $("#customer-selling-form #customer").val();
	var sales_amount = $("#customer-selling-form #selling-total").text();
	sales_amount = sales_amount.replace('$', '');
	if(customer == ''){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select a customer first',
	    });
		//alert('Please select a customer');
	}else if(items.length <= 0){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select an item',
	    });
		//alert('Please select an item');
	}else if(qtys.length <= 0){
		$.alert({
	        title: 'Alert!',
	        content: 'Please input some quantity',
	    });
		//alert('Please input some quantity');
	}else{
		$.confirm({
	        title : 'Confirmation',
	        content : 'Quote has been saved, clear screen?',
	        buttons: {
	                Ok : {
	                        text: 'Ok',
	                        btnClass: 'btn-blue',
	                        action: function(){
	                            var sellingData = new FormData();
								sellingData.append('items', items);
								sellingData.append('qtys', qtys);
								sellingData.append('costs', costs);
								sellingData.append('discs', discs);
								sellingData.append('taxs', taxs);
								sellingData.append('ItemNote', ItemNote);
								sellingData.append('sales_amount', sales_amount);
								sellingData.append('customer', customer);
								sellingData.append('purchaseOrder', purchaseOrder);
								sellingData.append('RequestTo', 'Quote');
								sellingData.append('action', 'move_to_quote');
								$.ajax({
									'url'			: 'requests/actions.php',
							        'type'			: 'post',
							        'dataType'		: 'JSON',
							        'cache'			: false,
							        'contentType'	: false,
							        'processData'	: false,
							        'data'			: sellingData,
							        beforeSend: function() {
							        },
							        success: function(data){
							        	if(data.response === true){
						        			window.location.href = data.url;
						        		}
							        },
								});        
	                        },     
	                },
	                Cancel : {
	                        text: 'Cancel',
	                        btnClass: 'btn-red',
	                }
	        }
		});
	}
	
});

//Update Quote
$("#customer-selling-form #quote-invoice").on('click', function(e){
	e.preventDefault();
	var InvoiceNum = $(this).attr('data-invoice');
	var SalesID = $(this).attr('data-sales_id');
	var salesDetailsIDs = [];
	$("#customer-selling-form .quote-item").each(function(){
		salesDetailsIDs.push(this.id);
	});
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var itemTypes = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			itemTypes.push($(this).find('option:selected').attr('data-type'));
		}
	});
	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	var costs = [];
	$("#customer-selling-form .cost").each(function(){
		if($(this).val() != ''){
			costs.push($(this).val());
		}
	});
	var discs = [];
	$("#customer-selling-form .disc").each(function(){
		if($(this).val() != ''){
			discs.push($(this).val());
		}else{
			discs.push('0');
		}
	});
	var taxs = [];
	$("#customer-selling-form .tax").each(function(){
		if($(this).val() != ''){
			taxs.push($(this).val());
		}else{
			taxs.push('0');
		}
	});
	var ItemNote = [];
	$("#customer-selling-form .note").each(function(){
		var noteVal = $(this).val();
		noteVal = noteVal ? noteVal : 'NULL';
		ItemNote.push(noteVal);
	});
	var purchaseOrder = $("#customer-selling-form #purchase-order").val();
	var customer = $("#customer-selling-form #customer").val();
	var sales_amount = $("#customer-selling-form #selling-total").text();
	sales_amount = sales_amount.replace('$', '');
	if(customer == 1){
		$.alert({
	        title: 'Alert!',
	        content: 'When creating a quote you must have a valid customer, it cannot be a Walk-In. Please choose a valid customer.',
	    });
	}else{
		if(customer == ''){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select a customer first',
	    });
		}else if(items.length <= 0){
			$.alert({
		        title: 'Alert!',
		        content: 'Please select an item',
		    });
		}else if(qtys.length <= 0){
			$.alert({
		        title: 'Alert!',
		        content: 'Please input some quantity',
		    });
		}else{
			$.confirm({
                title : 'Confirmation',
                content : 'Quote has been updated, clear screen?',
                buttons: {
                    Ok : {
                            text: 'Ok',
                            btnClass: 'btn-blue',
                            action: function(){
                                var sellingData = new FormData();
								sellingData.append('items', items);
								sellingData.append('itemTypes', itemTypes);
								sellingData.append('qtys', qtys);
								sellingData.append('costs', costs);
								sellingData.append('discs', discs);
								sellingData.append('taxs', taxs);
								sellingData.append('ItemNote', ItemNote);
								sellingData.append('sales_amount', sales_amount);
								sellingData.append('customer', customer);
								sellingData.append('InvoiceNum', InvoiceNum);
								sellingData.append('SalesID', SalesID);
								sellingData.append('salesDetailsIDs', salesDetailsIDs);
								sellingData.append('purchaseOrder', purchaseOrder);
								sellingData.append('RequestTo', 'UpdateQuote');
								sellingData.append('action', 'update_quote');
								$.ajax({
									'url'			: 'requests/actions.php',
							        'type'			: 'post',
							        'dataType'		: 'JSON',
							        'cache'			: false,
							        'contentType'	: false,
							        'processData'	: false,
							        'data'			: sellingData,
							        beforeSend: function() {
							        },
							        success: function(data){
							        	if(data.response === true){
						        			window.location.href = data.url;
						        		}
							        },
								});        
                            },     
                    },
                    Cancel : {
                        text: 'Cancel',
                        btnClass: 'btn-red',
                    }
                }
	        });
		}
	}
});

//Update Park
$("#customer-selling-form #park-update").on('click', function(e){
	e.preventDefault();
	var InvoiceNum = $(this).attr('data-invoice');
	var SalesID = $(this).attr('data-sales_id');
	var salesDetailsIDs = [];
	$("#customer-selling-form .quote-item").each(function(){
		salesDetailsIDs.push(this.id);
	});
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var itemTypes = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			itemTypes.push($(this).find('option:selected').attr('data-type'));
		}
	});
	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	var costs = [];
	$("#customer-selling-form .cost").each(function(){
		if($(this).val() != ''){
			costs.push($(this).val());
		}
	});
	var discs = [];
	$("#customer-selling-form .disc").each(function(){
		if($(this).val() != ''){
			discs.push($(this).val());
		}else{
			discs.push('0');
		}
	});
	var taxs = [];
	$("#customer-selling-form .tax").each(function(){
		if($(this).val() != ''){
			taxs.push($(this).val());
		}else{
			taxs.push('0');
		}
	});
	var ItemNote = [];
	$("#customer-selling-form .note").each(function(){
		var noteVal = $(this).val();
		noteVal = noteVal ? noteVal : 'NULL';
		ItemNote.push(noteVal);
	});
	var purchaseOrder = $("#customer-selling-form #purchase-order").val();
	var customer = $("#customer-selling-form #customer").val();
	var sales_amount = $("#customer-selling-form #selling-total").text();
	sales_amount = sales_amount.replace('$', '');
	if(customer == ''){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select a customer first',
	    });
	}else if(items.length <= 0){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select an item',
	    });
	}else if(qtys.length <= 0){
		$.alert({
	        title: 'Alert!',
	        content: 'Please input some quantity',
	    });
	}else{
		$.confirm({
            title : 'Confirmation',
            content : 'Quote has been updated, clear screen?',
            buttons: {
                Ok : {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function(){
                        var sellingData = new FormData();
						sellingData.append('items', items);
						sellingData.append('itemTypes', itemTypes);
						sellingData.append('qtys', qtys);
						sellingData.append('costs', costs);
						sellingData.append('discs', discs);
						sellingData.append('taxs', taxs);
						sellingData.append('ItemNote', ItemNote);
						sellingData.append('sales_amount', sales_amount);
						sellingData.append('customer', customer);
						sellingData.append('InvoiceNum', InvoiceNum);
						sellingData.append('SalesID', SalesID);
						sellingData.append('salesDetailsIDs', salesDetailsIDs);
						sellingData.append('purchaseOrder', purchaseOrder);
						sellingData.append('RequestTo', 'UpdateQuote');
						sellingData.append('action', 'update_quote');
						$.ajax({
							'url'			: 'requests/actions.php',
					        'type'			: 'post',
					        'dataType'		: 'JSON',
					        'cache'			: false,
					        'contentType'	: false,
					        'processData'	: false,
					        'data'			: sellingData,
					        beforeSend: function() {
					        },
					        success: function(data){
					        	if(data.response === true){
				        			window.location.href = data.url;
				        		}
					        },
						});        
                    },     
                },
                Cancel : {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
	}
});

//Open payment modal
$("#do-sell, #update-tender").on('click', function(){
	var customer = $("#customer-selling-form #customer").val();
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	if(customer == ''){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select a customer',
	    });
	}else if(items.length <= 0){
		$.alert({
	        title: 'Alert!',
	        content: 'Please select an item',
	    });
	}else if(qtys.length <= 0){
		$.alert({
	        title: 'Alert!',
	        content: 'Please input some quantity',
	    });
	}else{
		$("#selling-payment").modal("show");
		if(customer == 1){
			$("#selling-payment #ha").hide();
		}else{
			$("#selling-payment #ha").show();
		}
	}
});

//Get upto 2 decimal value with rounding
function Dec2(num) {
  	num = String(num);
  	if(num.indexOf('.') !== -1) {
    	var numarr = num.split(".");
	    if (numarr.length == 1) {
	      	return Number(num);
	    }
	    else {
	      	return Number(numarr[0]+"."+numarr[1].charAt(0)+numarr[1].charAt(1));
	    }
  	}
  	else {
    	return Number(num);
  	}  
}

function SetCashAmount(e){
	var ThisVal = $(e).val();
	var NTB = $("#net-total-block");
	var dueVal = $("#loop-sales-due").val();
	var TOT = '';
	if(dueVal && typeof dueVal !== 'undefined'){
		TOT = dueVal;
	}else{
		TOT = $("#customer-selling-form #selling-total").text().replace("$", "");
		if(NTB.is(':visible')){
			TOT = $("#net-total").text().replace("$", "");
		}
	}
	var CtmVal = 0.00;
	if(ThisVal){
		CtmVal = parseFloat(ThisVal) - parseFloat(TOT);
	}
	var Dec2Val = Dec2(CtmVal);
	$("#selling-payment #ctm").text('$'+Dec2Val);

	$("#selling-payment #ctm").css({ 'color' : '#333' });
	if(Dec2Val > 0){
		$("#selling-payment #ctm").css({ 'color' : '#228B22' });
	}
	if(Dec2Val < 0){
		$("#selling-payment #ctm").css({ 'color' : '#FF0000' });
	}
}

function SetLoopCashAmount(e){
	var ThisVal = $(e).val();
	var NTB = $("#net-total-block");
	var TOT = $("#selling-payment-loop #loop-sales-due").val();
	var CtmVal = 0.00;
	if(ThisVal){
		CtmVal = parseFloat(ThisVal) - parseFloat(TOT);
	}
	var Dec2Val = Dec2(CtmVal);
	$("#selling-payment-loop #ctm").text('$'+Dec2Val);

	$("#selling-payment-loop #ctm").css({ 'color' : '#333' });
	if(Dec2Val > 0){
		$("#selling-payment-loop #ctm").css({ 'color' : '#228B22' });
	}
	if(Dec2Val < 0){
		$("#selling-payment-loop #ctm").css({ 'color' : '#FF0000' });
	}
}

function SalesNetAmount(){
	var SNetAmt = $('#selling-payment .samt').map(function() {
		if(this.value != ''){
			return this.value;
		}
	}).get();
	var NetAmt = SNetAmt.reduce(function(accumulator, currentvalue){
		return parseFloat(accumulator)+parseFloat(currentvalue);
	},0);
	$("#selling-payment #cmg").val(Dec2(NetAmt));
	var TOT = $("#customer-selling-form #selling-total").text().replace("$", "");
	$("#selling-payment #ctm").text(Dec2(parseFloat(NetAmt) - parseFloat(TOT)));
}

//Making Sale
$("#complete-sale").on('click', function(){
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var itemTypes = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			itemTypes.push($(this).find('option:selected').attr('data-type'));
		}
	});
	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	var costs = [];
	$("#customer-selling-form .cost").each(function(){
		if($(this).val() != ''){
			costs.push($(this).val());
		}
	});
	var discs = [];
	$("#customer-selling-form .disc").each(function(){
		var discsVal = $(this).val();
		//discsVal = discsVal ? discsVal : 0;
		discsVal = discsVal.replace('-','');
		if(discsVal != ''){
			discs.push(discsVal);
		}else{
			discs.push('0');
		}
	});
	var taxs = [];
	$("#customer-selling-form .tax").each(function(){
		if($(this).val() != ''){
			taxs.push($(this).val());
		}
	});
	var ItemNote = [];
	$("#customer-selling-form .note").each(function(){
		var noteVal = $(this).val();
		noteVal = noteVal ? noteVal : 'NULL';
		ItemNote.push(noteVal);
	});
	var payment_method = $("#selling-payment .selling-payment-method.active").attr('id');
	var pm_name = $("#selling-payment .selling-payment-method.active").text();
	var CalculatedTOT = $("#customer-selling-form #selling-total").text().replace('$', '');
	var EnteredTOT = $("#selling-payment #cmg").val().replace('$', '');
	var paid_amount = '';
	if($('#net-total-block').is(':hidden')){
		paid_amount = $("#customer-selling-form #selling-total").text();
	}else{
		paid_amount = $("#net-total").text();
	}
	paid_amount = paid_amount.replace('$', '');

	var ctm = $("#selling-payment #ctm").text();
	//For Check
	var _check_no = $("#selling-payment #_check_no").val();
	//For Card
	var _5digit = $("#selling-payment #_5digit").val();
	var _auth_code = $("#selling-payment #_auth_code").val();
	var _receipt_total = '';
	if(pm_name =='Cash'){
		_receipt_total = $("#selling-payment #cmg").val();
	}else if(pm_name =='Check'){
		_receipt_total = $("#selling-payment #_check_amt").val();
	}else{
		_receipt_total = $("#selling-payment #_receipt_total").val();
	}
	var customer = $("#customer-selling-form #customer").val();
	var purchaseOrder = $("#customer-selling-form #purchase-order").val();
	var note = '';
	var sellingData = new FormData();
	sellingData.append('items', items);
	sellingData.append('itemTypes', itemTypes);
	sellingData.append('qtys', qtys);
	sellingData.append('costs', costs);
	sellingData.append('discs', discs);
	sellingData.append('taxs', taxs);
	sellingData.append('ItemNote', ItemNote);
	sellingData.append('paid_amount', paid_amount);
	sellingData.append('ctm', ctm);
	sellingData.append('customer', customer);
	sellingData.append('purchaseOrder', purchaseOrder);
	sellingData.append('payment_method', payment_method);
	sellingData.append('_check_no', _check_no);
	sellingData.append('_5digit', _5digit);
	sellingData.append('_auth_code', _auth_code);
	sellingData.append('_receipt_total', _receipt_total);
	sellingData.append('pm_name', pm_name);

	if(pm_name == 'cash' || pm_name == 'Cash'){
		note = 'Payment Type : '+pm_name+' | Cash Amount Given : '+paid_amount+' | Change to Customer : '+ctm;
	}
	if(pm_name == 'check' || pm_name == 'Check'){
		note = 'Payment Type : '+pm_name+' | Check No : '+_check_no;
	}
	if(pm_name == 'Visa' || pm_name == 'Mastercard' || pm_name == 'Discover' || pm_name == 'American Express'){ 
		note = 'Payment Type : '+pm_name+' | Last 5 Digit : '+_5digit+' | Authentication Code : '+_auth_code+' | Credit Card Receipt Total : '+_receipt_total;
	}
	sellingData.append('note', note);
	sellingData.append('action', 'making_sale');

	if( items.length > 0 && qtys.length > 0 ){
		if(pm_name == 'Cash' && parseFloat(CalculatedTOT) > parseFloat(EnteredTOT)){
			alert('You entered an amount less than what is owed.  Please enter the proper amount or choose a different payment method.');
			return false;
		}
		if(customer != ''){
			$.ajax({
				'url'			: 'requests/actions.php',
		        'type'			: 'post',
		        'dataType'		: 'JSON',
		        'cache'			: false,
		        'contentType'	: false,
		        'processData'	: false,
		        'data'			: sellingData,
		        beforeSend: function() {
		      
		        },
		        success: function(data){
		        	if(data.response === true){
		        		if(data.cont){
		        			$("#auto-sales-receipt").html(data.cont);
		        		}
		        		if(data.PaymentLoop == true){
		        			$("#selling-payment").trigger('click');
		        			setTimeout(function(){
		        				$("#selling-payment-loop").modal('show');
		        				$("#loop-pm-details").html(data.lpmd);
		        				$("#selling-payment").modal('hide');
		        			});
		        			$("#selling-payment-loop #loop-sales-id").val(data.SalesID);
		        			$("#selling-payment-loop #loop-sales-due").val(data.due);
		        		}else{
		        			printDiv('auto-sales-receipt');
		        			window.location.href = data.url;
		        		}
		        	}
		        },
			});
		}else{
			$.alert({
		        title: 'Alert!',
		        content: 'Please select a customer first',
		    });
		}
	}else{
		if(items.length < 1){
			$.alert({
		        title: 'Alert!',
		        content: 'Please select an item',
		    });
			//alert('Please select an item');
		}else if(qtys.length < 1){
			$.alert({
		        title: 'Alert!',
		        content: 'Please input some quantity',
		    });
		}
	}
});


//Payment Loop
function SalesPaymentLoop(){
	var PM = $("#selling-payment-loop .selling-payment-method.active");
	var PMType = PM.attr('data-card');
	var PMName = PM.text();
	var PMID = PM.attr('id');
	var ctm = $("#selling-payment-loop #ctm").text();
	//For Check
	var _check_no = $("#selling-payment-loop #_check_no").val();
	//For Card
	var _5digit = $("#selling-payment-loop #_5digit").val();
	var _auth_code = $("#selling-payment-loop #_auth_code").val();
	var _receipt_total = '';
	if(PMName =='Cash'){
		_receipt_total = $("#selling-payment-loop #cmg").val();

		if(_receipt_total == ''){
			alert('Please enter Cash Amount Given');
			$("#selling-payment-loop #cmg").focus();
			return false;
		}
	}else if(PMName =='Check'){
		_receipt_total = $("#selling-payment-loop #_check_amt").val();
		if(_receipt_total == ''){
			alert('Please enter Input Amount');
			$("#selling-payment-loop #_check_amt").focus();
			return false;
		}
	}else{
		_receipt_total = $("#selling-payment-loop #_receipt_total").val();

		if(_receipt_total == ''){
			alert('Please enter Receipt Total');
			$("#selling-payment-loop #_receipt_total").focus();
			return false;
		}
	}
	var SalesID = $("#loop-sales-id").val();
	var requestFrom = $("#loop-sales-rf").val();
	if(SalesID != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'data'			: { 'action' : 'sales_payment_loop', 'SalesID' : SalesID, 'PMType' : PMType, 'PMName' : PMName, 'PMID' : PMID, '_check_no' : _check_no, '_5digit' : _5digit, '_auth_code' : _auth_code, '_receipt_total' : _receipt_total, 'ctm' : ctm, 'requestFrom' : requestFrom },
	        beforeSend: function() {
	        },
	        success: function(data){
	        	if(data.PaymentLoop == true){
	        		$("#selling-payment-loop .selling-payment-method").removeClass('active');
	        		$("#selling-payment-loop ._p_m").hide();
	        		$("#selling-payment-loop .close").trigger('click');
	    			$("#selling-payment-loop").modal('show');
	    			$("#loop-pm-details").html(data.lpmd);
	    			$("#selling-payment-loop #loop-sales-id").val(data.SalesID);
	    			$("#selling-payment-loop #loop-sales-due").val(data.due);
	    			$("#selling-payment-loop #cmg, #selling-payment-loop #_check_no, #selling-payment-loop #_check_amt, #selling-payment-loop #_5digit, #selling-payment-loop #_auth_code, #selling-payment-loop #_receipt_total").val('');
	    			$("#selling-payment-loop #ctm").text('');
	    		}else{
	    			if(data.pmhtml){
	    				$("#auto-sales-receipt #pm-block").html(data.pmhtml);
	    			}
	    			$("#selling-payment-loop .close").trigger('click');
	    			
	    			$("#selling-payment-loop").modal('hide');
	    			if(data.requestFrom && data.requestFrom == 'sales-details'){
	    				$("#pm-block").html(data.pmhtml);
	    				printDiv('print-sd');
	    				location.reload();
	    			}else{
	    				printDiv('auto-sales-receipt');
	    				window.location.href = data.url;
	    			}
	    		}
	        },
		});
	}
}

//Sale Quote Items
$("#sell-quote-item").on('click', function(){
	var salesID = $(this).attr('data-id');
	var invoice = $("#customer-selling-form #quote-invoice").attr('data-invoice');
	var salesDetailsIDs = [];
	$("#customer-selling-form .quote-item").each(function(){
		salesDetailsIDs.push(this.id);
	});
	var items = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			items.push($(this).val());
		}
	});
	var itemTypes = [];
	$("#customer-selling-form .item").each(function(){
		if($(this).val() != ''){
			itemTypes.push($(this).find('option:selected').attr('data-type'));
		}
	});
	var qtys = [];
	$("#customer-selling-form .qty").each(function(){
		if($(this).val() != ''){
			qtys.push($(this).val());
		}
	});
	var costs = [];
	$("#customer-selling-form .cost").each(function(){
		if($(this).val() != ''){
			costs.push($(this).val());
		}
	});
	var discs = [];
	$("#customer-selling-form .disc").each(function(){
		if($(this).val() != ''){
			discs.push($(this).val());
		}
	});
	var taxs = [];
	$("#customer-selling-form .tax").each(function(){
		if($(this).val() != ''){
			taxs.push($(this).val());
		}
	});
	var ItemNote = [];
	$("#customer-selling-form .note").each(function(){
		var noteVal = $(this).val();
		noteVal = noteVal ? noteVal : 'NULL';
		ItemNote.push(noteVal);
	});
	var payment_method = $("#selling-payment .selling-payment-method.active").attr('id');
	var pm_name = $("#selling-payment .selling-payment-method.active").text();
	var CalculatedTOT = $("#customer-selling-form #selling-total").text().replace('$', '');
	var EnteredTOT = $("#selling-payment #cmg").val().replace('$', '');
	var paid_amount = '';
	if($('#net-total-block').is(':hidden')){
		paid_amount = $("#customer-selling-form #selling-total").text();
	}else{
		paid_amount = $("#net-total").text();
	}
	paid_amount = paid_amount.replace('$', '');
	var ctm = $("#selling-payment #ctm").text();
	//For Check
	var _check_no = $("#selling-payment #_check_no").val();
	//For Card
	var _5digit = $("#selling-payment #_5digit").val();
	var _auth_code = $("#selling-payment #_auth_code").val();
	var _receipt_total = '';
	if(pm_name =='Cash'){
		_receipt_total = $("#selling-payment #cmg").val();
	}else if(pm_name =='Check'){
		_receipt_total = $("#selling-payment #_check_amt").val();
	}else{
		_receipt_total = $("#selling-payment #_receipt_total").val();
	}
	var customer = $("#customer-selling-form #customer").val();
	var note = '';
	var customer = $("#customer-selling-form #customer").val();
	var employee_id = $("#customer-selling-form #current-employee-id").val();
	if(pm_name == 'cash' || pm_name == 'Cash'){
		note = 'Payment Type : '+pm_name+' | Cash Amount Given : '+paid_amount+' | Change to Customer : '+ctm;
	}
	if(pm_name == 'check' || pm_name == 'Check'){
		note = 'Payment Type : '+pm_name+' | Check No : '+_check_no;
	}
	if(pm_name == 'Visa' || pm_name == 'Mastercard' || pm_name == 'Discover' || pm_name == 'American Express'){ 
		note = 'Payment Type : '+pm_name+' | Last 5 Digit : '+_5digit+' | Authentication Code : '+_auth_code+' | Credit Card Receipt Total : '+_receipt_total;
	}
	var purchaseOrder = $("#customer-selling-form #purchase-order").val();
	var sellingData = new FormData();
	sellingData.append('salesDetailsIDs', salesDetailsIDs);
	sellingData.append('items', items);
	sellingData.append('itemTypes', itemTypes);
	sellingData.append('qtys', qtys);
	sellingData.append('costs', costs);
	sellingData.append('discs', discs);
	sellingData.append('taxs', taxs);
	sellingData.append('ItemNote', ItemNote);
	sellingData.append('is_paid', 1);
	sellingData.append('paid_amount', paid_amount);
	sellingData.append('customer', customer);
	sellingData.append('payment_method', payment_method);
	sellingData.append('pm_name', pm_name);
	sellingData.append('employee_id', employee_id);
	sellingData.append('salesID', salesID);
	sellingData.append('invoice', invoice);
	sellingData.append('note', note);
	sellingData.append('ctm', ctm);
	sellingData.append('_check_no', _check_no);
	sellingData.append('_5digit', _5digit);
	sellingData.append('_auth_code', _auth_code);
	sellingData.append('_receipt_total', _receipt_total);
	sellingData.append('purchaseOrder', purchaseOrder);
	sellingData.append('action', 'update_invoice');

	if(pm_name == 'Cash' && parseFloat(CalculatedTOT) > parseFloat(EnteredTOT)){
		alert('You entered an amount less than what is owed.  Please enter the proper amount or choose a different payment method.');
		return false;
	}
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: sellingData,
        beforeSend: function() {
      
        },
        success: function(data){
        	if(data.response === true){
        		if(data.cont){
        			$("#auto-sales-receipt").html(data.cont);
        		}
        		if(data.PaymentLoop == true){
        			$("#loop-pm-details").html(data.lpmd);
        			$("#selling-payment-loop").modal('show');
        			$("#selling-payment").modal('hide');
        			$("#selling-payment-loop #loop-sales-id").val(data.SalesID);
        			$("#selling-payment-loop #loop-sales-due").val(data.due);
        		}else{
        			printDiv('auto-sales-receipt');
        			window.location.href = data.url;
        		}



        	}
        },
	});
});

//Onchange sales customer
function getSalesCustomer(e){
	var Customer_ID = $(e).val();
	var pcid = $("#customer-selling-form #hiddn-customer-id").val();
	var CustomerName = $("#customer-selling-form #customer-name").val();
	var Item = $("#customer-selling-form select.item").map(function(){
		if(this.value != ''){
			return this.value;
		}
	}).get();
	console.log(Item.length);
	if(Item.length > 0){
		$.confirm({
			title : 'Confirmation',
			content : 'Are you sure you want to change the customer?',
			buttons : {
				confirm : {
					text : 'Ok',
					btnCLass : 'btn-blue',
					action : function(){
						if(Customer_ID == ''){
							$("#price-list-name").text('');
							$("#price-list-name").removeAttr('style');
						}
						var SalesCustoerData = new FormData();
						SalesCustoerData.append('Customer_ID', Customer_ID);
						SalesCustoerData.append('action', 'get_sales_customer_data');
						$.ajax({
							'url'			: 'requests/actions.php',
							'type'			: 'post',
							'dataType'		: 'json',
							'cache'			: false,
							'contentType'	: false,
							'processData'	: false,
							'data'			: SalesCustoerData,
							beforeSend		: function(){
							},
							success 		: function(data){
											$("#customer-selling-form #customer-name").val(data.customer_name);
											$("#customer-selling-form #hiddn-customer-id").val(data.cid);
											if(data.Url){
												$.confirm({
									                title : 'Confirmation',
									                content : 'Customer has pending quotes. Would you like to see them now?',
									                buttons: {
								                        Ok : {
								                                text: 'Ok',
								                                btnClass: 'btn-blue',
								                                action: function(){
								                                       window.location.href = data.Url;        
								                                },     
								                        },
								                        Cancel : {
								                                text: 'Cancel',
								                                btnClass: 'btn-red',
								                        }
									                }
										        });
											}
											if(data.pl){
												$("#price-list-name").text(data.pl);
												$("#price-list-name").css({'background-color' : data.plcolor, 'width' : '100%', 'color' : '#fff', 'text-align' : 'center', 'padding' : '3px', 'height' : '35px', 'font-size' : '24px', 'margin' : '0',});
												$("#invoice-block .up-box").css({'background-color' : data.plcolor});
											}else{
												$("#price-list-name").text('');
												$("#price-list-name").removeAttr('style');
												$("#invoice-block .up-box").css({'background-color' : '#4b77d2'});
											}
											switch(data.taxExempt){
												case true :
													$("#tax-exempt-word").text("Tax Exempt");
													$("#tax-exempt-word").css({'background-color' : '#f48b1b', 'width' : '100%', 'color' : '#fff', 'text-align' : 'center', 'padding' : '3px', 'height' : '35px', 'font-size' : '24px', 'margin' : '0',});
													$(e).closest('form').find('.tax').val(0);
													break;
												case false :
													$("#tax-exempt-word").text("");
													$("#tax-exempt-word").removeAttr('style');
													$("label[for=tax]:eq(0)").html('Tax');
													break;
											}
							}
						});
					}
				},
				cancel : {
					text : 'Cancel',
					btnClass : 'btn-red',
					action : function(){
						$(e).selectpicker('val', pcid);
					}
				}
			}
		});
	}else{
		if(Customer_ID == ''){
			$("#price-list-name").text('');
			$("#price-list-name").removeAttr('style');
		}
		var SalesCustoerData = new FormData();
		SalesCustoerData.append('Customer_ID', Customer_ID);
		SalesCustoerData.append('action', 'get_sales_customer_data');
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: SalesCustoerData,
			beforeSend		: function(){
			},
			success 		: function(data){
								$("#customer-selling-form #customer-name").val(data.customer_name);
								$("#customer-selling-form #hiddn-customer-id").val(data.cid);
								if(data.Url){
									$.confirm({
						                title : 'Confirmation',
						                content : 'Customer has pending quotes. Would you like to see them now?',
						                buttons: {
					                        Ok : {
					                                text: 'Ok',
					                                btnClass: 'btn-blue',
					                                action: function(){
					                                       window.location.href = data.Url;        
					                                },     
					                        },
					                        Cancel : {
					                                text: 'Cancel',
					                                btnClass: 'btn-red',
					                        }
						                }
							        });
								}
								if(data.pl){
									$("#price-list-name").text(data.pl);
									$("#price-list-name").css({'background-color' : data.plcolor, 'width' : '100%', 'color' : '#fff', 'text-align' : 'center', 'padding' : '3px', 'height' : '35px', 'font-size' : '24px', 'margin' : '0',});
									$("#invoice-block .up-box").css({'background-color' : data.plcolor});

								}else{
									$("#price-list-name").text('');
									$("#price-list-name").removeAttr('style');
									$("#invoice-block .up-box").css({'background-color' : '#4b77d2'});
								}
								switch(data.taxExempt){
									case true :
										$("#tax-exempt-word").text("Tax Exempt");
										$("#tax-exempt-word").css({'background-color' : '#f48b1b', 'width' : '100%', 'color' : '#fff', 'text-align' : 'center', 'padding' : '3px', 'height' : '35px', 'font-size' : '24px', 'margin' : '0',});
										$(e).closest('form').find('.tax').val(0);
										break;
									case false :
										$("#tax-exempt-word").text("");
										$("#tax-exempt-word").removeAttr('style');
										$("label[for=tax]:eq(0)").html('Tax');
										break;
								}
			}
		});
	}
	if(Customer_ID != ''){
		$(e).attr('data-previous', '1');
	}else{
		$(e).removeAttr('data-previous');
	}
}

//Get discount value
function GetDiscValFromPercentVal($cost, $percent){
	var DiscValue = parseFloat($cost)*(parseFloat($percent)/100);
	return DiscValue;
}

//Discount percentage
function DiscPercentage(){
	var DiscPercentageOBJ = $(".disc-percentage");
	DiscPercentageOBJ.each(function(){
		$(this).on('click', function(){
			var PercentageVal = parseFloat($(this).data('percentage'));
			var TotalValue = $('#selling-total').text().replace('$', '').replace(',', '');
			TotalValue = parseFloat($.trim(TotalValue));
			var DiscountValue = GetDiscValFromPercentVal(TotalValue, PercentageVal);
			var NetTotal = TotalValue - DiscountValue;
			var CPL = $("#price-list-name").text();
			if(CPL.length != ''){
				$.confirm({
			        title : 'Confirmation',
			        content : 'Customer already has a special price list. Do you want to continue with more discounts?',
			        buttons: {
		                Ok : {
	                        text: 'CONFIRM',
	                        btnClass: 'btn-blue',
	                        action: function(){
	                           if(TotalValue > 0){
									DiscPercentageOBJ.removeClass('active');
									$(this).addClass('active');
									$('#disc-over-total-block').show();
									$('#disc-over-total').text('$ '+DiscountValue.toFixed(2));
									$('#net-total-block').show();
									$('#net-total').text('$ '+NetTotal.toFixed(2));
								}        
	                        },     
		                },
		                Cancel : {
		                        text: 'Cancel',
		                        btnClass: 'btn-red',
		                }
			        }
			    });
			}else{
				if(TotalValue > 0){
					DiscPercentageOBJ.removeClass('active');
					$(this).addClass('active');
					$('#disc-over-total-block').show();
					$('#disc-over-total').text('$ '+DiscountValue.toFixed(2));
					$('#net-total-block').show();
					$('#net-total').text('$ '+NetTotal.toFixed(2));
				}
			}			
		});
	});
}
DiscPercentage();

//Clear all sales discount
function ClearAllDiscounts(){
	//alert('clear');
	$.confirm({
        title : 'Confirmation',
        content : 'Do you want to celar all the discounts?',
        buttons: {
                Ok : {
                        text: 'Ok',
                        btnClass: 'btn-blue',
                        action: function(){
                           $('#customer-selling-form .disc').each(function(){
								$(this).val('');
								getItemNetPrice(this);
							});
							$("#disc-over-total-block").hide();
							$("#net-total-block").hide();
							$(".disc-percentage").removeClass('active');        
                        },     
                },
                Cancel : {
                        text: 'Cancel',
                        btnClass: 'btn-red',
                }
        }
    });	
}

//Modify Sales extra discount percentage
function ModifyDiscPercent(){
	$('.modify-disc-percent').each(function(){
		$(this).on('click', function(){
			var RequestTo = $(this).data('request');
			$.ajax({
				'url' 		: 'requests/actions.php',
				'type' 		: 'post',
				'dataType' 	: 'json',
				'cache' 	: false,
				'data' 		: {'action' : 'modify_sales_discount_over_total', 'RequestTo' : RequestTo},
				beforeSend 	: function(){

				},
				success 	: function(Response){
							if(Response.permission === false){
								$("#manager-approval-popup").modal('show');
							}
							if(Response.permission === true){
								if(Response.request == 'Remove'){
									$("#disc-over-total-block").hide();
									$("#net-total-block").hide();
									$(".disc-percentage").removeClass('active');
									getItemNetPrice(this);
								}
							}
				}
			});
		});
	});
}
ModifyDiscPercent();


/*===============================================*/
			//Warehouse Start
/*===============================================*/
/*=================Add Warehouse=================*/
$("#add-new-warehouse").on('click', function(e){
	e.preventDefault();
	var WH_Name 		= $("#add-warehouse-form #house-name").val();
	var WH_Address 		= $("#add-warehouse-form #house-address").val();
	var WH_City 		= $("#add-warehouse-form #house-city").val();
	var WH_State 		= $("#add-warehouse-form #house-state").val();
	var WH_Zip 			= $("#add-warehouse-form #house-zip").val();
	var WH_Phone 		= $("#add-warehouse-form #house-phone").val();
	var NewWareHouse 	= new FormData();
	NewWareHouse.append('WH_Name', WH_Name);
	NewWareHouse.append('WH_Address', WH_Address);
	NewWareHouse.append('WH_City', WH_City);
	NewWareHouse.append('WH_State', WH_State);
	NewWareHouse.append('WH_Zip', WH_Zip);
	NewWareHouse.append('WH_Phone', WH_Phone);
	NewWareHouse.append('action', 'add_warehouse');
	if($.trim(WH_Name) != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: NewWareHouse,
			beforeSend		: function(){
			},
			success 		: function(data){
								switch(data.response){
									case true :
										$("#warehouse-table tbody").append(data.row);
										$('#warehouse-table').DataTable();
										$("#add-warehouse-form")[0].reset();
										alert("Warehouse successfully added");
										break;
									case false :
										//
										break;
								}
								
			}
		});
	}else{
		$.alert({
	        title: 'Alert!',
	        content: 'You must enter a warehouse name',
	    });
		$("#add-warehouse-form #house-name").val('');
		$("#add-warehouse-form #house-name").focus();
	}
});

/*================Edit Warehouse================*/
function editWareHouse(e){
	var WH_ID = $(e).attr('data-id');
	var WareHouseData = new FormData();
	WareHouseData.append('WH_ID', WH_ID);
	WareHouseData.append('action', 'edit_warehouse');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: WareHouseData,
        beforeSend		: function() {
      		
        },
        success 		: function(data){
        				$("#edit-warehouse-form #house-name").val(data.WH_Name);
        				$("#edit-warehouse-form #house-address").val(data.WH_Address);
        				$("#edit-warehouse-form #house-city").val(data.WH_City);
        				$("#edit-warehouse-form #house-state").val(data.WH_State).prop('selected', true);
        				$("#edit-warehouse-form #house-zip").val(data.WH_Zip);
        				$("#edit-warehouse-form #house-phone").val(data.WH_Phone);
        				$("#edit-warehouse-form #update-warehouse").attr('data-id', data.WH_ID);
        				$("#edit-warehouse").modal('show');
        },
	});
}

/*================Update Warehouse================*/
$("#update-warehouse").on('click', function(e){
	e.preventDefault();
	var WH_Name 		= $("#edit-warehouse-form #house-name").val();
	var WH_Address 		= $("#edit-warehouse-form #house-address").val();
	var WH_City 		= $("#edit-warehouse-form #house-city").val();
	var WH_State 		= $("#edit-warehouse-form #house-state").val();
	var WH_Zip 			= $("#edit-warehouse-form #house-zip").val();
	var WH_Phone 		= $("#edit-warehouse-form #house-phone").val();
	var WH_ID 			= $(this).attr('data-id');
	var WareHouseData 	= new FormData();
	WareHouseData.append('WH_Name', WH_Name);
	WareHouseData.append('WH_Address', WH_Address);
	WareHouseData.append('WH_City', WH_City);
	WareHouseData.append('WH_State', WH_State);
	WareHouseData.append('WH_Zip', WH_Zip);
	WareHouseData.append('WH_Phone', WH_Phone);
	WareHouseData.append('WH_ID', WH_ID);
	WareHouseData.append('action', 'update_warehouse');
	if($.trim(WH_Name) != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: WareHouseData,
			beforeSend		: function(){
			},
			success 		: function(data){
								switch(data.response){
									case true :
										$("#warehouse-table tbody tr#row-"+data.WH_ID).html(data.row);
										$('#warehouse-table').DataTable();
										$("#edit-warehouse").modal('hide');
										$.alert("Warehouse details successfully updated");
										break;
									case false :
										//
										break;
								}
								
			}
		});
	}else{
		$.alert({
	        title: 'Alert!',
	        content: 'You must enter a warehouse name',
	    });
		$("#edit-warehouse-form #house-name").val('');
		$("#edit-warehouse-form #house-name").focus();
	}
});

/*===============================================*/
			//Warehouse End
/*===============================================*/


/*===============================================*/
			//Shelves Start
/*===============================================*/
/*=================Add Shelf=================*/

$("#add-new-shelf").on('click', function(e){
	e.preventDefault();
	var Shelf_Name 		= $("#add-shelf-form #shelf-name").val();
	var Shelf_Location 	= $("#add-shelf-form #shelf-location").val();
	var Shelf_Warehouse = $("#add-shelf-form #warehouse").val();

	var NewShelf 	= new FormData();
	NewShelf.append('Shelf_Name', Shelf_Name);
	NewShelf.append('Shelf_Location', Shelf_Location);
	NewShelf.append('Shelf_Warehouse', Shelf_Warehouse);
	NewShelf.append('action', 'add_shelf');
	if($.trim(Shelf_Name) != '' && $.trim(Shelf_Warehouse) != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: NewShelf,
			beforeSend		: function(){
			},
			success 		: function(data){
								switch(data.response){
									case true :
										$("#shelf-table tbody").append(data.row);
										$('#shelf-table').DataTable();
										$("#add-shelf-form")[0].reset();
										$.alert({
									        title: 'Alert!',
									        content: 'Shelf successfully added',
									    });
										//alert("Shelf successfully added");
										break;
									case false :
										//
										break;
								}
								
			}
		});
	}else{
		if($.trim(Shelf_Name) == ''){
			$.alert("You must enter a shelf name");
			$("#add-shelf-form #shelf-name").val('');
			$("#add-shelf-form #shelf-name").focus();
		}
		else if($.trim(Shelf_Warehouse) == ''){
			$.alert("You must select a warehouse");
			$("#add-shelf-form #warehouse").focus();
		}
	}
});

/*================Edit Shelf================*/
function editShelf(e){
	var Shelf_ID = $(e).attr('data-id');
	var ShelfData = new FormData();
	ShelfData.append('Shelf_ID', Shelf_ID);
	ShelfData.append('action', 'edit_shelf');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'contentType'	: false,
        'processData'	: false,
        'data'			: ShelfData,
        beforeSend		: function() {
      		
        },
        success 		: function(data){
        				$("#edit-shelf-form #shelf-name").val(data.Shelf_Name);
        				$("#edit-shelf-form #shelf-location").val(data.Shelf_Location);
        				$("#edit-shelf-form #warehouse option").each(function(){
        					$(this).removeAttr('selected');
        				});
        				$("#edit-shelf-form #wh-dropdown").html(data.Shelf_Warehouse);
        				SdSelection();
        				$("#edit-shelf-form #update-shelf").attr('data-id', data.Shelf_ID);
        				$("#edit-shelf").modal('show');
        },
	});
}

/*================Update Shelf================*/
$("#update-shelf").on('click', function(e){
	e.preventDefault();
	var Shelf_Name 		= $("#edit-shelf-form #shelf-name").val();
	var Shelf_Location 	= $("#edit-shelf-form #shelf-location").val();
	var Shelf_Warehouse = $("#edit-shelf-form #warehouse").val();
	var Shelf_ID 		= $(this).attr('data-id');
	var ShelfData 	= new FormData();
	ShelfData.append('Shelf_Name', Shelf_Name);
	ShelfData.append('Shelf_Location', Shelf_Location);
	ShelfData.append('Shelf_Warehouse', Shelf_Warehouse);
	ShelfData.append('Shelf_ID', Shelf_ID);
	ShelfData.append('action', 'update_shelf');
	if($.trim(Shelf_Name) != '' && $.trim(Shelf_Warehouse) != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: ShelfData,
			beforeSend		: function(){
			},
			success 		: function(data){
								switch(data.response){
									case true :
										$("#shelf-table tbody tr#row-"+data.Shelf_ID).html(data.row);
										$('#shelf-table').DataTable();
										$("#edit-shelf").modal('hide');
										$.alert({
									        title: 'Alert!',
									        content: 'Shelf details successfully updated',
									    });
										break;
									case false :
										//
										break;
								}
								
			}
		});
	}else{
		if($.trim(Shelf_Name) == ''){
			$.alert({
		        title: 'Alert!',
		        content: 'You must enter a shelf name',
		    });
			$("#edit-shelf-form #shelf-name").val('');
			$("#edit-shelf-form #shelf-name").focus();
		}
		else if($.trim(Shelf_Warehouse) == ''){
			$.alert({
		        title: 'Alert!',
		        content: 'You must select a warehouse',
		    });
			$("#edit-shelf-form #warehouse").focus();
		}
	}
});

/*===============================================*/
			//Shelves End
/*===============================================*/




//Add Price List
$("#add-price-list").on('click', function(e){
	e.preventDefault();
	var NewPriceList = $("#price-list-form").serialize();
	var PL = $("#price-list-form #pl-name").val();
	var HasItem = $("#price-list-form #has_items_n");

	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: NewPriceList+'&'+$.param({'action' : 'add_price_list'}),
		beforeSend		: function(){
		},
		success 		: function(data){
						if(data.error){
							$.alert({
						        title: 'Alert!',
						        content: data.error,
						    });
							$("#price-list-form input[name=pl_name]").focus();
						}else{
							if(data.exist === true){
								$.alert({
							        title: 'Alert!',
							        content: data.return_msg,
							    });
								$("#price-list-form input[name=pl_name]").focus();
							}else{
								switch(data.resp){
									case true:
										$("#price-list-form")[0].reset();
										$("#new-price-list .close").trigger('click');
										$("#price-list-table tbody").append(data.tr);

										if(data.has_items == 1){

											$.confirm({
										        title : 'Confirmation',
										        content : 'Add items now?',
										        buttons: {
										                Ok : {
										                        text: 'Ok',
										                        btnClass: 'btn-blue',
										                        action: function(){
										                           $("#price-list-item").modal('show');
																	$("#price-list-item-form #plipl-dropdown").html(data.plDropDown);        
										                        },     
										                },
										                Cancel : {
										                        text: 'Cancel',
										                        btnClass: 'btn-red',
										                }
										        }
										    });
										}else{
											$.alert({
										        title: 'Alert!',
										        content: data.msg,
										    });
										}
										//location.reload();
										break;
									case false:
										$.alert({
									        title: 'Alert!',
									        content: data.msg,
									    });
										break;
								}
								SdSelection();
								SdTable();
							}
						}					
		}
	});
	
	
});

//Edit Price List
function editPriceList($this){
	var ListID = $this.getAttribute('data-id');
	var PriceListData = new FormData();
	PriceListData.append('ListID', ListID);
	PriceListData.append('action', 'edit_price_list');
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'contentType'	: false,
		'processData'	: false,
		'data'			: PriceListData,
		beforeSend		: function(){
		},
		success 		: function(data){
							$("#edit-pricelist-form #pl-name").val(data.PLName);
							if(data.PLIsPercentage == 1){
								$("#edit-pricelist-form #pl_is_percentage").prop('checked', true);
							}
							if(data.PLIsPercentage == 0){
								$("#edit-pricelist-form #pl_is_percentage").prop('checked', false);
							}

							if(data.PLHasItems == 1){
								$("#edit-pricelist-form #has_items").prop('checked', true);
							}
							if(data.PLHasItems == 0){
								$("#edit-pricelist-form #has_items").prop('checked', false);
							}
							$("#edit-pricelist-form #pl-amount").val(data.PLAmount);
							$("#edit-pricelist-form #plid").val(data.PLID);
							$("#edit-pricelist-form #pl-color").val(data.PLColor);

							$("#edit-pricelist-form #plcolor i").css({'background-color' : data.PLColor});

							$("#edit-pricelist").modal('show');
							SdSelection();
							SDColorPicker();
		}
	});
}


function MakeFormat($this){
	var pip = $($this).closest('form').find('.is_percentage');
	var amt = $($this).closest('form').find('.pl_amount');
	var plamt = amt.val();
	pipval = 0;
	if(pip.is(':checked')){
		pipval = 1;
	}
	if(plamt != ''){
		if(pipval == 1){
			plamt = plamt.replace('$', '').replace('%', '');
			amt.val(plamt+'%');
		}else{
			plamt = plamt.replace('%', '').replace('$', '');
			amt.val('$'+plamt);
		}
	}
	
}

//Update Price List
$("#update-pricelist").on('click', function(e){
	e.preventDefault();
	var PriceListData = $("#edit-pricelist-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: PriceListData+'&'+$.param({'action' : 'update_price_list'}),
		beforeSend		: function(){
		},
		success 		: function(data){
						if(data.error){
							$.alert({
						        title: 'Alert!',
						        content: data.error,
						    });
							$("#edit-pricelist-form input[name=pl_name]").focus();
						}
						if(data.message){
							$("#price-list-table tbody #"+data.plid).html(data.tr);
							$("#price-list-table tbody #"+data.plid).css({'background-color' : data.color});
							$("#edit-pricelist .close").trigger('click');


							if(data.has_items == 1){
								var HasItemConf = confirm("Add items now?");
								if(HasItemConf === true){
									$("#price-list-item").modal('show');
									$("#price-list-item-form #plipl-dropdown").html(data.plDropDown);
								}
							}else{
								$.alert({
							        title: 'Alert!',
							        content: data.message,
							    });
							}
							//location.reload();
						}
						SdSelection();
						SDColorPicker();			
		}
	});
});


//Add new price list item
function AddPriceListItem(){
	var NewListItem = $("#price-list-item-form").serialize();
	$.ajax({
		'url' 		: 	'requests/actions.php',
		'type' 		: 	'post',
		'dataType' 	: 	'json',
		'cache'		: 	false,
		'data'		: 	NewListItem+'&'+$.param({'action' : 'new_list_item'}),
		beforeSend  : 	function(){

		},
		success 	:  	function(data){
					if(data.error){
						$.alert({
					        title: 'Alert!',
					        content: data.error,
					    });
					}else{
						if(data.resp === true){
							$("#price-list-item-form")[0].reset();
							$("#price-list-item.modal .close").trigger('click');
							$("#price-list-item-table tbody").append(data.tr);
							$.alert({
						        title: 'Alert!',
						        content: data.message,
						    });
						}
						if(data.resp === false){
							$.alert({
						        title: 'Alert!',
						        content: data.message,
						    });
						}
					}
		}
	});
}

//Edit Price List Item
function editPriceListItem($this){
	var ListItemID = $this.getAttribute('data-id');
	var ListItemData = new FormData();
	ListItemData.append('ListItemID', ListItemID);
	ListItemData.append('action', 'edit_price_list_item');
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'contentType'	: false,
		'processData'	: false,
		'data'			: ListItemData,
		beforeSend		: function(){
		},
		success 		: function(data){
							$("#edit-pricelist-item-form #pl-dropdown").html(data.plDropDown);
							$("#edit-pricelist-item-form #item-dropdown").html(data.itemDropdown);
							if(data.PLIIsPercentage == 1){
								$("#edit-pricelist-item-form #is-percentage").prop('checked', true);
							}
							if(data.PLIIsPercentage == 0){
								$("#edit-pricelist-item-form #is-percentage").prop('checked', false);
							}
							$("#edit-pricelist-item-form #pl-amount").val(data.PLIAmount);
							$("#edit-pricelist-item-form #pliid").val(data.pliid);
							$("#edit-pricelist-item").modal('show');
							SdSelection();
		}
	});
}

//Update Price list item
function UpdatePriceListItem(){
	var ListItemData = $("#edit-pricelist-item-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: ListItemData+'&'+$.param({'action' : 'update_price_list_item'}),
		beforeSend		: function(){
		},
		success 		: function(data){
						if(data.error){
							alert(data.error);
						}
						if(data.message){
							$("#price-list-item-table tbody #"+data.pliid).html(data.tr);
							$("#edit-pricelist-item .close").trigger('click');
							alert(data.message);
							//location.reload();
						}
						SdSelection();				
		}
	});
}

/*=======Payment=======*/

$("#vendor_p_").on('change', function(){
	var $VID = $(this).val();
	var $VP_Data = new FormData();
	$VP_Data.append('action', 'get_the_vendor_payment_details');
	$VP_Data.append('VID', $VID);
	if($VID !='' && typeof $VID !== 'undefined'){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: $VP_Data,
			beforeSend		: function(){
			},
			success 		: function(data){
								if(data.content){
									$("#vc_p_d_").html(data.content);
								}else{
									$("#vc_p_d_").html('');
								}
								$("#customer_p_").selectpicker('val', '');
								PriceValidation();
								RestrictCCPD();
			}
		});
	}else{
		$("#vc_p_d_").html('');
	}
});

$("#customer_p_").on('change', function(){
	var $CID = $(this).val();
	var $CP_Data = new FormData();
	$CP_Data.append('action', 'get_the_customer_payment_details');
	$CP_Data.append('CID', $CID);
	if($CID !='' && typeof $CID !== 'undefined'){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: $CP_Data,
			beforeSend		: function(){
			},
			success 		: function(data){
								if(data.content){
									$("#vc_p_d_").html(data.content);
								}else{
									$("#vc_p_d_").html('');
								}
								$("#vendor_p_").selectpicker('val', '');
								PriceValidation();
								RestrictCCPD();
			}
		});
	}else{
		$("#vc_p_d_").html('');
	}
});

function SavePayment(){
	var Amts 	= [];
	var OIDs 	= [];
	var VID 	= $("#check-vendor-payment #vendor-save-payment").attr('data-vendorid');
	var VPName = $("#check-vendor-payment #vpm").children(":selected").text();
	$("#check-vendor-payment table tbody .pay-vendor").each(function(){
		if($(this).val() != ''){
			Amts.push($(this).val());
			OIDs.push($(this).attr('data-orderid'));
		}
	});
	var ExtraAmt = $("#check-vendor-payment #vendor-extra-payment").val();
	var VPM = $("#check-vendor-payment #vpm").val();
	var CheckNo = $("#check-vendor-payment #vendor-check-number").val();
	var Last5Digits = $("#check-vendor-payment #last5digits").val();
	var AuthCode = $("#check-vendor-payment #authcode").val();
	var Note = $("#check-vendor-payment #vp-note").val();
	var EmployeeID = $("#check-vendor-payment #current-employee-id").val();
	var $VP_Data = new FormData();
	$VP_Data.append('action', 'update_vendor_payment_details');
	$VP_Data.append('OIDs', OIDs);
	$VP_Data.append('VID', VID);
	$VP_Data.append('Amts', Amts);
	$VP_Data.append('VPM', VPM);
	$VP_Data.append('VPName', VPName);
	$VP_Data.append('ExtraAmt', ExtraAmt);
	$VP_Data.append('CheckNo', CheckNo);
	$VP_Data.append('Last5Digits', Last5Digits);
	$VP_Data.append('AuthCode', AuthCode);
	$VP_Data.append('Note', Note);
	$VP_Data.append('EmployeeID', EmployeeID);
	if(Amts.length > 0){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: $VP_Data,
			beforeSend		: function(){
				$("#check-vendor-payment #vendor-save-payment").text('Saving...');
			},
			success 		: function(data){
								if(data.content){
									$("#vc_p_d_").html(data.content);
								}else{
									$("#vc_p_d_").html('');
								}
								PriceValidation();
								RestrictCCPD();
								$("#vendor-pay-receipt").html(data.slip);
								$.confirm({
							        title : 'Confirmation',
							        content : 'Do you need receipt?',
							        buttons: {
							                Ok : {
							                        text: 'YES',
							                        btnClass: 'btn-blue',
							                        action: function(){
							                           printDiv('vendor-pay-receipt');        
							                        },     
							                },
							                Cancel : {
							                        text: 'NO',
							                        btnClass: 'btn-red',
							                }
							        }
							    });			
			}
		});
	}
}

function SaveCustomerPayment(){
	var Amts 	= [];
	var SIDs 	= [];
	var CID 	= $("#check-vendor-payment #vendor-save-payment").attr('data-customerid');
	var CPName = $("#check-vendor-payment #vpm").children(":selected").text();
	$("#check-vendor-payment table tbody .pay-vendor").each(function(){
		if($(this).val() != ''){
			Amts.push($(this).val());
			SIDs.push($(this).attr('data-salesid'));
		}
	});
	var CPM = $("#check-vendor-payment #vpm").val();
	var CheckNo = $("#check-vendor-payment #vendor-check-number").val();
	var Last5Digits = $("#check-vendor-payment #last5digits").val();
	var AuthCode = $("#check-vendor-payment #authcode").val();
	//var Note = $("#check-vendor-payment #vp-note").val();

	var EmployeeID = $("#check-vendor-payment #current-employee-id").val();
	var $CP_Data = new FormData();
	$CP_Data.append('action', 'update_customer_payment_details');
	$CP_Data.append('SIDs', SIDs);
	$CP_Data.append('CID', CID);
	$CP_Data.append('Amts', Amts);
	$CP_Data.append('CPM', CPM);
	$CP_Data.append('CPName', CPName);
	//$CP_Data.append('ExtraAmt', ExtraAmt);
	$CP_Data.append('CheckNo', CheckNo);
	$CP_Data.append('Last5Digits', Last5Digits);
	$CP_Data.append('AuthCode', AuthCode);
	//$CP_Data.append('Note', Note);
	$CP_Data.append('EmployeeID', EmployeeID);
	if(Amts.length > 0){
		$.ajax({
			'url'			: 'requests/actions.php',
			'type'			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'contentType'	: false,
			'processData'	: false,
			'data'			: $CP_Data,
			beforeSend		: function(){
				$("#check-vendor-payment #vendor-save-payment").text('Saving...');
			},
			success 		: function(data){
								if(data.content){
									$("#vc_p_d_").html(data.content);
								}else{
									$("#vc_p_d_").html('');
								}
								PriceValidation();
								RestrictCCPD();
								//var needSlip = confirm('Do you need receipt?');
								$("#vendor-pay-receipt").html(data.slip);
								$.confirm({
							        title : 'Confirmation',
							        content : 'Do you need receipt?',
							        buttons: {
							                Ok : {
							                        text: 'YES',
							                        btnClass: 'btn-blue',
							                        action: function(){
							                           printDiv('vendor-pay-receipt');        
							                        },     
							                },
							                Cancel : {
							                        text: 'NO',
							                        btnClass: 'btn-red',
							                }
							        }
							    });
								
			}
		});
	}
}

function GetVendorPaymentTotal(){
	var PayAmt = $('.pay-vendor').map(function() {
		if(this.value != ''){
			return this.value;
		}
	}).get();
	var TotalPayAmt = PayAmt.reduce(function(accumulator, currentvalue){
		return parseFloat(accumulator)+parseFloat(currentvalue);
	},0);
	var Extra = $("#check-vendor-payment #vendor-extra-payment").val();
	Extra = Extra?Extra:0;
	TotalPayAmt = TotalPayAmt + parseFloat(Extra);
	$("#vendor-total-payment").val(TotalPayAmt.toFixed(2));
}


function VpSet($this){
	var ThisPM = $($this).children(":selected").text();
	if(ThisPM == 'check' || ThisPM == 'Check'){
		$($this).closest('.vendor-extra').find('.card-data').hide();
		$($this).closest('.vendor-extra').find('.check-data').show();
	}else{
		if(ThisPM == 'cash' || ThisPM == 'Cash'){
			$($this).closest('.vendor-extra').find('.card-data').hide();
			$($this).closest('.vendor-extra').find('.check-data').hide();
		}else{
			$($this).closest('.vendor-extra').find('.card-data').show();
			$($this).closest('.vendor-extra').find('.check-data').hide();
		}
	}
}

/*===============================================*/
				/*Receiving Start*/
/*===============================================*/

//Receive Order
function ReceiveOrder($this){
	var RID = $($this).closest('tr').data('receive_id');
	var EID = $($this).data('current_empid');
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: {'action' : 'receive_order', 'RID' : RID, 'EID' : EID},
		beforeSend		: function(){
			
		},
		success 		: function(data){
							if(data.content){
								$('#receive-order-item').html(data.content);
								$("#receive-order-item-popup").modal('show');
								ReceiveOrderItemTable();
							}
							
		}
	});
}

//Receive Order Item
function ReceiveOrderItem(e){
	var ODID = $(e).closest('tr').data('odid');
	var ORDID = $(e).closest('tr').data('ordid');
	var ORID = $(e).closest('tr').data('orid');
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: {'action' : 'receive_order_item', 'ODID' : ODID, 'ORDID' : ORDID, 'ORID' : ORID},
		beforeSend		: function(){
			
		},
		success 		: function(data){
							if(data.content){
								$('#receive-item').html(data.content);
								$("#receive-item-popup").modal('show');
							}
							
		}
	});
}

function ReceiveItem($this){
	var ODID = $($this).data('odid');
	var ORDID = $($this).data('ordid');
	var ORID = $($this).data('orid');
	var RQty = $($this).closest('.receive-details').find('.receive-qty').val();
	var RNote = $($this).closest('.receive-details').find('.receive-note').val();
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: {'action' : 'receive_item', 'ODID' : ODID, 'ORDID' : ORDID, 'ORID' : ORID, 'RQty' : RQty, 'RNote' : RNote},
		beforeSend		: function(){
			
		},
		success 		: function(data){
				        	if(data.error){
				        		$("#receive-details-alert").show();
				        		$("#receive-details-alert").removeClass('alert-success');
				        		$("#receive-details-alert").addClass('alert-danger');
				        		$("#receive-details-alert-message").text(data.error);
				        	}
							if(data.content){
								$("#receive-details-alert").hide();
								$("#receive-item-popup").modal('hide');
								$('#receive-order-item').html(data.content);
								$("#receive-order-item-popup").modal('show');
								ReceiveOrderItemTable();
							}
							CloseAlert();
		}
	});
}

function CofirmAmt($this){
	var ThisVal = parseInt($($this).val());
	var qpc = parseInt($($this).closest('.each-item').find('.qpc').val());

	if(ThisVal < qpc){
		$($this).closest('.each-item').find('.ar-c').val(0);
	}
	if(ThisVal == qpc){
		$($this).closest('.each-item').find('.ar-c').val('-1');
	}
	if($($this).closest('.each-item').find('.ar-c').val() < 1){
		if(ThisVal > qpc){
			var rc = confirm('Want to receive extra amount ?');
			if(rc === true){
				$($this).closest('.each-item').find('.ar-c').val(1);
			}
			if(rc === false){
				$($this).val(qpc);
				$($this).closest('.each-item').find('.ar-c').val(0);
			}
		}
	}
}

function FinishReceiving(){
	var ODID = [];
	var ORDID = [];
	var ORID = [];
	var RQty = [];
	var RNote = [];
	var Item = [];
	var AR_C = [];
	SetEachAmt("#receive-items .odid", ODID);
	SetEachAmt("#receive-items .ordid", ORDID);
	SetEachAmt("#receive-items .orid", ORID);
	SetEachAmt("#receive-items .amt-received", RQty);
	SetEachOne("#receive-items .note", RNote);
	SetEachOne("#receive-items .item", Item);
	SetEachOne("#receive-items .ar-c", AR_C);
	$.ajax({
		'url'			: 'requests/actions.php',
		'type'			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: {'action' : 'finish_receiving', 'ODID' : ODID, 'ORDID' : ORDID, 'ORID' : ORID, 'RQty' : RQty, 'RNote' : RNote, 'Item' : Item, 'AR_C' : AR_C},
		beforeSend		: function(){
			
		},
		success 		: function(data){
				        	if(data.message){
				        		$.alert({
					    			title : 'Alert!',
					    			content : data.message
					    		});
				        		//alert(data.message);
				        		location.reload();
				        	}
				        	if(data.error){
				        		$.alert({
					    			title : 'Alert!',
					    			content : data.error
					    		});
				        	}	
		}
	});
}


/*===============================================*/
				/*Receiving End*/
/*===============================================*/

/*Search Inventory*/
function SearchInventory(){
	var InputOBJ = $("#inventory-search");
	var SearchValue = InputOBJ.val();
	var PerPageItem = $("#inv-per-page").val();
	var PageNum = $("#page-no").val();
	$.ajax({
		'url' 		: 'requests/actions.php',
		'type' 		: 'post',
		'dataType' 	: 'json',
		'cache' 	: false,
		'data' 		: {'action' : 'search_inventory', 'SearchValue' : SearchValue, 'PerPageItem' : PerPageItem, 'PageNum' : PageNum},
		beforeSend 	: function(){
					$("#inventory-table tbody").html('<tr><td colspan=7><img src="dist/images/ajax-loader.svg" class="ajax-loader" style="margin: 0 auto;"></td></tr>');
		},
		success 	: function(Response){
					$("#inventory-table tbody").html(Response.content);
		}
	});
}

/*==============================================*/
	/*Cash Drawer - Add/Edit/Update START*/
/*==============================================*/
$("#cash-drawer-form #add-cash-drawer").on('click', function(e){
	e.preventDefault();
	var NewCashDrawer = $("#cash-drawer-form").serialize();
	console.log(NewCashDrawer);
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: NewCashDrawer+'&'+$.param({ 'action' : 'new_cash_drawer' }),
        beforeSend: function() {
        },
        success: function(data){
	        $("#new-cashdrawer-alert").show();
        	if(data.error){
        		$("#new-cashdrawer-alert").removeClass('alert-success');
        		$("#new-cashdrawer-alert").addClass('alert-danger');
        		$("#new-cashdrawer-alert #new-cashdrawer-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#new-cashdrawer-alert").removeClass('alert-danger');
        		$("#new-cashdrawer-alert").addClass('alert-success');
        		$("#new-cashdrawer-alert #new-cashdrawer-alert-message").text(data.message);
        		$("#cash-drawer-form")[0].reset();
        		$("#cash-drawer").modal('hide');
        		$("#cash-drawer-table tbody").append(data.tr);
        		$.alert({
	    			title : 'Alert!',
	    			content : 'New Drawer successfully added'
	    		});
        		//alert("New Drawer successfully added");
        		setTimeout(function(){
        			$("#new-cashdrawer-alert").hide();
        		},2000);
        	}
        },
	});
});

function EditCashDrawer($this){
	var CDID = $($this).attr('data-id');
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: { 'action' : 'edit_cash_drawer', 'CDID' : CDID },
        beforeSend: function() {

        },
        success: function(data){
        	$("#edit-cashdrawer-alert").hide();
        	$("#edit-cash-drawer").modal('show');
        	$("#edit-cash-drawer-form #drawer-name").val(data.CD_Name);
        	$("#edit-cash-drawer-form #wh-dropdown").html(data.CD_dd);
        	$("#edit-cash-drawer-form #computer-name").val(data.CD_Computer);
        	$("#edit-cash-drawer-form #cdid").val(data.CDID);
        },
	});
}

$("#edit-cash-drawer-form #update-cash-drawer").on('click', function(e){
	e.preventDefault();
	var CashDrawerData = $("#edit-cash-drawer-form").serialize();
	$.ajax({
		'url'			: 'requests/actions.php',
        'type'			: 'post',
        'dataType'		: 'JSON',
        'cache'			: false,
        'data'			: CashDrawerData+'&'+$.param({ 'action' : 'update_cash_drawer' }),
        beforeSend: function() {
        },
        success: function(data){
        	$("#edit-cashdrawer-alert").show();
        	if(data.error){
        		$("#edit-cashdrawer-alert").removeClass('alert-success');
        		$("#edit-cashdrawer-alert").addClass('alert-danger');
        		$("#edit-cashdrawer-alert #edit-cashdrawer-alert-message").text(data.error);
        	}
        	if(data.message){
        		$("#edit-cashdrawer-alert").removeClass('alert-danger');
        		$("#edit-cashdrawer-alert").addClass('alert-success');
        		$("#edit-cashdrawer-alert #edit-cashdrawer-alert-message").text(data.message);
        		$("#edit-cash-drawer-form")[0].reset();
        		$("#edit-cash-drawer").modal('hide');
        		$("#cash-drawer-table tbody tr#"+data.cdid).html(data.tr);
        		$.alert({
	    			title : 'Alert!',
	    			content : 'Details successfully updated'
	    		});
        	}
        },
	});
});

/*==============================================*/
	/*Cash Drawer - Add/Edit/Update END*/
/*==============================================*/

/*=================================================*/
		/*=========Scroller START=========*/
/*=================================================*/
(function($){
	jQuery(window).load(function(){
		
		jQuery(".lt-thumb-block .wrapper").mCustomScrollbar({
			theme:"minimal-dark"
		});

		jQuery(".rt-thumb-block .main-content-inner").mCustomScrollbar({
			theme:"minimal-dark",
			callbacks: {
				whileScrolling : function(){
					var $scrollerOuter  = $( '.rt-thumb-block' );
				    var $dragger        = $scrollerOuter.find( '.mCSB_dragger' );
				    var scrollHeight    = $scrollerOuter.find( '.mCSB_container' ).height();
				    var draggerTop      = $dragger.position().top;
				    var scrollTop = draggerTop / ($scrollerOuter.height() - $dragger.height()) * (scrollHeight - $scrollerOuter.height());
				    var offsetVal = 70;
				}
			}
		});
	});
})(jQuery);
/*=================================================*/
		/*=========Scroller END=========*/
/*=================================================*/


function CashoutEntryQty($this){
	var ThisVal = $this.value;
	var DCA = $($this).closest('div').find('.dca');
	var PriceVal = $($this).attr('data-price');
	var CV = '';
	CV = parseFloat(PriceVal)*parseInt(ThisVal);
	if(ThisVal != ''){
		DCA.text('$'+Dec2(CV));
	}else{
		DCA.text('');
	}
	CollectDrawerCash();
	CashDifference();
}

function CashOutByCard($this){
	var ThisPrice = $this.value;
	var DCA = $($this).closest('div').find('.dca');
	var DCAB = $($this).closest('div').find('.card-value');
	var CV = '';
	CV = parseFloat(ThisPrice);
	if(ThisPrice != ''){
		DCA.text('$'+Dec2(CV));
		DCAB.text('$'+Dec2(CV));
	}else{
		DCA.text('');
		DCAB.text('');
	}
	CollectDrawerCash();
	CashDifference();

}

function CollectDrawerCash(){
	var DCA = [];
	$("#from-drawer .dca").each(function(){
		if($(this).text() != ''){
			DCA.push($(this).text().replace('$', ''));
		}
	});
	console.log(DCA);
	var TDC = '$00.00';
	if(DCA.length > 0){
		TDC = DCA.reduce(function (accumulator, currentValue) {
			return parseFloat(accumulator) + parseFloat(currentValue);
		}, 0);
		$("#drawer-cash").text('$'+Dec2(TDC));
	}else{
		$("#drawer-cash").text(TDC);
	}
}
CollectDrawerCash();

function CashDifference(){
	var DSC = $("#drawer-starting-cash").text().replace("$", '');
	var SC = $("#total-system-cash").text().replace("$", '');
	var DC = $("#drawer-cash").text().replace("$", '');
	var Diff = 0;
	Diff = parseFloat(DC) - ( parseFloat(SC) + parseFloat(DSC) );
	$("#difference").text('$'+Dec2(Diff));
	if(Diff < 0){
		$("#difference").css({'color' : '#FF0000'});
	}
	if(Diff > 0){
		$("#difference").css({'color' : '#FFFF00'});
	}
	if(Diff == 0){
		$("#difference").css({'color' : '#39FF14'});
		$("#difference").text('$0.00');
	}
}

//Cashout
function CompleteCashout(){
	var DSC = $("#drawer-starting-cash").text().replace("$", '');
	var SC = $("#total-system-cash").text().replace("$", '');
	var DC = $("#drawer-cash").text().replace("$", '');
	var Diff = $("#difference").text().replace("$", '');
	var AV = $("#adjusted-value").val();
	if(DC > 0 && Diff >= 0){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'data'			: {'action' : 'drawer_cashout', 'DSC' : DSC, 'SC' : SC, 'DC' : DC, 'Diff' : Diff, 'AV' : AV},
	        beforeSend	: function() {
	        },
	        success 	: function(data){
	        	if(data.resp === true){
	        		location.reload();
	        	}
	        }
		});
	}else{
		if($.trim(DC) == '' || $.trim(DC) == 0){
			$.alert({
    			title : 'Alert!',
    			content : 'No drawer cash has been collected yet'
    		});
		}else if(Diff < 0){
			$.alert({
    			title : 'Alert!',
    			content : 'Missing Amount : $'+Diff,
    			buttons : {
    				Ok : {
                        text: 'Ok',
                        action: function(){
							var conf = confirm("Continue with value adjustment");
							if(conf === true){

								$.ajax({
									'url'			: 'requests/actions.php',
							        'type'			: 'post',
							        'dataType'		: 'JSON',
							        'cache'			: false,
							        'data'			: {'action' : 'check_manager_auth', 'RequestTo' : 'adjust cash drawer value'},
							        beforeSend	: function() {
							        },
							        success 	: function(data){
							        	if(data.permission === true){
							        		Diff = Diff.replace('-','');
							        		var IAV = prompt("Adjusted Value", Diff);

							        		var AV_Reason = prompt("Reason for adjustment");
							        		if(IAV != ''){
							        			$.ajax({
													'url'			: 'requests/actions.php',
											        'type'			: 'post',
											        'dataType'		: 'JSON',
											        'cache'			: false,
											        'data'			: {'action' : 'drawer_cashout', 'DSC' : DSC, 'SC' : SC, 'DC' : DC, 'Diff' : Diff, 'AV' : IAV, 'AV_Reason' : AV_Reason},
											        beforeSend	: function() {
											        },
											        success 	: function(data){
											        	if(data.resp === true){
											        		location.reload();
											        	}
											        }
								        		});
							        		}

							        	}else{
							        		$("#manager-approval-popup").modal('show');
							        	}
							        }
								});
							}       
                        },     
	                },	
    			}
    		});
		}
	}
}

function SDColorPicker(){
	$('.color-picker').each(function(){
		$(this).colorpicker();
	});
}
SDColorPicker();

//Set On Hand Item Price According to Price List
function SetItemPrice($this){
	var iohpid = $($this).attr('data-iohpid');
	var price = $($this).val();
	var ptid = $($this).attr('data-ptid');
	var ohid = $($this).attr('data-ohid');
	var loader = $($this).closest('div').find('.loader');
	var OK = $($this).closest('div').find('.ok');
	if($.trim(price) != ''){
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'data'			: { 'action' : 'set_on_hand_item_price', 'iohpid' : iohpid, 'price' : price, 'ptid' : ptid, 'ohid' : ohid },
	        beforeSend: function() {
	        	loader.show();
	        },
	        success: function(data){
	        	if(data.iohpid){
	        		$($this).attr('data-iohpid', data.iohpid);
	        		$($this).attr('data-ptid', data.ptid);
	        		$($this).attr('data-ohid', data.ohid);
	        	}
	        	loader.hide();
	        	OK.show();
	        	setTimeout(function(){
	        		OK.hide();
	        	}, 3000);
	        },
		});
	}
}


//
function OnSelectPurchaseItem($this){
	var item = $($this).val();
	var vendor = $("#order-form #vendor").val();
	if(vendor == ''){
		$($this).selectpicker('val', '');
		$.alert({
	        title: 'Alert!',
	        content: 'Please select a vendor first',
	    });
	}else{
		$.ajax({
			'url'			: 'requests/actions.php',
	        'type'			: 'post',
	        'dataType'		: 'JSON',
	        'cache'			: false,
	        'data'			: { 'action' : 'get_last_order_data_of_this_item', 'vendor' : vendor, 'item' : item },
	        beforeSend: function() {
	        },
	        success: function(data){
	        	if(data.cost){
	        		$($this).closest('.item-block').find('.cost').val(data.cost);
	        		$($this).closest('.item-block').find('.tax').val(data.tax);
					$($this).closest('.item-block').find('.unit-tax').val(data.tax);
					$($this).closest('.item-block').find('.shipping-cost').val(data.scost);
	        	}else{
	        		$($this).closest('.item-block').find('.cost').val('');
	        		$($this).closest('.item-block').find('.tax').val('');
	        		$($this).closest('.item-block').find('.unit-tax').val('');
	        		$($this).closest('.item-block').find('.shipping-cost').val('');
	        	}
	        	$($this).closest('.item-block').find('.case-qty').val('1');
	        	$($this).closest('.item-block').find('.qty-per-case').val('1');
	        },
		});
	}
}


//Permission
function SavePermission(e){
	var SectionID = $(e).closest('tr').attr('id');
	var Permission = $(e).closest('tr').find('.permission').val();
	if($.trim(Permission) != ''){
		if(parseInt(Permission) >= 0 && parseInt(Permission) <= 100){
			$.ajax({
				'url'			: 'requests/actions.php',
		        'type'			: 'post',
		        'dataType'		: 'JSON',
		        'cache'			: false,
		        'data'			: { 'action' : 'section_permission', 'SectionID' : SectionID, 'Permission' : Permission },
		        beforeSend: function() {
		        },
		        success: function(data){
		        	if(data.resp === true){
		        		$.alert({
		        			title : 'Alert!',
		        			content : 'Permission saved'
		        		});
		        	}
		        },
			});
		}else{
		    $.alert({
		        title: 'Alert!',
		        content: 'Value must be set between 0 to 100',
		    });
		}
	}
}

//Complete Sales Payments of whichever is still being unpaid the total
$("#complete-the-sp").on('click', function(){
	var SalesID = $(this).attr('data-sp_sid');
	var DueVal = $(this).attr('data-due_val');
	var RF = 'sales-details';
	$.ajax({
		'url' 			: 'requests/actions.php',
		'type' 			: 'post',
		'dataType'		: 'json',
		'cache'			: false,
		'data'			: { 'action' : 'get_the_due_pm_details', 'SalesID' : SalesID },
		'beforeSend'	: function(){
		},
		success 		: function(data){
							$("#selling-payment-loop").modal('show');
							$("#loop-sales-id").val(SalesID);
							$("#loop-sales-due").val(DueVal);
							$("#loop-sales-rf").val(RF);
							$("#loop-pm-details").html(data.lpmd);
		}
	});
});

//Sales Void
function MakeVoid($this){
	var SalesID = $($this).closest('tr').attr('id');
	var CustomerID = $($this).closest('tr').attr('data-customer');
	$.confirm({
        title : 'Confirmation',
        content : 'Are you sure you want to void this sale?',
        buttons: {
            Ok : {
                    text: 'Yes',
                    btnClass: 'btn-blue',
                    action: function(){
                    	$.confirm({
					        title: 'Please enter a reason for this void',
					        content: '' +
					        '<form action="" class="formName">' +
					        '<div class="form-group">' +
					        '<textarea class="form-control void-reason"></textarea>' +
					        '</div>' +
					        '</form>',
					        buttons: {
					            formSubmit: {
					                text: 'Submit',
					                btnClass: 'btn-blue',
					                action: function () {
					                    var reason = this.$content.find('.void-reason').val();
					                    if(!reason){
					                        $.alert('Please enter a reason for this void');
					                        return false;
					                    }
					                    $.ajax({
											'url'			: 'requests/actions.php',
									        'type'			: 'post',
									        'dataType'		: 'JSON',
									        'cache'			: false,
									        'data'			: { 'action' : 'void_sale', 'SalesID' : SalesID, 'CustomerID' : CustomerID, 'VoidNote' : reason },
									        beforeSend: function() {
									        },
									        success: function(data){
									        	if(data.resp === true){
									        		location.reload();
									        	}
									        	if(data.error_msg){
									        		$.alert({
									        			title : 'Alert!',
				        								content : data.error_msg
									        		});
									        	}
									        },
										});
					                }
					            },
					            cancel: function () {
					                //close
					            },
					        },
					    }); 
                    },     
            },
            Cancel : {
                    text: 'No',
                    btnClass: 'btn-red',
            }
        }
    });
}

//Purchase Page
function MatchWithInventoryMaxQty(e){
	var InputQty = $(e).val();
	var SelectedItem = $(e).closest('.item-block').find('.item option:selected').val();
	if(SelectedItem != '' && typeof SelectedItem !== 'undefined'){
		$.ajax({
			'url' 			: 'requests/actions.php',
			'type' 			: 'post',
			'dataType'		: 'json',
			'cache'			: false,
			'data'			: { 'action' : 'match_order_item_qty_with_inventory_max_qty', 'SelectedItem' : SelectedItem, 'InputQty' : InputQty },
			'beforeSend'	: function(){
			},
			success 		: function(data){
					if(data.resp === false){
						$.alert({
		        			title : 'Alert!',
		        			content : data.msg
		        		});
					}
					$(e).closest('.item-block').find('.qty-per-case').val(data.MyQty);		
			}
		});
	}
}


//Go to inventory item details page
function GoToItemDetails($this){
	var item = $($this).closest('tr').attr('data-item');
	$.ajax({
		'url' 		: 'requests/actions.php',
		'type' 		: 'post',
		'dataType'	: 'json',
		'cache'		: false,
		'data'		: { 'action' : 'go_to_the_details_page', 'item' : item, 'request_profile' : 'inventory_item' },
		success 	: function(data){
			if(data.dest_link){
				window.location = data.dest_link;
			}	
		}
	});
}

//Add new iten according to the choosen value inventory/non-inventory
function AddItem(){
	$.confirm({
        title: 'Choose Item Type',
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<label for="inv" class="radio-inline"><input type="radio" id="inv" name="item-type" class="item-type" value="inv" checked required />' +
        'Inventory</label>' +
        '<label for="non-inv" class="radio-inline"><input type="radio" id="non-inv" name="item-type" class="item-type" value="non-inv" required />' +
        'Non-Inventory</label>' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-blue',
                action: function () {
                    var it = this.$content.find('.item-type:checked').val();
                    if(!it){
                        $.alert('Please choose an item type');
                        return false;
                    }
                    //$.alert('Your name is ' + it);
                    window.location = site_url()+'?destination=inventory&action=add-'+it+'-item';
                }
            },
            cancel: function () {
                //close
            },
        },
        
    });
}
/*===============Non Inventory START===============*/

//Add new non inventory item
$("#add-non-inv-btn").on('click', function(e){
	e.preventDefault();
	var thisForm = $(this).closest('form');
	var alertDiv = thisForm.find('.alert');
	var alertMsg = thisForm.find('.alert-message');
	var data = $("#add-non-inv-form").serialize();
	$.ajax({
		'url' 		: 'requests/actions.php',
		'type' 		: 'post',
		'dataType'	: 'json',
		'cache'		: false,
		'data'		: data+'&'+$.param({ 'action': 'new_non_inv_item' }),
		success 	: function(data){
				alertDiv.show();
				if(data.error){
					alertDiv.removeClass('alert-success');
	        		alertDiv.addClass('alert-danger');
	        		alertMsg.text(data.error);
				}
				if(data.message){
					alertDiv.removeClass('alert-danger');
	        		alertDiv.addClass('alert-success');
	        		alertMsg.text(data.message);
	        		thisForm[0].reset();
	        		setTimeout(function(){
	        			alertDiv.hide();
	        		}, 3000);
				}
		}
	});
});

//edit non inventory - open modal
function EditNonInventoryItem(e){
	var tr = $(e).closest('tr');
	var item = tr.attr('id');
	$.ajax({
		'url' 		: 'requests/actions.php',
		'type' 		: 'post',
		'dataType'	: 'json',
		'cache'		: false,
		'data'		: {'item' : item, 'action' : 'edit_non_inv_item'},
		'beforeSend': function(){
				$("#modal-edit-non-inv-item").modal('show');
				var thisForm = $("#edit-non-inv-item-form");
				var alertDiv = thisForm.find('.alert');
				alertDiv.hide();
		},
		success 	: function(data){
				$("#edit-non-inv-item-form #part-number").val(data.part_number);
				$("#edit-non-inv-item-form #item-name").val(data.item_name);
				$("#edit-non-inv-item-form #item-cost").val(data.item_cost);
				$("#edit-non-inv-item-form #item-desc").val(data.item_desc);
				$("#edit-non-inv-item-form #is-active").html(data.isActive);
				$("#edit-non-inv-item-form #ledger-id").html(data.LSelect);
				$("#edit-non-inv-item-form #item-price-type").html(data.NPSelect);
				$("#edit-non-inv-item-form #item-price").val(data.item_price);
				$("#edit-non-inv-item-form #non-inv-item-id").val(data.item_id);
		}
	});
}

//update non inventory item
$("#update-non-inv-item").on('click', function(e){
	e.preventDefault();
	var data = $("#edit-non-inv-item-form").serialize();
	var thisForm = $(this).closest('form');
	var alertDiv = thisForm.find('.alert');
	var alertMsg = thisForm.find('.alert-message');
	var closestModal = $(this).closest('div.modal');
	$.ajax({
		'url' 		: 'requests/actions.php',
		'type' 		: 'post',
		'dataType'	: 'json',
		'cache'		: false,
		'data'		: data+'&'+$.param({ 'action': 'update_non_inv_item' }),
		success 	: function(data){
				alertDiv.show();
				if(data.error){
					alertDiv.removeClass('alert-success');
	        		alertDiv.addClass('alert-danger');
	        		alertMsg.text(data.error);
				}
				if(data.message){
					alertDiv.removeClass('alert-danger');
	        		alertDiv.addClass('alert-success');
	        		alertMsg.text(data.message);
	        		//thisForm[0].reset();
	        		var tblTR = $("#non-inv-items-table tbody tr#"+data.item_id);
	        		tblTR.html(data.tr);
	        		setTimeout(function(){
	        			closestModal.modal('hide');
	        		}, 1000);

	        		$("#non-inv-items-table").DataTable();
				}
		}
	});
});

/*===============Non Inventory END===============*/

$("#transfer-warehouse-btn").on('click', function(e){
	e.preventDefault();
	var data = $("#transfer-warehouse-form").serialize();
	$.ajax({
		'url' 		: 'requests/actions.php',
		'type' 		: 'post',
		'dataType'	: 'json',
		'cache'		: false,
		'data'		: data+'&'+$.param({ 'action': 'transfer_warehouse' }),
		success 	: function(data){
				if(data.error){
					$.alert({
				        title: 'Alert!',
				        content: data.error,
				    });
				}
				if(data.message){
					$.alert({
				        title: 'Alert!',
				        content: data.message,
				    });
				    $("#transfer-warehouse-form")[0].reset();
				    $("#transfer-warehouse-modal").modal('hide');
				}
		}
	});
});


$("#transfer-warehouse-form #warehouse-from").on('change', function(){
	var $fwh = $(this).val();
	var $twh = $("#transfer-warehouse-form #warehouse-to").val();
	if($twh == $fwh){
		$.alert("FROM and TO can't be same warehouse");
		$("#transfer-warehouse-form #warehouse-from").selectpicker('val', '');
	}else{
		$.ajax({
			'url' 		: 'requests/actions.php',
			'type' 		: 'post',
			'dataType'	: 'json',
			'cache'		: false,
			'data'		: { 'action' : 'get_warehouse_items', 'fwh' : $fwh },
			success 	: function(data){
					if(data.cont){
						$("#transfer-warehouse-form #wh-items").html(data.cont);
					}
					if(data.error){
						$.alert(data.error);
						$("#transfer-warehouse-form #on-hand-item").selectpicker('val', '');
						$("#transfer-warehouse-form #wh-items").html(data.cont);
					}
					SdSelection();
			}
		});
	}
});

$("#transfer-warehouse-form #warehouse-to").on('change', function(){
	var $twh = $(this).val();
	var $fwh = $("#transfer-warehouse-form #warehouse-from").val();
	if($twh == $fwh){
		$.alert("FROM and TO can't be same warehouse");
		$("#transfer-warehouse-form #warehouse-to").selectpicker('val', '');
	}
});


function editTaxRate(e){
	$("#add-tax-rate").modal('show');
	$("#add-tax-rate .modal-title").text('Update Tax Rate');
	$("#add-tax-rate a.btn").text('Update');
	$("#add-tax-rate a.btn").attr('onclick', 'updateTaxRate()');
}