<?php

//logs.php

define('SALES_LOG', true);
define('SALES_LOG_NOTE', 'Selling complete');
define('SALES_QUOTE_LOG_NOTE', 'Selling items quoted');
define('QUOTED_ITEMS_SALES_LOG_NOTE', 'Selling completed from quote history');


/*define('NEW_INVENTORY_LOG', true);
define('NEW_INVENTORY_LOG_NOTE', 'New item added');*/

define('UPDATE_INVENTORY_LOG', true);
define('UPDATE_INVENTORY_LOG_NOTE', 'Item details updated');

/*==========Vendor Log Config==========*/
define('NEW_VENDOR_LOG', true);
define('NEW_VENDOR_LOG_NOTE', 'Added New Vendor');

define('UPDATE_VENDOR_LOG', true);
define('UPDATE_VENDOR_LOG_NOTE', "Vendor details updated");

/*==========Customer Log Config==========*/
//New Customer Log
define('NEW_CUSTOMER_LOG', true);
define('NEW_CUSTOMER_LOG_NOTE', 'Added New Customer');

//Customer Details Update Log
define('UPDATE_CUSTOMER_LOG', true);
define('UPDATE_CUSTOMER_LOG_NOTE', 'Cutomer Details Updated');

//Customer Make a Sale
define('CUSTOMER_MAKE_SALE_LOG', true);
define('CUSTOMER_MAKE_SALE_LOG_NOTE', 'Make a Sale');

//Customer Make a Quote
define('CUSTOMER_MAKE_QUOTE_LOG', true);
define('CUSTOMER_MAKE_QUOTE_LOG_NOTE', 'Make a Quote');

//Customer Quote Update Log
define('CUSTOMER_QUOTE_UPDATE_LOG', true);
define('CUSTOMER_QUOTE_UPDATE_LOG_NOTE', 'Quote Updated');

//Customer Complete Sales From Quote Log
define('CUSTOMER_COMPLETE_SALES_FROM_QUOTE_LOG', true);
define('CUSTOMER_COMPLETE_SALES_FROM_QUOTE_LOG_NOTE', 'Sales Completed From Quote History');


/*==========Sales Log Config==========*/
//Sales Make Void Log
define('SALES_MAKE_VOID', true);
define('SALES_MAKE_VOID_NOTE', 'Void Sale');

//Sales Complete Log
define('SALES_COMPLETE', true);
define('SALES_COMPLETE_NOTE', 'Complete Sale');

//Sales Quote Log
define('SALES_QUOTE', true);
define('SALES_QUOTE_NOTE', 'Create Quote');

//Sales Quote Update Log
define('SALES_QUOTE_UPDATE', true);
define('SALES_QUOTE_UPDATE_NOTE', 'Quote Updated');


/*==========Inventory Item Log Config==========*/
//Inventory Item New Order Log
define('INVENTORY_ITEM_ORDER_LOG', true);
define('INVENTORY_ITEM_ORDER_LOG_NOTE', 'Void Sale');

//Add New Inventory Item  Log
define('ADD_NEW_INVENTORY', true);
define('ADD_NEW_INVENTORY_NOTE', 'New item added to the inventory');

//Update Inventory Item  Log
define('UPDATE_INVENTORY', true);
define('UPDATE_INVENTORY_NOTE', 'Update inventory item');

?>