<?php
//index.php

/*=============Create errors.log START=============*/
error_reporting(E_ALL); // Error engine - always ON!
ini_set('display_errors', TRUE); // Error display - OFF in production env or real server
ini_set('log_errors', TRUE); // Error logging
ini_set('error_log', dirname(__FILE__).'/errors.log'); // Logging file
ini_set('log_errors_max_len', 1024); // Logging file size
/*=============Create errors.log END=============*/
/*Test add - avd*/

session_start();
require_once ('functions.php');
$currentUserRole = get_the_role(currentUser('ELRoleID'));
$preTitle = isset($_GET['destination']) ? ucwords(str_replace('-', ' ', $_GET['destination'])) : 'TNB';
$pagetitle = $preTitle;
if(isset($_GET['action']) && !empty($_GET['action'])){
	$pagetitle = ucwords(str_replace('-', ' ', $_GET['action']));
}
$title = is_user_logged_in()? $pagetitle." | Dashboard | ".$currentUserRole : "TNB | Login";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title; ?></title>
		<?php require_once('links.php'); ?>
	</head>
	<body class="<?=MainClasses();?>">
		<div class="page-loader"></div>
		<?php if(isset($_SESSION['systemuser']) && !empty($_SESSION['systemuser'])){
			echo getPage('content');
		}else{
			echo getPage('login');
		} ?>
		<script src="dist/js/custom.js"></script>
	</body>
</html>