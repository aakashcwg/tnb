<?php
//update-tax-rate.php
?>
<div id="update-tax-rate" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Update Tax Rate</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="update-tax-rate-form">
		    		<div class="row">
		    			<div class="col-md-12">
		    				<div class="form-group">
		    					<div class="row">
		    						<div class="col-md-6">
		    							<label for="tax-rate">Tax Rate</label>
		    							<input type="text" name="tax-rate" id="tax-rate" class="form-control qty">
		    						</div>
		    						<div class="col-md-6">
		    							<label for="tax-rate-name">Tax Rate Name</label>
		    							<input type="text" name="tax-rate-name" id="tax-rate-name" class="form-control ">
		    						</div>
		    						<input type="hidden" name="tax-rate-id" id="tax-rate-id">
		    						<div class="col-md-12 text-center">
		    							<a href="javascript:void(0)" class="btn theme-default" onclick="updateTaxRate()">Update</a>
		    						</div>	
		    					</div>
				    		</div>
		    			</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>