<div id="low-stock-items-list" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Low Stock Items</h4>
		  	</div>
		  	<div class="modal-body">
		  		<div class="low-stock-table-container">
		  			<?php echo LowStockItems(); ?>
		  		</div>
		  	</div>
		</div>
	</div>
</div>