<div id="edit-customer" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 800px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Customer</h4>
            </div>
            <div class="modal-body" id="edit-customer-body">       
                
            </div>
        </div>
    </div>
</div>