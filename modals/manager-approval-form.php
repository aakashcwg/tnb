<div id="manager-approval-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Manager Approve</h4>
            </div>
            <div class="modal-body">

            	<div class="alert alert-dismissible" role="alert" id="manager-auth-alert" style="display: none;">
                    <strong id="manager-auth-alert-message"></strong>
                    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="manager-approval-form">
                	<div class="row">
                		<div class="col-md-6">
	                		<div class="form-group">
	                			<label for="manager-username">Username<sup>*</sup></label>
	                			<input type="text" name="manager-username" id="manager-username" class="form-control manager-username">
	                		</div>
	                	</div>
	                	<div class="col-md-6">
	                		<div class="form-group">
	                			<label for="manager-password">Password<sup>*</sup></label>
	                			<input type="password" name="manager-password" id="manager-password" class="form-control manager-password">
	                		</div>
	                	</div>
	                	<div class="col-md-12 text-center">
	                		<a href="javascript:void(0)" class="btn theme-default-orange" onclick="SaveManagerApproval()">Submit</a>
	                	</div>
                	</div>
                </form>
            </div>
        </div>
    </div>
</div>