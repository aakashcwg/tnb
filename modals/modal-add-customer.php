<div id="add-customer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Customer</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-success alert-dismissible" role="alert" id="add-customer-alert" style="display: none;">
                    <strong id="add-customer-alert-message"></strong>
                    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <form id="add-customer-form">

                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cfname">First Name<sup>*</sup></label>
                                <input type="text" name="cfname" id="cfname" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="clname">Last Name<sup>*</sup></label>
                                <input type="text" name="clname" id="clname" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ccompany">Company Name</label>
                                <input type="text" name="ccompany" id="ccompany" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="caccno">Account Number</label>
                                <input type="text" name="caccno" id="caccno" class="form-control number-validation">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tax-exempt">Tax Exempt</label>
                                <select id="tax-exempt" name="tax_exempt" class="form-control">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="caddrtype">Address Type<sup>*</sup></label>
                                <select id="caddrtype" name="caddrtype" class="form-control">
                                    <option value="">Select Address Type</option>
                                    <?php if(getAddressTypes()) { ?>
                                    <?php foreach (getAddressTypes() as $type) {
                                        ?>
                                        <option value="<?php echo $type->ATID; ?>"><?php echo $type->ATName; ?></option>
                                        <?php
                                    } } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="caddr1">Address 1</label>
                                <input type="text" name="caddr1" id="caddr1" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="caddr2">Address 2</label>
                                <input type="text" name="caddr2" id="caddr2" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ccity">City</label>
                                <input type="text" name="ccity" id="ccity" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cstate">State</label>
                                <select class="form-control" name="cstate" id="cstate">
                                    <option value="">Select State</option>
                                    <
                                    <?php foreach (getUSAStates() as $key => $val) {
                                        ?>
                                        <option value="<?=$key;?>"><?=$val;?></option>
                                        <?php
                                    } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="czip">Zip Code<sup>*</sup></label>
                                <input type="text" name="czip" id="czip" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="customertype">Customer Type<sup>*</sup></label>
                                <select id="customertype" name="customertype" class="form-control">
                                    <option value="">Select Customer Type</option>
                                    <?php if(getCustomerTypes()) { ?>
                                    <?php foreach (getCustomerTypes() as $type) {
                                        ?>
                                        <option value="<?php echo $type->CATLID; ?>"><?php echo $type->CATLName; ?></option>
                                        <?php
                                    } } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ccontacttype">Contact Type<sup>*</sup></label>
                                <select id="ccontacttype" name="ccontacttype" class="form-control">
                                    <option value="">Select Contact Type</option>
                                    <?php if(getContactTypes()) { ?>
                                    <?php foreach (getContactTypes() as $type) {
                                        ?>
                                        <option value="<?php echo $type->CTID; ?>"><?php echo $type->CTName; ?></option>
                                        <?php
                                    } } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ccdata">Contact Data<sup>*</sup></label>
                                <input type="text" name="ccdata" id="ccdata" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group check-primary">
                                <label for="cpcontact"><input type="checkbox" name="cpcontact" id="cpcontact" class="form-control" checked>Primary Contact ?</label>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="customer-pl">Price List</label>
                                <select class="form-control" id="customer-pl" name="customer-pl">
                                    <option value="">Select Price List</option>
                                        <?php if(getPriceLists()) { ?>
                                        <?php foreach (getPriceLists() as $PL) {
                                            $selected = '';
                                            if($TheCustomer){
                                                $selected = $PL->PLID == $TheCustomer->CustomerPLID ? 'selected' : '';
                                            }
                                            ?>
                                            <option value="<?=$PL->PLID;?>" <?=$selected?>><?=$PL->PLName;?></option>
                                            <?php
                                        } } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group check-primary">
                                <label for="cisactivecheck"><input type="checkbox" name="cisactivecheck" id="cisactivecheck" class="form-control" checked>Is Active ?</label>
                            </div>
                        </div>

                        <input type="hidden" name="tax-id" id="tax-id">
                        <input type="hidden" name="expiration-date" id="expiration-date">
                        <input type="hidden" name="current_page" id="current-page" value="selling">

                        <div class="col-md-12 text-center">
                            <button class="btn theme-default" id="add-customer-btn">Save</button>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>