<?php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2>You are unable to access this page.</h2>");
	
//new-employee-modal.php
?>
<div id="new-employee-modal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 700px;">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">New Employee</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="new-employee-form">
		    		<div class="row">
		    			<div class="col-md-6">
		    				<div class="form-group">
				    			<label for="efname">First Name<sup>*</sup></label>
				    			<input type="text" name="efname" id="efname" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
		    			</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="elname">Last Name</label>
				    			<input type="text" name="elname" id="elname" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="eaddr1">Address Line 1</label>
				    			<input type="text" name="eaddr1" id="eaddr1" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="eaddr2">Address Line 2</label>
				    			<input type="text" name="eaddr2" id="eaddr2" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="ecity">City</label>
				    			<input type="text" name="ecity" id="ecity" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="estate">State</label>
				    			<select class="form-control" name="estate" id="estate">
					    			<option value="">-- Select State --</option>
				                    <
				                    <?php foreach (getUSAStates() as $key => $val) {
				                        ?>
				                        <option value="<?=$key;?>"><?=$val;?></option>
				                        <?php
				                    } ?>
			                	</select>
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="ezip">Zip Code</label>
				    			<input type="text" name="ezip" id="ezip" class="form-control">
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="erole">Role<sup>*</sup></label>
				    			<select id="erole" name="erole" class="form-control">
				    				<option value="" data-permisssion=''>--Select Role--</option>
				    				<?php foreach (getRoles() as $role) {
				    					?>
				    					<option value="<?php echo $role->ERRoleName; ?>" data-permisssion="<?php echo $role->ERID; ?>"><?php echo $role->ERRoleName; ?></option>
				    					<?php
				    				} ?>
				    			</select>
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="euser">Username<sup>*</sup></label>
				    			<input type="text" name="euser" id="euser" class="form-control" onkeyup="this.value=sdMakeUsername(this.value)">
				    		</div>
			    		</div>
			    		<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="epass">Password<sup>*</sup></label>
				    			<input type="text" name="epass" id="epass" class="form-control">
				    		</div>
			    		</div>
			    		<div class="col-md-12 text-center">
			    			<button class="btn theme-default" id="add-employee-btn">Create</button>
			    		</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>