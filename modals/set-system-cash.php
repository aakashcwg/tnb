<?php
//set-system-cash.php
?>
<div id="set-system-cash" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Set System Cash</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="set-system-cash-form">
		    		<div class="row">
		    			<div class="col-md-12">
		    				<div class="form-group">
		    					<div class="row">
		    						<div class="col-md-6">
		    							<label for="system-cash">System Cash<sup>*</sup></label>
		    							<input type="text" name="system-cash" id="system-cash" class="form-control qty">
		    						</div>
		    						<div class="col-md-6">
		    							<a href="javascript:void(0)" class="btn theme-default" onclick="SetSystemCash()">Set</a>
		    						</div>	
		    					</div>
				    		</div>
		    			</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>