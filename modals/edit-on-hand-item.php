<div id="edit-on-hand-item" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit On Hand Item</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-on-hand-item-form">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="qty"></label>
                                <input type="number" name="qty" class="form-control qty" id="qty">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="item-dropdown">
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="form-group check-primary">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                
                            </div>
                        </div>
                        
                        <input type="hidden" name="current_emp_id" id="current-emp-id" class="form-control" value="<?php echo currentUser('EmployeeID'); ?>">

                        <div class="col-md-12 text-center">
                            <button class="btn theme-default" id="update-on-hand-item">Update</button>
                        </div>

                    </div>

                </form>
		  	</div>
		</div>
	</div>
</div>