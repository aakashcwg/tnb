<?php

$btnID = 'complete-sale';
$salesID = '';

if(isset($_GET['get_from']) && $_GET['get_from'] == 'quote-history' && isset($_GET['selling_id']) && !empty($_GET['selling_id']) ){
    $btnID = 'sell-quote-item';
    $salesID = $_GET['selling_id'];
}

?>

<div id="selling-payment" class="modal fade selling-payment" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Payment Process</h4>
            </div>
            <div class="modal-body" style="text-align:center;">
                <div class="text">
                    <h5>Payment Method</h5>
                    <b>(Multiple payments CANNOT start with cash)</b>
                </div>
                <br>
                <div class="link-block">
                    <?php
                    global $ListTypeOBJ;
                    $PMs = $ListTypeOBJ->GetAllPaymentMethods();
                    $sec = '';
                    if(!empty($PMs)){
                        foreach ($PMs as $PM) {
                            if($PM->PaymentMethodName == 'Cash'){
                                $attr = '_cash';
                            }elseif($PM->PaymentMethodName == 'Check'){
                                $attr = '_check';
                            }elseif($PM->PaymentMethodName == 'House Account'){
                                $attr = '_ha';
                                $sec = 'ha';
                            }else{
                                $attr = '_card';
                            }
                            ?>
                            <div class="section" id="<?=$sec;?>">
                                <a href="javascript:void(0)" class="selling-payment-method" id="<?=$PM->PMID;?>" data-card="<?=$attr;?>"><?=$PM->PaymentMethodName;?></a>
                            </div>
                            <?php
                        } 
                    } ?>
                </div>

                <!-- <p id="_pm_text" style="display: none;"><span>Cash</span> amount given or <span>Check</span> number or <span>Authorization</span> code:</p> -->
                <div class="_cash _p_m" style="display: none;">
                    <label for="cmg" style="margin-bottom: 15px;">Cash Amount Given</label>
                    <input type="text" class="form-control price-validation" style="font-size: 20px;" id="cmg" onkeyup="SetCashAmount(this)">
                    <label for="ctm" style="margin-top: 15px;"><font size='6'><strong>Customer Change</strong></font> : <span id="ctm">0.00</span></label>
                    <!-- <input type="text" class="form-control" id="ctm" value="0.00" readonly> -->
                </div>
                <div class="_check _p_m" style="display: none;">
                    <label for="_check_no">Cheque No</label>
                    <input type="text" class="form-control" id="_check_no">
                    <label for="_check_amt">Input Cheque Amount</label>
                    <input type="text" class="form-control price-validation" id="_check_amt">
                </div>
                <div class="_card _p_m" style="display: none;">
                    <label for="_5digit">Last 5 Digits</label>
                    <input type="text" class="form-control" id="_5digit">
                    <label for="_auth_code">Authorization Code</label>
                    <input type="text" class="form-control" id="_auth_code">
                    <label for="_receipt_total">Credit Card Receipt Total</label>
                    <input type="text" class="form-control price-validation" id="_receipt_total">
                </div>

                <!-- <div class="_cash _p_m" style="display: none;">
                    <label for="cmg">Cash Amount Given</label>
                    <input type="text" class="form-control" id="cmg" onkeyup="SetCashAmount(this)">
                    <label for="ctm">Change to Customer</label>
                    <input type="text" class="form-control" id="ctm" value="0.00" readonly>
                </div>
                <div class="_check _p_m" style="display: none;">
                    <label for="_check_no">Check No</label>
                    <input type="text" class="form-control" id="_check_no">
                </div>
                <div class="_card _p_m" style="display: none;">
                    <label for="_5digit">Last 5 Digits</label>
                    <input type="text" class="form-control" id="_5digit">
                    <label for="_auth_code">Authentication Code</label>
                    <input type="text" class="form-control" id="_auth_code">
                    <label for="_receipt_total">Credit Card Receipt Total</label>
                    <input type="text" class="form-control price-validation" id="_receipt_total">
                </div> -->
                
                <div class="button-block">
                    <button class="btn theme-default-orange" id="<?=$btnID?>" data-id="<?=$salesID?>">Ok</button>
                    <button class="btn theme-default-orange" onclick="$('#selling-payment').modal('hide');$('#invoice-block').show();">Cancel</button>
                </div>

            </div>
        </div>
    </div>
</div>










<!-- <div id="selling-payment" class="modal fade selling-payment" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
		  	<div class="modal-header">

		    	<h4 class="modal-title">Payment Process</h4>
		  	</div>
		  	<div class="modal-body" style="text-align:center;">
                <div class="text">
                    <h5>Payment Method</h5>
                </div>
                <div class="link-block">
                    <?php
                    global $ListTypeOBJ;
                    $PMs = $ListTypeOBJ->GetAllPaymentMethods();
                    if(!empty($PMs)){
                        foreach ($PMs as $PM) {
                            if($PM->PaymentMethodName == 'Cash'){
                                $attr = '_cash';
                            }elseif($PM->PaymentMethodName == 'Check'){
                                $attr = '_check';
                            }else{
                                $attr = '_card';
                            }
                            ?>
                            <div class="section">
                                <a href="javascript:void(0)" class="selling-payment-method" id="<?=$PM->PMID;?>" data-card="<?=$attr;?>"><?=$PM->PaymentMethodName;?></a>
                            </div>
                            <?php
                        } 
                    } ?>
                </div>
                <div class="_cash _p_m" style="display: none;">
                    <label for="cmg">Cash Amount Given</label>
                    <input type="text" class="form-control" id="cmg" onkeyup="SetCashAmount(this)">
                    <label for="ctm">Change to Customer</label>
                    <input type="text" class="form-control" id="ctm" value="0.00" readonly>
                </div>
                <div class="_check _p_m" style="display: none;">
                    <label for="_check_no">Check No</label>
                    <input type="text" class="form-control" id="_check_no">
                </div>
                <div class="_card _p_m" style="display: none;">
                    <label for="_5digit">Last 5 Digits</label>
                    <input type="text" class="form-control" id="_5digit">
                    <label for="_auth_code">Authentication Code</label>
                    <input type="text" class="form-control" id="_auth_code">
                    <label for="_receipt_total">Credit Card Receipt Total</label>
                    <input type="text" class="form-control price-validation" id="_receipt_total">
                </div>
                
                <div class="button-block">
                    <button class="btn theme-default-orange" id="<?=$btnID?>" data-id="<?=$salesID?>">Ok</button>
                    <button class="btn theme-default-orange" onclick="$('#selling-payment').modal('hide')">Cancel</button>
                </div>

		  	</div>
		</div>
	</div>
</div> -->