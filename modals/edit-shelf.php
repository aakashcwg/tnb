<div id="edit-shelf" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Shelf</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-shelf-form">
		    		<div class="row">
						<div class="col-md-12">
				    		<div class="form-group">
				    			<label for="shelf-name">Shelf Name<sup>*</sup></label>
				    			<input type="text" name="shelf_name" id="shelf-name" class="form-control">
				    		</div>
						</div>
						<div class="col-md-12">
				    		<div class="form-group">
				    			<label for="shelf-location">Shelf Loocation</label>
				    			<input type="text" name="shelf_location" id="shelf-location" class="form-control">
				    		</div>
						</div>

						<div class="col-md-12">
				    		<div class="form-group" id="wh-dropdown">
				    			
				    		</div>
						</div>
						<div class="col-md-12 text-center">
				    		<button class="btn theme-default" id="update-shelf">Update</button>
						</div>
					</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>