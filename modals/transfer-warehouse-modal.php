<?php
//transfer-warehouse-modal.php
global $InventoryOBJ;
?>

<div id="transfer-warehouse-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Transfer Warehouse</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="transfer-warehouse-form">
		    		<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="warehouse-from">FROM<sup>*</sup></label>
								<select id="warehouse-from" class="form-control sd-select" data-show-subtext="true" data-live-search="true" name="warehouse-from">
				    				<option value="">Select Warehouse</option>
				    				<?php if(getWareHouses()) { ?>
				    				<?php foreach (getWareHouses() as $WH) {
				    					echo '<option value="'.$WH->WarehouseID.'">'.$WH->WName.'</option>';
				    				} } ?>
				    			</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="warehouse-to">TO<sup>*</sup></label>
								<select id="warehouse-to" class="form-control sd-select" data-show-subtext="true" data-live-search="true" name="warehouse-to">
				    				<option value="">Select Warehouse</option>
				    				<?php if(getWareHouses()) { ?>
				    				<?php foreach (getWareHouses() as $WH) {
				    					echo '<option value="'.$WH->WarehouseID.'">'.$WH->WName.'</option>';
				    				} } ?>
				    			</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="warehouse-qty">QTY<sup>*</sup></label>
								<input type="text" name="warehouse-qty" id="warehouse-qty" class="form-control qty">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group" id="wh-items">
								<label for="on-hand-item">On Hand Item<sup>*</sup></label>
								<select id="on-hand-item" class="form-control sd-select" data-show-subtext="true" data-live-search="true" name="on-hand-item">
				    				<option value="">Select Item</option>
				    				<?php if($InventoryOBJ->GetOnHandItems()) { ?>
				    				<?php foreach ($InventoryOBJ->GetOnHandItems() as $OHI) {
				    					echo '<option value="'.$OHI->IOHInventoryID.'">'.getItemWithNumber($OHI->IOHInventoryID).'</option>';
				    				} } ?>
				    			</select>
							</div>
						</div>
						<div class="col-md-12 text-center">
					    	<button class="btn theme-default" id="transfer-warehouse-btn">Transfer</button>
						</div>
					</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>