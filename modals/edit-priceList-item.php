<div id="edit-pricelist-item" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Price List</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-pricelist-item-form">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="pl-dropdown">
                                <label for="price-list">Price List <sup>*</sup></label>
                                <select class="form-control sd-select" id="price-list" name="price-list" data-live-search="true">
                                    <option value="">Select Price List</option>
                                    <?php foreach (getPriceLists() as $PL) {
                                        ?>
                                        <option value="<?=$PL->PLID;?>"><?=$PL->PLName;?></option>
                                        <?php
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="item-dropdown">
                                <label for="item">Item <sup>*</sup></label>
                                <select class="form-control sd-select" id="item" name="item" data-live-search="true">
                                    <option value="">Select an Item</option>
                                    <?php foreach (getInventories() as $INV) {
                                        ?>
                                        <option value="<?=$INV->INVID;?>"><?=getItemWithNumber($INV->INVID);?></option>
                                        <?php
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="form-group check-primary">
                                <label for="is-percentage">Is Percentage ?</label>
                                <input type="checkbox" class="form-control is_percentage" name="is-percentage" id="is-percentage" onclick="MakeFormat(this)">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="pl-amount">Discount Amt/%</label>
                                <input type="text" class="form-control price-validation pl_amount" name="pl-amount" id="pl-amount" onblur="MakeFormat(this)">
                            </div>
                        </div>

                        <input type="hidden" id="pliid" name="pliid">
                        <input type="hidden" name="current_emp_id" id="current-emp-id" class="form-control" value="<?php echo currentUser('EmployeeID'); ?>">

                        <div class="col-md-12 text-center">
                            <a href="javascript:void(0)" onclick="UpdatePriceListItem()" class="btn theme-default" id="update-pricelist-item">Update</a>
                        </div>

                    </div>

                </form>
		  	</div>
		</div>
	</div>
</div>