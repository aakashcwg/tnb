<div id="edit-ledger-number" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Ledger Number</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-ledger-number-form">
		    		<div class="row">
		    			<div class="col-md-12">
		    				<div class="form-group">
		    					<div class="row">
		    						<div class="col-md-6">
		    							<label for="ledger-number">Ledger Number<sup>*</sup></label>
		    							<input type="text" name="ledger-number" id="ledger-number" class="form-control" onkeyup="this.value=sdMakeLedgerId(this.value)">
		    						</div>
		    						<div class="col-md-6">
		    							<label for="ledger-number-desc">Description</label>
		    							<textarea name="ledger_number_desc" id="ledger-number-desc" class="form-control"></textarea>
		    						</div>
		    						<div class="col-md-4 col-md-offset-4">
		    							<button class="btn theme-default" id="update-ledger-number-btn">Update</button>
		    						</div>	
		    					</div>
				    		</div>
		    			</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>