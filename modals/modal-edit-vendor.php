<div id="edit-vendor" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Vendor</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-vendor-form">
					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
				    			<label for="vcompany">Vandor Company<sup>*</sup></label>
				    			<input type="text" name="vcompany" id="vcompany" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaccno">Account Number</label>
				    			<input type="text" name="vaccno" id="vaccno" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vfname">First Name</label>
				    			<input type="text" name="vfname" id="vfname" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vlname">Last Name</label>
				    			<input type="text" name="vlname" id="vlname" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaddrtype">Address Type</label>
				    			<select id="vaddrtype" name="vaddrtype" class="form-control">
				    				<option value="" id="opt_0" data-id=''>--Select Address Type--</option>
				    				<?php foreach (getAddressTypes() as $type) {
				    					?>
				    					<option value="<?php echo $type->ATName; ?>" id="opt_<?php echo $type->ATID; ?>" data-id="<?php echo $type->ATID; ?>"><?php echo $type->ATName; ?></option>
				    					<?php
				    				} ?>
				    			</select>
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaddr1">Address 1</label>
				    			<input type="text" name="vaddr1" id="vaddr1" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaddr2">Address 2</label>
				    			<input type="text" name="vaddr2" id="vaddr2" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vcity">City</label>
				    			<input type="text" name="vcity" id="vcity" class="form-control">
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vstate">State</label>
				    			<select class="form-control" name="vstate" id="vstate">
				                    <option value="">-- Select State --</option>
				                    <
				                    <?php foreach (getUSAStates() as $key => $val) {
				                        ?>
				                        <option value="<?=$key;?>" id="<?=str_replace(' ', '-', strtolower($key));?>"><?=$val;?></option>
				                        <?php
				                    } ?>
				                </select>
				    		</div>
						</div>

						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vzip">Zip Code</label>
				    			<input type="text" name="vzip" id="vzip" class="form-control">
				    		</div>
						</div>

				        <div class="col-md-3">
				            <div class="form-group">
				                <label for="vcontacttype">Contact Type</label>
				                <select id="vcontacttype" name="vcontacttype" class="form-control">
				                    <option value="" id="opt_0" data-id=''>--Select Contact Type--</option>
				                    <?php foreach (getContactTypes() as $type) {
				                        ?>
				                        <option value="<?php echo $type->CTName; ?>" id="opt_<?php echo $type->CTID; ?>" data-id="<?php echo $type->CTID; ?>"><?php echo $type->CTName; ?></option>
				                        <?php
				                    } ?>
				                </select>
				            </div>
				        </div>

						<div class="col-md-3">
				    		<div class="form-group">
				    			<label for="vcdata">Contact Data</label>
				    			<input type="text" name="vcdata" id="vcdata" class="form-control">
				    		</div>
						</div>

				        <div class="col-md-3">
				            <div class="form-group check-primary">
				                <label for="vpcontact">Primary Contact ?</label>
				                <input type="checkbox" value="1" name="vpcontact" id="vpcontact" class="form-control">
				            </div>
				        </div>

				        <div class="col-md-3">
				            <div class="form-group check-primary">
				                <label for="visactivecheck"><input type="checkbox" value="1" name="visactivecheck" id="visactivecheck" class="form-control" checked>Is Active ?</label>
				            </div>
				        </div>

				        <input type="hidden" id="hidden-vendor-id">
				        <input type="hidden" id="request-from" value="modal">

						<div class="col-md-2 col-md-offset-5">
							<a href="javascript:void(0)" class="btn theme-default" id="update-vendor-btn" data-venid="" onclick="updateVandor(this)">Update</a>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>