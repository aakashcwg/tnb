<div id="add-customer-tax-details" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Give Details</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="add-customer-tax-details">
		    		<div class="row">
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="tax-id-number">Tax ID Number</label>
				    			<input type="text" name="tax_id_number" id="tax-id-number" class="form-control" onkeyup="$('#add-customer-form #tax-id').val(this.value);$('#edit-customer-form #tax-id').val(this.value)">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="expire-date">Expiration Date</label>
				    			<input type="text" name="expire_date" id="expire-date" class="form-control" onkeyup="$('#add-customer-form #expiration-date').val(this.value);$('#edit-customer-form #expiration-date').val(this.value)">
				    		</div>
						</div>
						<div class="col-md-12 text-center">
				    		<a href="javascript:void(0)" class="btn theme-default" id="update-customer-tax-details" onclick="$('#add-customer-tax-details .close').click()">Update</a>
						</div>
					</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>