<div id="edit-order" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Vendor</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-order-form">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
				    			<label for="vcompany">Vandor Company<sup>*</sup></label>
				    			<input type="text" name="vcompany" id="vcompany" class="form-control">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaccno">Account Number</label>
				    			<input type="text" name="vaccno" id="vaccno" class="form-control">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vfname">First Name</label>
				    			<input type="text" name="vfname" id="vfname" class="form-control">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vlname">Last Name</label>
				    			<input type="text" name="vlname" id="vlname" class="form-control">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaddrtype">Address Type</label>
				    			<select id="vaddrtype" name="vaddrtype" class="form-control">
				    				<option value="" id="opt_0" data-id=''>--Select Address Type--</option>
				    				<?php foreach (getAddressTypes() as $type) {
				    					?>
				    					<option value="<?php echo $type->ATName; ?>" id="opt_<?php echo $type->ATID; ?>" data-id="<?php echo $type->ATID; ?>"><?php echo $type->ATName; ?></option>
				    					<?php
				    				} ?>
				    			</select>
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaddr1">Address 1</label>
				    			<input type="text" name="vaddr1" id="vaddr1" class="form-control" onkeypress="if($('#vaddrtype option:selected').val()==''){ alert('Please select an address type first'); $('#vaddrtype').focus(); return false;} " onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vaddr2">Address 2</label>
				    			<input type="text" name="vaddr2" id="vaddr2" class="form-control" onkeypress="if($('#vaddrtype option:selected').val()==''){ alert('Please select an address type first'); $('#vaddrtype').focus(); return false;}" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vcity">City</label>
				    			<input type="text" name="vcity" id="vcity" class="form-control" onkeypress="if($('#vaddrtype option:selected').val()==''){alert('Please select an address type first');$('#vaddrtype').focus();return false;}" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vstate">State</label>
				    			<input type="text" name="vstate" id="vstate" class="form-control" onkeypress="if($('#vaddrtype option:selected').val()==''){alert('Please select an address type first');$('#vaddrtype').focus();return false;}" onkeyup="this.value=sdUcWords(this.value)">
				    		</div>
						</div>
						<div class="col-md-6">
				    		<div class="form-group">
				    			<label for="vzip">Zip Code</label>
				    			<input type="text" name="vzip" id="vzip" class="form-control" onkeypress="if($('#vaddrtype option:selected').val()==''){alert('Please select an address type first');$('#vaddrtype').focus();return false;}">
				    		</div>
						</div>
				        <div class="col-md-4">
				            <div class="form-group">
				                <label for="vcontacttype">Contact Type</label>
				                <select id="vcontacttype" name="vcontacttype" class="form-control">
				                    <option value="" id="opt_0" data-id=''>--Select Contact Type--</option>
				                    <?php foreach (getContactTypes() as $type) {
				                        ?>
				                        <option value="<?php echo $type->CTName; ?>" id="opt_<?php echo $type->CTID; ?>" data-id="<?php echo $type->CTID; ?>"><?php echo $type->CTName; ?></option>
				                        <?php
				                    } ?>
				                </select>
				            </div>
				        </div>
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="vcdata">Contact Data</label>
				    			<input type="text" name="vcdata" id="vcdata" class="form-control" onkeypress="if($('#vcontacttype option:selected').val()==''){alert('Please select a contact type first');$('#vcontacttype').focus();return false;}">
				    		</div>
						</div>
				        <div class="col-md-4">
				            <div class="form-group check-primary">
				                <label for="check">Primary Contact ?</label>
				                <input type="checkbox" value="1" name="vpcontact" id="check" class="form-control">
				            </div>
				        </div>

						<div class="col-md-2 col-md-offset-5">
							<button class="btn theme-default" id="update-vendor-btn" data-venid="">Update</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>