<?php 
//new-price-list-item.php
?>
<div id="price-list-item" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">New List Item</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="price-list-item-form">
		    		<div class="row">
						<div class="col-md-6">
							<div class="form-group" id="plipl-dropdown">
								<label for="price-list">Price List <sup>*</sup></label>
								<select class="form-control sd-select" id="price-list" name="price-list" data-live-search="true">
									<option value="">Select Price List</option>
									<?php foreach (getPriceLists() as $PL) {
										?>
										<option value="<?=$PL->PLID;?>"><?=$PL->PLName;?></option>
										<?php
									} ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="item">Item <sup>*</sup></label>
								<select class="form-control sd-select" id="item" name="item" data-live-search="true">
									<option value="">Select an Item</option>
									<?php foreach (getInventories() as $INV) {
										?>
										<option value="<?=$INV->INVID;?>"><?=getItemWithNumber($INV->INVID);?></option>
										<?php
									} ?>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-6">
							<div class="form-group check-primary">
								<label for="is-percentage-pli">Is Percentage ?</label>
								<input type="checkbox" class="form-control is_percentage" name="is-percentage" id="is-percentage-pli" onclick="MakeFormat(this)">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="pl-amount">Discount Amt/%</label>
								<input type="text" class="form-control price-validation pl_amount" name="pl-amount" id="pl-amount" onblur="MakeFormat(this)">
							</div>
						</div>
						<div class="col-md-12 text-center">
							<a href="javascript:void(0)" class="btn theme-default" id="add-list-item" onclick="AddPriceListItem()">Add</a>
						</div>
					</div>
				</form>
		  	</div>
		</div>
	</div>
</div>