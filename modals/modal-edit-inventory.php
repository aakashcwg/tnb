<div id="edit-inventory" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Inventory</h4>
		  	</div>
		  	<div class="modal-body">

		        <div class="alert alert-dismissible" role="alert" id="modal-inv-alert" style="display: none;">
		            <strong id="modal-inv-alert-message"></strong>
		            <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
		            </button>
		        </div>

		    	<form id="edit-inventory-form">
		    		<div class="row">
		    			<div class="col-md-4">
		    				<div class="form-group">
				    			<label for="itemname">Item Name<sup>*</sup></label>
				    			<input type="text" name="itemname" id="itemname" class="form-control" value="">
				    		</div>
		    			</div>
			    		<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="itemnumber">Item Number<sup>*</sup></label>
				    			<input type="text" name="itemnumber" id="itemnumber" class="form-control" value="">
				    		</div>
			    		</div>
			    		<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="itembarcode">Item Barcode</label>
				    			<input type="text" name="itembarcode" id="itembarcode" class="form-control" value="">
				    		</div>
			    		</div>
			    		<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="itemserialnumber">Item Serial Number</label>
				    			<input type="text" name="itemserialnumber" id="itemserialnumber" class="form-control" value="">
				    		</div>
			    		</div>
			    		<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="itemdescription">Item Description</label>
				    			<input type="text" name="itemdescription" id="itemdescription" class="form-control" value="">
				    		</div>
			    		</div>
			    		<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="itemwarrenty">Item Warrenty</label>
		                        <select id="itemwarrenty" name="itemwarrenty" class="form-control">
		                            <option id="opt_1" value="1">Yes</option>
		                            <option id="opt_0" value="0">No</option>
		                        </select>
				    		</div>
			    		</div>
			    		<div class="col-md-4">
				            <div class="form-group">
				                <div class="form-group check-primary">
				                	<label for="itemisactive"><input type="checkbox" name="itemisactive" id="itemisactive" class="form-control" checked>Is Active ?</label>
				            	</div>
				            </div>
				        </div>

				        <div class="col-md-4">
				            <div class="form-group">
				                <label for="minqty">Min Qty<sup>*</sup></label>
				                <input type="number" name="minqty" id="minqty" class="form-control qty" value="1">
				            </div>
				        </div>

				        <div class="col-md-4">
				            <div class="form-group">
				                <label for="maxqty">Max Qty<sup>*</sup></label>
				                <input type="number" name="maxqty" id="maxqty" class="form-control qty" value="10">
				            </div>
				        </div>

				        <input type="hidden" name="invid" value="">
				        <input type="hidden" name="request-from" class="request-from" value="<?=Rf();?>">

			    		<div class="col-md-12 text-center">
			    			<button class="btn theme-default" id="update-inventory-btn" data-invid="">Update</button>
			    		</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>