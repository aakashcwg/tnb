<div id="edit-warehouse" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Edit Warehouse</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-warehouse-form">
		    		<div class="row">
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="house-name">House Name<sup>*</sup></label>
				    			<input type="text" name="house_name" id="house-name" class="form-control">
				    		</div>
						</div>
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="house-address">Address</label>
				    			<input type="text" name="house_address" id="house-address" class="form-control">
				    		</div>
						</div>
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="house-city">City</label>
				    			<input type="text" name="house_city" id="house-city" class="form-control">
				    		</div>
						</div>
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="house-state">State</label>
				    			<select class="form-control" name="house_state" id="house-state">
				                    <option value="">Select State</option>
				                    <
				                    <?php foreach (getUSAStates() as $key => $val) {
				                        ?>
				                        <option value="<?=$key;?>"><?=$val;?></option>
				                        <?php
				                    } ?>
				                </select>
				    		</div>
						</div>
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="house-zip">Zip Code</label>
				    			<input type="text" name="house_zip" id="house-zip" class="form-control">
				    		</div>
						</div>
						<div class="col-md-4">
				    		<div class="form-group">
				    			<label for="house-phone">Phone</label>
				    			<input type="text" name="house_phone" id="house-phone" class="form-control">
				    		</div>
						</div>
						<div class="col-md-12 text-center">
				    		<button class="btn theme-default" id="update-warehouse">Update</button>
						</div>
					</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>