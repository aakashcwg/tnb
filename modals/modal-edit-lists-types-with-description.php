<div id="edit-list-type-with-description" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title"></h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="edit-list-type-with-description-form">
		    		<div class="row">
		    			<div class="col-md-12">
		    				<div class="form-group">
		    					<div class="row">
		    						<div class="col-md-6">
		    							<label for="list-type"></label>
		    							<input type="text" name="list_type" id="list-type" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
		    						</div>
		    						<div class="col-md-6">
		    							<label for="list-type-desc">Description</label>
		    							<textarea name="list_type_desc" id="list-type-desc" class="form-control"></textarea>
		    						</div>
		    						<input type="hidden" id="hidden-id">
		    						<input type="hidden" id="hidden-action">
		    						<div class="col-md-4 col-md-offset-4">
		    							<a href="javascript:void(0)" class="btn theme-default" id="update-list-type-btn" onclick="updateListsTypes(this)">Update</a>
		    						</div>	
		    					</div>
				    		</div>
		    			</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>