<?php
//add-tax-rate.php
?>
<div id="add-tax-rate" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal">&times;</button>
		    	<h4 class="modal-title">Add New Tax Rate</h4>
		  	</div>
		  	<div class="modal-body">
		    	<form id="add-tax-rate-form">
		    		<div class="row">
		    			<div class="col-md-12">
		    				<div class="form-group">
		    					<div class="row">
		    						<div class="col-md-6">
		    							<label for="tax-rate">Tax Rate<sup>*</sup></label>
		    							<input type="text" name="tax-rate" id="tax-rate" class="form-control qty">
		    						</div>
		    						<div class="col-md-6">
		    							<label for="tax-rate-name">Tax Rate Name</label>
		    							<input type="text" name="tax-rate-name" id="tax-rate-name" class="form-control ">
		    						</div>
		    						<div class="col-md-12 text-center">
		    							<a href="javascript:void(0)" class="btn theme-default" onclick="addTaxRate()">Add</a>
		    						</div>	
		    					</div>
				    		</div>
		    			</div>
		    		</div>
		    	</form>
		  	</div>
		</div>
	</div>
</div>