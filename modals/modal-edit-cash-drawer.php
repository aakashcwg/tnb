<div id="edit-cash-drawer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Cash Drawer</h4>
            </div>
            <div class="alert alert-success alert-dismissible" role="alert" id="edit-cashdrawer-alert" style="display: none;">
                <strong id="edit-cashdrawer-alert-message"></strong>
                <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="edit-cash-drawer-form">
                	<div class="row">
                		<div class="col-md-6">
                    		<div class="form-group">
                    			<label for="drawer-name">Drawer Name<sup>*</sup></label>
                    			<input type="text" name="drawer-name" id="drawer-name" class="form-control">
                    		</div>
                		</div>
                        <div class="col-md-6">
                            <div class="form-group" id="wh-dropdown">
                                <label for="warehouse">Warehouse</label>
                                <select class="form-control" name="warehouse" id="warehouse">
                                    <option value="">Select Warehouse</option>
                                    <
                                    <?php foreach (getWareHouses() as $WH) {
                                        ?>
                                        <option value="<?=$WH->WarehouseID;?>"><?=$WH->WName;?></option>
                                        <?php
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="computer-name">Computer Name<sup>*</sup></label>
                                <input type="text" name="computer-name" id="computer-name" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" name="cdid" id="cdid">
                		<input type="hidden" name="active-employee" id="active-employee" class="form-control" value="<?php echo currentUser('EmployeeID'); ?>">
                		<div class="col-md-12 text-center">
                			<button class="btn theme-default" id="update-cash-drawer">Update</button>
                		</div>
                	</div>
                </form>
            </div>
        </div>
    </div>
</div>