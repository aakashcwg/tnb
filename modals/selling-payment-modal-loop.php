<div id="selling-payment-loop" class="modal fade selling-payment" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Compelete Payment Process</h4>
            </div>
            <div class="modal-body" style="text-align:center;">
                <div class="text">
                    <h5>Payment Method</h5>
                </div>
                <div class="link-block">
                    <?php
                    global $ListTypeOBJ;
                    $PMs = $ListTypeOBJ->GetAllPaymentMethods();
                    if(!empty($PMs)){
                        foreach ($PMs as $PM) {
                            if($PM->PaymentMethodName != 'House Account'){
                                if($PM->PaymentMethodName == 'Cash'){
                                    $attr = '_cash';
                                }elseif($PM->PaymentMethodName == 'Check'){
                                    $attr = '_check';
                                }elseif($PM->PaymentMethodName == 'House Account'){
                                    $attr = '_ha';
                                }else{
                                    $attr = '_card';
                                }
                            ?>
                            <div class="section">
                                <a href="javascript:void(0)" class="selling-payment-method" id="<?=$PM->PMID;?>" data-card="<?=$attr;?>"><?=$PM->PaymentMethodName;?></a>
                            </div>
                            <?php
                            }
                        } 
                    } ?>
                </div>
                <div class="_cash _p_m" style="display: none;">
                    <label for="cmg" style="margin-bottom: 15px;">Cash Amount Given</label>
                    <input type="text" class="form-control price-validation" style="font-size: 20px;" id="cmg" onkeyup="SetLoopCashAmount(this)">
                    <label for="ctm" style="margin-top: 15px;"><strong>Customer Change</strong> : <span id="ctm">0.00</span></label>
                </div>
                <div class="_check _p_m" style="display: none;">
                    <label for="_check_no">Check No</label>
                    <input type="text" class="form-control" id="_check_no">
                    <label for="_check_amt">Input Amount</label>
                    <input type="text" class="form-control price-validation" id="_check_amt">
                </div>
                <div class="_card _p_m" style="display: none;">
                    <label for="_5digit">Last 5 Digits</label>
                    <input type="text" class="form-control" id="_5digit">
                    <label for="_auth_code">Authorization Code</label>
                    <input type="text" class="form-control" id="_auth_code">
                    <label for="_receipt_total">Credit Card Receipt Total</label>
                    <input type="text" class="form-control price-validation" id="_receipt_total">
                </div>
                <input type="hidden" id="loop-sales-id">
                <input type="hidden" id="loop-sales-due">
                <input type="hidden" id="loop-sales-rf">
                <div class="button-block">
                    <button class="btn theme-default-orange" onclick="SalesPaymentLoop()">Ok</button>
                    <!-- <button class="btn theme-default-orange" onclick="$('#selling-payment').modal('hide');$('#invoice-block').show();">Cancel</button> -->
                </div>

                <div id="loop-pm-details">
                    
                </div>
            </div>
        </div>
    </div>
</div>