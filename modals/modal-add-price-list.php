<div id="new-price-list" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Price List</h4>
            </div>
            <div class="modal-body">
                <form id="price-list-form">

                	<div class="row">

                		<div class="col-md-4">
                    		<div class="form-group">
                    			<label for="pl-name">Name<sup>*</sup></label>
                    			<input type="text" name="pl_name" id="pl-name" class="form-control">
                    		</div>
                		</div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pl-amount">Discount Amt/%</label>
                                <input type="text" name="pl_amount" id="pl-amount" class="form-control price-validation pl_amount" onblur="MakeFormat(this)">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pl-color">List Color</label>
                                <div id="plcolor" class="input-group colorpicker-component color-picker"> 
                                    <input type="text" value="#00AABB" name="pl_color" id="pl-color" class="form-control" /> 
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-4">
                            <div class="form-group check-primary">
                                <label for="pl_is_percentage_n">Is Percentage ?
                                <input type="checkbox" name="pl_is_percentage" id="pl_is_percentage_n" class="form-control is_percentage" onclick="MakeFormat(this)"></label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group check-primary">
                                <label for="has_items_n">Has Items ?
                                <input type="checkbox" name="has_items" id="has_items_n" class="form-control"></label>
                            </div>
                        </div>

                		<input type="hidden" name="current_emp_id" id="current-emp-id" class="form-control" value="<?php echo currentUser('EmployeeID'); ?>">

                		<div class="col-md-12 text-center">
                			<button class="btn theme-default" id="add-price-list">Add</button>
                		</div>

                	</div>

                </form>
            </div>
        </div>
    </div>
</div>