<?php
//add-non-inv-item.php
global $ListTypeOBJ;
?>
<form id="add-non-inv-form">

	<div class="alert alert-dismissible" role="alert" style="display: none;">
	    <strong class="alert-message"></strong>
	    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	</div>

	<div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="part-number">Part Number<sup>*</sup></label>
                <input type="text" name="part-number" id="part-number" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="item-name">Item Name<sup>*</sup></label>
                <input type="text" name="item-name" id="item-name" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="item-cost">Cost</label>
                <input type="text" name="item-cost" id="item-cost" class="form-control price-validation">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="item-desc">Description</label>
                <input type="text" name="item-desc" id="item-desc" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group check-primary">
                <label for="item-is-active">Is Active?<input type="checkbox" id="item-is-active" class="form-control" name="item-is-active" checked></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="ledger-id">Ledger ID</label>
                <select id="ledger-id" class="form-control" name="ledger-id">
                    <?php if($ListTypeOBJ->GetAllLedgerNumbers()) { ?>
                    <?php foreach ($ListTypeOBJ->GetAllLedgerNumbers() as $num) {
                        ?>
                        <option value="<?=$num->GeneralLedgerID;?>"><?=$num->GLNumber;?></option>
                        <?php
                    } } ?>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="item-price-type">Pricing Type</label>
                <select id="item-price-type" class="form-control" name="item-price-type">
                    <?php if($ListTypeOBJ->GetAllPricingTypes()) { ?>
                    <?php foreach ($ListTypeOBJ->GetAllPricingTypes() as $PricingType) {
                        ?>
                        <option value="<?=$PricingType->PTID;?>"><?=$PricingType->PTName;?></option>
                        <?php
                    } } ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="item-price">Price</label>
                <input type="text" name="item-price" id="item-price" class="form-control price-validation">
            </div>
        </div>
        <input type="hidden" name="current_employee_id" id="current-employee-id" value="<?=currentUser('EmployeeID')?>">
        <div class="col-md-12 text-center">
            <button class="btn theme-default" id="add-non-inv-btn">Add</button>
        </div>
    </div>
</form>

