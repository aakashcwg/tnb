<?php
//non-inv-items.php
?>
<?php
global $InventoryOBJ, $ListTypeOBJ;

//print_r($InventoryOBJ->GetTheNonInventoryItem(1));
?>
<table id="non-inv-items-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Part Number</th>
            <th>Name</th>
            <th>Cost</th>
            <th>Description</th>
            <th>Pricing Type</th>
            <th>Price</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        if($InventoryOBJ->GetAllNonInventoryItems()):
        	foreach ($InventoryOBJ->GetAllNonInventoryItems() as $Item) {
        		?>
        		<tr id="<?=$Item->NonInvID;?>" style="cursor: pointer;" onmouseover="$(this).css({'background-color' : '#4b77d2'})" onmouseleave="$(this).css({'background-color' : '#fff'})">
    	            <td><?=$Item->NonInvPartNumber;?></td>
    	            <td><?=$Item->NonInvName;?></td>
    	            <td><?=number_format($Item->NonInvCost, 2);?></td>
    	            <td><?=$Item->NonInvDescription;?></td>
    	            <td><?=$ListTypeOBJ->GetThePricingTypeByID($Item->NIPPriceTypeID);?></td>
    	            <td><?=number_format($Item->NIPPrice, 2);?></td>
    	            <td class="action-btns">
    	            	<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$Item->NonInvID;?>" style="font-size:24px;cursor:pointer;" onclick="EditNonInventoryItem(this)"></i>
                        <!-- <a style="margin-left: 10px;" href="<?php //echo SITE_URL.'?destination=purchasing&itemID='.$inventory->INVID; ?>"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="Order this item"></i></a> -->
    				</td>
    	        </tr>
    	    <?php
			}
        endif;
        ?>
    </tbody>
</table>