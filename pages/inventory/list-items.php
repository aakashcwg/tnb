<?php
//list-items.php
?>

<div class="row">
	<div class="alert alert-dismissible" role="alert" id="list-item-alert" style="display: none;">
	    <strong id="list-item-alert-message"></strong>
	    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	</div>
</div>

<?php

$PLs = GetPriceListItems();
$pli = '';
$PLURL = SITE_URL.'?destination=inventory&action=list-items';
if(isset($_GET['pli'])){
	$pli = $_GET['pli'];
	$PLs = GetPriceListItemsByPLID($pli);
}

?>
<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#price-list-item">Add(+)</a>
	<div class="price-list-filter">
		<select class="form-control" onchange="window.location= this.value ==''?'<?=$PLURL?>':'<?=$PLURL?>&pli='+this.value">
			<option value="">Select Price List</option>
			<?php
			if(getPriceLists()){
				foreach (getPriceLists() as $PL) {
					$selected = '';
					if($PL->PLID == $pli){
						$selected = 'selected';
					}else{
						$selected = '';
					}
					?>
					<option value="<?=$PL->PLID;?>" <?=$selected?>><?=$PL->PLName;?></option>
					<?php
				}
			}
			?>
		</select>
		<?php
		if($pli){
			$clear_filter = $PLURL;
		}else{
			$clear_filter = 'javascript:void(0)';
		}
		?>
		<a href="<?=$clear_filter?>" class="btn btn-default theme-default-orange" style="float: right; display: inherit;">Clear Filter</a>
	</div>
</div>
<div class="clearfix"></div>
<table class="table-bordered sd-table" id="price-list-item-table" width="100%">
	<thead>
		<tr>
			<th>Price List</th>
			<th>Item</th>
			<th>Is Percentage ?</th>
			<th>Discount Amt/%</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if($PLs) { ?>
			<?php foreach ($PLs as $PLI) {
				?>
				<tr id="<?=$PLI->PLIID;?>">
					<td><?=PriceListName($PLI->PLIPLID);?></td>
					<td><a href="<?=ItemUrl($PLI->PLIINVID);?>"><?=getItemWithNumber($PLI->PLIINVID);?></a></td>
					<td><?=$PLI->PLIIsPercentage==1?'Yes':'No';?></td>
					<td><?=number_format($PLI->PLIAmount, 2);?></td>
					<td><i class="fa fa-edit sd-tootip edit-price-list-item" data-id="<?=$PLI->PLIID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editPriceListItem(this)"></i></td>
				</tr>
				<?php
				}
			?>
			<?php
		} ?>
	</tbody>
</table>