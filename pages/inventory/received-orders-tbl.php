<?php
global $OrderOBJ, $EmployeeOBJ;
?>

<table id="received-orders-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Order Id</th>
            <th>Order Date</th>
            <th>Receiving Date</th>
            <th>Note</th>
            <th>Status</th>
            <th>Order By</th>
            <th>Receive By</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    	<?php
    	$Count = 1;
    	foreach ($OrderOBJ->GetAllOrdersWithReceive() as $OBJ) {
            $ReceiveClass = $OBJ->IORIsComplete == 1 ? 'received' : 'not-receive';
    		?>
    		<tr data-order_id="<?=$OBJ->IOID;?>" data-receive_id="<?=$OBJ->IORID;?>" class="<?=$ReceiveClass;?>">
	            <td><a href="<?=SITE_URL.'?destination=orders&action=view-order&id='.$OBJ->IOID;?>"><strong>#<?=$OBJ->IOID;?></strong></a></td>
                <td><?=date(DATE_TIME_FORMAT, strtotime($OBJ->IODateOrdered));?></td>
	            <td><?=date(DATE_TIME_FORMAT, strtotime($OBJ->IORDateReceived));?></td>
	            <td><?=$OBJ->IORNotes;?></td>
	            <td><?=$OBJ->IORIsComplete==0?'No Received yet':'Order Received';?></td>
                <td><?=$EmployeeOBJ->GetUserName($OBJ->IOEID);?></td>
                <td><?=$EmployeeOBJ->GetUserName($OBJ->IOREID);?></td>
                <td class="action-btns">
                    <?php if($OBJ->IORIsComplete==0) { ?>
                        <a href="<?=SITE_URL.'?destination=receiving&action=receive-items&order_id='.$OBJ->IOID.'&receive_id='.$OBJ->IORID;?>">Receive</a>
                    <!-- <a href="javascript:void(0)" onclick="ReceiveOrder(this)" data-current_empid="<?=currentUser('EmployeeID');?>">Receive</a> -->
                    <?php }else{ ?>
                        <a href="javascript:void(0)" style="cursor: no-drop;" class="disabled">Receive</a>
                    <?php } ?>
                </td>
	        </tr>
	    <?php
    	$Count++; }
        ?>
    </tbody>
</table>
