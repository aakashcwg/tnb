<table id="orders-table" class="table table-striped table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Order Id</th>
            <th>Customer</th>
            <th>Total Cost</th>
            <th>Total Sales Tax</th>
            <th>Total Shipping Cost</th>
            <th>Order Date</th>
            <th>PO Number</th>
            <th>Employee</th>
            <th>Ledger ID</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    	<?php
    	$countOrder = 1;
    	foreach (getOrders() as $order) {
            $empObj = getEployeeByID($order->IOEID);
            $theEmp = $empObj[$order->IOEID];
            $customerOBJ = getCustomerByID($order->IOCustomerID)[$order->IOCustomerID];
            if($customerOBJ){
                $theCustomer = $customerOBJ->CustomerFName.' '.$customerOBJ->CustomerLName.' ('.$customerOBJ->CustomerCompanyName.')';
            }else{
                $theCustomer = 'Anonymous';
            }

    		?>
    		<tr id="row-<?=$order->IOID;?>">
	            <td><a href="<?=SITE_URL.'?destination=view-order&id='.$order->IOID;?>"><strong>#<?=$order->IOID;?></strong></a></td>
	            <td><?=$theCustomer;?></td>
	            <td><?=number_format($order->IOTotalCost, 2);?></td>
	            <td><?=number_format($order->IOTotalSalesTax, 2);?></td>
	            <td><?=number_format($order->IOTotalShippingCost, 2);?></td>
	            <td><?=date(DATE_TIME_FORMAT, strtotime($order->IODateOrdered));?></td>
                <td><?=$order->IOOrderPONumber;?></td>
                <td><?=($theEmp)?$theEmp->ELUsername:'';?></td>
                <td><?=getLedgerNum($order->IOGLID);?></td>
                <td class="action-btns">
                    <a href="<?=SITE_URL.'?destination=orders&action=view-order&id='.$order->IOID;?>">View</a>
                </td>
	            <!-- <td class="action-btns">
	            	<i class="fa fa-edit sd-tootip" title="Edit" data-id="<?=$order->IOID;?>" style="font-size:24px;cursor:pointer;" onclick="editOrder(this)"></i> -->
	            	<!-- <i class="fa fa-trash-o" data-id="<?=$order->IOID;?>" style="font-size:24px;color:red;cursor:pointer;" onclick="delInventory(this)"></i> 
				</td>-->
	        </tr>
	    <?php
    	$countOrder++; }
        ?>
    </tbody>

    <!-- <tfoot>
        <tr>
            <th>Order Id</th>
            <th>Customer</th>
            <th>Total Cost</th>
            <th>Total Sales Tax</th>
            <th>Total Shipping Cost</th>
            <th>Order Date</th>
            <th>PO Number</th>
            <th>Employee</th>
            <th>Ledger ID</th>
            <th>Actions</th>
        </tr>
    </tfoot> -->
</table>
