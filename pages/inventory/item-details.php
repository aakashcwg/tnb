<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>
<?php
global $InventoryOBJ, $ListTypeOBJ, $VendorOBJ;
$itemID = isset($_GET['item-id']) ? $_GET['item-id'] : '';
if($InventoryOBJ->GetTheInventoryByID($itemID)){
	$item = $InventoryOBJ->GetTheInventoryByID($itemID)[0];
}

if($InventoryOBJ->GetOnHandItemByInventoryID($itemID)){
	$OnHand = $InventoryOBJ->GetOnHandItemByInventoryID($itemID);
}

$isActive = $item->IsActive==1?'<img src="dist/images/active.png" class="active-employee sd-tootip isactive" title="Active">':'<img src="dist/images/disabled.png" class="inactive-employee sd-tootip isactive" title="Inactive">';

echo '<h2>'.$item->INVItemName.' ( '.$item->INVItemNumber.' )</h2>';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 basic-details">
			<h3><u>Basic Details</u><span> <i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$item->INVID;?>" style="font-size:20px;cursor:pointer;" onclick="editInventory(this)"></i></span></h3>
			<div class="row">
				<div class="col-md-6">
					<strong>Item Name</strong>
				</div>
				<div class="col-md-6">
					<p><?=$item->INVItemName;?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Item Number</strong>
				</div>
				<div class="col-md-6">
					<p><?=$item->INVItemNumber;?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Bar Code</strong>
				</div>
				<div class="col-md-6">
					<p><?=$item->INVBarcodeNumber?$item->INVBarcodeNumber:'<i>Not Available</i>';?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Serial Number</strong>
				</div>
				<div class="col-md-6">
					<p><?=$item->INVSerialNumber?$item->INVSerialNumber:'<i>Not Available</i>';?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Description</strong>
				</div>
				<div class="col-md-6">
					<p><?=$item->INVDescription?$item->INVDescription:'<i>Not Available</i>';?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Has Warranty</strong>
				</div>
				<div class="col-md-6">
					<p><?=$item->INVIsWarrantyItem==1?'Yes':'No';?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Status</strong>
				</div>
				<div class="col-md-6">
					<p><?=$isActive;?></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="col-md-4 on-hand-details">
			<?php ?>
			<h3><u>On Hand</u><!-- <span> <i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$OnHand->IOHID;?>" style="font-size:20px;cursor:pointer;" onclick="EditOnHandItem(this)"></i></span> --></h3>
			<div class="row">
				<div class="col-md-6">
					<strong>Order ID</strong>
				</div>
				<div class="col-md-6">
					<p><b><?='#'.$OnHand->IOHIORDID;?></b></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Quantity</strong>
				</div>
				<div class="col-md-6">
					<p><?=$OnHand->IOHQuantity;?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Warehouse</strong>
				</div>
				<div class="col-md-6">
					<p><?=getWareHouseName($OnHand->IOHWarehouseID)?getWareHouseName($OnHand->IOHWarehouseID):'<i>None Provided</i>';?></p>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-6">
					<strong>Shelf</strong>
				</div>
				<div class="col-md-6">
					<p><?=getShelfName($OnHand->IOHShelfID)?getShelfName($OnHand->IOHShelfID):'<i>None Provided</i>';?></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="col-md-4 item-prices">
			
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row">
		<div class="col-md-4 list-prices">
			<h3>Item Prices</h3>
			<div class="row">
				<?php if($ListTypeOBJ->GetAllPricingTypes()){
					$cnt = 1;
				foreach ($ListTypeOBJ->GetAllPricingTypes() as $PT) {
					if($cnt == 2) break;
					$OHP = $InventoryOBJ->GetItemPriceByListID($OnHand->IOHID, $PT->PTID);
					$OHPPrice = '';
					$OHPID = '';
					if($OHP){
						$OHPPrice = number_format($OHP->IOHPPrice, 2);
						$OHPID = $OHP->IOHPID;
					}
					?>
				 	<div class="col-md-6">
				 		<label for="<?=strtolower(str_replace(' ', '-', $PT->PTName));?>"><?=ucwords($PT->PTName);?></label>
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control item-price price-validation" style="width:70%; float:left;" name="item-price" data-iohpid="<?=$OHPID?>" data-ptid="<?=$PT->PTID?>" data-ohid="<?=$OnHand->IOHID?>" value="<?=$OHPPrice?>" onblur="SetItemPrice(this)">
						<img src="<?=SITE_URL;?>/dist/images/reload.svg" class="loader" alt="" style="width:30px; height:30px; display: none; float:left; margin:0 0 0 10px" />
						<i class="fa fa-check ok" aria-hidden="true" style="float:left; font-size:20px; color:#527eda; margin:5px 0 0 10px; display: none;"></i>
					</div>
					<div class="clearfix"></div>
					<?php
				$cnt++;}
				} ?>
			</div>
		</div>
		<div class="col-md-6">
			<h3>Unique Vendors</h3>
			<div class="unique-vendors-block">
				<?php if($InventoryOBJ->GetAllUniqueVendors($itemID)) { ?>
				<table border="1">
					<tr>
						<th>Name</th>
						<th>Last Date of Purchased</th>
						<th>Latest Price</th>
					</tr>
				<?php
					foreach ($InventoryOBJ->GetAllUniqueVendors($itemID) as $UV) {
						$LID = $VendorOBJ->GetLastPurchaseDetails($UV->IODVendorID, $itemID);
						$V = $VendorOBJ->GetTheVendorByID($UV->IODVendorID);
						if($V){
							?>
							<tr>
								<td><?=VC_Name($V->VCIDFName, $V->VCIDLname, $V->VendorCompanyName);?></td>
								<td><?=date(DATE_TIME_FORMAT, strtotime($LID->IODDateOrdered));?></td>
								<td><?='$'.number_format($LID->IODCost, 2);?></td>
							</tr>
						<?php
						}
					}
				?>
				</table>
				<?php } ?>
			</div>
		</div>
	</div>
</div>



