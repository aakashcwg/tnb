<div class="row">
	<div class="col-md-12">
		<form id="add-warehouse-form">
			<div class="row">
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="house-name">House Name<sup>*</sup></label>
		    			<input type="text" name="house_name" id="house-name" class="form-control">
		    		</div>
				</div>
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="house-address">Address</label>
		    			<input type="text" name="house_address" id="house-address" class="form-control">
		    		</div>
				</div>
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="house-city">City</label>
		    			<input type="text" name="house_city" id="house-city" class="form-control">
		    		</div>
				</div>
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="house-state">State</label>
		    			<select class="form-control" name="house_state" id="house-state">
		                    <option value="">Select State</option>
		                    <
		                    <?php foreach (getUSAStates() as $key => $val) {
		                        ?>
		                        <option value="<?=$key;?>"><?=$val;?></option>
		                        <?php
		                    } ?>
		                </select>
		    		</div>
				</div>
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="house-zip">Zip Code</label>
		    			<input type="text" name="house_zip" id="house-zip" class="form-control">
		    		</div>
				</div>
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="house-phone">Phone</label>
		    			<input type="text" name="house_phone" id="house-phone" class="form-control">
		    		</div>
				</div>
				<div class="col-md-12 text-center">
		    		<button class="btn theme-default" id="add-new-warehouse">Add</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-12">
		<a href="javascript:void(0)" class="btn btn-default theme-default-orange sd-tootip" title="Transfer Warehouse" style="float: right;" data-toggle="modal" data-target="#transfer-warehouse-modal">Transfer</a>
		<h2>Warehouses</h2>
		<?php if(!empty(getWareHouses())) { ?>
		<table class="table-bordered warehouse-table sd-table" id="warehouse-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Address</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Phone</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach (getWareHouses() as $WH) { ?>
				<tr id="row-<?=$WH->WarehouseID;?>">
					<td><?=$WH->WName;?></td>
					<td><?=$WH->WAddress;?></td>
					<td><?=$WH->WCity;?></td>
					<td><?=$WH->WState?getUSAStates()[$WH->WState]:'';?></td>
					<td><?=$WH->WZip;?></td>
					<td><?=$WH->WMainPhone;?></td>
					<td>
						<i class="fa fa-edit sd-tootip" title="" data-id="<?=$WH->WarehouseID;?>" style="font-size:24px;cursor:pointer;" onclick="editWareHouse(this)" data-original-title="Quick Edit"></i>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{ ?>
			<b>No warehouse availbale</b>
		<?php } ?>

	</div>
</div>