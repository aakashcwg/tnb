<?php

global $OrderOBJ;


$orderID = isset($_GET['id']) ? $_GET['id'] : '';

$OrderStatus = $OrderOBJ->GetOrderStatusByOrderID($orderID);



if(existOrder($orderID)) :

$thisOrder = getOrder($orderID);
$orderDate = date('M d, Y h:i:sa', strtotime($thisOrder->IODateOrdered));
$empOBJ = getEployeeByID($thisOrder->IOEID);
$orderedBy = $empOBJ[0]->ELUsername;
$designation = get_the_role($empOBJ[0]->ELRoleID);
$customerOBJ = getCustomerByID($thisOrder->IOCustomerID)[$thisOrder->IOCustomerID];
if($customerOBJ){
	$theCustomer = $customerOBJ->CustomerFName.' '.$customerOBJ->CustomerLName.' ('.$customerOBJ->CustomerCompanyName.')';
}else{
	$theCustomer = 'Anonymous';
}
?>

	<div class="main-block">
	    <div class="my-account-block">
            <div class="all-block">
                <div class="rt-block">
                    <div class="order-details-block">
                        <h3 class="default">Order Details</h3>
                        <div class="thumb-box">
                            <aside class="block border">
                                <p><span>Order ID : </span><?php echo '#'.$orderID; ?></p>
                                <p><span>Date :</span> <?php echo $orderDate; ?></p>
                                <p><span>Ordered By :</span> <?php echo $orderedBy.' ( '.$designation.' ) '; ?></p>
                                <p><span>Ordered For :</span> <?php echo $theCustomer; ?></p>
                            </aside>
                            <aside class="block border text-center">
                                <h4>Order Status</h4>
                                <?php if($OrderStatus == 'Complete') : ?>
                                    <b>COMPLETE</b> 
                                    <!-- <button class="order-status btn theme-default-orange" id="order-completed" data-status="completed" data-orderid="<?=$orderID;?>">Complete</button> -->
                                <?php endif; ?>
                                <?php if($OrderStatus == 'Pending') : ?>
                                    <b>PENDING</b>
                                    <!-- <button class="order-status btn btn-default" id="order-pending" data-status="pending" data-orderid="<?=$orderID;?>">Pending</button> -->
                                <?php endif; ?>
                                <!-- <button class="order-status btn <?=order_status($orderID)=='Pending'?'theme-default-orange':'btn-default';?>" id="order-pending" data-status="pending" data-orderid="<?=$orderID;?>">Pending</button>
                                <button class="order-status btn <?=order_status($orderID)=='Completed'?'theme-default-orange':'btn-default';?>" id="order-completed" data-status="completed" data-orderid="<?=$orderID;?>">Completed</button> -->
                            </aside>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="table-responsive"> 
                            <table class="table table-bordered sd-table" id="view-order-table">
                                <thead>
                                    <tr>
                                        <!-- <th>#No</th> -->
							            <th>Inventory Item No</th>
							            <th>Item Cost</th>
							            <th>Sales Tax</th>
							            <th>Shipping Cost</th>
							            <th>Quantity</th>
							            <th>Total</th>
							            <th>Shipping Date</th>
							            <!-- <th>Status</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
							    	$countItem = 1;
							    	$orderTotal = array();
							    	foreach (getOrderDetails($orderID) as $theItem) {

							    		$tot = ($theItem->IODCost)*$theItem->IODQtyPerCase +$theItem->IODSalesTax+$theItem->IODShippingCost;
							    		$orderTotal[] = $tot;

							    		?>
							    		<tr id="row-<?=$theItem->IODID;?>">
							    			<!-- <td><input type="checkbox" id="check-no-<?=$countItem;?>"><label for="check-no-<?=$countItem;?>"><?=$countItem;?></label></td> -->
								            <td><?=invNum($theItem->IODINVID);?></td>
								            <td><?=number_format($theItem->IODCost, 2);?></td>
								            <td><?=number_format($theItem->IODSalesTax, 2);?></td>
								            <td><?=number_format($theItem->IODShippingCost, 2);?></td>
								            <td><?=$theItem->IODQtyPerCase;?></td>
								            <td><?=number_format($tot, 2);?></td>
							                <td class="shipping-date-td" id="shiptd-<?=$theItem->IODID;?>"><?=$theItem->IODDateShipped ? date('d-m-Y h:i:sa', strtotime($theItem->IODDateShipped)) : 'Null';?></td>
							                <!-- <td>
							                	<input type="radio" name="item-status-<?=$countItem;?>" id="pstatusfor-<?=$countItem;?>" <?=!$theItem->IODDateShipped ? 'checked' : '';?> class="status-check ps" data-status="pending" data-itemid="<?=$theItem->IODID;?>"><label for="pstatusfor-<?=$countItem;?>">Pending</label>
							                	<br>
							                	<input type="radio" name="item-status-<?=$countItem;?>" id="cstatusfor-<?=$countItem;?>" <?=$theItem->IODDateShipped ? 'checked' : '';?> class="status-check cs" data-status="completed" data-itemid="<?=$theItem->IODID;?>"><label for="cstatusfor-<?=$countItem;?>">Completed</label>
							                </td> -->
							                <!-- <td><?=$theItem->IODDateShipped ? 'Colpleted' : 'Pending';?></td> -->
								        </tr>
								    <?php
							    	$countItem++; }
							        ?>
                                </tbody>
                            </table>
                        </div>


                        <h3 class="default">Order Total</h3>
                        <div class="thumb-box">
                            <aside class="block border">
                            	<p><span>Total Cost :</span></p>
                            	<p><span>Total Sales Tax :</span></p>
                            	<p><span>Total Shipping Cost :</span></p>
                                <p><span>Sub Total :</span></p>
                                <p><span>Total :</span></p>
                            </aside>
                            
                            <aside class="block">
                            	<p><?php echo '$ '.theOrderTotalCost($orderID);?></p>
                            	<p><?php echo '$ '.theOrderTotalSalesTax($orderID);?></p>
                            	<p><?php echo '$ '.theOrderTotalShippingCost($orderID);?></p>
                                <p><?php echo '$ '.number_format(array_sum($orderTotal), 2);?></p>
                                <p><span><?php echo '$ '.number_format(array_sum($orderTotal), 2);?></span></p>
                            </aside>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
	    </div>
	</div>

<?php else: ?>

	<div class="wrong-order">
		<h2 style="text-align: center;">The order doesn't exists!</h2>
	</div>

<?php endif; ?>
