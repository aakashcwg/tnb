<?php
/*if(isset($_GET['itemID'])){
	$itemOBJ = getInvById($_GET['itemID']);
}*/
global $VendorOBJ, $CustomerOBJ;
?>
<div id="cerate-order">
	<form id="order-form">
		<div class="row">
			<div class="items-wrapper" id="items-wrapper">
				<div class="item-block" data-item="1">
					<div class="col-md-2">
						<div class="form-group">
							<label for="item">Item<sup>*</sup></label>
							<select class="form-control sd-select item" id="item" name="item" data-show-subtext="true" data-live-search="true" class="form-control">
                                <option value="">Select Item</option>
                                <?php foreach (getInventories() as $inventory) {
                                    ?>
                                    <option value="<?=$inventory->INVID;?>" <?=(isset($_GET['itemID']) && $_GET['itemID']==$inventory->INVID)?'selected':'';?>><?=getItemWithNumber($inventory->INVID);?></option>
                                    <?php
                                } ?>
                            </select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="quantity">Quantity<sup>*</sup></label>
							<input type="number" name="quantity[]" min="1" value="1" id="quantity" class="form-control quantity">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="cost">Cost<sup>*</sup></label>
							<input type="text" name="cost[]" id="cost" class="form-control cost price-validation">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="tax">Sales Tax<sup>*</sup></label>
							<input type="text" name="tax[]" id="tax" class="form-control tax price-validation">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="shipping-cost">Shipping Cost<sup>*</sup></label>
							<input type="text" name="shipping_cost[]" id="shipping-cost" class="form-control shipping-cost price-validation">
						</div>
					</div>

					<div class="col-md-2">
						<a href="javascript:void(0)" class="add-remove-btn add-selling-item" style="margin-top:24px;" onclick="addOrderItem(this)" data-btnid="1">+</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label for="vendor">Vendor</label><br>
					<select class="selectpicker" id="vendor" name="vendor" data-show-subtext="true" data-live-search="true" class="form-control">
						<?php 
						foreach ($VendorOBJ->GetAllVendors() as $vendor) {
							$theVendor = VC_Name($vendor->VCIDFName, $vendor->VCIDLname, $vendor->VendorCompanyName);
						 	echo '<option value="'.$vendor->VendorID.'">'.$theVendor.'</option>';
						}
						?>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="case">Order Case</label>
					<select id="case" name="case" class="form-control" onchange="this.value==1 ? $('#customer-block').show() : $('#customer-block').hide()">
						<option value="0">General</option>
						<option value="1">Special</option>
					</select>
				</div>
			</div>

			<div class="col-md-2" id="customer-block" style="display: none;">
				<div class="form-group">
					<label for="customer">Customer</label><br>
					<select class="selectpicker" id="customer" name="customer" data-show-subtext="true" data-live-search="true" class="form-control">
					<?php 
						foreach ($CustomerOBJ->GetAllCustomers() as $customer) {
							$theCustomer = VC_Name($customer->CustomerFName, $customer->CustomerLName, $customer->CustomerCompanyName);
						 	echo '<option value="'.$customer->CustomerID.'">'.$theCustomer.'</option>';
						}
					?>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="order-phone">Phone Number</label>
					<input type="text" name="order_phone" id="order-phone" class="form-control number-validation" placeholder="Phone">
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="ledger-number">Ledger ID</label>
					<select id="ledger-number" class="form-control" name="ledger_number">
						<?php foreach (getLedgerNumbers() as $num) {
							?>
							<option value="<?=$num->GLNumber;?>"><?=$num->GLNumber;?></option>
							<?php
						} ?>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="order-status">Order Status</label>
					<select class="form-control" name="order_status" id="order-status">
						<option value="1">Completed</option>
						<option value="0">Pending</option>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label for="order-note">Order Note</label>
					<textarea name="order_note" id="order-note" class="form-control" placeholder="Order Note"></textarea>
				</div>
			</div>

			<input type="hidden" name="current_emp" value="<?php echo currentUser('EmployeeID'); ?>">
			<div class="clearfix"></div>
			<div class="col-md-12 text-center">
				<button class="btn theme-default" id="create-order-btn">Order</button>
			</div>		
		</div>
	</form>
</div>