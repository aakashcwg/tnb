<table id="pending-orders-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Order Id</th>
            <th>Customer</th>
            <th>Total Cost</th>
            <th>Total Sales Tax</th>
            <th>Total Shipping Cost</th>
            <th>Order Date</th>
            <th>PO Number</th>
            <th>Employee</th>
            <th>Ledger ID</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    	<?php
    	$countOrder = 1;
    	foreach (get_pending_orders() as $order) {
            $empObj = getEployeeByID($order->IOEID);
            $theEmp = $empObj[$order->IOEID];
            $customerOBJ = getCustomerByID($order->IOCustomerID)[$order->IOCustomerID];
            
    		?>
    		<tr id="row-<?=$order->IOID;?>">
	            <td><a href="<?=SITE_URL.'?destination=view-order&id='.$order->IOID;?>"><strong>#<?=$order->IOID;?></strong></a></td>
	            <td><?=($customerOBJ)?$customerOBJ->CustomerFName.' '.$customerOBJ->CustomerLName.' ('.$customerOBJ->CustomerCompanyName.')' : '';?></td>
	            <td><?=number_format($order->IOTotalCost, 2);?></td>
	            <td><?=number_format($order->IOTotalSalesTax, 2);?></td>
	            <td><?=number_format($order->IOTotalShippingCost, 2);?></td>
	            <td><?=$order->IODateOrdered;?></td>
                <td><?=$order->IOOrderPONumber;?></td>
                <td><?=$theEmp->ELUsername;?></td>
                <td><?=getLedgerNum($order->IOGLID);?></td>
                <td class="action-btns">
                    <a href="<?=SITE_URL.'?destination=view-order&id='.$order->IOID;?>">View</a>
                </td>
	        </tr>
	    <?php
    	$countOrder++; }
        ?>
    </tbody>
</table>
