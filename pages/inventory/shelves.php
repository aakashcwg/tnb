<div class="row">
	<div class="col-md-12">
		<form id="add-shelf-form">
			<div class="row">
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="shelf-name">Shelf Name<sup>*</sup></label>
		    			<input type="text" name="shelf_name" id="shelf-name" class="form-control">
		    		</div>
				</div>
				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="shelf-location">Shelf Location</label>
		    			<input type="text" name="shelf_location" id="shelf-location" class="form-control">
		    		</div>
				</div>

				<div class="col-md-4">
		    		<div class="form-group">
		    			<label for="warehouse">Warehouse<sup>*</sup></label>
		    			<select id="warehouse" class="form-control sd-select" data-show-subtext="true" data-live-search="true">
		    				<option value="">-- Select Warehouse --</option>
		    				<?php foreach (getWareHouses() as $WH) {
		    					echo '<option value="'.$WH->WarehouseID.'">'.$WH->WName.'</option>';
		    				} ?>
		    			</select>
		    		</div>
				</div>

				<div class="col-md-12 text-center">
		    		<button class="btn theme-default" id="add-new-shelf">Add</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-12">
		<h2>Shelves</h2>
		<?php if(!empty(getShelves())) { ?>
		<table class="table-bordered shelf-table sd-table" id="shelf-table" style="width: 100%">
			<thead>
				<tr>
					<th>Name</th>
					<th>Location</th>
					<th>Warehouse</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach (getShelves() as $Shelf) { ?>
				<tr id="row-<?=$Shelf->ShelfID;?>">
					<td><?=$Shelf->ShelfName;?></td>
					<td><?=$Shelf->ShelfLocation;?></td>
					<td><?=getWareHouseName($Shelf->SWarehouseID);?></td>
					<td>
						<i class="fa fa-edit sd-tootip" title="" data-id="<?=$Shelf->ShelfID;?>" style="font-size:24px;cursor:pointer;" onclick="editShelf(this)" data-original-title="Quick Edit"></i>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{ ?>
			<b>No shelf availbale</b>
		<?php } ?>

	</div>
</div>