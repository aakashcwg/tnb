<?php
global $InventoryOBJ;
?>
<table id="on-hand-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Item</th>
            <th>Quantity</th>
            <th>Order ID</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        if(!empty($InventoryOBJ->GetOnHandItems())):
        	$countOnHandInventory = 1;
        	foreach ($InventoryOBJ->GetOnHandItems() as $OnHand) {
        		?>
        		<tr id="<?=$OnHand->IOHID;?>" style="cursor: pointer;" onmouseover="$(this).css({'background-color' : '#4b77d2'})" onmouseleave="$(this).css({'background-color' : '#fff'})" onclick="window.location.href='<?=ItemUrl($OnHand->IOHInventoryID);?>'">
    	            <td><?=($OnHand->IOHInventoryID)?getItemWithNumber($OnHand->IOHInventoryID):'';?></td>
    	            <td><?=$OnHand->IOHQuantity;?></td>
    	            <td><b><?='#'.$OnHand->IOHIORDID;?></b></td>
    	        </tr>
    	    <?php
        	$countOnHandInventory++; }
        endif;
        ?>
    </tbody>
</table>
