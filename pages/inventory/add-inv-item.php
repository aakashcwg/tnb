<?php

//add-inv.php.php

if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false){
    die("<h2>You are unable to access this page.</h2>");
}

?>

<div class="alert alert-dismissible" role="alert" style="display: none;" id="add-inv-alert">
    <strong id="add-inv-alert-message"></strong>
    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form id="add-inv-form">
	<div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="itemname">Item Name<sup>*</sup></label>
                <input type="text" name="itemname" id="itemname" class="form-control" value="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="itemnumber">Item Number<sup>*</sup></label>
                <input type="text" name="itemnumber" id="itemnumber" class="form-control" value="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="itembarcode">Item Barcode</label>
                <input type="text" name="itembarcode" id="itembarcode" class="form-control" value="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="itemserialnumber">Item Serial Number</label>
                <input type="text" name="itemserialnumber" id="itemserialnumber" class="form-control" value="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="itemdescription">Item Description</label>
                <input type="text" name="itemdescription" id="itemdescription" class="form-control" value="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="itemwarrenty">Is Warrant ?</label>
                <select id="itemwarrenty" name="itemwarrenty" class="form-control">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="form-group check-primary">
                <label for="itemisactive"><input type="checkbox" name="itemisactive" id="itemisactive" class="form-control" checked>Is Active ?</label>
            </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="minqty">Min Qty<sup>*</sup></label>
                <input type="number" name="minqty" id="minqty" class="form-control qty" value="1">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="maxqty">Max Qty<sup>*</sup></label>
                <input type="number" name="maxqty" id="maxqty" class="form-control qty" value="10">
            </div>
        </div>

        <input type="hidden" name="current_employee_id" id="current-employee-id" value="<?=currentUser('EmployeeID')?>">
        <div class="col-md-12 text-center">
            <button class="btn theme-default" id="add-inventory-btn" data-invid="">Add</button>
        </div>
    </div>
</form>