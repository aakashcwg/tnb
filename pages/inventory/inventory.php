<?php
global $InventoryOBJ;
$page = $_GET['page'] ?? 1;
$items = $_GET['items'] ?? 10;
$pageLimit = getInvMaxPage($items) - 1;
?>
<div class="row">
    <div class="col-md-2">
        <div class="form-group" style="width: 45%; float: left;">
            <select class="form-control" id="inv-per-page" onchange="window.location.href='<?=SITE_URL;?>?destination=inventory&page=<?=$page;?>&items='+this.value">
                <option value="10"  <?php echo $items==10?'selected':''; ?>>10</option>
                <option value="25"  <?php echo $items==25?'selected':''; ?>>25</option>
                <option value="50"  <?php echo $items==50?'selected':''; ?>>50</option>
                <option value="100" <?php echo $items==100?'selected':''; ?>>100</option>
            </select>
        </div>
        <div style="width: 50%; float: right;">
            <span style="">Items</span>
        </div>
    </div>
    <div class="col-md-8">
        <div class="alert alert-success alert-dismissible" role="alert" id="inv-alert" style="display: none;">
            <strong id="inv-alert-message"></strong>
            <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group" style="float:right;">
            <input type="text" class="form-control" name="inventory-search" id="inventory-search" placeholder="Search Items" onkeyup="SearchInventory()">
        </div>
    </div>
</div>

<div class="table-block">
    <table id="inventory-table" class="table table-bordered " style="width:100%">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Item Number</th>
                <th>Description</th>
                <th>On Hand</th>
                <th>Has Warranty?</th>
                <th>Is Active</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            if(!empty($InventoryOBJ->GetAllInventories())):
            	$countInventory = 1;
                if($page <= $pageLimit):
            	foreach (FetchInventoriesByPage($page, $items) as $inventory) {
                    $isActive = $inventory->IsActive==1?'<img src="dist/images/active.png" class="active-inv-item sd-tootip isactive" title="Active" onclick="invIsActive(this)" data-status="active">':'<img src="dist/images/disabled.png" class="inactive-inv-item sd-tootip isactive" title="Inactive" onclick="invIsActive(this)" data-status="inactive">';
            		?>
            		<tr id="row-<?=$inventory->INVID;?>" data-item="<?=$inventory->INVID;?>">
        	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)"><?=$inventory->INVItemName;?></td>
        	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)"><?=$inventory->INVItemNumber;?></td>
        	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)"><?=$inventory->INVDescription;?></td>
                        <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)">
                        <?=$InventoryOBJ->GetOnHandQtyByInvID($inventory->INVID);?>
                        </td>
        	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetails(this)"><?=$inventory->INVIsWarrantyItem==1?'Yes':'No';?></td>
                        <td class="isactive"><?=$isActive;?></td>
        	            <td class="action-btns">
        	            	<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$inventory->INVID;?>" style="font-size:24px;cursor:pointer;" onclick="editInventory(this)"></i>
                            <a style="margin-left: 10px;" href="<?php echo SITE_URL.'?destination=purchasing&itemID='.$inventory->INVID; ?>"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="Order this item"></i></a>
        				</td>
        	        </tr>
        	    <?php
            	$countInventory++; }
                else:
                    echo '<tr><td colspan="7">You have crossed maximum page limits.</td></tr>';
                endif;
            else :
                echo '<tr><td colspan="7">Inventory is empty.</td></tr>';
            endif;
            ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-6" style="display: flex;">
        <?php if($page > 1) : ?>
            <button class="btn theme-default-orange" onclick="window.location.href='<?=SITE_URL;?>?destination=inventory&page=<?=$page-1;?>&items=<?=$items;?>'">Previous</button>
        <?php endif; ?>
        <input type="number" class="form-control qty" id="page-no" value="<?=$page;?>" min="1" max="<?php echo getInvMaxPage($items)-1; ?>" style="width:70px;" onkeyup="ValidInputPageNum(this)">
        <button type="submit" id="go-page" class="btn theme-default-orange go-page">Go</button>
        <?php if($page < getInvMaxPage($items)-1) : ?>
            <button class="btn theme-default-orange" onclick="window.location.href='<?=SITE_URL;?>?destination=inventory&page=<?=$page+1;?>&items=<?=$items;?>'">Next</button>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    $("#go-page").on('click', function(){
        var pageno = $('#page-no').val();
        if(pageno == '' || isNaN(pageno)){
            pageno = 1;
        }
        var items = '<?php echo $items;?>';
        var siteurl = '<?php echo SITE_URL;?>';
        window.location.href = siteurl+'?destination=inventory&page='+pageno+'&items='+items;
    });
</script>
