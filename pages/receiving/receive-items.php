<?php

//receive-items.php
global $OrderOBJ, $EmployeeOBJ;
$order_id 	= $_GET['order_id'] ?? '';
$receive_id = $_GET['receive_id'] ?? '';

$Order = $OrderOBJ->GetTheOrder($order_id);

$Items = $OrderOBJ->GetAllOrderItemsByReceiveID($receive_id);

?>

<div class="row">
	<div class="col-md-6">
		<div class="row">

			<div class="col-md-6">
				<label>Order ID</label>
			</div>
			<div class="col-md-6">
				<p>#<?=$order_id;?></p>
			</div>

			<div class="col-md-6">
				<label>Order Date</label>
			</div>
			<div class="col-md-6">
				<p><?=date(DATE_TIME_FORMAT, strtotime($OrderOBJ->GetOrderDate($order_id)));?></p>
			</div>

			<div class="col-md-6">
				<label>Order By</label>
			</div>
			<div class="col-md-6">
				<p><?=$EmployeeOBJ->GetUserName($Order->IOEID);?></p>
			</div>

		</div>
	</div>
	<div class="clearfix"></div>
</div>
<hr>
<div class="row">

<?php
if($OrderOBJ->AllItemsReceived($receive_id) !== true){
?>
	<div id="receive-items">
		<?php
		foreach ($Items as $Item) {
			$OrderQty = $OrderOBJ->GetItemQuantityByOrderDetailsID($Item->IODID);
			if($Item->IORDQuantityReceived <= 0){
			?>
			<div class="each-item">
				<div class="col-md-2">
					<label for="item">Item</label>
					<input type="text" id="item" readonly value="#<?=$Item->IODINVID;?>" class="form-control item">
				</div>
				<div class="col-md-2">
					<label for="vitem">Vendor Item</label>
					<input type="text" id="vitem" readonly value="<?=$Item->IODVendorItemNumber;?>" class="form-control">
				</div>
				<div class="col-md-1">
					<label for="case">Case</label>
					<input type="Checkbox" id="case" class="form-control" <?=$Item->IODIsCase==1?'checked':'';?>>
				</div>
				<div class="col-md-1">
					<label for="case-qty">Case Qty</label>
					<input type="text" id="case-qty" readonly class="form-control" value="<?=$Item->IODQtyOfCases;?>">
				</div>
				<div class="col-md-1">
					<label for="qty-per-case">Qty Per Case</label>
					<input type="text" id="qty-per-case" readonly class="form-control qpc" value="<?=$Item->IODQtyPerCase;?>">
				</div>
				<div class="col-md-1">
					<div class="border"></div>
				</div>
				<div class="col-md-2">
					<label for="amt-received">Amt Received</label>
					<input type="text" id="amt-received" onkeyup="CofirmAmt(this)" class="form-control number-validation amt-received">
					<input type="hidden" name="" class="ar-c" value="-1">
				</div>
				<div class="col-md-2">
					<label for="note">Note</label>
					<input type="text" id="note" class="form-control note">
				</div>
				<input type="hidden" name="odid" id="odid" class="odid" value="<?=$Item->IODID;?>">
				<input type="hidden" name="ordid" id="ordid" class="ordid" value="<?=$Item->IORDID;?>">
				<input type="hidden" name="orid" id="orid" class="orid" value="<?=$Item->IORDIORID;?>">
			</div>
			<div class="clearfix"></div>
			<?php
			}
		}
		?>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12 text-center" style="margin-top: 50px;">
		<a href="javascript:void(0)" class="btn theme-default-orange" onclick="FinishReceiving()">Finish Receiving</a>
	</div>
<?php
}else{
	echo '<h2 class="text-center">No item is left for receiving<h2>';
}
?>
</div>