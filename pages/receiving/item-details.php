<?php
//item-details.php
include '../../functions.php';

$ODID 	= $_POST['ODID'];
$ORDID 	= $_POST['ORDID'];
$ORID 	= $_POST['ORID'];

$OrderQty = $OrderOBJ->GetItemQuantityByOrderDetailsID($ODID);

?>
<div class="row receive-details">
	<div class="alert alert-dismissible" role="alert" style="display: none;" id="receive-details-alert">
	    <strong id="receive-details-alert-message"></strong>
	    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	</div>
	<form>
		<div class="col-md-6">
			<label for="receive-qty">Received Quantity<sup>*</sup></label>
			<input type="number" name="receive-qty" id="receive-qty" class="form-control receive-qty" value="<?=$OrderQty;?>">
		</div>
		<div class="col-md-6">
			<label for="receive-note">Note</label>
			<textarea name="receive-note" id="receive-note" class="form-control receive-note"></textarea>
		</div>
		<div class="col-md-12 text-center">
			<a href="javascript:void(0)" onclick="ReceiveItem(this)" class="btn theme-default-orange" style="margin-top: 20px;" data-odid="<?=$ODID;?>" data-ordid="<?=$ORDID;?>" data-orid="<?=$ORID;?>">SAVE</a>
		</div>
	</form>
</div>