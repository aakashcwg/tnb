<?php
//receiving.php
include '../../functions.php';

$RID = $_POST['RID'];
$Items = $OrderOBJ->GetAllOrderItemsByReceiveID($RID);

?>

<table id="receive-order-item-table" class="table table-bordered sd-table">
	<thead>
		<tr>
			<th>Item</th>
			<th>Order Quantity</th>
			<th>Receive Quantity</th>
			<th>Note</th>
			<th>Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($Items as $Item) {
			$OrderQty = $OrderOBJ->GetItemQuantityByOrderDetailsID($Item->IODID);
			$ReceiveClass = $Item->IORDIsComplete == 1 ? 'received' : 'not-receive';
			?>
			<tr data-odid="<?=$Item->IODID;?>" data-ordid="<?=$Item->IORDID;?>" data-orid="<?=$Item->IORDIORID;?>" class="<?=$ReceiveClass;?>">
				<td><?=getItemWithNumber($Item->IODINVID);?></td>
				<td><?=$Item->IODQtyPerCase;?></td>
				<td><?=$Item->IORDQuantityReceived;?></td>
				<td><?=$Item->IORDNotes;?></td>
				<td><?=$Item->IORDDateReceived ? date(DATE_TIME_FORMAT, strtotime($Item->IORDDateReceived)) : '';?></td>
				<td>
					<?php if($Item->IORDIsComplete == 1) { ?>
						<a href="javascript:void(0)" style="cursor: no-drop;">Receive</a>
					<?php }else{ ?>
						<a href="javascript:void(0)" onclick="ReceiveOrderItem(this)">Receive</a>
					<?php } ?>
				</td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>