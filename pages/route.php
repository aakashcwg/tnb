<?php

switch ($_GET['action']) {

	case 'add-inv-item':
		echo getPage('inventory/add-inv-item');
		break;

	case 'add-non-inv-item':
		echo getPage('inventory/add-non-inv-item');
		break;

	case 'non-inv-items':
		echo getPage('inventory/non-inv-items');
		break;

	case 'employees-history':
			echo getPage('employee/employees-history');
		break;

	case 'new-customer':
		echo getPage('customer/new-customer');
		break;

	case 'edit-customer':
		echo getPage('customer/edit-customer');
		break;

	case 'edit-employee':
		echo getPage('employee/edit-employee');
		break;

	case 'new-vendor':
		echo getPage('vendor/new-vendor');
		break;

	case 'add-order':
		echo getPage('inventory/add-order');
		break;

	case 'view-order':
		echo getPage('inventory/view-order');
		break;

	case 'received-orders':
			echo getPage('inventory/received-orders-tbl');
		break;

	case 'pending-orders':
			echo getPage('inventory/pending-orders-tbl');
		break;

/*================List Types - START================*/

	case 'address-types':
		echo getPage('list-types/address-types');
		break;

	case 'contact-types':
		echo getPage('list-types/contact-types');
		break;

	case 'payment-types':
		echo getPage('list-types/payment-types');
		break;

	case 'customer-types':
		echo getPage('list-types/customer-types');
		break;

	case 'role-types':
		echo getPage('list-types/role-types');
		break;

	case 'pricing-types':
		echo getPage('list-types/pricing-types');
		break;

	case 'general-ledger-ids':
		echo getPage('list-types/general-ledger-ids');
		break;

	case 'price-list':
		echo getPage('list-types/price-list');
		break;

	case 'cash-drawers':
		echo getPage('list-types/cash-drawers');
		break;

/*================List Types - END================*/

	case 'vendor-profile':
		echo getPage('vendor/profile');
		break;

	case 'edit-vendor':
		echo getPage('vendor/edit-vendor');
		break;

	case 'employee-profile':
		echo getPage('employee/profile');
		break;

	case 'new-employee':
		echo getPage('employee/new-employee');
		break;

	case 'item-details':
		echo getPage('inventory/item-details');
		break;

	case 'customer-profile':
		echo getPage('customer/profile');
		break;

	case 'warehouse':
		echo getPage('inventory/warehouse');
		break;

	case 'shelves':
		echo getPage('inventory/shelves');
		break;

	case 'on-hand':
		echo getPage('inventory/on-hand');
		break;

	case 'sales-details':
		echo getPage('sales/details');
		break;

	case 'sales-history':
		echo getPage('sales/sales-history');
		break;

	case 'quote-history':
		echo getPage('sales/quote-history');
		break;

	case 'receive-items':
		echo getPage('receiving/receive-items');
		break;
	
	case 'sales-payments':
		echo getPage('sales/sales-payments');
		break;

	case 'list-items':
		echo getPage('inventory/list-items');
		break;

	case 'cash-out':
		echo getPage('sales/cash-out');
		break;

	default:
		# code...
		break;
}

?>