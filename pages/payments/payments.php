<?php 
global $VendorOBJ, $CustomerOBJ;

$vid = $_GET['vid'] ?? null;
$cid = $_GET['cid'] ?? null;

$Vendors = $VendorOBJ->GetAllVendors();
$Customers = $CustomerOBJ->GetAllCustomers();	
?>
<form id="check-vendor-payment">
	<div class="row">
		<div class="col-md-6 vendor-dropdown">
			<div class="form-group">
				<label for="vendor_p_">Select Vendor</label><br>
				<select id="vendor_p_" class="sd-select" data-show-subtext="true" data-live-search="true">
					<option value="">Select Vendor</option>
					<?php if($Vendors) {
						$selected = ''; 
					?>
					<?php foreach ($Vendors as $Vendor) {
						$selected = ($Vendor->VendorID == $vid)?'selected':'';
						?>
						<option value="<?=$Vendor->VendorID;?>" <?=$selected;?>><?php echo VC_Name($Vendor->VCIDFName, $Vendor->VCIDLname, $Vendor->VendorCompanyName); ?></option>
						<?php
					} } ?>
				</select>
			</div>
		</div>
		<div class="col-md-6 customer-dropdown">
			<div class="form-group">
				<label for="customer_p_">Select Customer</label><br>
				<select id="customer_p_" class="sd-select" data-show-subtext="true" data-live-search="true">
					<option value="">Select Customer</option>
					<?php if($Customers) { 
						$selected = '';
					?>
					<?php foreach ($Customers as $Customer) {
						$selected = ($Customer->CustomerID == $cid)?'selected':'';
						?>
						<option value="<?=$Customer->CustomerID;?>" <?=$selected;?>><?php echo VC_Name($Customer->CustomerFName, $Customer->CustomerLName, $Customer->CustomerCompanyName); ?></option>
						<?php
					} } ?>
				</select>
			</div>
		</div>
		<input type="hidden" name="current-employee-id" id="current-employee-id" value="<?=currentUser('EmployeeID')?>">
		<div class="col-md-12" id="vc_p_d_">
			<?php
				if($cid){
					echo CustomerPaymentHTML($cid);
				}
				if($vid){
					echo VendorPaymentHTML($vid);
				}
			?>
		</div>
	</div>
</form>
<div id="vendor-pay-receipt"  style="display: none;">
	
</div>
	