<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>

<!-- Header START -->
<?php if(isset($_GET['action']) && $_GET['action'] == 'customer-profile') : ?>

<?php else: ?>
	<a class="btn theme-default <?php echo (isset($_GET['action']) && $_GET['action'] == 'new-customer')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=customers&action=new-customer'; ?>">Add New Customer</a>
<?php endif; ?>
<!-- Header END -->

<!-- Content START -->
<div class="main-content-block">
	<?php
		if(isset($_GET['action']) && !empty($_GET['action'])){
			echo getPage('route');
		}else{
			echo getPage('customer/customers');
		}	
	?>
</div>
<!-- Content END -->