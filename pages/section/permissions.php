<?php
//permissions.php
global $ListTypeOBJ;
?>
<div class="row">
	<div class="col-md-6">
		<table class="table-bordered list-type-table" id="permission-table">
			<thead>
				<tr>
					<th>Section</th>
					<th>Permission</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php $cnt=1; foreach (getMenus() as $item) { 
					$Pval = GetSectionPermissionValue($item->SectionID);
					?>
					<tr id="<?=$item->SectionID;?>">
						<td><?=$item->SectionName;?></td>
						<td>
							<input type="number" name="permission" class="form-control permission" min="0" max="100" value="<?=$Pval;?>" onkeypress="return isNumberKey(event)">
							<!-- <?php
							global $ListTypeOBJ;
							foreach ($ListTypeOBJ->GetAllEmployeeRoles() as $RT) {
								?>
								<input type="checkbox" name="<?=strtolower($RT->ERRoleName);?>" value="<?=$RT->ERID;?>" id="<?=strtolower($RT->ERRoleName).'_'.$cnt;?>"><label for="<?=strtolower($RT->ERRoleName).'_'.$cnt;?>"><?=$RT->ERRoleName;?></label>
								
								<?php
							} ?> -->
						</td>
						<td><a href="javascript:void(0)" class="btn btn-default theme-default-orange" onclick="SavePermission(this)">Save</a></td>
					</tr>
				<?php
				$cnt++;} ?>
			</tbody>
		</table>
	</div>
	<div class="col-md-6">
		<table class="table-bordered list-type-table" id="section-items-table">
			<thead>
				<tr>
					<th style="text-align: center">Section Title</th>
					<th style="text-align: center">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach (getMenus() as $item) {
					?>
					<tr id="<?=$item->SectionID;?>" data-order="<?=$item->SectionOrder;?>">
						<td><?=$item->SectionName;?></td>
						<td><i class="fa fa-edit sd-tootip edit-section" data-action="edit_section" data-id="<?=$item->SectionID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
					</tr>
					<?php
				} ?>
			</tbody>
		</table>
	</div>
</div>
