<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>
<div class="row">
	<div class="col-md-8">
		<form id="create-section-form">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
		    			<input type="text" name="section" id="section" class="form-control" placeholder="Enter Section Name">
		    		</div>
				</div>
				<div class="col-md-2">
					<button class="btn theme-default" id="create-section-btn">Create</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-4">
		<table class="table-bordered list-type-table" id="section-items-table">
			<thead>
				<tr>
					<th style="text-align: center">Section Title</th>
					<th style="text-align: center">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach (getMenus() as $item) {
					?>
					<tr id="<?=$item->SectionID;?>" data-order="<?=$item->SectionOrder;?>">
						<td><?=$item->SectionName;?></td>
						<td><i class="fa fa-edit sd-tootip edit-section" data-action="edit_section" data-id="<?=$item->SectionID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
					</tr>
					<?php
				} ?>
			</tbody>
		</table>
	</div>
</div>