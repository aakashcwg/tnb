<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>

<!-- Header START -->
<?php if(isset($_GET['action']) && ($_GET['action'] == 'vendor-profile' || $_GET['action'] == 'edit-vendor-profile')) : ?>



<?php else: ?>

<a class="btn theme-default <?php echo (isset($_GET['action']) && $_GET['action'] == 'new-vendor')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=vendors&action=new-vendor'; ?>">Add New Vendor</a>

<?php endif; ?>
<!-- Header END -->

<!-- Content START -->
<div class="main-content-block">
	<?php
		if(isset($_GET['destination']) && $_GET['destination'] == 'vendors'){
			if(isset($_GET['action']) && !empty($_GET['action'])){
				echo getPage('route');
			}else{
				echo getPage('vendor/vendors');
			}
		}		
	?>
</div>
<!-- Content END -->