<?php if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false){
    die("<h2>You are unable to access this page.</h2>");
}
?>

<!-- Header START -->


<!-- Header END -->

<!-- Content START -->
<div class="main-content-block">
	<?php
		if(isset($_GET['destination']) && $_GET['destination'] == 'orders'){
			if(isset($_GET['action']) && !empty($_GET['action'])){
				echo getPage('route');
			}else{
				//echo '<div class="table-container">';
					echo getPage('inventory/orders-tbl');
				//echo '</div>';
			}
		}		
	?>
</div>
<!-- Content END -->
