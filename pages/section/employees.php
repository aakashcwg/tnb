<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");

$action = $_GET['action'] ?? '';
?>

<!-- Header START -->
<?php if($action == 'profile') : ?>


<?php else: ?>
		<a href="<?= SITE_URL.'?destination=employees&action=new-employee';?>" class="btn theme-default <?php echo ($action == 'new-employee')?'active':''; ?>" title="Add New Employee">Add New Employee</a>

		<a class="btn theme-default <?php echo ($action == 'employees-history')?'active':''; ?>" href="<?= SITE_URL.'?destination=employees&action=employees-history'?>" title="View Employees History">Employee History</a>
<?php endif; ?>	
<!-- Header END -->

<!-- Content START -->
<div class="main-content-block" id="employee-data-block">
	<?php
		if(isset($_GET['action']) && !empty($_GET['action'])){
			echo getPage('route');
		}else{
			echo getPage('employee/employees');
		}	
	?>
</div>
<!-- Content END -->
