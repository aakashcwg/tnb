<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>

<!-- Header START -->

<?php // ?>

<!-- Header END -->
<a class="btn btn-default theme-default-orange" href="<?php echo SITE_URL.'?destination=orders'; ?>">View Orders</a>
<!-- Content START -->
<div class="main-content-block">
	<?php
		if(isset($_GET['action']) && !empty($_GET['action'])){
			echo getPage('route');
		}else{
			echo getPage('purchase/purchase');
		}	
	?>
</div>
<!-- Content END -->