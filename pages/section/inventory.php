<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>

<!-- Top Content START -->

<a class="btn btn-default theme-default-orange  <?php ActiveOnAction(array('add-inv-item', 'add-non-inv-item')); ?>" href="javascript:void(0)" onclick="AddItem()">Add Item(+)</a>

<a class="btn btn-default theme-default-orange <?php ActiveOnAction('on-hand'); ?>" href="<?php echo SITE_URL.'?destination=inventory&action=on-hand'; ?>">On Hand</a>

<a class="btn btn-default theme-default-orange <?php ActiveOnAction('non-inv-items'); ?>" href="<?php echo SITE_URL.'?destination=inventory&action=non-inv-items'; ?>">Non-Inventory Items</a>

<a class="btn btn-default theme-default-orange <?php ActiveOnAction('warehouse'); ?>" href="<?php echo SITE_URL.'?destination=inventory&action=warehouse'; ?>">Warehouse</a>

<a class="btn btn-default theme-default-orange <?php ActiveOnAction('shelves'); ?>" href="<?php echo SITE_URL.'?destination=inventory&action=shelves'; ?>">Shelves</a>

<a class="btn btn-default theme-default-orange <?php ActiveOnAction('price-list'); ?>" href="<?php echo SITE_URL.'?destination=inventory&action=price-list'; ?>">Price List</a>

<a class="btn btn-default theme-default-orange <?php ActiveOnAction('list-items'); ?>" href="<?php echo SITE_URL.'?destination=inventory&action=list-items'; ?>">List Items</a>

<!-- Top Content END -->

<!-- Content START -->
<div class="main-content-block">
	<?php
		if(isset($_GET['action']) && !empty($_GET['action'])){
			echo getPage('route');
		}else{
			echo getPage('inventory/inventory');
		}	
	?>
</div>
<!-- Content END -->