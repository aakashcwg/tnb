<?php
//list-types.php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2 align='center'>You are unable to access this page.</h2>");
?>

<!-- Top Section START -->
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'address-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=address-types'; ?>">Address Types</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'contact-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=contact-types'; ?>">Contact Types</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'payment-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=payment-types'; ?>">Payment Types</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'customer-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=customer-types'; ?>">Customer Types</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'role-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=role-types'; ?>">Role Types</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'pricing-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=pricing-types'; ?>">Pricing Types</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'pricing-types')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=general-ledger-ids'; ?>">General Ledger IDs</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'price-list')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=price-list'; ?>">Price List</a>
	<a class="btn btn-default theme-default-orange <?php echo (isset($_GET['action']) && $_GET['action'] == 'cash-drawers')?'active':''; ?>" href="<?php echo SITE_URL.'?destination=list-types&action=cash-drawers'; ?>">Cash Drawers</a>

	<!-- <a class="btn btn-default theme-default-orange <?php //echo (isset($_GET['action']) && $_GET['action'] == 'tax-rates')?'active':''; ?>" href="<?php //echo SITE_URL.'?destination=list-types&action=tax-rates'; ?>">Tax Rates</a> -->
<!-- Top Section END -->

<!-- Content START -->
<div class="main-content-block">
<?php
	if(isset($_GET['action']) && !empty($_GET['action'])){
		echo getPage('route');
	}else{
		echo getPage('list-types/list-types');
	}	
?>
</div>
<!-- Content END -->
