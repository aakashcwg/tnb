<?php
$eid = isset($_GET['eid']) ? base64_decode($_GET['eid']) : '';
global $EmployeeOBJ, $ListTypeOBJ;
$theEmployee = $EmployeeOBJ->GetTheEmployeeByID($eid);

$isActive = $theEmployee->IsActive==1?'<img src="'.SITE_URL.'/dist/images/active.png" class="active-employee sd-tootip isactive" title="Active">':'<img src="'.SITE_URL.'/dist/images/disabled.png" class="inactive-employee sd-tootip isactive" title="Inactive">';
?>

<div class="emp-profile">
    <div class="row">
        <!-- <div class="col-md-3">
            <div class="profile-img">
                <img src="<?=SITE_URL?>/dist/images/default-profile-pic.jpg" height="140px" width="140px" alt="<?=$theEmployee->EFName.' '.$theEmployee->ELName;?>">
            </div>
        </div> -->
        <div class="col-md-9">
            <div class="profile-head">
                <h3><?=$theEmployee->EFName.' '.$theEmployee->ELName;?></h3>
                <h6>Role : <i><?=get_the_role($theEmployee->ELRoleID);?></i></h6>
                <?php if($EmployeeOBJ->GetEmployeeJoiningDate($theEmployee->EmployeeID)) { ?>
                    <h6>Joining Date : <i><?=date(DATE_TIME_FORMAT, strtotime($EmployeeOBJ->GetEmployeeJoiningDate($theEmployee->EmployeeID)));?></i></h6>
                <?php } ?>
                
                <div class="tab-block">
                    <ul class="nav nav-tabs">
    				    <li class="active"><a data-toggle="tab" href="#basic-details">Basic Details</a></li>
    				    <li><a data-toggle="tab" href="#contact">Contact</a></li>
    				    <li><a data-toggle="tab" href="#address">Address</a></li>
                        <li><a data-toggle="tab" href="#reset-password">Reset Password</a></li>
    				</ul>
                    
    			  	<div class="tab-content profile-tab">
    			    	<div id="basic-details" class="tab-pane fade in active">
    				      	<h3>Basic Details</h3>
    				      	<div class="row">
    				      		<div class="col-md-6">
                                    <label>First Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->EFName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Last Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->ELName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Username</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->ELUsername;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Status</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$isActive;?></p>
                                </div>
                                <div class="clearfix"></div>
    				      	</div>
    			    	</div>
    			    	<div id="contact" class="tab-pane fade">
    					    <h3>Contact</h3>
    					    <div class="row">

    					    	<?php if(!empty($theEmployee->ECData)) { ?>
    				      		<div class="col-md-6">
                                    <label><?=$ListTypeOBJ->GetTheContactTypeByID($theEmployee->ECCTID);?></label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->ECData;?></p>
                                </div>
                            	<?php } ?>
    				      	</div>
    			    	</div>
    			    	<div id="address" class="tab-pane fade">
    			      		<h3>Address</h3>
    			      		<div class="row">

    					    	<?php if(!empty($theEmployee->EAddress1)) { ?>
    				      		<div class="col-md-6">
                                    <label>Address 1</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->EAddress1;?></p>
                                </div>
                            	<?php } ?>

                            	<?php if(!empty($theEmployee->EAddress2)) { ?>
                                <div class="col-md-6">
                                    <label>Address 2</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->EAddress2;?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($theEmployee->ECity)) { ?>
                                <div class="col-md-6">
                                    <label>City</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->ECity;?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($theEmployee->EState)) { ?>
                                <div class="col-md-6">
                                    <label>State</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=getUSAStates()[strtoupper($theEmployee->EState)];?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($theEmployee->EZip)) { ?>
                                <div class="col-md-6">
                                    <label>Zip Code</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theEmployee->EZip;?></p>
                                </div>
                                <?php } ?>

    				      	</div>
    			    	</div>
                        <div id="reset-password" class="tab-pane fade">
                            <h3>Reset Paassword</h3>
                            <div class="row">
                                <div class="alert alert-dismissible" role="alert" id="reset-employee-password-alert" style="display: none;">
                                    <strong id="reset-employee-password-alert-message"></strong>
                                    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form id="reset-password-form">
                                    <div class="col-md-4">
                                        <label>Enter New Password</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="password" id="password" class="form-control">
                                    </div>
                                    <input type="hidden" name="employee_id" value="<?=$theEmployee->EmployeeID;?>">
                                    <div class="col-md-2">
                                        <a href="javascript:void(0)" class="btn theme-default-orange" onclick="ResetPassword()">Update</a>
                                    </div>
                                </form>
                                <div class="clearfix"></div>
                            </div>
                        </div>
    			  	</div>
              </div>
            </div>
        </div>
        <div class="col-md-3 text-right">
            <a class="btn theme-default-orange" href="<?=SITE_URL.'?destination=employees&action=edit-employee&eid='.base64_encode($theEmployee->EmployeeID);?>">Edit profile</a>
        </div>
    </div>         
</div>












<!-- <div class="row">

	<div class="col-md-4 col-md-offset-4">
		<img src="<?=SITE_URL?>/dist/images/default-profile-pic.jpg" height="140px" width="140px">
		<a href="<?=SITE_URL.'?destination=employees&action=edit-employee-profile&eid='.base64_encode($eid);?>">Edit<i class="fa fa-edit" style="font-size:24px;"></i></a>
		<h3>Username : <?=$theEmployee->ELUsername;?></h3>
	</div>

	<div class="col-md-8 col-md-offset-2">

		<p><label>Name : </label><?=$theEmployee->EFName.' '.$theEmployee->ELName;?></p>
		<p><label>Role : </label><?=get_the_role($theEmployee->ELRoleID);?></p>
		<p><label>Adddress 1 : </label><?=$theEmployee->EAddress1;?></p>
		<p><label>Adddress 2 : </label><?=$theEmployee->EAddress2;?></p>
		<p><label>City : </label><?=$theEmployee->ECity;?></p>
		<p><label>State : </label><?=$theEmployee->EState;?></p>
		<p><label>Zip Code : </label><?=$theEmployee->EZip;?></p>
		<p><label>Is active : </label><?=$isActive;?></p>

	</div>

</div> -->