<?php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2>You are unable to access this page.</h2>");
	
//new-employee.php
global $ListTypeOBJ;
?>

<div class="alert alert-dismissible" role="alert" id="new-employee-alert" style="display: none;">
    <strong id="new-employee-alert-message"></strong>
    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<form id="new-employee-form">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
    			<label for="efname">First Name<sup>*</sup></label>
    			<input type="text" name="efname" id="efname" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="elname">Last Name</label>
    			<input type="text" name="elname" id="elname" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="eaddr1">Address Line 1</label>
    			<input type="text" name="eaddr1" id="eaddr1" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="eaddr2">Address Line 2</label>
    			<input type="text" name="eaddr2" id="eaddr2" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="ecity">City<sup>*</sup></label>
    			<input type="text" name="ecity" id="ecity" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="estate">State<sup>*</sup></label>
    			<select class="form-control" name="estate" id="estate">
	    			<option value="">Select State</option>
                    <
                    <?php foreach (getUSAStates() as $key => $val) {
                        ?>
                        <option value="<?=$key;?>"><?=$val;?></option>
                        <?php
                    } ?>
            	</select>
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="ezip">Zip Code</label>
    			<input type="text" name="ezip" id="ezip" class="form-control">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="erole">Role<sup>*</sup></label>
    			<select id="erole" name="erole" class="form-control">
    				<option value="" data-permisssion=''>Select Role</option>
    				<?php foreach (getRoles() as $role) {
    					?>
    					<option value="<?php echo $role->ERID; ?>"><?php echo $role->ERRoleName; ?></option>
    					<?php
    				} ?>
    			</select>
    		</div>
		</div>

		<div class="col-md-6">
    		<div class="form-group">
    			<label for="econtact-type">Contact Type</label>
    			<select id="econtact-type" name="econtact-type" class="form-control">
    				<option value="">Select Contact Type</option>
    				<?php foreach ($ListTypeOBJ->GetAllContactTypes() as $CT) {
    					?>
    					<option value="<?=$CT->CTID;?>"><?=$CT->CTName;?></option>
    					<?php
    				} ?>
    			</select>
    		</div>
		</div>

		<div class="col-md-6">
    		<div class="form-group">
    			<label for="econtact-data">Contact Data</label>
    			<input type="text" name="econtact-data" id="econtact-data" class="form-control">
    		</div>
		</div>

		<div class="col-md-6">
    		<div class="form-group">
    			<label for="euser">Username<sup>*</sup></label>
    			<input type="text" name="euser" id="euser" class="form-control" onkeyup="this.value=sdMakeUsername(this.value)">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="epass">Password<sup>*</sup></label>
    			<input type="text" name="epass" id="epass" class="form-control">
    		</div>
		</div>
		<div class="col-md-12 text-center">
			<button class="btn theme-default" id="add-employee-btn">SAVE</button>
		</div>
	</div>
</form>