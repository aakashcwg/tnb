
<?php
$eid = '';
$TheEmployee = '';
if(isset($_POST['eid'])){
include '../../functions.php';
	$eid = $_POST['eid'];
	$TheEmployee = $EmployeeOBJ->GetTheEmployeeByID($eid);
}
if(isset($_GET['eid'])){
	global $EmployeeOBJ, $ListTypeOBJ;
	$eid = base64_decode($_GET['eid']);
	$TheEmployee = $EmployeeOBJ->GetTheEmployeeByID($eid);
}
?>
<div class="alert alert-dismissible" role="alert" id="edit-employee-alert" style="display: none;">
    <strong id="edit-employee-alert-message"></strong>
    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form id="edit-employee-form">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
    			<label for="efname">First Name<sup>*</sup></label>
    			<input type="text" name="efname" id="efname" class="form-control" onkeyup="this.value=sdUcWords(this.value)" value="<?=$TheEmployee->EFName;?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="elname">Last Name</label>
    			<input type="text" name="elname" id="elname" class="form-control" onkeyup="this.value=sdUcWords(this.value)" value="<?=$TheEmployee->ELName;?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="eaddr1">Address Line 1</label>
    			<input type="text" name="eaddr1" id="eaddr1" class="form-control" onkeyup="this.value=sdUcWords(this.value)" value="<?=$TheEmployee->EAddress1;?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="eaddr2">Address Line 2</label>
    			<input type="text" name="eaddr2" id="eaddr2" class="form-control" onkeyup="this.value=sdUcWords(this.value)" value="<?=$TheEmployee->EAddress2;?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="ecity">City<sup>*</sup></label>
    			<input type="text" name="ecity" id="ecity" class="form-control" onkeyup="this.value=sdUcWords(this.value)" value="<?=$TheEmployee->ECity;?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="estate">State<sup>*</sup></label>
    			<select class="form-control" name="estate" id="estate">
	    			<option value="">Select State</option>
                    <
                    <?php foreach (getUSAStates() as $key => $val) {
                    	$selected = '';
                    	if(strtoupper($TheEmployee->EState) == $key){
                    		$selected = 'selected';
                    	}
                        ?>
                        <option value="<?=$key;?>" <?=$selected;?>><?=$val;?></option>
                        <?php
                    } ?>
            	</select>
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="ezip">Zip Code</label>
    			<input type="text" name="ezip" id="ezip" class="form-control" value="<?=$TheEmployee->EZip;?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="erole">Role<sup>*</sup></label>
    			<select id="erole" name="erole" class="form-control">
    				<option value="">Select Role</option>
    				<?php foreach (getRoles() as $role) {
    					$selected = '';
    					if($role->ERID == $TheEmployee->ELRoleID){
    						$selected = 'selected';
    					}
    					?>
    					<option value="<?php echo $role->ERID; ?>" <?=$selected;?>><?php echo $role->ERRoleName; ?></option>
    					<?php
    				} ?>
    			</select>
    		</div>
		</div>

		<div class="col-md-6">
    		<div class="form-group">
    			<label for="econtact-type">Contact Type</label>
    			<select id="econtact-type" name="econtact-type" class="form-control">
    				<option value="">Select Contact Type</option>
    				<?php foreach ($ListTypeOBJ->GetAllContactTypes() as $CT) {
                        $selected = '';
                        if($CT->CTID == $TheEmployee->ECCTID){
                            $selected = 'selected';
                        }
                        ?>
    					<option value="<?=$CT->CTID;?>" <?=$selected;?>><?=$CT->CTName;?></option>
    					<?php
    				} ?>
    			</select>
    		</div>
		</div>

		<div class="col-md-6">
    		<div class="form-group">
    			<label for="econtact-data">Contact Data</label>
    			<input type="text" name="econtact-data" id="econtact-data" class="form-control" value="<?=$TheEmployee->ECData;?>">
    		</div>
		</div>


		<div class="col-md-6">
    		<div class="form-group">
    			<label for="euser">Username<sup>*</sup></label>
    			<input type="text" name="euser" id="euser" class="form-control" value="<?=$TheEmployee->ELUsername;?>" readonly>
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="epass">Password<sup>*</sup></label>
    			<input type="text" name="epass" id="epass" class="form-control" value="<?=$TheEmployee->ELPassword;?>">
    		</div>
		</div>
		<input type="hidden" name="eid" value="<?=$TheEmployee->EmployeeID;?>">
		<div class="col-md-12 text-center">
			<a class="btn theme-default" href="javascript:void(0)" id="update-employee-btn" data-eid="<?=$TheEmployee->EmployeeID;?>" onclick="UpdateEmployee()">Update</a>
		</div>
	</div>
</form>