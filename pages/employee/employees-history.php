<table id="employees-history-table" class="table table-striped table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Employee Name</th>
            <th>Address 1</th>
            <th>Address 2</th>
            <th>City</th>
            <th>State</th>
            <th>Zip Code</th>
            <th>Employee Start</th>
            <th>Employee End</th>
            <th>Notes</th>
        </tr>
    </thead>
    <tbody>
   	<?php
    $countEmployee = 1;
        if(!empty(getEployees('', 'history'))):
    	foreach (getEployees('', 'history') as $employee) {
    		?>
    		<tr id="row-<?php echo $employee->EmployeeID; ?>">
	            <td><?=$employee->EFName.' '.$employee->ELName;?></td>
	            <td><?=$employee->EAddress1;?></td>
	            <td><?=$employee->EAddress2;?></td>
	            <td><?=$employee->ECity;?></td>
	            <td><?=$employee->EState;?></td>
	            <td><?=$employee->EZip;?></td>
	            <td><?=date('d/m/Y H:i:sa',strtotime($employee->EHStartDate));?></td>
	            <td><?=empty($employee->EHEndDate)?'AVAILABLE':date('d/m/Y H:i:sa',strtotime($employee->EHEndDate));?></td>
	            <td><?=$employee->EHNotes;?></td>
	        </tr>
	        <?php
    	$countEmployee++; }
        endif;
        ?>
    </tbody>
</table>