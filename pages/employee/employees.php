<?php
//employees.php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
    

global $EmployeeOBJ;
$Role = $_GET['role'] ?? null;
$TNB_Employees = $EmployeeOBJ->GetAllEmployees();
if($Role){
    $TNB_Employees = $EmployeeOBJ->GetEmployeesByRole($Role);
}
?>
<table id="employee-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Employee Name</th>
            <th>User Role</th>
            <th>Username</th>
            <th>Is Active</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
   	    <?php
        $countEmployee = 1;
        if($TNB_Employees){
        	foreach ($TNB_Employees as $employee) {
        		if($employee->EmployeeID > 0){
        			$isActive = $employee->IsActive==1?'<img src="dist/images/active.png" class="active-employee sd-tootip isactive" title="Active">':'<img src="dist/images/disabled.png" class="inactive-employee sd-tootip isactive" title="Inactive">';
        			?>
        			<tr id="row-<?php echo $employee->EmployeeID; ?>">
    		            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=employees&action=employee-profile&eid=<?=base64_encode($employee->EmployeeID);?>'"><?=$employee->EFName.' '.$employee->ELName;?></td>
    		            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=employees&action=employee-profile&eid=<?=base64_encode($employee->EmployeeID);?>'"><strong><?=get_the_role($employee->ELRoleID);?></strong></td>
    		            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=employees&action=employee-profile&eid=<?=base64_encode($employee->EmployeeID);?>'"><?=$employee->ELUsername;?></td>
    		            <td><?=$isActive;?></td>
    		            <td class="action-btns">
    		            	<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$employee->EmployeeID;?>" style="font-size:24px;cursor:pointer;" onclick="editEmployee(this)"></i>
    		            	<?php
    		            	if($employee->EmployeeID != 0) : ?>
    		            	<i class="fa fa-trash-o sd-tootip" title="Delete" data-id="<?=$employee->EmployeeID;?>" style="font-size:24px;color:red;cursor:pointer;margin-left:10px" onclick="delEmployee(this)"></i>
    		            	<?php endif; ?>
    					</td>
    	        	</tr>
        			<?php
        		}
        		?>
    	        <?php
        	   $countEmployee++;
            }
        }
        ?>
    </tbody>
</table>