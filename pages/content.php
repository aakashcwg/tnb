<?php
//content.php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");
?>
<?php require_once(BASE_PATH.'/header.php'); ?>
<div class="body-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 lt-thumb-block">
				<div class="wrapper">
				    <?php require_once('sidebar.php'); ?>
				</div>
			</div>
			<div class="col-md-10 rt-thumb-block" id="main-content">
				<div class="main-content-inner" onscroll="alert('grege5y4747');">
					<!-- Header Block Start -->
					<div class="top-bar">
						<div class="lt-block">
						<?php
						/*~Page Title - Start~*/
						PageTitle();
						/*~Page Title - End~*/
						?>
						</div>
						<div class="rt-block">
							<?php if(isset($_SESSION['drawer_id']) && !empty($_SESSION['drawer_id'])) { ?>
							<a href="javascript:void(0)" title="Active Drawer" class="btn btn-default sd-tootip"><?=CashDrawerName($_SESSION['drawer_id']);?></a>
							<?php } ?>
							<a class="btn theme-default-orange logout-btn" href="logout.php">Logout</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Header Block End -->	

					<!-- Main Content Start-->
					<?php
					ThePage();
					?>
					<!-- Main Content End-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php
Modals();
?>

