<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2 align='center'>You are unable to access this page.</h2>");
?>
<div class="container login-container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                	<h2>Login</h2>
                </div>
                <div class="panel-body">

                    <div class="alert alert-dismissible" role="alert" style="display: none; font-size:14px;" id="login-alert">
                        <strong id="login-alert-message"></strong>
                        <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form class="form-horizontal" id="login-form" role="form">
                        <div class="form-group">
                            <label for="username" class="col-sm-4 control-label">Username</label>
                            <div class="col-sm-8">
                                <input type="text" name="username" onblur="checkUser()" class="form-control" id="username" placeholder="Username" value="<?php if(isset($_COOKIE['member_login'])) { echo $_COOKIE['member_login']; } ?>" required >
                                <span id="user-auth-resp"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-4 control-label">Password</label>
                            <div class="col-sm-8">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="<?php if(isset($_COOKIE['member_password'])) { echo $_COOKIE['member_password']; } ?>" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label class="">
                                        <input class="" name="remember" id="remember" type="checkbox" <?php if(isset($_COOKIE["member_login"])) { ?> checked <?php } ?>>Remember me</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-success btn-sm" id="login">Sign in</button>
                                <a href="javascript:void(0)" class="btn btn-default btn-sm" onclick="clearForm()">Reset</a>
                            </div>
                        </div>
                    </form>
                    <div class="tnb-version">
                        <?=TNBVersion()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>