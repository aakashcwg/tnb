<?php
$venId = $_GET['vid'] ?? '';
$cid = $_GET['cid'] ?? '';
global $VendorOBJ, $CustomerOBJ, $InventoryOBJ, $ListTypeOBJ, $OrderOBJ;
?>

<div id="cerate-order">
    <form id="order-form">
        <div class="row">

            <div class="col-md-2">
                <div class="form-group">
                    <label for="vendor">Vendor<sup>*</sup></label><br>
                    <select class="selectpicker" id="vendor" name="vendor" data-show-subtext="true" data-live-search="true" class="form-control" onchange="OnSelectPurchaseVendor()">
                        <option value="">Select Vendor</option>
                        <?php 
                        foreach ($VendorOBJ->GetAllVendors() as $vendor) {
                            $theVendor = VC_Name($vendor->VCIDFName, $vendor->VCIDLname, $vendor->VendorCompanyName);
                            if($venId == $vendor->VendorID){
                                $ActiveVendor = 'selected';
                            }else{
                                $ActiveVendor = '';
                            }
                            echo '<option value="'.$vendor->VendorID.'" '.$ActiveVendor.'>'.$theVendor.'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <!-- <div class="col-md-2">
                <div class="form-group">
                    <label for="customer">Customer</label><br>
                    <select class="selectpicker" id="customer" name="customer" data-show-subtext="true" data-live-search="true" class="form-control">
                        <option value="">Select Customer</option>
                    <?php 
                        foreach ($CustomerOBJ->GetAllCustomers() as $customer) {
                            $theCustomer = VC_Name($customer->CustomerFName, $customer->CustomerLName, $customer->CustomerCompanyName);
                            echo '<option value="'.$customer->CustomerID.'">'.$theCustomer.'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div> -->

            <div class="clearfix"></div>

            <div class="items-wrapper" id="items-wrapper">
                <div class="item-block" data-item="1">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="item">Item<sup>*</sup></label>
                            <select class="form-control sd-select item" id="item" name="item" data-show-subtext="true" data-live-search="true" class="form-control" onchange="OnSelectPurchaseItem(this)">
                                <option value="">Select Item</option>
                                <?php foreach ($InventoryOBJ->GetAllInventories() as $inventory) {
                                    ?>
                                    <option value="<?=$inventory->INVID;?>" <?=(isset($_GET['itemID']) && $_GET['itemID']==$inventory->INVID)?'selected':'';?>><?=getItemWithNumber($inventory->INVID);?></option>
                                    <?php
                                } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="vitem">Vendor Item#</label>
                            <input type="vitem" name="vitem[]" id="vitem" class="form-control vitem">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="case">Is Case</label>
                            <input type="checkbox" name="case[]" id="case" class="form-control case">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="case-qty">How many cases?<sup>*</sup></label>
                            <input type="number" name="case-qty[]" min="1" value="1" id="case-qty" class="form-control case-qty qty">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="qty-per-case">Qty Per Case<sup>*</sup></label>
                            <input type="text" name="qty-per-case[]" min="1" value="1" id="qty-per-case" class="form-control qty-per-case qty" onblur="MatchWithInventoryMaxQty(this)">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="cost">Cost per Item<sup>*</sup></label>
                            <input type="text" name="cost[]" id="cost" class="form-control cost price-validation">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="tax">Sales Tax<sup>*</sup></label>
                            <input type="text" name="tax[]" id="tax" class="form-control tax price-validation">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="shipping-cost">Shipping Cost<sup>*</sup></label>
                            <input type="text" name="shipping-cost[]" id="shipping-cost" class="form-control shipping-cost price-validation">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="gl">GL#<sup>*</sup></label>
                            <select name="gl[]" id="gl" class="form-control gl">
                            <?php foreach ($ListTypeOBJ->GetAllLedgerNumbers() as $ListType) {
                                ?>
                                <option value="<?=$ListType->GeneralLedgerID;?>"><?=$ListType->GLNumber;?></option>
                                <?php
                            } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="note">Note</label>
                            <input type="text" name="note" id="note" class="form-control note">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="customer">Customer</label>
                            <select class="form-control customer selectpicker" name="customer" data-show-subtext="true" data-live-search="true">
                                <option value="NULL">Select Customer</option>
                            <?php 
                                foreach ($CustomerOBJ->GetAllCustomers() as $customer) {
                                    $theCustomer = VC_Name($customer->CustomerFName, $customer->CustomerLName, $customer->CustomerCompanyName);
                                    ?>
                                    <option value="<?=$customer->CustomerID;?>" <?=(isset($_GET['cid']) && $_GET['cid']==$customer->CustomerID)?'selected':'';?>><?=$theCustomer;?></option>
                                    <?php
                                }
                            ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <a href="javascript:void(0)" class="add-remove-btn add-selling-item" style="margin-top:24px;" onclick="AddOrderItem(this)" data-btnid="1">+</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="order-phone">PO Number</label>
                    <?php
                    $POVal = '';
                    if($OrderOBJ->LastOrderID()){
                        $POVal = $OrderOBJ->LastOrderID() + 1;
                    }
                    ?>
                    <input type="text" name="order-phone" id="order-phone" class="form-control number-validation" value="<?php echo $POVal; ?>">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="ledger-id">Ledger ID</label>
                    <select id="ledger-id" class="form-control" name="ledger-id">
                        <?php if(getLedgerNumbers()) { ?>
                        <?php foreach (getLedgerNumbers() as $num) {
                            ?>
                            <option value="<?=$num->GeneralLedgerID;?>"><?=$num->GLNumber;?></option>
                            <?php
                        } } ?>
                    </select>
                </div>
            </div>

            <input type="hidden" id="current-emp" name="current-emp" value="<?php echo currentUser('EmployeeID'); ?>">
            <div class="clearfix"></div>
            <div class="col-md-12 text-center">
                <button class="btn theme-default" id="create-order-btn">Order</button>
            </div>      
        </div>
    </form>
</div>