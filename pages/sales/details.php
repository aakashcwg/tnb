<?php
global $InventoryOBJ, $SalesOBJ;

$SalesID = $_GET['selling_id'];
$SDs = $SalesOBJ->SalesDetailsBySalesID($SalesID);
$SIsVoid = $SalesOBJ->GetSalesInfo($SalesID, 'SIsVoid');
$CustomerID = $SalesOBJ->GetSalesInfo($SalesID, 'SCustomerID');
$Invoice = $SalesOBJ->GetSalesInfo($SalesID, 'SalesInvoiceNumber');
$SalesDate = $SalesOBJ->GetSalesInfo($SalesID, 'SalesDate');
$SalesDate = date('M d, Y h:i:sa', strtotime($SalesDate));
$SalesAmount = $SalesOBJ->GetSalesInfo($SalesID, 'SAmount');
?>
<!-- <a href="#" id="print-invoice" style="float: right">Print</a>
<div class="container" id="sales-details">
	<div class="row">
		<div class="col-md-6">
			<h2><?=GetTheCustomer($CustomerID);?></h2>
		</div>
		<div class="col-md-6">
			<h2><?='#'.$Invoice;?></h2>
			<p><span><b>Date : </b></span><?=$SalesDate;?></p>
			<p><span><b>Paid Amount : </b></span><?='$ '.number_format($SalesAmount, 2);?></p>
		</div>
	</div>
	<div class="">
		<table class="table-bordered list-type-table" style="width:900px;">
			<thead style="text-align:left;">
				<tr>
					<th>Item</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Discount</th>
					<th>Tax</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($SDs as $SD) {
					$Item = $InventoryOBJ->GetOnHandItemByID($SD->SDIOHID);
					if($Item){
						$ItemName = getItemWithNumber($Item[0]->IOHInventoryID);
					}else{
						$ItemName = 'Anonymous';
					}
					?>
					<tr>
						<td><?=$ItemName;?></td>
						<td><?=$SD->SDQuantity;?></td>
						<td><?=number_format($SD->SDPrice, 2);?></td>
						<td><?=$SD->SDDiscountAmount;?></td>
						<td><?=number_format($SD->SDTaxAmount, 2);?></td>
					</tr>
					<?php
				} ?>
			</tbody>
		</table>
	</div>
</div> -->


<div class="row">
	<a href="#" class="btn btn-default theme-default-orange" id="print-invoice" style="float: right; margin-right: 60px; font-size: 20px;">Print</a>
	<?php
	if($SIsVoid != 1){
		if(($SalesOBJ->Total($SalesID) - $SalesOBJ->PaidAmt($SalesID)) > 0){
			$dueVal = $SalesOBJ->Total($SalesID) - $SalesOBJ->PaidAmt($SalesID);
			$dueVal = number_format($dueVal, 2);
			?>
			<a href="javascript:void(0)" class="btn btn-default theme-default-orange" id="complete-the-sp" style="float: left; margin-left: 18px;" data-due_val="<?=$dueVal;?>" data-sp_sid="<?=$SalesID;?>">Complete Payment</a>
			<?php
		}
	}
	?>
</div>

<div id="print-sd">
	<?=SalesReceipt($SalesID);?>
</div>
