<?php
//quote-history.php 
global $SalesOBJ, $CustomerOBJ;

$QuoteUrl = SITE_URL.'/?destination=sales&action=quote-history';

$Quotes = $SalesOBJ->GetAllQuotes();

$customer_id = $_GET['customer_id'] ?? null;

if(!empty($customer_id)){
    $Quotes = $SalesOBJ->GetQuotesByCustomerID($customer_id);
}

?>

<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-3">
        <select class="form-control sd-select" onchange="window.location= this.value ==''?'<?=$QuoteUrl?>':'<?=$QuoteUrl?>&customer_id='+this.value">
            <option value="">Select Customer</option>
            <?php foreach ($CustomerOBJ->GetAllCustomers() as $QC) {
                if($CustomerOBJ->HasQuotes($QC->CustomerID)){
                    $selected = $QC->CustomerID == $customer_id ? 'selected' : '';
                    ?>
                    <option value="<?=$QC->CustomerID;?>" <?=$selected?>><?=GetTheCustomer($QC->CustomerID);?></option>
                    <?php
                }
            } ?>
        </select>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-3">
        <a href="<?=$QuoteUrl;?>" style="float: right;" class="btn theme-default-orange">Clear Filter</a>
    </div>
</div>

<table class="table table-bordered sd-table" id="sales-quote-table">
    <thead>
        <tr>
            <th>Invoice</th>
            <th>Customer</th>
            <th>Amount</th>
            <th>Sales Tax</th>
            <th>Paid ?</th>
            <th>Date</th>
            <th>Action</th> 
        </tr>
    </thead>
    <tbody>
        <?php if($Quotes) { ?>
           <?php foreach ($Quotes as $Quote) {?>
            <tr id="<?=$Quote->SalesID;?>">
                <td><a href="javascript:void(0)">#<?=$Quote->SalesInvoiceNumber;?></a></td>
                <td><?=GetTheCustomer($Quote->SCustomerID);?></td>
                <td>$<?=number_format($Quote->SAmount, 2);?></td>
                <td>$<?=number_format($Quote->STax, 2);?></td>
                <td><?=$Quote->SIsPaid==1?'Yes':'No';?></td>
                <td><?=date(DATE_FORMAT,strtotime($Quote->SalesDate));?></td>
                <td><a class="btn" href="<?=SITE_URL;?>?destination=sales&fetch=invoice&get_from=quote-history&selling_id=<?=$Quote->SalesID;?>">Invoice</a></td>
            </tr>
            <?php } ?>
        <?php } ?> 
    </tbody>
</table>