<?php
//cash-out.php
global $SalesOBJ;
$SalesCash = $SalesOBJ->EmployeeSalesValue(currentUser('EmployeeID'));
$loginDateTime = date('Y-m-d H:i:s', strtotime($_SESSION['time_start_login']));
$OutedSystemCash = $SalesOBJ->CashoutedCash($loginDateTime, null, 'COSystemValue');


if($OutedSystemCash){
	?>
	<div>
		<h2>Cashout successfully completed for this session.</h2>
	</div>
	<?php
}else{

?>
<div class="row cash-out" id="cash-out">
	<div class="col-md-4">
		<div class="row from-drawer" id="from-drawer">
			<div class="col-md-12 text-center">
				<h3>From Drawer</h3>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$.01</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price=".01" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$.05</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price=".05" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$.10</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price=".10" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$.25</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price=".25" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$1</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price="1" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$5</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price="5" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$10</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price="10" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$20</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price="20" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="fd-block">
				<div class="row">
					<div class="col-md-6">
						<div class="dollar"><strong>$100</strong></div>
					</div>
					<div class="col-md-6">
						<div class="dollar qty"><input type="text" name="cash-qty" class="form-control cash-qty" data-price="100" onkeyup="CashoutEntryQty(this)" onblur="CashoutEntryQty(this)"><span class="dca"></span></div>
					</div>
				</div>
			</div>

			<div class="cards" style="padding:15px 0 0 0; margin:10px 0 0 0; border-top:1px solid #dadada;">
				<div class="fd-block">
					<div class="row">
						<div class="col-md-6">
							<div class="dollar"><strong>Mastercard</strong></div>
						</div>
						<div class="col-md-6">
							<div class="dollar price-validation price-box"><input type="text" name="cash-qty" class="form-control cash-qty" data-card="Mastercard" onkeyup="CashOutByCard(this)">
							<span class="dca"></span> <div class="box-price"><span class="card-value"></span></div></div>
						</div>
					</div>
				</div>

				<div class="fd-block">
					<div class="row">
						<div class="col-md-6">
							<div class="dollar"><strong>Visa</strong></div>
						</div>
						<div class="col-md-6">
							<div class="dollar price-validation price-box"><input type="text" name="cash-qty" class="form-control cash-qty" data-card="Visa" onkeyup="CashOutByCard(this)"><span class="dca"></span> <div class="box-price"><span class="card-value"></span></div></div>
						</div>
					</div>
				</div>

				<div class="fd-block">
					<div class="row">
						<div class="col-md-6">
							<div class="dollar"><strong>Amex</strong></div>
						</div>
						<div class="col-md-6">
							<div class="dollar price-validation price-box"><input type="text" name="cash-qty" class="form-control cash-qty" data-card="Amex" onkeyup="CashOutByCard(this)"><span class="dca"></span> <div class="box-price"><span class="card-value"></span></div></div>
						</div>
					</div>
				</div>

				<div class="fd-block">
					<div class="row">
						<div class="col-md-6">
							<div class="dollar"><strong>Checks</strong></div>
						</div>
						<div class="col-md-6">
							<div class="dollar price-validation price-box"><input type="text" name="cash-qty" class="form-control cash-qty" data-card="House Account" onkeyup="CashOutByCard(this)"><span class="dca"></span> <div class="box-price"><span class="card-value"></span></div></div>
						</div>
					</div>
				</div>

				<div class="fd-block">
					<div class="row">
						<div class="col-md-6">
							<div class="dollar"><strong>Junk tickets</strong></div>
						</div>
						<div class="col-md-6">
							<div class="dollar price-validation price-box"><input type="text" name="cash-qty" class="form-control cash-qty" data-card="Junk Tickets" onkeyup="CashOutByCard(this)"><span class="dca"></span> <div class="box-price"><span class="card-value"></span></div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>From POS System</h3>
				<label for="system-cash">Total Cash : </label>
				<input type="text" name="system-cash" class="form-control system-cash" readonly style="width: 100px; margin-left: 73px; text-align:center;" id="system-cash" value="$<?php echo number_format($SalesCash, 2);?>">
			</div>
		</div>
	</div>
	<div class="col-md-5">
	 	<div class="comparison-box">
	 		<h3>Comparison</h3>
	 		<div class="row">
	 			<div class="col-md-6">
	 				<h6>System : </h6>
	 			</div>
	 			<div class="col-md-6">
	 				<p id="total-system-cash">$<?php echo number_format($SalesCash, 2);?></p>
	 			</div>
	 			<div class="clearfix"></div>
	 			<div class="col-md-6">
	 				<h6>Drawer Start : </h6>
	 			</div>
	 			<div class="col-md-6">
	 				<p id="drawer-starting-cash">$100.00</p>
	 			</div>
	 			<div class="clearfix"></div>
	 			<div class="col-md-6">
	 				<h6>Drawer : </h6>
	 			</div>
	 			<div class="col-md-6">
	 				<p id="drawer-cash">$00.00</p>
	 			</div>
	 			<div class="clearfix"></div>
	 			<hr>
	 			<div class="col-md-6">
	 				<h6>Difference : </h6>
	 			</div>
	 			<div class="col-md-6">
	 				<p id="difference">$00.00</p>
	 			</div>
	 			<div class="clearfix"></div>
	 		</div>
	 	</div>
	 	<div class="row">
	 		<input type="hidden" id="adjusted-value" value="0.00">
	 		<?php if(isset($_SESSION['time_start_login'])) { ?>
		 		<input type="hidden" id="start-datetime" value="<?=$_SESSION['time_start_login'];?>">
		 	<?php } ?>
	 		<div class="col-md-12 text-center">
	 			<a href="javascript:void(0)" class="btn btn-default theme-default-orange" style="margin:25px auto 0;" onclick="CompleteCashout()">Complete Cashout</a>
	 		</div>
	 	</div>
	 	<?php



	 	?>
	</div>
</div>
<?php
}
?>