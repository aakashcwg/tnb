<?php
//sales-history.php
global $SalesOBJ, $CustomerOBJ;

$customer_id = $_GET['customer_id'] ?? null;
$SalesHistory = $SalesOBJ->AllSales();
if($customer_id){
    $SalesHistory = $SalesOBJ->AllSalesByCustomer($customer_id);
}
$SH_URL = SITE_URL.'/?destination=sales&action=sales-history';
?>

<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-3">
        <select class="form-control sd-select" onchange="window.location= this.value ==''?'<?=$SH_URL?>':'<?=$SH_URL?>&customer_id='+this.value">
            <option value="">Select Customer</option>
            <?php foreach ($CustomerOBJ->GetAllCustomers() as $SC) {
                    $selected = $SC->CustomerID == $customer_id ? 'selected' : '';
                    ?>
                    <option value="<?=$SC->CustomerID;?>"  <?=$selected?>><?=GetTheCustomer($SC->CustomerID);?></option>
                    <?php
            } ?>
        </select>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-3">
        <a href="<?=$SH_URL?>" style="float: right;" class="btn theme-default-orange">Clear Filter</a>
    </div>
</div>

<table class="table table-bordered sd-table" id="invoice-table">
    <thead>
        <tr>
            <th>Invoice</th>
            <th>Customer</th>
            <th>Amount</th>
            <th>Sales Tax</th>
            <th>Paid?</th>
            <th>Date</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php if($SalesHistory) { ?>
            <?php foreach ($SalesHistory as $SH) {
            if(!$SH->SPreviousSalesID){
                $rowClass = '';
                $voidTag = '';
                $Minus = '';
                if($SH->SIsVoid == 1){
                    $rowClass = 'void-sale';
                    $voidTag = '-Void/Refund';
                    $Minus = '-';
                }
                ?>
                <tr id="<?=$SH->SalesID;?>" class="<?=$rowClass;?>" data-customer="<?=$SH->SCustomerID;?>">
                    <td><a href="<?=SITE_URL;?>?destination=sales&action=sales-details&selling_id=<?=$SH->SalesID;?>">#<?=$SH->SalesInvoiceNumber.$voidTag;?></a></td>
                    <td><?=GetTheCustomer($SH->SCustomerID);?></td>
                    <td>$<?=$Minus;?><?=number_format($SH->SAmount, 2);?></td>
                    <td>$<?=$Minus;?><?=number_format($SH->STax, 2);?></td>
                    <td><?=$SH->SIsPaid==1?'Yes':'No';?></td>
                    <td><?=date(DATE_FORMAT,strtotime($SH->SalesDate));?></td>
                    <td><?php if($SH->SIsVoid == 0) { ?>
                        <a href="javascript:void(0)" class="btn btn-default theme-default" onclick="MakeVoid(this)">Void/Refund</a>
                    <?php } ?></td>
                </tr>
            <?php } ?> 
        <?php } } ?>
    </tbody>
</table>