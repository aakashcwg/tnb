<?php
//sales.php
global $CustomerOBJ, $InventoryOBJ;
$GetQH = false;
$SPONO = '';
if(Isset($_GET['fetch']) && $_GET['fetch'] == 'invoice' && isset($_GET['get_from']) && $_GET['get_from'] == 'quote-history'){
    $GetQH = true;
    $selling_id = $_GET['selling_id'];
    $QH_CustomerID = getSalesField($selling_id, 'SCustomerID');
    $SPONO = getSalesField($selling_id, 'SPONumber');
    $QH = getSalesDetails($selling_id);

    $html = '';
    $newNum = 1;
    $TCP = 0;
    $TTP = 0;
    $TEXTP = 0;

    $QDisc = 0;

    foreach ($QH as $TheQH) {

        $TCP += $TheQH->SDQuantity*$TheQH->SDPrice;
        //$TTP += $TheQH->SDTaxAmount*$TheQH->SDQuantity;
        $TTP += GetTaxTotal($TheQH->SDQuantity, $TheQH->SDTaxAmount);       //AVD 12/30/18
        $TEXTP += GetExtCost($TheQH->SDQuantity, $TheQH->SDPrice, $TheQH->SDDiscountAmount, $TheQH->SDTaxAmount);

        $QDisc = $QDisc + CalculateSalesDisc($TheQH->SDQuantity, $TheQH->SDPrice, $TheQH->SDDiscountAmount);

        $html .= '<div class="item-block quote-item" id="'.$TheQH->SDID.'">';
    
            //Quantity
            $html .= '<div class="col-md-1 qty-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="qty'.$newNum.'">Qty</label>';
                    $html .= '<input type="text" name="qty" id="qty'.$newNum.'" min="1" class="form-control qty" value="'.$TheQH->SDQuantity.'" onkeyup="getItemNetPrice(this)">';
                $html .= '</div>';
            $html .= '</div>';

            //Item
            $html .= '<div class="col-md-2 item-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="item'.$newNum.'">Item</label>';
                     $html .= GetInvAndNonInvItems('item'.$newNum, array(), array(), $InventoryOBJ->GetInvItemIDByOnHandID($TheQH->SDIOHID), $TheQH->SDNonInvID);
                $html .= '</div>';
            $html .= '</div>';

            //Cost
            $html .= '<div class="col-md-2 sales-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="cost'.$newNum.'">Sales Cost</label>';
                    $html .= '<input type="text" name="cost" id="cost'.$newNum.'" value="'.number_format($TheQH->SDPrice, 2).'" class="form-control cost" readonly>';
                $html .= '</div>';
            $html .= '</div>';

            //Discount
            $html .= '<div class="col-md-2 discount-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="disc'.$newNum.'">Discount</label>';
                    $html .= '<input type="text" name="disc" id="disc'.$newNum.'" value="'.$TheQH->SDDiscountAmount.'" class="form-control disc disc-format" onkeyup="getItemNetPrice(this)" onblur="getItemNetPrice(this)">';
                    $html .= '<input type="hidden" class="discvalue" name="discvalue" id="discvalue'.$newNum.'">';
                $html .= '</div>';
            $html .= '</div>';

            //Tax
            $html .= '<div class="col-md-1 tax-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="tax'.$newNum.'">Tassx</label>';
                    $html .= '<input type="text" name="tax" id="tax'.$newNum.'" value="'.number_format($TheQH->SDTaxAmount, 2).'" class="form-control tax" readonly>';
                    $html .= '<input type="hidden" class="taxvalue" name="taxvalue" id="taxvalue'.$newNum.'">';
                    //$html .= '<input type="hidden" class="unit-tax" id="unit-tax"'.$newNum.'>';

        $html .= '</div>';
            $html .= '</div>';

            //Ext Cost
            $html .= '<div class="col-md-2 ext-cost-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="ext-cost'.$newNum.'">Ext Cost</label>';
                    $html .= '<input type="text" name="ext_cost" id="ext-cost'.$newNum.'" value="'.GetExtCost($TheQH->SDQuantity, $TheQH->SDPrice, $TheQH->SDDiscountAmount, $TheQH->SDTaxAmount).'" class="form-control ext-cost" readonly>';
                $html .= '</div>';
            $html .= '</div>';

            //Note
            $QHNotes = '';
            if($TheQH->SDNotes && $TheQH->SDNotes != 'NULL'){
                $QHNotes = $TheQH->SDNotes;
            }
            $html .= '<div class="col-md-2 note-grid">';
                $html .= '<div class="form-group">';
                    $html .= '<label for="note'.$newNum.'">Note</label>';
                    $html .= '<textarea name="note" id="note'.$newNum.'" class="form-control note">'.$QHNotes.'</textarea>';
                $html .= '</div>';
            $html .= '</div>';

            $html .= '<div class="col-md-2">';
                $html .= '<a href="javascript:void(0)" class="add-remove-btn add-selling-item" style="margin-top:58px;" onclick="addSellingItem(this)" data-btnid="'.$newNum.'">+</a>';
            $html .= '</div>';

            $html .= '<div class="clearfix"></div>';
        $html .= '</div>';

    $newNum++; }

}

?>


<div class="all-block">
    <div class="top-panel">
        <div class="lower-block">
            <ul>
                <li><a href="<?=SITE_URL.'?destination=sales&action=sales-history';?>">Sales History</a></li>
                <li><a href="<?=SITE_URL.'?destination=sales&action=quote-history';?>">Quote History</a></li>
                <li><a href="javascript:void(0)" class="disc-percentage" data-percentage="5">5% Off</a></li>
                <li><a href="javascript:void(0)" class="disc-percentage" data-percentage="10">10% Off</a></li>
                <li><a href="javascript:void(0)" id="clear-discs" class="clear-discs" onclick="ClearAllDiscounts()">Clear Discounts</a></li>
                <li><a href="<?=SITE_URL.'?destination=sales&action=cash-out';?>" id="cash-out" class="cash-out">Cashout</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
    <form id="customer-selling-form">
        <div class="row">
            <div class="col-md-4">
                <div id="selling-customer">
                    <select class="sd-select" id="customer" name="customer" data-show-subtext="true" data-live-search="true" class="form-control" onchange="getSalesCustomer(this)">
                        <option value="">Select Customer</option>
                    <?php

                        foreach ($CustomerOBJ->GetAllCustomers() as $customer) {

                            $ActiveCustomer = '';
                            if($GetQH === true) {
                                if($customer->CustomerID == $QH_CustomerID)
                                $ActiveCustomer = 'selected';
                            }else{
                                $ActiveCustomer = '';
                            }

                            $theCustomer = VC_Name($customer->CustomerFName, $customer->CustomerLName, $customer->CustomerCompanyName);
                            echo '<option value="'.$customer->CustomerID.'" '.$ActiveCustomer.'>'.$theCustomer.'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <h4 id="price-list-name"></h4>
            </div>
            <input type="hidden" id="hiddn-customer-id">
            <input type="hidden" id="customer-name">
            <div class="col-md-2">
                <h4 id="tax-exempt-word"></h4>
            </div>
            <div class="col-md-4">
                <a style="float: right" href="javascript:void(0)" class="btn theme-default" data-toggle="modal" data-target="#add-customer">+ Add New Customer</a>
            </div>
            
        </div>
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="purchase-order">Purchase Order</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name="purchase-order" id="purchase-order" class="form-control purchase-order" value="<?=$SPONO;?>">
                </div>
            </div>
        </div>

        <div class="bottom-panel">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="lt-section">
                        <div class="row" id="item-row">

                            <?php if($GetQH === true) { ?>

                                <?php echo $html; ?>

                            <?php } else { ?>

                            <div class="item-block">
                                <div class="col-md-1 qty-grid">
                                    <div class="form-group">
                                        <label for="qty">Qty</label>
                                        <input type="text" name="qty" id="qty1" min="1" class="form-control qty" value="1" onkeyup="getItemNetPrice(this)">
                                    </div>
                                </div>    
                                <div class="col-md-2 item-grid">
                                    <div class="form-group">
                                        <label for="item">Item</label>
                                        <?=GetInvAndNonInvItems('item1', array(), array());?>
                                    </div>
                                </div>
                                <div class="col-md-2 sales-grid">
                                    <div class="form-group">
                                        <label for="cost">Sales Cost</label>
                                        <input type="text" name="cost" id="cost1" class="form-control cost" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2 discount-grid">
                                    <div class="form-group">
                                        <label for="disc">Discount</label>
                                        <input type="text" name="disc" id="disc1" class="form-control disc disc-format" onkeyup="getItemNetPrice(this)">
                                        <input type="hidden" class="discvalue" name="discvalue" id="discvalue1">
                                    </div>
                                </div>
                                <div class="col-md-1 tax-grid">
                                    <div class="form-group">
                                        <label for="tax">Tax</label>
                                        <input type="text" name="tax" id="tax1" class="form-control tax" readonly>
                                        <input type="hidden" class="taxvalue" name="taxvalue" id="taxvalue1">
                                        <input type="hidden" class="unit-tax" name="unit-tax" id="unit-tax1">
                                    </div>
                                </div>
                                <div class="col-md-2 ext-cost-grid">
                                    <div class="form-group">
                                        <label for="ext-cost">Ext Cost</label>
                                        <input type="text" name="ext_cost" id="ext-cost1" class="form-control ext-cost" onchange="VarifyExtCost(this)" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2 note-grid">
                                    <div class="form-group">
                                        <label for="note">Note</label>
                                        <textarea name="note" id="note" class="form-control note"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a href="javascript:void(0)" class="add-remove-btn add-selling-item" style="margin-top:58px;" onclick="addSellingItem(this)" data-btnid="1">+</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="rt-section" id="invoice-block">
                        <div class="up-box">
                            <div class="top-block">
                                <?php if($GetQH === true) { ?>
                                    <a id="quote-invoice" title="Update Quote" data-sales_id="<?=$selling_id?>" class="btn theme-default-orange sd-tootip" data-invoice="<?php echo getSalesField($selling_id, 'SalesInvoiceNumber'); ?>">QUOTE</a>
                                    <a id="park-update" title="Update Quote" class="btn theme-default-orange sd-tootip park" style="cursor: pointer;" data-isquote="0" data-sales_id="<?=$selling_id?>" data-invoice="<?php echo getSalesField($selling_id, 'SalesInvoiceNumber'); ?>">PARK</a>
                                <?php } else { ?>
                                    <a id="invoice" title="Move to Quote" class="btn theme-default-orange sd-tootip" style="cursor: pointer;" data-isquote="0">QUOTE</a>
                                    <a id="park" title="Park Sale" class="btn theme-default-orange sd-tootip park" style="cursor: pointer;" data-isquote="0">PARK</a>
                                <?php } ?>
                            </div>
                            <div class="mid-block">
                                <div class="block">
                                    <div class="lt-text">
                                        <h3>Sales</h3>
                                    </div>
                                    <div class="rt-text">
                                        <h4 id="selling-price" >
                                            <?php if($GetQH === true) { ?>
                                            <?php echo '$'. number_format($TCP, 2); ?>
                                            <?php } else{ ?>
                                            $ 0.00
                                            <?php } ?>
                                        </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="block">
                                    <div class="lt-text">
                                        <h3>Discount</h3>
                                    </div>
                                    <div class="rt-text">
                                        <h4 id="selling-disc">
                                            <?php if($GetQH === true) { ?>
                                            <?php echo '$'. number_format($QDisc, 2); ?>
                                            <?php } else{ ?>
                                            $ 0.00
                                            <?php } ?>
                                        </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="block">
                                    <div class="lt-text">
                                        <h3>TAX</h3>
                                    </div>
                                    <div class="rt-text">
                                        <h4 id="selling-tax">
                                            <?php if($GetQH === true) { ?>
                                            <?php //echo '$'. number_format($TTP, 2); ?>
                                            <?php echo '$'. number_format(GetTaxTotal($TheQH->SDQuantity, $TheQH->SDTaxAmount), 2); ?>  <?php //AVD 12/30/18 ?>
                                            <?php } else{ ?>
                                            $ 0.00
                                            <?php } ?>
                                        </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="bottom-block">
                                <div class="block">
                                    <div class="lt-text">
                                        <h3>Total</h3>
                                    </div>
                                    <div class="rt-text">
                                        <h4 id="selling-total">
                                            <?php if($GetQH === true) { ?>
                                            <?php echo '$'. number_format($TEXTP, 2); ?>
                                            <?php } else{
                                            echo "$ 0.00";
                                            } ?>
                                        </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="bottom-block" style="display: none;" id="disc-over-total-block">
                                <div class="block">
                                    <div class="lt-text">
                                        <h3>Discount</h3>
                                    </div>
                                    <div class="rt-text">
                                        <h4 id="disc-over-total">$ 0.00</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="link-block">
                                    <a href="javascript:void(0)" class="btn modify-disc-percent theme-default-orange" data-request="Change">Change</a>
                                    <a href="javascript:void(0)" class="btn modify-disc-percent theme-default-orange" data-request="Remove">Remove</a>
                                </div>
                            </div>
                            <div class="bottom-block" style="display: none;" id="net-total-block">
                                <div class="block">
                                    <div class="lt-text">
                                        <h3>Net Total</h3>
                                    </div>
                                    <div class="rt-text">
                                        <h4 id="net-total">$ 0.00</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="down-box">
                            <input type="hidden" id="current-employee-id" value="<?php echo currentUser('EmployeeID'); ?>">
                            <?php
                            $AuthBy = '-1';
                            $employee_id = currentUser('EmployeeID');
                            if(isset($_SESSION['manager_auth_for_'.$employee_id])){
                                $AuthBy = $_SESSION['manager_auth_for_'.$employee_id];
                            } ?>
                            <input type="hidden" id="authenticated-by" value="<?=$AuthBy;?>">
                            <div class="lower-panel">
                                <?php if($GetQH === true) { ?>
                                    <a href="javascript:void(0)" class="btn theme-default-orange" id="update-tender" data-id="<?php echo $selling_id; ?>">Complete Sale [enter]</a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" class="btn theme-default-orange" id="do-sell">Complete Sale [enter]</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="auto-sales-receipt" style="display: none;">
        
    </div>
</div>

<script type="text/javascript">

    document.addEventListener('keydown', function(e){
        console.log('key code is: '+e.which);
        if(e.ctrlKey && e.altKey){
            $(".add-selling-item:last").trigger('click');
        }
        if(e.ctrlKey && e.shiftKey){
            $(".remove-selling-item:last").trigger('click');
        }
    });

    $('body').click(function(evt){    
        if(!$("#selling-payment").hasClass('in')) {
            $("#invoice-block").show();
        }
    });

</script>