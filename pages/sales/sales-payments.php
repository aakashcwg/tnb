<?php
//sales-payments.php
global $SalesOBJ;
?>

<div class="sp-list">
	<table id="sp-table" class="table table-bordered sd-table" style="width: 100%;">
		<thead>
			<tr>
				<th>Sales ID</th>
				<th>Sales Amount</th>
				<th>Paid Amount</th>
				<th>Due Amount</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$SPs = $SalesOBJ->AllPayments();
			if($SPs){
				foreach ($SPs as $SP) {
					$SalesAmount = $SalesOBJ->Total($SP->SPSalesID);
					$AmountDiff = floatval($SP->Amount) - floatval($SalesAmount);
					$DueAmount = 0;
					$trColor = '';
					$pm_status = 'complete';
					if($AmountDiff < 0){
						$DueAmount = $AmountDiff*(-1);
						$trColor = '#FF0000';
						$pm_status = 'pending';
					}
					?>
					<tr id="<?=$SP->SPSalesID;?>" data-pm_status="<?=$pm_status;?>" style="background-color :<?=$trColor?>">
						<td><?='#'.$SP->SPSalesID;?></td>
						<td><?=number_format($SalesAmount, 2);?></td>
						<td><?=number_format($SP->Amount, 2);?></td>
						<td><?=number_format($DueAmount, 2);?></td>
						<td>
							<a href="<?=SITE_URL.'?destination=sales&action=sales-details&selling_id='.$SP->SPSalesID;?>" class="btn btn-default theme-default-orange">View Details</a>
							<!-- <?php if($pm_status == 'pending') { ?>
								<a href="javascript:void(0)" class="btn btn-default theme-default-orange">Quick Pay</a>
							<?php } ?> -->
						</td>
					</tr>
					<?php
				}
			}
			?>
		</tbody>
	</table>
</div>