<table class="table table-bordered sd-table" id="invoice-table">
    <thead>
        <tr>
            <th>Invoice</th>
            <th>Customer</th>
            <th>Amount</th>
            <th>Sales Tax</th>
            <th>Paid?</th>
            <th>Date</th> 
        </tr>
    </thead>
    <tbody>
           <?php foreach (getInvoices() as $SalesOBJ) {
            ?><tr id="<?=$SalesOBJ->SalesID;?>">
                <td><a href="<?=SITE_URL;?>?destination=sales&action=sales-details&selling_id=<?=$SalesOBJ->SalesID;?>">#<?=$SalesOBJ->SalesInvoiceNumber;?></a></td>
                <td><?=GetTheCustomer($SalesOBJ->SCustomerID);?></td>
                <td>$<?=number_format($SalesOBJ->SAmount, 2);?></td>
                <td>$<?=number_format($SalesOBJ->STax, 2);?></td>
                <td><?=$SalesOBJ->SIsPaid==1?'Yes':'No';?></td>
                <td><?=date(DATE_FORMAT,strtotime($SalesOBJ->SalesDate));?></td>
            </tr>
                <?php
            } ?> 
        
    </tbody>
</table>