<table id="vendor-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Company Name</th>
            <th>Account Number</th>
            <th>City</th>
            <th>State</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        if(!empty(getVendors())):
        	$countVendor = 1;
        	foreach (getVendors() as $vendor) {
        		?>
        		<tr id="row-<?=$vendor->VendorID;?>">
    	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=vendors&action=vendor-profile&vid=<?=base64_encode($vendor->VendorID);?>'"><?=$vendor->VendorCompanyName;?></td>
    	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=vendors&action=vendor-profile&vid=<?=base64_encode($vendor->VendorID);?>'"><?=$vendor->VendorAccountNumber;?></td>
                    <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=vendors&action=vendor-profile&vid=<?=base64_encode($vendor->VendorID);?>'"><?=$vendor->VACity;?></td>
                    <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=vendors&action=vendor-profile&vid=<?=base64_encode($vendor->VendorID);?>'"><?=getUSAStates()[$vendor->VAState] ?? '';?></td>
    	            <td class="action-btns">
    	            	<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$vendor->VendorID;?>" style="font-size:24px;cursor:pointer;" onclick="editVendor(this)"></i>

                        <a style="margin-left: 10px;" href="<?=SITE_URL;?>?destination=purchasing&vid=<?=$vendor->VendorID;?>"><i class="fa fa-shopping-cart sd-tootip" style="font-size: 24px" title="" data-original-title="place order"></i></a>
    				</td>
    	        </tr>
    	    <?php
        	$countVendor++; }
        endif;
        ?>
    </tbody>
</table>
