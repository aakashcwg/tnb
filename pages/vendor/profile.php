<?php
global $VendorOBJ, $EmployeeOBJ, $ListTypeOBJ;
$vid = isset($_GET['vid']) ? base64_decode($_GET['vid']) : '';
$theVendor = $VendorOBJ->GetTheVendorByID($vid);

$isActive = $theVendor->IsActive==1?'<img src="'.SITE_URL.'/dist/images/active.png" class="active-vendor sd-tootip isactive" title="Active">':'<img src="'.SITE_URL.'/dist/images/disabled.png" class="inactive-vendor sd-tootip isactive" title="Inactive">';
?>


<div class="container emp-profile">
    <div class="row">
        <!-- <div class="col-md-3">
            <div class="profile-img">
                <img src="<?=SITE_URL?>/dist/images/default-profile-pic.jpg" height="140px" width="140px" alt="<?=$theVendor->VendorCompanyName;?>">
            </div>
        </div> -->
        <div class="col-md-9">
            <div class="profile-head">
                <h3><?=$theVendor->VendorCompanyName;?></h3>
                <h6>Entry Date : <i><?=date(DATE_TIME_FORMAT, strtotime($theVendor->VendorDOE));?></i></h6>
                <div class="tab-block">
                    <ul class="nav nav-tabs">
    				    <li class="active"><a data-toggle="tab" href="#basic-details">Basic Details</a></li>
    				    <li><a data-toggle="tab" href="#contact">Contact</a></li>
    				    <li><a data-toggle="tab" href="#address">Address</a></li>
    				</ul>
    			  	<div class="tab-content profile-tab">
    			    	<div id="basic-details" class="tab-pane fade in active">
    				      	<h3>Basic Details</h3>
    				      	<div class="row">

    				      		<div class="col-md-6">
                                    <label>First Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VCIDFName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Last Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VCIDLname;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Company Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VendorCompanyName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Account Number</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VendorAccountNumber;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Added By</label>
                                </div>
                                <div class="col-md-6">
                                	<?php $EMP=$EmployeeOBJ->GetTheEmployeeByID($theVendor->VendorEmployeeID); ?>
                                    <p><?=$EMP->EFName.' '.$EMP->ELName.' ( '.$EMP->ELUsername.' )';?></p>
                                </div>
                                <div class="clearfix"></div>
    				      	</div>
    			    	</div>
    			    	<div id="contact" class="tab-pane fade">
    					    <h3>Contact</h3>
    					    <div class="row">
    					    	<?php if(!empty($theVendor->VCCTID)) { ?>
    				      		<div class="col-md-6">
                                    <label><?=$ListTypeOBJ->GetTheContactTypeByID($theVendor->VCCTID);?></label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VCData;?></p>
                                </div>
                            	<?php } ?>
    				      	</div>
    			    	</div>
    			    	<div id="address" class="tab-pane fade">
    			      		<h3>Address</h3>
    			      		<div class="row">

    					    	<?php if(!empty($theVendor->VAAddress1)) { ?>
    				      		<div class="col-md-6">
                                    <label>Address 1</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VAAddress1;?></p>
                                </div>
                            	<?php } ?>

                            	<?php if(!empty($theVendor->VAAddress2)) { ?>
                                <div class="col-md-6">
                                    <label>Address 2</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VAAddress2;?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($theVendor->VACity)) { ?>
                                <div class="col-md-6">
                                    <label>City</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VACity;?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($theVendor->VAState)) { ?>
                                <div class="col-md-6">
                                    <label>State</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=getUSAStates()[strtoupper($theVendor->VAState)];?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($theVendor->VAZip)) { ?>
                                <div class="col-md-6">
                                    <label>Zip Code</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$theVendor->VAZip;?></p>
                                </div>
                                <?php } ?>

    				      	</div>
    			    	</div>
    			  	</div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <a class="btn theme-default-orange" href="<?=SITE_URL.'?destination=vendors&action=edit-vendor&vid='.base64_encode($theVendor->VendorID);?>">Edit profile</a>
        </div>
    </div>         
</div>




<!-- <div class="row">
	<div class="col-md-4 col-md-offset-4">
		<img src="<?=SITE_URL?>/dist/images/default-profile-pic.jpg" height="140px" width="140px">
		<a href="<?=SITE_URL.'?destination=vendors&action=edit-vendor-profile&vid='.base64_encode($vid);?>">Edit<i class="fa fa-edit" style="font-size:24px;"></i></a>
		<h3>Company : <?=profileField($theVendor->VendorCompanyName);?></h3>
	</div>

	<div class="col-md-8 col-md-offset-2">

		<p>Name : <span><?=($theVendor->VCIDFName || $theVendor->VCIDLname)?$theVendor->VCIDFName.' '.$theVendor->VCIDLname : '<span class="empty-field">Not Given</span>';?></span></p>
		<p>Account Number : <span><?=profileField($theVendor->VendorAccountNumber);?></span></p>
		<p>Address Type : <span><?=($theVendor->VAAddressTypeID)?getAddressTypeName($theVendor->VAAddressTypeID):'<span class="emptyfield">Not Given</span>';?></span></p>
		<p>Address 1 : <span><?=profileField($theVendor->VAAddress1);?></span></p>
		<p>Address 2 : <span><?=profileField($theVendor->VAAddress2);?></span></p>

		<p>City : <span><?=profileField($theVendor->VACity);?></span></p>
		<p>State : <span><?=($theVendor->VAState)?getUSAStates()[$theVendor->VAState] : '<span class="emptyfield">Not Given</span>';?></span></p>
		<p>Zip Code : <span><?=profileField($theVendor->VAZip);?></span></p>
		<p>Contact Type : <span><?=($theVendor->VCCTID)?getContactTypeName($theVendor->VCCTID) : '<span class="emptyfield">Not Given</span>';?></span></p>
		<p>Contact Data : <span><?=profileField($theVendor->VCData);?></span></p>
		<p>Is this Primary Contact? : <span><?=($theVendor->VCIsPrimary==1)?'Yes' : 'No';?></span>
		</p>
		<p>Is Active? : <span><?=$isActive;?></span>
		</p>
	</div>
</div> -->