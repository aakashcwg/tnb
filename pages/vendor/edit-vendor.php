<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>

<?php
$vid = isset($_GET['vid']) ? base64_decode($_GET['vid']) : '';
$theVendor = getVendorByID($vid)[$vid];
?>


<form id="edit-vendor-form">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
    			<label for="vcompany">Vandor Company<sup>*</sup></label>
    			<input type="text" name="vcompany" id="vcompany" class="form-control" value="<?=profileField($theVendor->VendorCompanyName, '');?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vaccno">Account Number</label>
    			<input type="text" name="vaccno" id="vaccno" class="form-control" value="<?=($theVendor->VendorAccountNumber)?$theVendor->VendorAccountNumber:'';?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vfname">First Name</label>
    			<input type="text" name="vfname" id="vfname" class="form-control" value="<?=($theVendor->VCIDFName)?$theVendor->VCIDFName:'';?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vlname">Last Name</label>
    			<input type="text" name="vlname" id="vlname" class="form-control" value="<?=($theVendor->VCIDLname)?$theVendor->VCIDLname:'';?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vaddrtype">Address Type</label>
    			<select id="vaddrtype" name="vaddrtype" class="form-control">
    				<option value="" id="opt_0" data-id=''>--Select Address Type--</option>
    				<?php foreach (getAddressTypes() as $type) {
    					?>
    					<option value="<?php echo $type->ATName; ?>" id="opt_<?php echo $type->ATID; ?>" data-id="<?php echo $type->ATID; ?>" <?=($theVendor->VAAddressTypeID && $theVendor->VAAddressTypeID==$type->ATID)?'selected':'';?>><?php echo $type->ATName; ?></option>
    					<?php
    				} ?>
    			</select>
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vaddr1">Address 1</label>
    			<input type="text" name="vaddr1" id="vaddr1" class="form-control" value="<?=($theVendor->VAAddress1)?$theVendor->VAAddress1:'';?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vaddr2">Address 2</label>
    			<input type="text" name="vaddr2" id="vaddr2" class="form-control" value="<?=($theVendor->VAAddress2)?$theVendor->VAAddress2:'';?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vcity">City</label>
    			<input type="text" name="vcity" id="vcity" class="form-control" value="<?=($theVendor->VACity)?$theVendor->VACity:'';?>">
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vstate">State</label>
    			<select class="form-control" name="vstate" id="vstate">
                    <option value="">-- Select State --</option>
                    <
                    <?php foreach (getUSAStates() as $key => $val) {
                        ?>
                        <option value="<?=$key;?>" id="<?=str_replace(' ', '-', strtolower($key));?>" <?=($theVendor->VAState && $theVendor->VAState==$key)?'selected':'';?>><?=$val;?></option>
                        <?php
                    } ?>
                </select>
    		</div>
		</div>
		<div class="col-md-6">
    		<div class="form-group">
    			<label for="vzip">Zip Code</label>
    			<input type="text" name="vzip" id="vzip" class="form-control" value="<?=($theVendor->VAZip)?$theVendor->VAZip:'';?>">
    		</div>
		</div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="vcontacttype">Contact Type</label>
                <select id="vcontacttype" name="vcontacttype" class="form-control">
                    <option value="" id="opt_0" data-id=''>--Select Contact Type--</option>
                    <?php foreach (getContactTypes() as $type) {
                        ?>
                        <option value="<?php echo $type->CTName; ?>" id="opt_<?php echo $type->CTID; ?>" data-id="<?php echo $type->CTID; ?>" <?=($theVendor->VCCTID && $theVendor->VCCTID==$type->CTID)?'selected':'';?>><?php echo $type->CTName; ?></option>
                        <?php
                    } ?>
                </select>
            </div>
        </div>
		<div class="col-md-3">
    		<div class="form-group">
    			<label for="vcdata">Contact Data</label>
    			<input type="text" name="vcdata" id="vcdata" class="form-control" value="<?=($theVendor->VCData)?$theVendor->VCData:'';?>">
    		</div>
		</div>
        <div class="col-md-3">
            <div class="form-group check-primary">
                <label for="vpcontact">Primary Contact ?</label>
                <input type="checkbox" name="vpcontact" id="vpcontact" class="form-control" <?=($theVendor->VCIsPrimary==1)?'checked':'';?>>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group check-primary">
                <label for="visactivecheck"><input type="checkbox" name="visactivecheck" id="visactivecheck" class="form-control" <?=($theVendor->IsActive==1)?'checked':'';?>>Is Active ?</label>
            </div>
        </div>

        <input type="hidden" id="request-from" value="profile">
        <input type="hidden" name="" id="hidden-vendor-id" value="<?=$vid;?>">
		<div class="col-md-12 text-center">
			<a href="javascript:void(0)" class="btn theme-default" id="update-vendor-btn" data-venid="<?=$vid;?>" onclick="updateVandor(this)">Update</a>
		</div>
	</div>
</form>