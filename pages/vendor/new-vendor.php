
<div class="alert alert-success alert-dismissible" role="alert" id="add-vendor-alert" style="display: none;">
    <strong id="add-vendor-alert-message"></strong>
    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<form id="add-vendor-form">
	<div class="row">
        
		<div class="col-md-4">
			<div class="form-group">
    			<label for="vcompany">Vendor Company<sup>*</sup></label>
    			<input type="text" name="vcompany" id="vcompany" class="form-control">
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vaccno">Account Number<sup>*</sup></label>
    			<input type="text" name="vaccno" id="vaccno" class="form-control">
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vfname">First Name</label>
    			<input type="text" name="vfname" id="vfname" class="form-control">
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vlname">Last Name</label>
    			<input type="text" name="vlname" id="vlname" class="form-control">
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vaddrtype">Address Type</label>
    			<select id="vaddrtype" name="vaddrtype" class="form-control">
    				<option value="" data-id=''>Select Address Type</option>
    				<?php foreach (getAddressTypes() as $type) {
    					?>
    					<option value="<?php echo $type->ATName; ?>" data-id="<?php echo $type->ATID; ?>"><?php echo $type->ATName; ?></option>
    					<?php
    				} ?>
    			</select>
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vaddr1">Address 1</label>
    			<input type="text" name="vaddr1" id="vaddr1" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vaddr2">Address 2</label>
    			<input type="text" name="vaddr2" id="vaddr2" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vcity">City</label>
    			<input type="text" name="vcity" id="vcity" class="form-control" onkeyup="this.value=sdUcWords(this.value)">
    		</div>
		</div>
		
        <div class="col-md-4">
            <div class="form-group">
                <label for="vstate">State</label>
                <select class="form-control" name="vstate" id="vstate">
                    <option value="">Select State</option>
                    <
                    <?php foreach (getUSAStates() as $key => $val) {
                        ?>
                        <option value="<?=$key;?>"><?=$val;?></option>
                        <?php
                    } ?>
                </select>
            </div>
        </div>
        
		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vzip">Zip Code</label>
    			<input type="text" name="vzip" id="vzip" class="form-control">
    		</div>
		</div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="vcontacttype">Contact Type</label>
                <select id="vcontacttype" name="vcontacttype" class="form-control">
                    <option value="" data-id=''>Select Contact Type</option>
                    <?php foreach (getContactTypes() as $type) {
                        ?>
                        <option value="<?php echo $type->CTName; ?>" data-id="<?php echo $type->CTID; ?>"><?php echo $type->CTName; ?></option>
                        <?php
                    } ?>
                </select>
            </div>
        </div>

		<div class="col-md-4">
    		<div class="form-group">
    			<label for="vcdata">Contact Data</label>
    			<input type="text" name="vcdata" id="vcdata" class="form-control">
    		</div>
		</div>

        <div class="col-md-4">
            <div class="form-group check-primary">
                <label for="vpcontactcheck"><input type="checkbox" value="1" name="vpcontactcheck" id="vpcontactcheck" class="form-control" checked>Primary Contact ?</label>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group check-primary">
                <label for="visactivecheck"><input type="checkbox" value="1" name="visactivecheck" id="visactivecheck" class="form-control" checked>Is Active ?</label>
            </div>
        </div>

		<input type="hidden" name="current_emp_id" id="current-emp-id" class="form-control" value="<?php echo currentUser('EmployeeID'); ?>">
		<div class="col-md-12 text-center">
			<button class="btn theme-default" id="add-vendor-btn">Save</button>
		</div>

	</div>
</form>