<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type" onclick="$('#add-list-type .modal-header h4').text('Add Role Type');$('#add-list-type form label:eq(0)').html('Role type<sup>*</sup>');$('#add-list-type form')[0].reset();">Add(+)</a>
</div>
<table class="table-bordered list-type-table" id="role-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Role Type</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllEmployeeRoles() as $RT) {
			?>
			<tr id="<?=$RT->ERID;?>">
				<td><?=$RT->ERRoleName;?></td>
				<td><i class="fa fa-edit sd-tootip edit-role-type" data-action="edit_role_type" data-id="<?=$RT->ERID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>