<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type-with-description" onclick="$('#add-list-type-with-description .modal-header h4').text('Add Payment Type');$('#add-list-type-with-description form label:eq(0)').html('Payment type<sup>*</sup>');$('#add-list-type-with-description form')[0].reset();">Add(+)</a>
</div>
<table class="table-bordered list-type-table" id="payment-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Payment Type</th>
			<th style="text-align: center">Description</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllPaymentMethods() as $PaymentType) {
			?>
			<tr id="<?=$PaymentType->PMID;?>">
				<td><?=$PaymentType->PaymentMethodName;?></td>
				<td><?=$PaymentType->PMDescription;?></td>
				<td><i class="fa fa-edit sd-tootip edit-payment-type" data-action="edit_payment_type" data-id="<?=$PaymentType->PMID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>