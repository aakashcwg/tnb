<?php
//tax-types.php
?>
<div class="new-list-type">
    <a href="#" data-toggle="modal" data-target="#add-tax-rate">Add(+)</a>
</div>
<div class="clearfix"></div>
<table class="table-bordered list-type-table" id="tax-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Tax Rate</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllTaxTypes() as $TT) {
			?>
			<tr id="<?=$TT->TaxRateID;?>">
				<td><?=$TT->TaxRatePercentage;?></td>
				<td><i class="fa fa-edit sd-tootip edit-address-type" data-action="edit_tax_type" data-id="<?=$TT->TaxRateID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editTaxRate(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>