<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type" onclick="$('#add-list-type .modal-header h4').text('Add Contact Type');$('#add-list-type form label:eq(0)').html('Contact type<sup>*</sup>');$('#add-list-type form')[0].reset();">Add(+)</a>
</div>
<table class="table-bordered list-type-table" id="contact-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Contact Type</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllContactTypes() as $CT) {
			?>
			<tr id="<?=$CT->CTID;?>">
				<td><?=$CT->CTName;?></td>
				<td><i class="fa fa-edit sd-tootip edit-contact-type" data-action="edit_contact_type" data-id="<?=$CT->CTID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>