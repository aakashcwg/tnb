<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type" onclick="$('#add-list-type .modal-header h4').text('Add Pricing Type');$('#add-list-type form label:eq(0)').html('Pricing type<sup>*</sup>');$('#add-list-type form')[0].reset();">Add(+)</a>
</div>
<table class="table-bordered list-type-table" id="pricing-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Pricing Type</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllPricingTypes() as $PricingType) {
			?>
			<tr id="<?=$PricingType->PTID;?>">
				<td><?=$PricingType->PTName;?></td>
				<td><i class="fa fa-edit sd-tootip edit-pricing-type" data-action="edit_pricing_type" data-id="<?=$PricingType->PTID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>