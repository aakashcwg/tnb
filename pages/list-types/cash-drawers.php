<?php
//cash-drawers.php
?>
<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2 align='center'>You are unable to access this page.</h2>");
?>
<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#cash-drawer">Add(+)</a>
</div>
<div class="clearfix"></div>
<table class="table-bordered list-type-table" id="cash-drawer-table">
	<thead>
		<tr>
			<th align="center">Drawer Name</th>
			<th align="center">Warehouse</th>
			<th align="center">Computer name</th>
			<th align="center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$CDs = GetCashDrawers();
		if($CDs){
			foreach ($CDs as $CD) {
			?>
			<tr id="<?=$CD->CashDrawerID;?>">
				<td><?=$CD->CashDrawerName;?></td>
				<td><?=getWareHouseName($CD->CDWarehouseID);?></td>
				<td><?=$CD->CDComputerName;?></td>
				<td><i class="fa fa-edit sd-tootip edit-cash-drawer" data-id="<?=$CD->CashDrawerID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="EditCashDrawer(this)"></i></td>
			</tr>
			<?php
			}
		} else { ?>

			<tr>
				<td colspan="4">No data available</td>
			</tr>

		<?php } ?>
	</tbody>
</table>
