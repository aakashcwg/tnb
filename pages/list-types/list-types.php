<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2>You are unable to access this page.</h2>");
?>

<div class="row">

	<div class="col-md-4">
		<h4 class="inline">Address Types</h4>
		<?=getPage('list-types/address-types'); ?>
	</div>

	<div class="col-md-4">
		<h4 class="inline">Contact Types</h4>
		<?=getPage('list-types/contact-types'); ?>
	</div>

	<div class="col-md-4">
		<h4 class="inline">Payment Types</h4>
		<?=getPage('list-types/payment-types'); ?>
	</div>

</div>

<div class="row">

	<div class="col-md-4">
		<h4 class="inline">Customer Types</h4>
		<?=getPage('list-types/customer-types'); ?>
	</div>

	<div class="col-md-4">
		<h4 class="inline">Role Types</h4>
		<?=getPage('list-types/role-types');?>
	</div>
	
	<?php if(is_admin() || is_manager()) { ?>
	<div class="col-md-4">
		<h4 class="inline">Pricing Types</h4>
		<?=getPage('list-types/pricing-types');?>
	</div>
	<?php } ?>
	
</div>

<div class="row">

	<div class="col-md-4">
		<h4 class="inline">General Ledger IDs</h4>
		<?=getPage('list-types/general-ledger-ids'); ?>
	</div>
	
	<div class="col-md-8">
		<h4 class="inline">Price List</h4>
		<?=getPage('list-types/price-list'); ?>
	</div>

</div>

<div class="row">

	<div class="col-md-6">
		<h4 class="inline">Cash Drawers</h4>
		<?=getPage('list-types/cash-drawers'); ?>
	</div>

    <div class="col-md-4">
        <h4 class="inline">Tax Rates</h4>
        <?=getPage('list-types/tax-types'); ?>
    </div>

</div>