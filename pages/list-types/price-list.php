<?php
//price-list.phpqa
?>
<?php 
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2 align='center'>You are unable to access this page.</h2>");
?>


<?php if(is_admin() || is_manager()) { ?>
	
<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#new-price-list">Add(+)</a>
</div>
<div class="clearfix"></div>
<table class="table-bordered list-type-table" id="price-list-table">
	<thead>
		<tr>
			<th align="center">Name</th>
			<th align="center">Is Percentage ?</th>
			<th align="center">Discount Amt/%</th>
			<th align="center">Has Items ?</th>
			<!-- <th align="center">List Color</th> -->
			<th align="center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$PLs = getPriceLists();
		if($PLs){
			foreach ($PLs as $PL) {
			?>
			<tr id="<?=$PL->PLID;?>" style="background-color: <?=$PL->PLColor;?>">
				<td><?=$PL->PLName;?></td>
				<td><?=$PL->PLIsPercentage==1?'Yes':'No';?></td>
				<td><?=number_format($PL->PLAmount, 2);?></td>
				<td><?=$PL->PLHasItems==1?'Yes':'No';?></td>
				<!-- <td><?=$PL->PLColor;?></td> -->
				<td>
					<img src="<?=SITE_URL?>/dist/images/tnbpos-multi-edit-pic.png" style="cursor:pointer;" data-id="<?=$PL->PLID;?>" data-original-title="Edit" onclick="editPriceList(this)">
					<!-- <i class="fa fa-edit sd-tootip edit-price-list" data-id="<?=$PL->PLID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editPriceList(this)"></i> --></td>
			</tr>
			<?php
			}
		} else { ?>

			<tr>
				<td colspan="6">No data available</td>
			</tr>

		<?php } ?>
	</tbody>
</table>

<?php } else { 
	echo '<h2 align="center">You are not allowed to access this page</h2>';
}?>