<?php
//address-types.php
?>
<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type" onclick="$('#add-list-type .modal-header h4').text('Add Address Type');$('#add-list-type form label:eq(0)').html('Address type<sup>*</sup>');$('#add-list-type form')[0].reset();">Add(+)</a>
</div>
<div class="clearfix"></div>
<table class="table-bordered list-type-table" id="address-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Address Type</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllAddressTypes() as $AT) {
			?>
			<tr id="<?=$AT->ATID;?>">
				<td><?=$AT->ATName;?></td>
				<td><i class="fa fa-edit sd-tootip edit-address-type" data-action="edit_address_type" data-id="<?=$AT->ATID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>