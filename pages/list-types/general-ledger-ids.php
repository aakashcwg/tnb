<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type-with-description" onclick="$('#add-list-type-with-description .modal-header h4').text('Add Ledger Id');$('#add-list-type-with-description form label:eq(0)').html('Ledger Id<sup>*</sup>');$('#add-list-type-with-description form')[0].reset();">Add(+)</a>
</div>
<table class="table-bordered list-type-table ledger-ids-table" id="ledger-ids-table">
	<thead>
		<tr>
			<th style="text-align: center">Ledger Id</th>
			<th style="text-align: center">Description</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllLedgerNumbers() as $NUM) {
			?>
			<tr id="<?=$NUM->GeneralLedgerID;?>">
				<td><?=$NUM->GLNumber;?></td>
				<td><?=$NUM->GLDescription;?></td>
				<td><i class="fa fa-edit sd-tootip edit-ledger-id" data-action="edit_ledger_id" data-id="<?=$NUM->GeneralLedgerID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>