<div class="new-list-type">
	<a href="javascript:void(0)" data-toggle="modal" data-target="#add-list-type" onclick="$('#add-list-type .modal-header h4').text('Add Customer Type');$('#add-list-type form label:eq(0)').html('Customer type<sup>*</sup>');$('#add-list-type form')[0].reset();">Add(+)</a>
</div>
<table class="table-bordered list-type-table" id="customer-type-table">
	<thead>
		<tr>
			<th style="text-align: center">Customer Type</th>
			<th style="text-align: center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		global $ListTypeOBJ;
		foreach ($ListTypeOBJ->GetAllCustomerAccountTypes() as $CustomerType) {
			?>
			<tr id="<?=$CustomerType->CATLID;?>">
				<td><?=$CustomerType->CATLName;?></td>
				<td><i class="fa fa-edit sd-tootip edit-customer-type" data-action="edit_customer_type" data-id="<?=$CustomerType->CATLID;?>" style="font-size:20px;cursor:pointer;" data-original-title="Edit" onclick="editListsTypes(this)"></i></td>
			</tr>
			<?php
		} ?>
	</tbody>
</table>