<?php
//edit-customer.php

$CustomerID = '';
$TheCustomer = '';
if(isset($_POST['CustomerID'])){
include '../../functions.php';
	$CustomerID = $_POST['CustomerID'];
	$TheCustomer = $CustomerOBJ->GetTheCustomerByID($CustomerID);
}
if(isset($_GET['cid'])){
	global $CustomerOBJ, $ListTypeOBJ;
	$cid = base64_decode($_GET['cid']);
	$TheCustomer = $CustomerOBJ->GetTheCustomerByID($cid);
}

?>

<div class="alert alert-success alert-dismissible" role="alert" id="edit-customer-alert" style="display: none;">
    <strong id="edit-customer-alert-message"></strong>
    <button type="button" class="close close-alert" data-dismiss="" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
                
<form id="edit-customer-form">

    <div class="row">

        <div class="col-md-3">
            <div class="form-group">
                <label for="cfname">First Name<sup>*</sup></label>
                <input type="text" name="cfname" id="cfname" class="form-control" value="<?=$TheCustomer->CustomerFName;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="clname">Last Name<sup>*</sup></label>
                <input type="text" name="clname" id="clname" class="form-control" value="<?=$TheCustomer->CustomerLName;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="ccompany">Company Name</label>
                <input type="text" name="ccompany" id="ccompany" class="form-control" value="<?=$TheCustomer->CustomerCompanyName;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="caccno">Account Number</label>
                <input type="text" name="caccno" id="caccno" class="form-control number-validation" value="<?=$TheCustomer->CustomerAccountNumber;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="tax-exempt">Tax Exempt</label>
                <?php $IsActive = $TheCustomer->CustomerIsTaxExempt; ?>
                <select id="tax-exempt" name="tax_exempt" class="form-control">
                    <option value="0" <?=$IsActive==0?'selected':'';?>>No</option>
                    <option value="1" <?=$IsActive==1?'selected':'';?>>Yes</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="caddrtype">Address Type<sup>*</sup></label>
                <?php $AT = $TheCustomer->CAAddressTypeID; ?>
                <select id="caddrtype" name="caddrtype" class="form-control">
                    <option value="">Select Address Type</option>
                    <?php if(getAddressTypes()) { ?>
                    <?php foreach (getAddressTypes() as $type) {
                    	$selected = '';
                    	if($type->ATID == $AT){
                    		$selected = 'selected';
                    	}
                        ?>
                        <option value="<?php echo $type->ATID; ?>" <?=$selected?>><?php echo $type->ATName; ?></option>
                        <?php
                    } } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="caddr1">Address 1</label>
                <input type="text" name="caddr1" id="caddr1" class="form-control" value="<?=$TheCustomer->CAAddress1;?>" onkeyup="this.value=sdUcWords(this.value)">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="caddr2">Address 2</label>
                <input type="text" name="caddr2" id="caddr2" class="form-control" value="<?=$TheCustomer->CAAddress2;?>" onkeyup="this.value=sdUcWords(this.value)">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="ccity">City</label>
                <input type="text" name="ccity" id="ccity" class="form-control" value="<?=$TheCustomer->CACity;?>" onkeyup="this.value=sdUcWords(this.value)">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="cstate">State</label>
                <?php 
                $State = '';
                if($TheCustomer->CAState){
                    $State = strtoupper($TheCustomer->CAState);
                }
                ?>
                <select class="form-control" name="cstate" id="cstate">
                    <option value="">Select State</option>
                    <
                    <?php foreach (getUSAStates() as $key => $val) {
                    	$selected = '';
                    	if($key == $State){
                    		$selected = 'selected';
                    	}
                        ?>
                        <option value="<?=$key;?>" <?=$selected;?>><?=$val;?></option>
                        <?php
                    } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="czip">Zip Code<sup>*</sup></label>
                <input type="text" name="czip" id="czip" class="form-control" value="<?=$TheCustomer->CAZip;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="customertype">Customer Type<sup>*</sup></label>
                <?php
                $CustomerType = '';
                if($TheCustomer->CATCATLID){
                    $CustomerType = $TheCustomer->CATCATLID;
                }
                ?>
                <select id="customertype" name="customertype" class="form-control">
                    <option value="">Select Customer Type</option>
                    <?php if(getCustomerTypes()) { ?>
                    <?php foreach (getCustomerTypes() as $type) {
                    	$selected = '';
                    	if($type->CATLID == $CustomerType){
                    		$selected = 'selected';
                    	}
                        ?>
                        <option value="<?php echo $type->CATLID; ?>" <?=$selected;?>><?php echo $type->CATLName; ?></option>
                        <?php
                    } } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="ccontacttype">Contact Type<sup>*</sup></label>
                <?php
                $ContactType = '';
                if($TheCustomer->CCCTID){
                    $ContactType = $TheCustomer->CCCTID;
                }
                ?>
                <select id="ccontacttype" name="ccontacttype" class="form-control">
                    <option value="">Select Contact Type</option>
                    <?php if(getContactTypes()) { ?>
                    <?php foreach (getContactTypes() as $type) {
                    	$selected = '';
                    	if($type->CTID == $ContactType){
                    		$selected = 'selected';
                    	}
                        ?>
                        <option value="<?php echo $type->CTID; ?>" <?=$selected;?>><?php echo $type->CTName; ?></option>
                        <?php
                    } } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="ccdata">Contact Data<sup>*</sup></label>
                <input type="text" name="ccdata" id="ccdata" class="form-control" value="<?=$TheCustomer->CCData;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group check-primary">
            	<?php
            	$checked = '';
            	if($TheCustomer->CCIsPrimary == 1){
            		$checked = 'checked';
            	}
            	?>
                <label for="cpcontact"><input type="checkbox" name="cpcontact" id="cpcontact" class="form-control" <?=$checked;?>>Primary Contact ?</label>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="customer-pl">Price List</label>
                <select class="form-control" id="customer-pl" name="customer-pl">
                    <option value="">Select Price List</option>
                    <?php if(getPriceLists()) { ?>
                        <?php foreach (getPriceLists() as $PL) {
                            $selected = $PL->PLID == $TheCustomer->CustomerPLID ? 'selected' : '';
                            ?>
                            <option value="<?=$PL->PLID;?>" <?=$selected?>><?=$PL->PLName;?></option>
                            <?php
                        } ?>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group check-primary">
            	<?php
            	$checked = '';
            	if($TheCustomer->IsActive == 1){
            		$checked = 'checked';
            	}
            	?>
                <label for="cisactivecheck"><input type="checkbox" name="cisactivecheck" id="cisactivecheck" class="form-control" <?=$checked;?>>Is Active ?</label>
            </div>
        </div>

        <input type="hidden" name="tax-id" id="tax-id" value="<?=$TheCustomer->CTDTaxIDNumber;?>">
        <input type="hidden" name="expiration-date" id="expiration-date" value="<?=$TheCustomer->CTDExpirationDate;?>">
        <input type="hidden" name="customer-id" id="customer-id" value="<?=$TheCustomer->CustomerID;?>">
        <div class="col-md-12 text-center">
            <a class="btn theme-default" href="javascript:void(0)" id="update-customer-btn" onclick="UpdateCustomer()">Update</a>
        </div>

    </div>

</form>