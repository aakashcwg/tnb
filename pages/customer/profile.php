<?php
$CustomerID = isset($_GET['cid']) ? base64_decode($_GET['cid']) : '';
global $EmployeeOBJ, $CustomerOBJ, $ListTypeOBJ;
$TheCustomer = $CustomerOBJ->GetTheCustomerByID($CustomerID);

$isActive = $TheCustomer->IsActive==1?'<img src="'.SITE_URL.'/dist/images/active.png" class="active-customer sd-tootip isactive" title="Active">':'<img src="'.SITE_URL.'/dist/images/disabled.png" class="inactive-customer sd-tootip isactive" title="Inactive">';
?>

<div class="container emp-profile">
    <div class="row">
        <!-- <div class="col-md-3">
            <div class="profile-img">
                <img src="<?=SITE_URL?>/dist/images/default-profile-pic.jpg" height="140px" width="140px" alt="<?=$TheCustomer->CustomerFName.' '.$TheCustomer->CustomerLName;?>">
            </div>
        </div> -->
        <div class="col-md-9">
            <div class="profile-head">
                <h3><?=$TheCustomer->CustomerFName.' '.$TheCustomer->CustomerLName;?></h3>
                <h6>Entry Date : <i><?=date(DATE_TIME_FORMAT, strtotime($TheCustomer->CustomerDOE));?></i></h6>

                <div class="sales-amt-details">
                    <?php $cid = $TheCustomer->CustomerID; ?>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="total-sales">Total Sales : 
                            <?php echo '$'.number_format($CustomerOBJ->GetPaidAmt($cid), 2); ?></label>
                        </div>
                        <div class="col-md-6">
                            <label for="amount-due">Amount Due : 
                            <?php echo '$'.number_format($CustomerOBJ->GetDueAmt($cid), 2); ?></label>
                        </div>
                    </div>
                </div>


                <div class="tab-block">
                    <ul class="nav nav-tabs">
    				    <li class="active"><a data-toggle="tab" href="#basic-details">Basic Details</a></li>
    				    <li><a data-toggle="tab" href="#contact">Contact</a></li>
    				    <li><a data-toggle="tab" href="#address">Address</a></li>
    				</ul>
    			  	<div class="tab-content profile-tab">
    			    	<div id="basic-details" class="tab-pane fade in active">
    				      	<h3>Basic Details</h3>
    				      	<div class="row">
    				      		<div class="col-md-6">
                                    <label>First Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CustomerFName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Last Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CustomerLName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Company Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CustomerCompanyName;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Account Number</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CustomerAccountNumber;?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Tax Exempt</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CustomerIsTaxExempt==1?'YES':'NO';?></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <label>Added By</label>
                                </div>
                                <div class="col-md-6">
                                	<?php $EMP=$EmployeeOBJ->GetTheEmployeeByID($TheCustomer->CustomerEmployeeID); ?>
                                    <p><?=$EMP->EFName.' '.$EMP->ELName.' ( '.$EMP->ELUsername.' )';?></p>
                                </div>

                                <?php if($TheCustomer->CustomerPLID) { ?>
                                <div class="col-md-6">
                                    <label>Price List</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=PriceListName($TheCustomer->CustomerPLID); ?></p>
                                </div>
                                <?php } ?>

                                <div class="col-md-6">
                                    <label>Status</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$isActive;?></p>
                                </div>

                                <div class="clearfix"></div>
    				      	</div>
    			    	</div>
    			    	<div id="contact" class="tab-pane fade">
    					    <h3>Contact</h3>
    					    <div class="row">
    					    	<?php if(!empty($TheCustomer->CCCTID)) { ?>
    				      		<div class="col-md-6">
                                    <label><?=$ListTypeOBJ->GetTheContactTypeByID($TheCustomer->CCCTID);?></label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CCData;?></p>
                                </div>
                            	<?php } ?>
    				      	</div>
    			    	</div>
    			    	<div id="address" class="tab-pane fade">
    			      		<h3>Address</h3>
    			      		<div class="row">

    					    	<?php if(!empty($TheCustomer->CAAddress1)) { ?>
    				      		<div class="col-md-6">
                                    <label>Address 1</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CAAddress1;?></p>
                                </div>
                            	<?php } ?>

                            	<?php if(!empty($TheCustomer->CAAddress2)) { ?>
                                <div class="col-md-6">
                                    <label>Address 2</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CAAddress2;?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($TheCustomer->CACity)) { ?>
                                <div class="col-md-6">
                                    <label>City</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CACity;?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($TheCustomer->CAState)) { ?>
                                <div class="col-md-6">
                                    <label>State</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=getUSAStates()[strtoupper($TheCustomer->CAState)];?></p>
                                </div>
                                <?php } ?>

                                <?php if(!empty($TheCustomer->CAZip)) { ?>
                                <div class="col-md-6">
                                    <label>Zip Code</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?=$TheCustomer->CAZip;?></p>
                                </div>
                                <?php } ?>

    				      	</div>
    			    	</div>
    			  	</div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <a class="btn theme-default-orange" href="<?=SITE_URL.'?destination=customers&action=edit-customer&cid='.base64_encode($TheCustomer->CustomerID);?>">Edit profile</a>
            <?php
            $SH_URL = SITE_URL.'/?destination=sales&action=sales-history&customer_id='.$TheCustomer->CustomerID;
            $PM_URL = SITE_URL.'/?destination=payments&cid='.$TheCustomer->CustomerID;
            ?>
            <a class="btn theme-default-orange" href="<?=$SH_URL;?>">Sales History</a>
            <a class="btn theme-default-orange" href="<?=$PM_URL;?>">Make Payment</a>
        </div>
    </div>         
</div>
