
<?php global $CustomerOBJ; ?>

<table id="customer-table" class="table table-bordered sd-table" style="width:100%">
    <thead>
        <tr>
            <th>Customer Name</th>
            <th>Company Name</th>
            <th>Account Number</th>
            <th>Tax Exempt</th>
            <th>Entry Date</th>
            <th>Price List</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
   	<?php
   	if(!empty($CustomerOBJ->GetAllCustomers())):
    	$countCustomer = 1;
    	foreach ($CustomerOBJ->GetAllCustomers() as $customer) {
    		$empObj = getEployeeByID($customer->CustomerEmployeeID);
			$theEmp = $empObj[$customer->CustomerEmployeeID];
    		?>
    		<tr id="row-<?php echo $customer->CustomerID; ?>">
	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=customers&action=customer-profile&cid=<?=base64_encode($customer->CustomerID);?>'"><?=$customer->CustomerFName.' '.$customer->CustomerLName;?></td>
	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=customers&action=customer-profile&cid=<?=base64_encode($customer->CustomerID);?>'"><?=$customer->CustomerCompanyName;?></td>
	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=customers&action=customer-profile&cid=<?=base64_encode($customer->CustomerID);?>'"><?=$customer->CustomerAccountNumber;?></td>
	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=customers&action=customer-profile&cid=<?=base64_encode($customer->CustomerID);?>'"><?=$customer->CustomerIsTaxExempt==0?'No':'Yes';?></td>
	            <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=customers&action=customer-profile&cid=<?=base64_encode($customer->CustomerID);?>'"><?=date(DATE_TIME_FORMAT, strtotime($customer->CustomerDOE));?></td>
                <td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="window.location.href='<?=SITE_URL;?>?destination=customers&action=customer-profile&cid=<?=base64_encode($customer->CustomerID);?>'"><?=PriceListName($customer->CustomerPLID); ?></td>
	            <td class="action-btns">
	            	<i class="fa fa-edit sd-tootip" title="Quick Edit" data-id="<?=$customer->CustomerID;?>" style="font-size:24px;cursor:pointer;" onclick="editCustomer(this)"></i>
				</td>
	        </tr>
	        <?php
    	$countCustomer++; }
    endif;
    ?>
    </tbody>
</table>