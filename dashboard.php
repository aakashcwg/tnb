<?php
//dashboard.php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false){
  die("<h2>You are unable to access this page.</h2>");
}
?>
<div class="dashboard-container">
    <h2>Dashboard</h2>
    <?php if(is_manager() || is_admin()) : ?>
    <div class="panel-group">
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Total Inventory<span class="right-more"><a href="<?= SITE_URL.'?destination=inventory'; ?>"><img src="dist/images/more.png"></a></span></div>
              <div class="panel-body"><?php echo count(getInventories()); ?></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Total Employees<span class="right-more"><a href="<?= SITE_URL.'?destination=employees'; ?>"><img src="dist/images/more.png"></a></span></div>
              <div class="panel-body"><?php echo (count(getEployees()) > 1)?count(getEployees())-1:0; ?></div>
          </div>
        </div>
      <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Managers<span class="right-more"><a href="<?= SITE_URL.'?destination=employees&role=Manager'; ?>"><img src="dist/images/more.png"></a></span></div>
              <div class="panel-body"><?php echo getEployees('Manager') ? count(getEployees('Manager')) : 0; ?></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Cashiers<span class="right-more"><a href="<?= SITE_URL.'?destination=employees&role=Cashier'; ?>"><img src="dist/images/more.png"></a></span></div>
              <div class="panel-body"><?php echo getEployees('Cashier') ? count(getEployees('Cashier')) : 0; ?></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Workers<span class="right-more"><a href="<?= SITE_URL.'?destination=employees&role=Worker'; ?>"><img src="dist/images/more.png"></a></span></div>
              <div class="panel-body"><?php echo getEployees('Worker') ? count(getEployees('Worker')) : 0; ?></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Low Stock Items<span class="right-more"><a href="javascript:void(0)" data-toggle="modal" data-target="#low-stock-items-list"><img src="dist/images/more.png"></a></span></div>
              <?php
              global $InventoryOBJ;
              $LSIs = $InventoryOBJ->GetLowStockItems();
              ?>
              <div class="panel-body"><?php echo $LSIs ? count($LSIs) : 0; ?></div>
          </div>
        </div>
      </div>
    </div>
    <?php else: ?>
      <script>window.location.href = "<?= SITE_URL.'?destination=sales'; ?>"</script>       <?php // AVD 12/29/18 0415 ?>
      <h3 class="text-center" style="margin: 200px 0 0 0;color: red;">You are not allowed to access this page. Please contact your Manager</h3>
    <?php endif; ?>
</div>