<?php
//header.php

if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false){
  die("<h2>You are unable to access this page.</h2>");
}

?>

<!-- Header Start -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
       <div class="logo-block">
           <a href="<?=SITE_URL;?>"><img src="dist/images/logo.jpg" alt="" /></a>
       </div>
       <div class="rt-block">
           <div class="box">
               <h2>Recharge your day</h2>
               <h3>Tennessee Battery Sales LLC</h3>
               <h4>Sales . Service . Delivery</h4>
           </div>
           <div class="box">
                <div class="icon-block">
                    <!-- <img src="dist/images/location-icon.png" alt="" /> -->
                </div>
                <div class="text-block">
                    <h5>1942 Hwy 46 S Dickson, TN 37055</h5>
                    <a href="tel:615.446.0315">615.446.0315</a>
                </div>
           </div>
           <div class="box">
                <div class="icon-block">
                    <!-- <img src="dist/images/clock-icon.png" alt="" /> -->
                </div>
                <div class="text-block">
                    <h6><span>M-F</span> 7:30am-5:00pm</h6>
                    <h6><span>Sat</span> 8:00am-3:00pm</h6>
                </div>
           </div>
           <div class="clearfix"></div>
       </div>
    </div>
    <div class="clearfix"></div>
</nav>
<!-- Header End -->