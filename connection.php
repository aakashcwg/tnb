<?php

//connection.php

require_once('config.php');
$driver = DRIVER;
$serverName = SERVER_NAME;
$dbname = DB_NAME;
$user = USER;
$pass = PASS;
try {
	//$pdo = new PDO("sqlsrv:server=$serverName; Database=$dbname"); //connection with windows authentication
	$pdo = new PDO("$driver:server=$serverName;Database=$dbname", $user, $pass);
  	$pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
}
catch(PDOException $e) {
	echo 'ERROR: ' . $e->getMessage();
}
?>