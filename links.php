<?php
//links.php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false){
	die("<h2>You are unable to access this page.</h2>");
}

/*=====================START - CSS=====================*/

echo '<link rel="stylesheet" href="dist/css/bootstrap.min.css">';
echo '<link rel="stylesheet" href="dist/css/bootstrap-theme.min.css">';
echo '<link rel="stylesheet" href="dist/css/style.css">';
echo '<link rel="stylesheet" href="dist/css/theme.css">';
echo '<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">';
echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" />';
echo '<link rel="stylesheet" href="dist/css/jquery.mCustomScrollbar.css" />';

echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';
//echo '<link rel="stylesheet" href="dist/css/bootstrap-multiselect.css" />';

echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">';


//jquery UI - CSS CDN
//echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';

echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">';


//CSS for Color-Picker
echo '<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css" rel="stylesheet">';

echo '<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/themes/smoothness/jquery-ui.css" />';

/*=====================START - CSS=====================*/


/*=====================START - JS=====================*/

echo '<script src="dist/js/jquery2.2.4.min.js"></script>';
echo '<script src="dist/js/bootstrap.min.js"></script>';
echo '<script src="dist/js/jquery-dataTable.min.js"></script>';
echo '<script src="dist/js/bootstrap-dataTable.min.js"></script>';
echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>';
echo '<script src="dist/js/jquery.mCustomScrollbar.concat.min.js"></script>';

echo '  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>';

echo '<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>';

//JS for Color-Picker
echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/js/bootstrap-colorpicker.js"></script>';

echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>';

/*=====================START - END=====================*/