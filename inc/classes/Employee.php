<?php

/*if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");*/

//Claas Employee.php

Class Employee extends SDPDO {

	const TBL_EMPLOYEES_FIELDS = array('EFName', 'ELName', 'EAddress1', 'EAddress2', 'ECity', 'EState', 'EZip', 'IsActive');

	const TBL_EMPLOYEE_LOGIN_FIELDS = array('ELEID','ELUsername','ELPassword','ELRoleID','ELDateExpire');

	const TBL_EMPLOYEE_CONTACT_FIELDS = array('ECIDEmployeeID','ECCTID','ECData','ECIsPrimary');


	//Add new Employee
	public function AddNewEmployee($tblEmployees_values){
		$tblEmployees_data = array_combine(self::TBL_EMPLOYEES_FIELDS, $tblEmployees_values);
		$NewEmployeeID = $this->InsertData(TBL_EMPLOYEES, $tblEmployees_data);
		return $NewEmployeeID;
	}

	//Add Employee Login Data
	public function AddEmployeeLoginData($tblEmployeeLogin_values){
		$tblEmployeeLogin_data = array_combine(self::TBL_EMPLOYEE_LOGIN_FIELDS, $tblEmployeeLogin_values);
		$InsertID = $this->InsertData(TBL_EMPLOYEE_LOGIN, $tblEmployeeLogin_data);
		return $InsertID;
	}

	//Add Employee Contact Data
	public function AddEmployeeContactData($tblEmployeeContact_values){
		$tblEmployeeContact_data = array_combine(self::TBL_EMPLOYEE_CONTACT_FIELDS, $tblEmployeeContact_values);
		$InsertID = $this->InsertData(TBL_EMPLOYEE_CONTACT, $tblEmployeeContact_data);
		return $InsertID;
	}

	//Add Employee Start Date - In History Table
	public function AddEmployeeStartDate($EID){
		$date = date(DATE_TIME_FORMAT);
		$InsertID = $this->InsertData(TBL_EMPLOYEE_HISTORY, array('EHEmployeeID' => $EID, 'EHStartDate' => $date));
		return $InsertID;
	}

	//Reset Password
	public function ResetPassword($EID, $NewPass){
		$return = $this->update(TBL_EMPLOYEE_LOGIN, array('ELPassword'), array($NewPass), array('ELEID', $EID));
		return $return;
	}

	//Get all Employees
	public function GetAllEmployees($Where=null){
		$whereStr = '';
		if($Where){
            foreach ($Where as $key => $value) {
                $whereStr .= $key.'='. "'$value'" .' AND ';
            }
			$whereStr = 'WHERE '.rtrim($whereStr, ' AND ');
		}
		$data = $this->MyQuery(' SELECT * FROM ('.TBL_EMPLOYEES.' RIGHT JOIN '.TBL_EMPLOYEE_LOGIN.' ON '.TBL_EMPLOYEES.'.EmployeeID = '.TBL_EMPLOYEE_LOGIN.'.ELEID LEFT JOIN '.TBL_EMPLOYEE_CONTACT.' ON '.TBL_EMPLOYEE_CONTACT.'.ECIDEmployeeID = '.TBL_EMPLOYEE_LOGIN.'.ELEID) '.$whereStr);
		return $data;
	}

	//Get the Employee By Employee ID
	public function GetTheEmployeeByID($EmployeeID){
		$data = $this->GetAllEmployees(array('EmployeeID' => $EmployeeID));
		return $data[0];
	}

	//Get the Employee
	public function GetTheEmployee($username){
		$data = $this->GetAllEmployees(array('ELUsername' => $username));
		return $data;
	}

	//Get the Employee
	public function GetUserName($EID){
		$data = $this->GetAll(TBL_EMPLOYEE_LOGIN, array('ELEID' => $EID));
		if($data){
			return $data[0]->ELUsername;
		}
	}

	//Delete Employee
	public function DeleteEmployee($EmployeeID, $note=null){
		$this->update(TBL_EMPLOYEE_HISTORY, array('EHEndDate', 'EHNotes'), array(date(DATE_TIME_FORMAT), $note), array('EHEmployeeID', $EmployeeID));
		$this->update(TBL_EMPLOYEES, array('IsActive'), array(0), array('EmployeeID', $EmployeeID));
		$stmt = $this->pdo->prepare(' DELETE FROM '.TBL_EMPLOYEE_LOGIN.' WHERE ELEID = '.$EmployeeID);
		$stmt->execute();
		if($stmt->rowCount() > 0){
			return true;
		}
	}

	//Get Employee Joning Date
	public function GetEmployeeJoiningDate($EmployeeID){
		$data = $this->MyQuery( 'SELECT EHStartDate AS JoinigDate FROM '.TBL_EMPLOYEE_HISTORY.' WHERE EHEmployeeID = '.$EmployeeID, true );
		if($data){
			return $data->JoinigDate;
		}
	}

	public function GetEmployeesByRole($role=''){
		$data = null;
		if($role){
			$data = $this->MyQuery("SELECT * FROM ".TBL_EMPLOYEES." RIGHT JOIN ".TBL_EMPLOYEE_LOGIN." ON ".TBL_EMPLOYEES.".EmployeeID = ".TBL_EMPLOYEE_LOGIN.".ELEID LEFT JOIN ".TBL_EMPLOYEE_CONTACT." ON ".TBL_EMPLOYEE_CONTACT.".ECIDEmployeeID = ".TBL_EMPLOYEE_LOGIN.".ELEID WHERE ".TBL_EMPLOYEE_LOGIN.".ELRoleID IN (SELECT ERID FROM ".TBL_EMPLOYEE_ROLES." WHERE ERRoleName = '".$role."')");
		}else{
			$data = $this->GetAllEmployees();
		}
		if($data){
			return $data;
		}
	}


}