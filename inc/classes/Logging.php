<?php
/**
 *  Logging Class
 */
class Logging extends SDPDO
{
	const TBL_AUDIT_CUSTOMER_LOG_FIELDS = array('ACLCustomerID', 'ACLEmployeeID', 'ACLDOE', 'ACLNotes');

	const TBL_AUDIT_SALES_LOG_FIELDS = array('ASLSalesID', 'ASLSalesDetailID', 'ASLEmployeeID', 'ASLDOE', 'ASLNotes', 'ASLIOHID');

	const TBL_AUDIT_INVENTORY_FIELDS = array('AIEmployeeID', 'AIIOID', 'AIIOHID');

	const TBL_AUDIT_VENDORS_LOG_FIELDS = array('AVLVendorID', 'AVLEmployeeID', 'AVLDOE', 'AVLNotes');


	/*==================Customer Logs START==================*/

	public function NewCustomerLog($values){
		if(NEW_CUSTOMER_LOG === true){
			$data = array_combine(self::TBL_AUDIT_CUSTOMER_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_CUSTOMER_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function UpdateCustomerLog($values){
		if(UPDATE_CUSTOMER_LOG === true){
			$data = array_combine(self::TBL_AUDIT_CUSTOMER_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_CUSTOMER_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function CustomerMakeSaleLog($values){
		if(CUSTOMER_MAKE_SALE_LOG === true){
			$data = array_combine(self::TBL_AUDIT_CUSTOMER_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_CUSTOMER_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function CustomerMakeQuoteLog($values){
		if(CUSTOMER_MAKE_QUOTE_LOG === true){
			$data = array_combine(self::TBL_AUDIT_CUSTOMER_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_CUSTOMER_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function CustomerQuoteUpdateLog($values){
		if(CUSTOMER_QUOTE_UPDATE_LOG === true){
			$data = array_combine(self::TBL_AUDIT_CUSTOMER_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_CUSTOMER_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function CustomerCompleteSalesFromQuoteLog($values){
		if(CUSTOMER_COMPLETE_SALES_FROM_QUOTE_LOG === true){
			$data = array_combine(self::TBL_AUDIT_CUSTOMER_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_CUSTOMER_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	/*==================Customer Logs END==================*/

	/*==================Sales Logs START==================*/

	public function SalesMakeVoidLog($values){
		if(SALES_MAKE_VOID === true){
			$data = array_combine(self::TBL_AUDIT_SALES_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_SALES_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function CompleteSalesLog($values){
		if(SALES_COMPLETE === true){
			$data = array_combine(self::TBL_AUDIT_SALES_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_SALES_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function QuoteMakeLog($values){
		if(SALES_QUOTE === true){
			$data = array_combine(self::TBL_AUDIT_SALES_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_SALES_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function QuoteUpdateLog($values){
		if(SALES_QUOTE_UPDATE === true){
			$data = array_combine(self::TBL_AUDIT_SALES_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_SALES_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	/*==================Sales Logs END==================*/

	/*===============Inventory Logs START===============*/

	public function AddNewInventoryItemLog($values){
		if(ADD_NEW_INVENTORY === true){
			$data = array_combine(self::TBL_AUDIT_INVENTORY_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_INVENTORY, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function UpdateInventoryItemLog($values){
		if(UPDATE_INVENTORY === true){
			$data = array_combine(self::TBL_AUDIT_INVENTORY_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_INVENTORY, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	public function InventoryItemOrderLog($values){
		if(INVENTORY_ITEM_ORDER_LOG === true){
			$data = array_combine(self::TBL_AUDIT_INVENTORY_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_INVENTORY, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}

	/*===============Inventory Logs END===============*/


	public function VendorLog($values){
		if(NEW_VENDOR_LOG === true){
			$data = array_combine(self::TBL_AUDIT_VENDORS_LOG_FIELDS, $values);
			$NewLogID = $this->InsertData(TBL_AUDIT_VENDORS_LOG, $data);
			if($NewLogID){
				return $NewLogID;
			}
		}
	}


}