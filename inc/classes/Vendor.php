<?php

/*if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");*/

//Claas Vendor.php

Class Vendor extends SDPDO {

	const TBL_VENDORS_FIELDS 					= array('VendorCompanyName', 'VendorAccountNumber', 'VendorEmployeeID', 'VendorDOE', 'IsActive');

	const TBL_VENDOR_ADDRESSES_FIELDS 			= array('VAVendorID','VAAddressTypeID','VAAddress1','VAAddress2','VACity', 'VAState', 'VAZip');

	const TBL_VENDOR_CONTACT_FIELDS 			= array('VCIDVendorID','VCCTID','VCData','VCIsPrimary','VCIDFName', 'VCIDLname');

	//Add new Vendor
	public function AddNewVendor($tblVendors_values){
		$tblVendors_data = array_combine(self::TBL_VENDORS_FIELDS, $tblVendors_values);
		$NewVendorID = $this->InsertData(TBL_VENDORS, $tblVendors_data);
		return $NewVendorID;
	}

	//Add Vendor Address Data
	public function AddVendorAddressData($tblVendorAddresses_values){
		$tblVendorAddresses_data = array_combine(self::TBL_VENDOR_ADDRESSES_FIELDS, $tblVendorAddresses_values);
		$InsertID = $this->InsertData(TBL_VENDOR_ADDRESSES, $tblVendorAddresses_data);
		return $InsertID;
	}

	//Add Vendor Contact Data
	public function AddVendorContactData($tblVendorContact_values){
		$tblVendorContact_data = array_combine(self::TBL_VENDOR_CONTACT_FIELDS, $tblVendorContact_values);
		$InsertID = $this->InsertData(TBL_VENDOR_CONTACT, $tblVendorContact_data);
		return $InsertID;
	}

	//Get all Vendors
	public function GetAllVendors(){
		$data = $this->MyQuery(' SELECT * FROM '.TBL_VENDORS.' INNER JOIN '.TBL_VENDOR_CONTACT.' ON '.TBL_VENDORS.'.VendorID = '.TBL_VENDOR_CONTACT.'.VCIDVendorID INNER JOIN '.TBL_VENDOR_ADDRESSES.' ON '.TBL_VENDOR_CONTACT.'.VCIDVendorID = '.TBL_VENDOR_ADDRESSES.'.VAVendorID ');
		return $data;
	}

	//Get the Vendor
	public function GetTheVendorByID($VendorID){
		$data = $this->MyQuery(' SELECT * FROM '.TBL_VENDORS.' INNER JOIN '.TBL_VENDOR_CONTACT.' ON '.TBL_VENDORS.'.VendorID = '.TBL_VENDOR_CONTACT.'.VCIDVendorID INNER JOIN '.TBL_VENDOR_ADDRESSES.' ON '.TBL_VENDOR_CONTACT.'.VCIDVendorID = '.TBL_VENDOR_ADDRESSES.'.VAVendorID WHERE '.TBL_VENDORS.'.VendorID = '.$VendorID, true );
		return $data;
	}

	//Get the Vendor Payments
	public function GetTheVendorPaymentsByID($VendorID){
		$data = $this->GetAll(TBL_INVENTORY_ORDER_DETAILS, array('IODVendorID' => $VendorID));
		return $data;
	}

	public function GetVendorPayableOrderIds($VendorID){
		$OrderIDs = array();
		$orders = $this->GetTheVendorPaymentsByID($VendorID);
		foreach ($orders as $order) {
			$OrderIDs[] = $order->IODIOID;
		}
		return array_unique($OrderIDs);
	}

	public function GetLastPurchaseDetails($VendorID, $ItemID){
		$data = $this->MyQuery('SELECT TOP 1 * FROM '.TBL_INVENTORY_ORDER_DETAILS.' WHERE IODVendorID = '.$VendorID.' AND IODINVID = '.$ItemID.' ORDER BY IODID DESC');
		if($data){
			return $data[0];
		}else{
			return false;
		}
	}


	public function UpdateVendor($values, $where){
		$update = $this->update(TBL_VENDORS, array('VendorCompanyName', 'VendorAccountNumber', 'IsActive'), $values, $where);
		return $update;
	}

	public function UpdateVendorAddresses($values, $where){
		$update = $this->update(TBL_VENDOR_ADDRESSES, array('VAAddressTypeID', 'VAAddress1', 'VAAddress2', 'VACity', 'VAState', 'VAZip'), $values, $where);
		return $update;
	}

	public function UpdateVendorContact($values, $where){
		$update = $this->update(TBL_VENDOR_CONTACT, array('VCCTID', 'VCData', 'VCIsPrimary', 'VCIDFName', 'VCIDLname'), $values, $where);
		return $update;
	}



	public function VAID($vid){
		$data = $this->MyQuery('SELECT VAID FROM '.TBL_VENDOR_ADDRESSES.' WHERE VAVendorID = '.$vid, true);
		if($data){
			return $data->VAID;
		}
	}

	public function VCID($vid){
		$data = $this->MyQuery('SELECT VCID FROM '.TBL_VENDOR_CONTACT.' WHERE VCIDVendorID = '.$vid, true);
		if($data){
			return $data->VCID;
		}
	}



}