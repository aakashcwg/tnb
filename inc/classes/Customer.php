<?php

/*if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");*/

//Claas Customer.php

Class Customer extends SDPDO {

	const TBL_CUSTOMERS_FIELDS = array('CustomerFName', 'CustomerLName', 'CustomerCompanyName', 'CustomerAccountNumber', 'CustomerIsTaxExempt', 'CustomerPLID', 'CustomerEmployeeID', 'CustomerDOE', 'IsActive');

	const TBL_CUSTOMER_CONTACT_FIELDS = array('CCIDCustomerID','CCCTID','CCData','CCIsPrimary','CCIDFName','CCIDLname');

	const TBL_CUSTOMER_ADDRESSES_FIELDS = array('CACustomerID','CAAddressTypeID','CAAddress1','CAAddress2','CACity','CAState', 'CAZip');

	const TBL_CUSTOMER_ACCOUNT_TYPE_FIELDS = array('CATCustomerID', 'CATCATLID');

	const TBL_CUSTOMER_TAX_DATA_FIELDS = array('CTDCustomerID', 'CTDTaxIDNumber', 'CTDExpirationDate');


	public function AddNewCustomer($tblCustomers_values){
		$tblCustomers_data = array_combine(self::TBL_CUSTOMERS_FIELDS, $tblCustomers_values);
		$NewCustomerID = $this->InsertData(TBL_CUSTOMERS, $tblCustomers_data);
		return $NewCustomerID;
	}

	public function AddCustomerContactData($tblCustomerContact_values){
		$tblCustomerContact_data = array_combine(self::TBL_CUSTOMER_CONTACT_FIELDS, $tblCustomerContact_values);
		$InserID = $this->InsertData(TBL_CUSTOMER_CONTACT, $tblCustomerContact_data);
		return $InserID;
	}

	public function AddCustomerAddressData($tblCustomerAddresses_values){
		$tblCustomerAddresses_data = array_combine(self::TBL_CUSTOMER_ADDRESSES_FIELDS, $tblCustomerAddresses_values);
		$InserID = $this->InsertData(TBL_CUSTOMER_ADDRESSES, $tblCustomerAddresses_data);
		return $InserID;
	}

	public function AddCustomerAccountTypeData($tblCustomerAccountType_values){
		$tblCustomerAccountType_data = array_combine(self::TBL_CUSTOMER_ACCOUNT_TYPE_FIELDS, $tblCustomerAccountType_values);
		$InserID = $this->InsertData(TBL_CUSTOMER_ACCOUNT_TYPE, $tblCustomerAccountType_data);
		return $InserID;
	}

	public function AddCustomerTaxData($tblCustomerTaxData_values){
		$tblCustomerTaxData_data = array_combine(self::TBL_CUSTOMER_TAX_DATA_FIELDS, $tblCustomerTaxData_values);
		$InserID = $this->InsertData(TBL_CUSTOMER_TAX_DATA, $tblCustomerTaxData_data);
		return $InserID;
	}

	public function GetAllCustomers(){
		return $this->MyQuery( 'SELECT * FROM '.TBL_CUSTOMERS.' INNER JOIN '.TBL_CUSTOMER_CONTACT.' ON '.TBL_CUSTOMERS.'.CustomerID = '.TBL_CUSTOMER_CONTACT.'.CCIDCustomerID INNER JOIN '.TBL_CUSTOMER_ADDRESSES.' ON '.TBL_CUSTOMER_CONTACT.'.CCIDCustomerID = '.TBL_CUSTOMER_ADDRESSES.'.CACustomerID INNER JOIN '.TBL_CUSTOMER_ACCOUNT_TYPE.' On '.TBL_CUSTOMER_ADDRESSES.'.CACustomerID = '.TBL_CUSTOMER_ACCOUNT_TYPE.'.CATCustomerID INNER JOIN '.TBL_CUSTOMER_TAX_DATA.' ON '.TBL_CUSTOMER_ACCOUNT_TYPE.'.CATCustomerID = '.TBL_CUSTOMER_TAX_DATA.'.CTDCustomerID' );
	}

	public function GetTheCustomerByID($CustomerID){
		$data = $this->MyQuery( 'SELECT * FROM '.TBL_CUSTOMERS.' INNER JOIN '.TBL_CUSTOMER_CONTACT.' ON '.TBL_CUSTOMERS.'.CustomerID = '.TBL_CUSTOMER_CONTACT.'.CCIDCustomerID INNER JOIN '.TBL_CUSTOMER_ADDRESSES.' ON '.TBL_CUSTOMER_CONTACT.'.CCIDCustomerID = '.TBL_CUSTOMER_ADDRESSES.'.CACustomerID INNER JOIN '.TBL_CUSTOMER_ACCOUNT_TYPE.' On '.TBL_CUSTOMER_ADDRESSES.'.CACustomerID = '.TBL_CUSTOMER_ACCOUNT_TYPE.'.CATCustomerID INNER JOIN '.TBL_CUSTOMER_TAX_DATA.' ON '.TBL_CUSTOMER_ACCOUNT_TYPE.'.CATCustomerID = '.TBL_CUSTOMER_TAX_DATA.'.CTDCustomerID WHERE '.TBL_CUSTOMERS.'.CustomerID = '.$CustomerID, true );
		return $data;
	}

	public function GetTheCustomerByAccountNumber($AccountNumber){
		$data = $this->GetAll(TBL_CUSTOMERS, array('CustomerAccountNumber' => $AccountNumber));
		if($data){
			return $data[0];
		}else{
			return null;
		}
		
	}

	public function UpdateCustomer($values, $where){
		$update = $this->update(TBL_CUSTOMERS, self::TBL_CUSTOMERS_FIELDS, $values, $where);
		return $update;
	}

	public function UpdateCustomerContactData($values, $where){
		$update = $this->update(TBL_CUSTOMER_CONTACT, self::TBL_CUSTOMER_CONTACT_FIELDS, $values, $where);
		return $update;
	}

	public function UpdateCustomerAddressesData($values, $where){
		$update = $this->update(TBL_CUSTOMER_ADDRESSES, self::TBL_CUSTOMER_ADDRESSES_FIELDS, $values, $where);
		return $update;
	}

	public function UpdateCustomerAccountTypeData($values, $where){
		$update = $this->update(TBL_CUSTOMER_ACCOUNT_TYPE, self::TBL_CUSTOMER_ACCOUNT_TYPE_FIELDS, $values, $where);
		return $update;
	}

	public function UpdateCustomerTaxData($values, $where){
		$update = $this->update(TBL_CUSTOMER_TAX_DATA, self::TBL_CUSTOMER_TAX_DATA_FIELDS, $values, $where);
		return $update;
	}

	public function IsCustomerHasSalesQuote($CustomerID){
		$data = $this->MyQuery( 'SELECT * FROM '.TBL_SALES.' WHERE SCustomerID = '.$CustomerID.' AND SIsPaid = 0 AND SIsVoid = 0 ORDER BY SalesID DESC' );
		if($data){
			$return = array(
				'SalesID' => $data[0]->SalesID,
				'QuoteUrl' => SITE_URL.'?destination=sales&fetch=invoice&get_from=quote-history&selling_id='.$data[0]->SalesID
			);
			return $data[0]->SalesID;
		}else{
			return false;
		}
	}

	//Check Customer has Quotes or not
	public function HasQuotes($CustomerID){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES.' WHERE SCustomerID = '.$CustomerID.' AND SIsPaid = 0 AND SIsVoid = 0 ORDER BY SalesID DESC');
		if($data){
			return true;
		}
	}

	function IsTaxExempt($CustomerID){
		$TheCustomer = $this->GetTheCustomerByID($CustomerID);
		if($TheCustomer->CustomerIsTaxExempt == 1)
			return true;
		else
			return false;
	}


	public function CCID($cid){
		$data = $this->MyQuery('SELECT CCID FROM '.TBL_CUSTOMER_CONTACT.' WHERE CCIDCustomerID = '.$cid, true);
		if($data){
			return $data->CCID;
		}
	}
	public function CAID($cid){
		$data = $this->MyQuery('SELECT CAID FROM '.TBL_CUSTOMER_ADDRESSES.' WHERE CACustomerID = '.$cid, true);
		if($data){
			return $data->CAID;
		}
	}
	public function CATID($cid){
		$data = $this->MyQuery('SELECT CATID FROM '.TBL_CUSTOMER_ACCOUNT_TYPE.' WHERE CATCustomerID = '.$cid, true);
		if($data){
			return $data->CATID;
		}
	}
	public function CTDID($cid){
		$data = $this->MyQuery('SELECT CTDID FROM '.TBL_CUSTOMER_TAX_DATA.' WHERE CTDCustomerID = '.$cid, true);
		if($data){
			return $data->CTDID;
		}
	}

	public function GetSalesAmt($cid){
		$SalesOBJ = new Sales();
		$Sales = $SalesOBJ->AllSales();
		$Amts = array();
		if($Sales){
			foreach ($Sales as $Each) {
				if($Each->SIsVoid != 1 && $Each->SIsPaid == 1){
					if($Each->SCustomerID == $cid){
						if($Each->SAmount){
							$Amts[] = $Each->SAmount;
						}
					}
				}
			}
		}
		if($Amts){
			return array_sum($Amts);
		}
	}

	public function GetPaidAmt($cid){
		$data = $this->MyQuery('SELECT * FROM '.TBL_SALES_PAYMENTS.' WHERE SPSalesID IN(SELECT SalesID FROM '.TBL_SALES.' WHERE SCustomerID = '.$cid.')');
		$Amts = array();
		if($data){
			foreach ($data as $val) {
				if($val->SPAmount){
					$Amts[] = $val->SPAmount;
				}
			}
		}
		return array_sum($Amts);
	}

	public function GetDueAmt($cid){
		$due = $this->GetSalesAmt($cid) - $this->GetPaidAmt($cid);
		if($due){
			return $due;
		}
	}


}