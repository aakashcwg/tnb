<?php

/*if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	die("<h2 align='center'>You are unable to access this page.</h2>");*/

//Claas ListType.php

Class ListType extends SDPDO {

	const TBL_ADDRESS_TYPES_FIELDS 				= array('ATName');

	const TBL_CONTACT_TYPES_FIELDS 				= array('CTName');

	const TBL_CUSTOMER_ACCOUNT_TYPE_LIST_FIELDS = array('CATLName');

	const TBL_EMPLOYEE_ROLES_FIELDS 			= array('ERRoleName');

	const TBL_PAYMENT_METHODS_FIELDS 			= array('PaymentMethodName', 'PMDescription');

	const TBL_PRICING_TYPES_FIELDS 				= array('PTName');

	const TBL_GENERAL_LEDGER_NUMBERS_FIELDS 	= array('GLNumber', 'GLDescription');


	/*=============================================================*/
					/*~~~~Address Types Start~~~~*/
	/*=============================================================*/

	//Insert new Address Type
	public function AddNewAddressType($tblAddressTypes_values){
		$tblAddressTypes_data = array_combine(self::TBL_ADDRESS_TYPES_FIELDS, $tblAddressTypes_values);
		$NewAddressTypeID = $this->InsertData(TBL_ADDRESS_TYPES, $tblAddressTypes_data);
		return $NewAddressTypeID;
	}

	//Get all Address Types
	public function GetAllAddressTypes(){
		return $this->GetAll(TBL_ADDRESS_TYPES);
	}

	//Get Address Type by ID
	public function GetTheAddressTypeByID($TypeID){
		$data = $this->MyQuery( 'SELECT ATName FROM '.TBL_ADDRESS_TYPES.' WHERE ATID = '.$TypeID, true );
		return $data->ATName;
	}

	//Update Address Type
	public function UpdateAddressType($values, $where){
		$update = $this->update(TBL_ADDRESS_TYPES, self::TBL_ADDRESS_TYPES_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
					/*~~~~Address Types End~~~~*/
	/*=============================================================*/

    /*=============================================================*/
    /*~~~~Tax Types Start~~~~*/
    /*=============================================================*/

    //Get all Address Types
    public function GetAllTaxTypes(){
        return $this->GetAll(TBL_TAX_RATES);
    }

    public function GetTheTaxRate($trid){
    	$data = $this->GetAll(TBL_TAX_RATES, array('TaxRateID' => $trid));
    	if($data){
    		return $data[0];
    	}
    }

    /*=============================================================*/
    /*~~~~Tax Types End~~~~*/
    /*=============================================================*/

    /*=============================================================*/
					/*~~~~Contact Types Start~~~~*/
	/*=============================================================*/

	//Insert new Contact Type
	public function AddNewContactType($tblContactTypes_values){
		$tblContactTypes_data = array_combine(self::TBL_CONTACT_TYPES_FIELDS, $tblContactTypes_values);
		$NewContactTypeID = $this->InsertData(TBL_CONTACT_TYPES, $tblContactTypes_data);
		return $NewContactTypeID;
	}

	//Get all Contact Types
	public function GetAllContactTypes(){
		return $this->GetAll(TBL_CONTACT_TYPES);
	}

	//Get Contact Type by ID
	public function GetTheContactTypeByID($TypeID){
		$data = $this->MyQuery( 'SELECT CTName FROM '.TBL_CONTACT_TYPES.' WHERE CTID = '.$TypeID, true );
		return $data->CTName;
	}

	//Update Contact Type
	public function UpdateContactType($values, $where){
		$update = $this->update(TBL_CONTACT_TYPES, self::TBL_CONTACT_TYPES_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
					/*~~~~Contact Types End~~~~*/
	/*=============================================================*/


	/*=============================================================*/
				/*~~~~Customer Account Types Start~~~~*/
	/*=============================================================*/

	//Insert new Customer Account Type
	public function AddNewCustomerAccountType($tblCustomerAccountTypesList_values){
		$tblCustomerAccountTypesList_data = array_combine(self::TBL_CUSTOMER_ACCOUNT_TYPE_LIST_FIELDS, $tblCustomerAccountTypesList_values);
		$NewCustomerAccountTypeID = $this->InsertData(TBL_CUSTOMER_ACCOUNT_TYPE_LIST, $tblCustomerAccountTypesList_data);
		return $NewCustomerAccountTypeID;
	}

	//Get all Customer Account Types
	public function GetAllCustomerAccountTypes(){
		return $this->GetAll(TBL_CUSTOMER_ACCOUNT_TYPE_LIST);
	}

	//Get Customer Account Type by ID
	public function GetTheCustomerAccountTypeByID($TypeID){
		$data = $this->MyQuery( 'SELECT CATLName FROM '.TBL_CUSTOMER_ACCOUNT_TYPE_LIST.' WHERE CATLID = '.$TypeID, true );
		return $data->CATLName;
	}

	//Update Customer Account Type
	public function UpdateCustomerAccountType($values, $where){
		$update = $this->update(TBL_CUSTOMER_ACCOUNT_TYPE_LIST, self::TBL_CUSTOMER_ACCOUNT_TYPE_LIST_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
				/*~~~~Customer Account Types End~~~~*/
	/*=============================================================*/


	/*=============================================================*/
					/*~~~~Employee Roles Start~~~~*/
	/*=============================================================*/

	//Insert new Employee Role
	public function AddNewEmployeeRole($tblEmployeeRoles_values){
		$tblEmployeeRoles_data = array_combine(self::TBL_EMPLOYEE_ROLES_FIELDS, $tblEmployeeRoles_values);
		$NewEmployeeRoleID = $this->InsertData(TBL_EMPLOYEE_ROLES, $tblEmployeeRoles_data);
		return $NewEmployeeRoleID;
	}

	//Get all Employee Roles
	public function GetAllEmployeeRoles(){
		return $this->GetAll(TBL_EMPLOYEE_ROLES);
	}

	//Get Employee Role by ID
	public function GetTheEmployeeRoleByID($RoleID){
		$data = $this->MyQuery( 'SELECT ERRoleName FROM '.TBL_EMPLOYEE_ROLES.' WHERE ERID = '.$RoleID, true );
		return $data->ERRoleName;
	}

	//Update Employee Role
	public function UpdateEmployeeRole($values, $where){
		$update = $this->update(TBL_EMPLOYEE_ROLES, self::TBL_EMPLOYEE_ROLES_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
					/*~~~~Employee Roles End~~~~*/
	/*=============================================================*/


	/*=============================================================*/
					/*~~~~Payment Methods Start~~~~*/
	/*=============================================================*/

	//Insert new Payment Method
	public function AddNewPaymentMethod($tblPaymentMethods_values){
		$tblPaymentMethods_data = array_combine(self::TBL_PAYMENT_METHODS_FIELDS, $tblPaymentMethods_values);
		$NewPaymentMethodID = $this->InsertData(TBL_PAYMENT_METHODS, $tblPaymentMethods_data);
		return $NewPaymentMethodID;
	}

	//Get all Payment Methods
	public function GetAllPaymentMethods(){
		return $this->GetAll(TBL_PAYMENT_METHODS);
	}

	//Get Payment Method by ID
	public function GetThePaymentMethodByID($PaymentMethodD){
		$data = $this->MyQuery( 'SELECT PaymentMethodName FROM '.TBL_PAYMENT_METHODS.' WHERE PMID = '.$PaymentMethodD, true );
		return $data->PaymentMethodName;
	}

	//Update Payment Method
	public function UpdatePaymentMethod($values, $where){
		$update = $this->update(TBL_PAYMENT_METHODS, self::TBL_PAYMENT_METHODS_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
					/*~~~~Payment Methods End~~~~*/
	/*=============================================================*/


	/*=============================================================*/
					/*~~~~Pricing Types Start~~~~*/
	/*=============================================================*/

	//Insert new Pricing Type
	public function AddNewPricingType($tblPricingTypes_values){
		$tblPricingTypes_data = array_combine(self::TBL_PRICING_TYPES_FIELDS, $tblPricingTypes_values);
		$NewPricingTypeID = $this->InsertData(TBL_PRICING_TYPES, $tblPricingTypes_data);
		return $NewPricingTypeID;
	}

	//Get all Pricing Types
	public function GetAllPricingTypes(){
		return $this->GetAll(TBL_PRICING_TYPES);
	}

	//Get Pricing Type by ID
	public function GetThePricingTypeByID($TypeID){
		$data = $this->MyQuery( 'SELECT PTName FROM '.TBL_PRICING_TYPES.' WHERE PTID = '.$TypeID, true );
		if($data){
			return $data->PTName;
		}
	}

	//Update Pricing Type
	public function UpdatePricingType($values, $where){
		$update = $this->update(TBL_PRICING_TYPES, self::TBL_PRICING_TYPES_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
					/*~~~~Pricing Types End~~~~*/
	/*=============================================================*/

	/*=============================================================*/
				/*~~~~General Ledger Numbers Satrt~~~~*/
	/*=============================================================*/

	//Insert new Ledger Number
	public function AddNewLedgerNumber($tblGeneralLedgerNumbers_values){
		$tblGeneralLedgerNumbers_data = array_combine(self::TBL_GENERAL_LEDGER_NUMBERS_FIELDS, $tblGeneralLedgerNumbers_values);
		$NewLedgerID = $this->InsertData(TBL_GENERAL_LEDGER_NUMBERS, $tblGeneralLedgerNumbers_data);
		return $NewLedgerID;
	}

	//Get all Ledger Numbers
	public function GetAllLedgerNumbers(){
		return $this->GetAll(TBL_GENERAL_LEDGER_NUMBERS);
	}

	//Get Ledger Number by ID
	public function GetTheLedgerNumberByID($LedgerID){
		$data = $this->MyQuery( 'SELECT GLNumber FROM '.TBL_GENERAL_LEDGER_NUMBERS.' WHERE GeneralLedgerID = '.$LedgerID, true );
		return $data->GLNumber;
	}

	//Update Ledger Number
	public function UpdateLedgerNumber($values, $where){
		$update = $this->update(TBL_GENERAL_LEDGER_NUMBERS, self::TBL_GENERAL_LEDGER_NUMBERS_FIELDS, $values, $where);
		return $update;
	}

	/*=============================================================*/
				/*~~~~General Ledger Numbers Satrt~~~~*/
	/*=============================================================*/


}