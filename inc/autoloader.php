<?php

if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
    die("<h2 align='center'>You are unable to access this page.</h2>");

//autoloader.php

class Autoloader
    {
        private $directory_name;
        const INC_PATH = BASE_PATH.'\inc\\';

        public function __construct($directory_name)
        {
            $this->directory_name = $directory_name;
        }

        public function autoload($class_name) 
        { 
            $file_name = strtolower($class_name).'.php';

            $file = self::INC_PATH.$this->directory_name.'/'.$file_name;

            if (file_exists($file) == false)
            {
                return false;
            }
            include ($file);
        }
    }

# instantiate the autoloader object
$classes = new Autoloader('classes');
# register the loader functions
spl_autoload_register(array($classes, 'autoload'));
