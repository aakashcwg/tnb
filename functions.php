<?php
//functions.php
date_default_timezone_set('US/Eastern');
include 'load.php';

// echo '<pre>';
// $d = date('Y-m-d H:i:s', strtotime($_SESSION['time_start_login']));
// print_r($SalesOBJ->CashoutedCash($d, null, 'COSystemValue')); die;
//print_r($SalesOBJ->IsVoid(10098)); die;
/*echo '<pre>'.$_SESSION['time_start_login'];
$d = date('Y-m-d H:i:s', strtotime($_SESSION['time_start_login']));
print_r($SalesOBJ->EmployeeSalesValue(currentUser('EmployeeID'))); die;*/


//Page Title
function PageTitle(){
	if(isset($_GET['fetch']) && !empty($_GET['fetch'])){
		echo '<h2>'.ucwords(str_replace('-', ' ', $_GET['fetch'])).'</h2>';
	}
	else if(isset($_GET['action']) && !empty($_GET['action'])){

		if($_GET['action'] == 'cash-out' && isset($_SESSION['drawer_id']) && !empty($_SESSION['drawer_id'])){
			$drawer = CashDrawerName($_SESSION['drawer_id']);
			echo '<h2>'.ucwords(str_replace('-', ' ', $_GET['action'])).' - '.$drawer.'</h2>';
		}else{
			echo '<h2>'.ucwords(str_replace('-', ' ', $_GET['action'])).'</h2>';
		}
	}
	else{
		if(isset($_GET['destination']) && !empty($_GET['destination'])){
			echo '<h2>'.ucwords(str_replace('-', ' ', $_GET['destination'])).'</h2>';
		}
	}
}

function Rf(){
	$rf = '';
	if(isset($_GET['action'])){
    	$rf = $_GET['action'];
    }else{
    	if(isset($_GET['destination'])){
    		$rf = $_GET['destination'];
    	}
    }
    return $rf;
}

//TNB Version
function TNBVersion(){
//	$m = date('m');
//	$d = date('d');
//	$y = substr( date('Y'), -2);
	echo 'v122818';
}

//Page content
function ThePage(){
	if( empty($_SERVER['QUERY_STRING']) ){
		require_once('dashboard.php');
	}else{
		if(isset($_GET['destination']) && !empty($_GET['destination'])){
			$destinationFile = isset($_GET['destination'])?$_GET['destination'].'.php':'';
			$sectionDir = BASE_PATH.'/pages/section/';
			$defautlDestination = 'default.php';
			if(file_exists($sectionDir.$destinationFile)){
				require_once($sectionDir.$destinationFile);
			}else{
				require_once($sectionDir.$defautlDestination);
			}
		}
	}
}

//Item URL
function ItemUrl($ItemID){
	$URL = SITE_URL.'?destination=inventory&action=item-details&item-id='.$ItemID;
	return $URL;
}

//Modals
function Modals(){
	$modalDir = BASE_PATH.'/modals/';
	$modals = scandir($modalDir);
	$commonFiles = array( '.', '..', 'index.php' );
	foreach (array_diff($modals, $commonFiles) as $modal){
		require_once ( $modalDir.$modal );
	}
}

//USA States
function getUSAStates(){
	$states = "Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming";

	$abbr = "AL|AK|AZ|AR|CA|CO|CT|DE|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY";
	$statesArray = explode('|', $states);
	$abbrArray = explode('|', $abbr);
	return array_combine($abbrArray, $statesArray);
}

//Vendor & Customer name format
function VC_Name($fname, $lname, $company){
	$defaultName = 'Anonymous';
	if($fname && $lname && $company){
		$name = $fname.' '.$lname.' ( '.$company.' ) ';
	}
	if($company && (!$fname || !$lname)){
		$name = $defaultName.' ( '.$company.' ) ';
	}
	if($fname && $lname && !$company){
		$name = $fname.' '.$lname;
	}
	return $name;
}

//Get page content
function getPage($path){
	if(ob_get_level() > 1){
		ob_end_flush();
	}
	ob_start();
    include('pages/'.$path.'.php');
    return ob_get_clean(); 
}

//Body Classes
function MainClasses(){
	$query_string = $_SERVER['QUERY_STRING'];
	if($query_string){
		$query_string_array = explode('&', $query_string);
		$class = '';
		foreach ($query_string_array as $eachString) {
			$eachStringArray = explode('=', $eachString);
			$class .= strtolower($eachStringArray[1]).' ';
		}
		return rtrim($class);
	}else{
		return 'dashboard';
	}
}

//Get Current User Data
function currentUser($field){
	global $SDPDO;
	if(isset($_SESSION) && !empty($_SESSION['systemuser'])){
		$data = $SDPDO->MyQuery(" SELECT * FROM (".TBL_EMPLOYEES." INNER JOIN ".TBL_EMPLOYEE_LOGIN." ON ".TBL_EMPLOYEES.".EmployeeID = ".TBL_EMPLOYEE_LOGIN.".ELEID) WHERE ".TBL_EMPLOYEE_LOGIN.".ELUsername = '".$_SESSION['systemuser']."'", true);
		if($data){
			return $data->$field;
		}
	}
}

//Get menus
function getMenus(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT * FROM '.TBL_SECTIONS.' ORDER BY SectionOrder ASC ');
	return $data;
}

//Get last section ID
function getLastSectionID(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT Max(SectionOrder) AS SO FROM '.TBL_SECTIONS, true);
	if($data){
		return $data->SO;
	}
}

//Get section name by ID
function getSection($SectionID){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT SectionName FROM '.TBL_SECTIONS.' WHERE SectionID = '.$SectionID, true);
	if($data){
		return $data->SectionName;		
	}
}


/*============================================*/
		/*Finctions Start - Employees*/
/*============================================*/

function getEployees($role='', $need=''){
	global $SDPDO;
	if(isset($_GET['role']) && !empty($_GET['role'])){
		$role = $_GET['role'];
	}
	if(!empty($role)){
		$query = " SELECT * FROM (".TBL_EMPLOYEES." INNER JOIN ".TBL_EMPLOYEE_LOGIN." ON ".TBL_EMPLOYEES.".EmployeeID = ".TBL_EMPLOYEE_LOGIN.".ELEID) WHERE ".TBL_EMPLOYEE_LOGIN.".ELRoleID IN (SELECT ERID FROM ".TBL_EMPLOYEE_ROLES." WHERE ".TBL_EMPLOYEE_ROLES.".ERRoleName = '".$role."') ";
	}elseif(empty($role) && $need == 'history'){
		$query = " SELECT * FROM (".TBL_EMPLOYEES." INNER JOIN ".TBL_EMPLOYEE_HISTORY." ON ".TBL_EMPLOYEES.".EmployeeID = ".TBL_EMPLOYEE_HISTORY.".EHEmployeeID) ORDER BY ".TBL_EMPLOYEES.".EmployeeID ASC ";
	}else{
		$query = " SELECT * FROM (".TBL_EMPLOYEES." RIGHT JOIN ".TBL_EMPLOYEE_LOGIN." ON ".TBL_EMPLOYEES.".EmployeeID = ".TBL_EMPLOYEE_LOGIN.".ELEID) ";
	}
	$data = $SDPDO->MyQuery($query);
	if($data){
		return $data;
	}
}


function getEployeeByID($employeeID){
	global $pdo;
	$query = " SELECT * FROM (".TBL_EMPLOYEES." INNER JOIN ".TBL_EMPLOYEE_LOGIN." ON ".TBL_EMPLOYEES.".EmployeeID = ".TBL_EMPLOYEE_LOGIN.".ELEID) WHERE ".TBL_EMPLOYEE_LOGIN.".ELEID = $employeeID AND ".TBL_EMPLOYEES.".EmployeeID = $employeeID ";
	$statement = $pdo->prepare($query);
	$statement->execute();
	$theEmployee[$employeeID] = $statement->fetch(PDO::FETCH_OBJ);
	return $theEmployee;
}


//getting all roles
function getRoles(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_EMPLOYEE_ROLES." ORDER BY ERID ASC ");
	return $data;
}
 
//getting role name by role id
function get_the_role($id){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT ERRoleName FROM ".TBL_EMPLOYEE_ROLES." WHERE ERID = ".$id, true);
	if($data){
		return $data->ERRoleName;
	}
}

//check user is logged in or not
function is_user_logged_in(){
	if(isset($_SESSION['systemuser']) && !empty($_SESSION['systemuser'])){
		return true;
	}
}
function is_admin(){
	$currentUserId = currentUser('ELRoleID');
	if(get_the_role($currentUserId) == 'Administrator'){
		return true;
	}
}
function is_manager(){
	$currentUserId = currentUser('ELRoleID');
	if(get_the_role($currentUserId) == 'Manager'){
		return true;
	}
}
function is_cashier(){
	$currentUserId = currentUser('ELRoleID');
	if(get_the_role($currentUserId) == 'Cashier'){
		return true;
	}
}
function is_worker(){
	$currentUserId = currentUser('ELRoleID');
	if(get_the_role($currentUserId) == 'Worker'){
		return true;
	}
}


/*============================================*/
		/*Finctions End - Employees*/
/*============================================*/


/*============================================*/
		/*Finctions Start - Inventory*/
/*============================================*/


function getInventoryByID($envID){
	global $pdo;
	$query = " SELECT * FROM tblInventory WHERE tblInventory.INVID = $envID ";
	$statement = $pdo->prepare($query);
	$statement->execute();
	$theInventory[$envID] = $statement->fetch(PDO::FETCH_OBJ);
	return $theInventory;
}

function getAllInventoryIds(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT INVID AS id FROM ".TBL_INVENTORY." ORDER BY id DESC ");
	$ids = array();
	foreach ($data as $value) {
		$ids[] = $value->id;
	}
	return $ids;
}


function getItemWithNumber($id){
	global $InventoryOBJ;
	$data = $InventoryOBJ->GetTheInventoryByID($id);
	if($data){
		$formatedItem = $data[0]->INVItemName.' ('.$data[0]->INVItemNumber.') ';
		return $formatedItem;
	}
	
}


function getInventories(){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_INVENTORY);
	return $data;
}

function InventoriesByParts($No){
	$inventories = getInventories();
	$NewInventoryArray = array_chunk($inventories, $No);
	return $NewInventoryArray;
}

function getInvMaxPage($No){
	$maxPage = count(InventoriesByParts($No));
	return $maxPage;
}


//echo '<pre>';
//print_r(getInventories()); die;
//print_r(InventoriesByParts(9)); die;


function FetchInventoriesByPage($Page=1, $No=10){
	$Invs = InventoriesByParts($No)[$Page];

	if($Invs){
		return $Invs;
	}else{
		return false;
	}
}


//print_r(FetchInventoriesByPage(2, 10)); die;


function getInvById($invID){
	global $pdo;
	$sql = ' SELECT * FROM tblInventory WHERE INVID = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$invID]);
	return $stmt->fetch(PDO::FETCH_OBJ);
}

function getInvByItemNo($ItemNo){
	global $pdo;
	$sql = ' SELECT * FROM tblInventory WHERE INVItemNumber = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$ItemNo]);
	return $stmt->fetch(PDO::FETCH_OBJ);
}

function invNum($invID){
	global $pdo;
	$sql = ' SELECT INVItemNumber FROM dbo.tblInventory WHERE INVID = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$invID]);
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	return $data->INVItemNumber;
}


function isItemNoExist($ItemNo){
	$OBJ = getInvByItemNo($ItemNo);
	if(!empty($OBJ))
		return true;
	else
		return false;
}


function getOrders(){
	global $pdo;
	$sql = ' SELECT * FROM tblInventoryOrders ORDER BY IOID DESC ';
	$statement = $pdo->prepare($sql);
	$statement->execute();
	$data = $statement->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function getOrderDetails($orderID){
	global $pdo;
	$sql = ' SELECT * FROM dbo.tblInventoryOrderDetails b WHERE b.IODIOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function existOrder($orderID){
	global $pdo;
	$sql = ' SELECT COUNT(IOID) AS num FROM dbo.tblInventoryOrders b WHERE b.IOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	if($data->num == 0)
		return false;
	else
		return true;
}

function getOrder($orderID){
	global $pdo;
	$sql = ' SELECT * FROM dbo.tblInventoryOrders WHERE IOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	return $data;
}

function theOrderTotalCost($orderID){
	global $pdo;
	$sql = ' SELECT SUM(IODCost*IODQtyPerCase) AS totalCost FROM dbo.tblInventoryOrderDetails a WHERE a.IODIOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	return number_format($data->totalCost, 2);
}

function theOrderTotalSalesTax($orderID){
	global $pdo;
	$sql = ' SELECT SUM(IODSalesTax) AS totalSTax FROM dbo.tblInventoryOrderDetails a WHERE a.IODIOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	return number_format($data->totalSTax, 2);
}

function theOrderTotalShippingCost($orderID){
	global $pdo;
	$sql = ' SELECT SUM(IODShippingCost) AS totalSCost FROM dbo.tblInventoryOrderDetails a WHERE a.IODIOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	return number_format($data->totalSCost, 2);
}

function GetOrderItemPaidAmount($orderDetailsID){
	global $pdo;
	$sql = ' SELECT IODPaidAmount FROM dbo.tblInventoryOrderDetails a WHERE a.IODID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderDetailsID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	return number_format($data->IODPaidAmount, 2);
}

function order_status($orderID){
	global $pdo;
	$sql = ' SELECT IODDateShipped AS shippingDate FROM dbo.tblInventoryOrderDetails a WHERE a.IODIOID = ? ';
	$statement = $pdo->prepare($sql);
	$statement->execute([$orderID]);
	$data = $statement->fetchAll(PDO::FETCH_OBJ);
	$shippingDateArray = array();
	foreach ($data as $val) {
		if(!empty($val->shippingDate)){
			array_push($shippingDateArray, $val->shippingDate);
		}
	}
	if(empty($shippingDateArray)){
		$status = 'Pending';
	}else{
		$status = 'Completed';
	}
	return $status;
}


function get_pending_orders(){
	foreach (getOrders() as $order) {
		if(order_status($order->IOID) == 'Pending'){
			$data[] = $order;
		}
	}
	return $data;
}

function get_received_orders(){
	foreach (getOrders() as $order) {
		if(order_status($order->IOID) == 'Completed'){
			$data[] = $order;
		}
	}
	return $data;
}

function profileField($field, $text=""){
	if($text){
		$default_text = $text;
	}else{
		$default_text = '<span class="empty-field">Not Given</span>';
	}
	if($field){
		$val = $field;
	}else{
		$val = $default_text;
	}
	return $val;
}


function getWareHouses(){
	global $SDPDO;
	$data = $SDPDO->MyQuery('SELECT * FROM '.TBL_WAREHOUSES);
	if($data){
		return $data;
	}
}

function getWareHouse($WH_ID){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_WAREHOUSES, array('WarehouseID' => $WH_ID));
	if($data){
		return $data[0];
	}
}

function getWareHouseName($WH_ID){
	global $SDPDO;
	$data = $SDPDO->MyQuery('SELECT WName FROM '.TBL_WAREHOUSES.' WHERE WarehouseID = '.$WH_ID, true);
	if($data){
		return $data->WName;
	}
}

function getShelves(){
	global $pdo;
	$sql = ' SELECT * FROM tblShelves ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	if($data){
		return $data;
	}

}

function getShelf($Shelf_ID){
	global $pdo;
	$sql = ' SELECT * FROM tblShelves WHERE ShelfID = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$Shelf_ID]);
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	if($data){
		return $data;
	}
}

function getShelfName($Shelf_ID){
	global $pdo;
	$sql = ' SELECT ShelfName FROM tblShelves WHERE ShelfID = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$Shelf_ID]);
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	if($data){
		return $data->ShelfName;
	}
}

function getOnHandItems(){
	global $pdo;
	$sql = ' SELECT * FROM ( tblInventoryOnHand INNER JOIN tblInventoryOnHandPricing ON tblInventoryOnHand.IOHID = tblInventoryOnHandPricing.IOHPID ) ORDER BY tblInventoryOnHand.IOHID DESC ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function getItemPrice($InvID){
	global $pdo;
	$sql = ' SELECT tblInventoryOnHandPricing.IOHPPrice AS ItemPrice FROM tblInventoryOnHandPricing WHERE tblInventoryOnHandPricing.IOHPIOHID IN ( SELECT tblInventoryOnHand.IOHID FROM tblInventoryOnHand WHERE tblInventoryOnHand.IOHInventoryID = ? ) ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([ $InvID ]);
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	return $data->ItemPrice;
}


function GetOnHandID($InventoryID){
	global $pdo;
	$query = ' SELECT IOHID AS OnHandId FROM tblInventoryOnHand WHERE IOHInventoryID = ? ';
	$statement = $pdo->prepare($query);
	$statement->execute([$InventoryID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	if($data){
		return $data->OnHandId;
	}
}


function GetOnHandQty($OnHandID){
	global $pdo;
	$sql = ' SELECT IOHQuantity FROM tblInventoryOnHand WHERE IOHID = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([ $OnHandID ]);
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	if($data){
		return $data->IOHQuantity;
	}
}

//echo GetOnHandQty(2); die;

function getPriceLists(){
	global $SDPDO;
	$data = $SDPDO->GetAll( TBL_PRICE_LISTS );
	if($data){
		return $data;
	}
}

function getPriceList($plid){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_PRICE_LISTS, array('PLID' => $plid));
	if($data){
		return $data[0];
	}
}

function PriceListName($plid){
	$PL = getPriceList($plid);
	if($PL){
		return $PL->PLName;
	}else{
		return null;
	}
}

function GetPriceListItems(){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_PRICE_LIST_ITEMS);
	return $data;
}

function GetPriceListItemsByPLID($plid){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_PRICE_LIST_ITEMS, array('PLIPLID' => $plid));
	if($data){
		return $data;
	}
}

function GetPriceListItem($pliid){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_PRICE_LIST_ITEMS, array('PLIID' => $pliid));
	if($data){
		return $data[0];
	}
}

function GetPriceListItemByPLIDAndItemID($plid, $itemID){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT * FROM '.TBL_PRICE_LIST_ITEMS.' WHERE PLIPLID = '.$plid.' AND PLIINVID = '.$itemID, true);
	if($data){
		return $data;
	}
}

function GetPLIAmountByPLIDAndItemID($plid, $itemID){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT * FROM '.TBL_PRICE_LIST_ITEMS.' WHERE PLIPLID = '.$plid.' AND PLIINVID = '.$itemID, true);
	if($data){
		return $data->PLIAmount;
	}
}

function GetPriceListColor($plid){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_PRICE_LISTS, array('PLID' => $plid));
	if($data){
		return $data[0]->PLColor;
	}
}

/*echo '<pre>';
print_r(GetPriceListItem(2)); die;*/


function GetCustomerPL($CustomerID){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT CustomerPLID AS PL FROM '.TBL_CUSTOMERS.' WHERE CustomerID = '.$CustomerID, true);
	if($data){
		return $data->PL;
	}
}

function PLItem($pliid){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT PLIINVID AS Item FROM '.TBL_PRICE_LIST_ITEMS.' WHERE PLIID = '.$pliid, true);
	if($data){
		return $data->Item;
	}
}

function PLPrice($pliid){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT PLIAmount AS PLA FROM '.TBL_PRICE_LIST_ITEMS.' WHERE PLID = '.$pliid, true);
	if($data){
		return $data->PLA;
	}
}

function ItemSPForCustomer($Item, $plid){
	global $SDPDO;
	$data = $SDPDO->MyQuery(' SELECT PLAmount AS PLA FROM '.TBL_PRICE_LISTS.' WHERE PLID = '.$plid.' AND PLINVID ='.$Item, true);
	if($data){
		return $data->PLA;
	}
}

/*print_r(GetCustomerSP(1037)); 
print_r(PLItem(1008));
print_r(PLPrice(1008));
die;*/


/*============================================*/
		/*Finctions End - Inventory*/
/*============================================*/


/*===============================================================*/
		/*Finctions Start - Address Type and Contact Type*/
/*===============================================================*/


//Get All Address Types
function getAddressTypes(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_ADDRESS_TYPES);
	if($data){
		return $data;
	}
}
//Get All Contact Types
function getContactTypes(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_CONTACT_TYPES);
	if($data){
		return $data;
	}
}
//Get Address Type Name By ID
function getAddressTypeName($typeID){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT ATName FROM ".TBL_ADDRESS_TYPES." WHERE ATID = ".$typeID, true);
	if($data){
		return $data->ATName;
	}
}
//Get Contact Type Name By ID
function getContactTypeName($typeID){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT CTName FROM ".TBL_CONTACT_TYPES." WHERE CTID = ".$typeID, true);
	if($data){
		return $data->CTName;
	}
}
//Get All Customer Account Types
function getCustomerTypes(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_CUSTOMER_ACCOUNT_TYPE_LIST);
	if($data){
		return $data;
	}
}
//Get Cuetomer Account Type Name By ID
function getCuetomerType($id){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT CATLName FROM ".TBL_CUSTOMER_ACCOUNT_TYPE_LIST." WHERE CATLID = ".$id, true);
	if($data){
		return $data->CATLName;
	}
}
//Get All Pricing Types
function getPricingTypes(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_PRICING_TYPES);
	if($data){
		return $data;
	}
}
//Get Pricing Type Name By ID
function getPricingType($id){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT PTName FROM ".TBL_PRICING_TYPES." WHERE PTID = ".$id, true);
	if($data){
		return $data->PTName;
	}
}
//Get All Payment Types
function getPaymentTypes(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_PAYMENT_METHODS);
	if($data){
		return $data;
	}
}
//Get Payment Type Name By ID
function getPaymentTypeName($id){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT PaymentMethodName FROM ".TBL_PAYMENT_METHODS." WHERE PMID = ".$id, true);
	if($data){
		return $data->PaymentMethodName;
	}
}
//Get Payment Type Desc By ID
function getPaymentTypeDesc($id){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT PMDescription FROM ".TBL_PAYMENT_METHODS." WHERE PMID = ".$id, true);
	if($data){
		return $data->PMDescription;
	}
}

/*===============================================================*/
		/*Finctions End - Address Type and Contact Type*/
/*===============================================================*/


/*============================================*/
		/*Finctions Start - Vendors*/
/*============================================*/

function getVendors(){
	global $pdo;
	$sql = " SELECT * FROM tblVendors INNER JOIN tblVendorContact ON tblVendors.VendorID=tblVendorContact.VCIDVendorID INNER JOIN tblVendorAddresses ON tblVendorContact.VCIDVendorID=tblVendorAddresses.VAVendorID ";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function getVendorByID($venID){
	global $pdo;
	$sql = " SELECT * FROM tblVendors INNER JOIN tblVendorContact ON tblVendors.VendorID=tblVendorContact.VCIDVendorID INNER JOIN tblVendorAddresses ON tblVendorContact.VCIDVendorID=tblVendorAddresses.VAVendorID WHERE tblVendors.VendorID=$venID ";
	$statement = $pdo->prepare($sql);
	$statement->execute();
	$theVendor[$venID] = $statement->fetch(PDO::FETCH_OBJ);
	return $theVendor;
}


/*============================================*/
		/*Finctions End - Vendors*/
/*============================================*/


/*================================================*/
		/*Finctions Start - Ledger Numbers*/
/*================================================*/

//Get All Ledger Numbers
function getLedgerNumbers(){
	global $SDPDO;
	$data = $SDPDO->MyQuery(" SELECT * FROM ".TBL_GENERAL_LEDGER_NUMBERS);
	if($data){
		return $data;
	}
}


function getLedgerNumberID($num){
	global $pdo;
	$sql = " SELECT GeneralLedgerID FROM dbo.tblGeneralLedgerNumbers WHERE GLNumber = $num ";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	return $data->GeneralLedgerID;
}

function getLedgerNum($id){
	global $pdo;
	$sql = " SELECT GLNumber FROM dbo.tblGeneralLedgerNumbers WHERE GeneralLedgerID = $id ";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	return $data->GLNumber;
}

function getLedgerNumDesc($id){
	global $pdo;
	$sql = " SELECT GLDescription FROM dbo.tblGeneralLedgerNumbers WHERE GeneralLedgerID = $id ";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetch(PDO::FETCH_OBJ);
	return $data->GLDescription;
}


/*================================================*/
		/*Finctions End - Ledger Numbers*/
/*================================================*/


/*================================================*/
		/*Finctions Start - Customers*/
/*================================================*/

function getCustomer($company=''){
	global $pdo;
	$sql = ' SELECT * FROM tblCustomers WHERE tblCustomers.CustomerCompanyName = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$company]);
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $data;
}


function getCustomers($limit=''){
	global $pdo;
	if(empty($limit))
		$sql = ' SELECT * FROM tblCustomers INNER JOIN tblCustomerContact ON tblCustomers.CustomerID=tblCustomerContact.CCIDCustomerID INNER JOIN tblCustomerAddresses ON tblCustomerContact.CCIDCustomerID=tblCustomerAddresses.CACustomerID ';
	else
		$sql = ' SELECT TOP 10 * FROM tblCustomers INNER JOIN tblCustomerContact ON tblCustomers.CustomerID=tblCustomerContact.CCIDCustomerID INNER JOIN tblCustomerAddresses ON tblCustomerContact.CCIDCustomerID=tblCustomerAddresses.CACustomerID ORDER BY CustomerID DESC ';
	
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function getCustomerMinId(){
	global $SDPDO;
	$sql = ' SELECT MIN(CustomerID) AS anonymas FROM '.TBL_CUSTOMERS;
	$data = $SDPDO->MyQuery($sql, true);
	if($data){
		return $data->anonymas;
	}
}


function getCustomerByID($customerID){
	global $pdo;
	$sql = ' SELECT * FROM tblCustomers INNER JOIN tblCustomerContact ON tblCustomers.CustomerID=tblCustomerContact.CCIDCustomerID INNER JOIN tblCustomerAddresses ON tblCustomerContact.CCIDCustomerID=tblCustomerAddresses.CACustomerID WHERE tblCustomers.CustomerID=? ';

	$statement = $pdo->prepare($sql);
	$statement->execute([$customerID]);
	$theCustomer[$customerID] = $statement->fetch(PDO::FETCH_OBJ);
	return $theCustomer;
}

function GetTheCustomer($CustomerID){
	$theCustomer = getCustomerByID($CustomerID)[$CustomerID];
	if($theCustomer){
		return VC_Name($theCustomer->CustomerFName, $theCustomer->CustomerLName, $theCustomer->CustomerCompanyName);
	}
}


//Check - whether a customer is tax extempt or not
function IsCustomerTaxExempt($CustomerID){
	global $pdo;
	$query = ' SELECT CustomerIsTaxExempt AS CITE FROM tblCustomers WHERE CustomerID = ? ';
	$statement = $pdo->prepare($query);
	$statement->execute([$CustomerID]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	if($data->CITE > 0){
		return true;
	}else{
		return false;
	}
}


//Get customer last sales id
if(!function_exists('LastSalesID')){
	function LastSalesID($CustomerID){
		global $pdo;
		$query = ' SELECT SalesID FROM tblSales WHERE SCustomerID = ? ORDER BY SalesID DESC ';
		$statement = $pdo->prepare($query);
		$statement->execute([$CustomerID]);
		$data = $statement->fetch(PDO::FETCH_OBJ);
		if($data){
			return $data->SalesID;
		}else{
			return null;
		}
	}
}




/*================================================*/
		/*Finctions End - Customers*/
/*================================================*/


function getSalesTax(){
	global $pdo;
	$taxRateName = 'Sales Tax';
	$sql = ' SELECT TaxRatePercentage AS TAX FROM tblTaxRates WHERE TaxRateName = ? ';

	$statement = $pdo->prepare($sql);
	$statement->execute([ $taxRateName ]);
	$data = $statement->fetch(PDO::FETCH_OBJ);
	return $data->TAX;
}


function getInvoices(){
	global $pdo;
	$sql = ' SELECT * FROM tblSales ORDER BY SalesID DESC ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function getSalesField($SalesID, $field){
	global $pdo;
	$sql = ' SELECT * FROM tblSales WHERE SalesID = ? ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$SalesID]);
	$data = $stmt->fetch(PDO::FETCH_ASSOC);
	return $data[$field];
}

function getSalesDetails($SalesID){
	global $pdo;
	$sql = ' SELECT * FROM ( tblSales INNER JOIN tblSalesDetails ON tblSales.SalesID = tblSalesDetails.SDSalesID ) WHERE tblSales.SalesID = ? ORDER BY tblSales.SalesID ASC ';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$SalesID]);
	$data = $stmt->fetchAll(PDO::FETCH_OBJ);
	return $data;
}

function GetExtCost($qty, $cost, $disc=0, $tax){
	$disc = $disc ?? 0;
	if(strpos($disc, '%') !== false){
		$EstVal = (intval($qty)*floatval($cost))*(1-(floatval(str_replace('%', '', $disc)) / 100)) + intval($qty)*floatval($tax);
	}elseif(strpos($disc, '$') !== false){
		$EstVal = intval($qty)*floatval($cost) - floatval(str_replace('$', '', $disc)) + intval($qty)*floatval($tax);
	}else{
		$EstVal = intval($qty)*floatval($cost) - floatval($disc) + intval($qty)*floatval($tax);
	}
	return $EstVal;
}

//AVD start - 12/30/18
function GetTaxTotal($qty, $tax){

    $EstVal = intval($qty)*floatval($tax);
    return $EstVal;
}
//AVD start - 12/30/18


function SalesPMDetails($SalesID){
	global $SalesOBJ, $ListTypeOBJ;
	$PMs = $SalesOBJ->PMs($SalesID);
	if($PMs){
		$html = '';
		foreach ($PMs as $PM) {
			$PMName = $ListTypeOBJ->GetThePaymentMethodByID($PM->SPPaymentMethodID);
			if($PMName == 'Cash' || $PMName == 'cash'){
				$html .= '<div>';
					$html .= '<label>Payment Method : '.$PMName.'</label><br>';
					$html .= '<label>Amount Paid : '.number_format($PM->SPAmount, 2).'</label>';
				$html .= '</div><br>';
			}
			if($PMName == 'Check' || $PMName == 'check'){
				$html .= '<div>';
					$html .= '<label>Payment Method : '.$PMName.'</label><br>';
					$html .= '<label>Check Number : '.$PM->SPCheckNumber.'</label><br>';
					$html .= '<label>Amount Paid : '.number_format($PM->SPAmount, 2).'</label>';
				$html .= '</div><br>';
			}
			if($PMName == 'Visa' || $PMName == 'Mastercard' || $PMName == 'Discover' || $PMName == 'American Express'){
				$html .= '<div>';
					$html .= '<label>Payment Method : '.$PMName.'</label><br>';
					$html .= '<label>Last 5 Digits : '.$PM->SPLast5Digits.'</label><br>';
					$html .= '<label>Authorization Code : '.$PM->SPAuthCode.'</label><br>';
					$html .= '<label>Amount Paid : '.number_format($PM->SPAmount, 2).'</label>';
				$html .= '</div><br>';
			}
			if($PMName == 'House Account'){
				$html .= '<div>';
					$html .= '<label>Payment Method : '.$PMName.'</label><br>';
					$html .= '<label>Amount Paid : $0.00</label><br>';
				$html .= '</div><br>';
			}
		}
		return $html;
	}
}

function LPMD($SalesID){
	global $SalesOBJ, $ListTypeOBJ;
	$PMs = $SalesOBJ->PMs($SalesID);
	if($PMs){
		$html = '';
		$html .= '<div id="lpmd-block">';
		$html .= '<h2>Payment already accepted on this sale :</h2>';
		foreach ($PMs as $PM) {
			$PMName = $ListTypeOBJ->GetThePaymentMethodByID($PM->SPPaymentMethodID);
			if($PMName == 'Cash' || $PMName == 'cash'){
				$html .= '<div class="cash block">';
					$html .= '<label class="lt-panel">Payment Method : '.$PMName.'</label>';
					$html .= '<label class="rt-panel">Amount Paid : '.number_format($PM->SPAmount, 2).'</label>';
				$html .= '<div class="clear"></div></div>';
			}
			if($PMName == 'Check' || $PMName == 'check'){
				$html .= '<div class="check block">';
					$html .= '<label class="lt-panel">'.$PMName.'</label>';
					$html .= '<label class="md-panel">#'.$PM->SPCheckNumber.'</label>';
					$html .= '<label class="rt-panel">$'.number_format($PM->SPAmount, 2).'</label>';
				$html .= '<div class="clear"></div></div>';
			}
			if($PMName == 'Visa' || $PMName == 'Mastercard' || $PMName == 'Discover' || $PMName == 'American Express'){
				$html .= '<div class="card block">';
					$html .= '<label class="lt-panel">'.$PMName.'</label>';
					$html .= '<label class="md-panel"></label>';
					$html .= '<label class="rt-panel">$'.number_format($PM->SPAmount, 2).'</label>';
				$html .= '<div class="clear"></div></div>';
			}
			if($PMName == 'House Account'){
				$html .= '<div class="house block">';
					$html .= '<label class="lt-panel">Payment Method : '.$PMName.'</label>';
					$html .= '<label class="rt-panel">Amount Paid : $0.00</label><br>';
				$html .= '<div class="clear"></div></div>';
			}
		}
		$html .= '</div>';
		return $html;
	}
}


// Sales Receipt - Heading
function SalesReceiptHeaderPart(){
	$html = '';
	$html .= '<table style=" width:100%; ">';
        $html .= '<tbody>';
            $html .= '<tr>';
                $html .= '<td style=" width:80%; padding:30px 0 0 315px; text-align:center;">';
                   $html .= '<h2 style=" font-familArialfont-size:25px; color:#000; line-height:27px; font-weight:700; margin:0; padding:0; ">Tennessee Battery Sales. LLC</h2>';
                    $html .= '<p style=" font-family:Arial; font-size:16px; color:#000; line-height:20px; font-weight:400; margin:0; padding:15px 0 5px; ">1942 Hwy 46s</br> Dickson, TN 37055</p>';
                    $html .= '<a href="tel:(615) 446-0315" style=" font-family:Arial; font-size:16px; color:#000; line-height:20px; font-weight:600; text-decoration:none; margin:0; padding:0 10px; ">(615) 446-0315</a>';
                    $html .= '<a href="tel:(615) 446-2861" style=" font-family:Arial; font-size:16px; color:#000; line-height:20px; font-weight:600; text-decoration:none; margin:0; padding:0 10px; ">(615) 446-2861</a>'; 
                $html .= '</td>';
                $html .= '<td style=" vertical-align:top; ">';
                    $html .= '<h6 style=" font-family:Arial; font-size:11px; color:#000; line-height:13px; font-weight:700; text-align:right; margin:0; padding:0; ">Print Date: '.date('M d, Y h:i:sa').'</h6>';
                    //$html .= '<h6 style=" font-family:Arial; font-size:11px; color:#000; line-height:13px; font-weight:700; text-align:right; margin:0; padding:0; ">1 of 1</h6>';
                $html .= '</td>';
            $html .= '</tr>';
        $html .= '</tbody>';
    $html .= '</table>';

    return $html;
}

// Sales Receipt - Address
function SaleReceiptAddressPart($SalesID){
	global $SalesOBJ, $CustomerOBJ;
	$SIsVoid = $SalesOBJ->GetSalesInfo($SalesID, 'SIsVoid');
	$Invoice = $SalesOBJ->GetSalesInfo($SalesID, 'SalesInvoiceNumber');
	$SalesDate = $SalesOBJ->GetSalesInfo($SalesID, 'SalesDate');
	$CustomerID = $SalesOBJ->GetSalesInfo($SalesID, 'SCustomerID');
	$SC = $CustomerOBJ->GetTheCustomerByID($CustomerID);

	$address = '';
	if($SC->CAAddress1){
		$address .= $SC->CAAddress1.', ';
	}
	if($SC->CAAddress2){
		$address .= $SC->CAAddress2;
	}
	if($SC->CACity){
		$address .= '</br> City : '.$SC->CACity;
	}
	if($SC->CAState){
		$address .= '</br> State : '.getUSAStates()[$SC->CAState];
	}
	if($SC->CAZip){
		$address .= '</br> ZIP : '.$SC->CAZip;
	}

	$html = '';
	$html .= '<table style=" width:100%; ">';
        $html .= '<tbody>';
            $html .= '<tr>';
                $html .= '<td style=" width:45%; padding:50px 0 0; ">';
                    $html .= '<h4 style=" font-family:Arial; font-size:20px; color:#000; line-height:22px; font-weight:600; text-align:left; margin:0; padding:0 0 5px; ">Ship To:</h4>';
                    $html .= '<p style=" font-family:Arial; font-size:15px; color:#000; line-height:20px; font-weight:400; text-align:left; margin:0; padding:0; ">'.VC_Name($SC->CustomerFName, $SC->CustomerLName, $SC->CustomerCompanyName).'</br> '.$address.'</p>';

                    $html .= '<a style=" float:left; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:600; text-decoration:none; margin:10px 0 0 0; padding:0; ">'.$SC->CCData.'</a>';

                $html .= '</td>';
                $html .= '<td style=" padding:50px 0 0; ">';
                    $html .= '<h4 style=" font-family:Arial; font-size:20px; color:#000; line-height:22px; font-weight:600; text-align:left; margin:0; padding:0 0 5px; ">Bill To:</h4>';
                    $html .= '<p style=" font-family:Arial; font-size:15px; color:#000; line-height:20px; font-weight:400; text-align:left; margin:0; padding:0; ">'.VC_Name($SC->CustomerFName, $SC->CustomerLName, $SC->CustomerCompanyName).'</br> '.$address.'</p>';
                    $html .= '<a style=" float:left; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:600; text-decoration:none; margin:10px 0 0 0; padding:0; ">'.$SC->CCData.'</a>';
                $html .= '</td>';
                $html .= '<td style=" vertical-align:top; ">';

                    $html .= '<h2 style=" font-family:Arial; font-size:20px; color:#000; line-height:22px; font-weight:600; text-align:right; margin:0; padding:0 0 12px; ">INVOICE: '.$Invoice.'</h2>';
                    $html .= '<h3 style=" font-family:Arial; font-size:18px; color:#000; line-height:20px; font-weight:400; text-align:right; margin:0; padding:0; ">Date: '.date('M d, Y h:i:sa', strtotime($SalesDate)).'</h3>';
                $html .= '</td>';
            $html .= '</tr>';
        $html .= '</tbody>';
    $html .= '</table>';

    return $html;
}

// Sales Receipt - Customer,PO
function SalesReceiptCustomer($SalesID){
	global $SalesOBJ, $CustomerOBJ;
	$SPONumber = $SalesOBJ->GetSalesInfo($SalesID, 'SPONumber');
	$CustomerID = $SalesOBJ->GetSalesInfo($SalesID, 'SCustomerID');
	$SC = $CustomerOBJ->GetTheCustomerByID($CustomerID);
	$Customer = VC_Name($SC->CustomerFName, $SC->CustomerLName, $SC->CustomerCompanyName);
	$html = '';
	$html .= '<tr>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; "></th>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; text-align:center; margin:0; padding:0; ">PO#</th>';
        $html .= '<th style=" width:20%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; "></th>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; "></th>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; text-align:center; margin:0; padding:0; ">SALES PERSON</th>';
        $html .= '<th style=" width:16%; height:30px; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; text-align:center; margin:0; padding:0; ">SHIP VIA</th>';
    $html .= '</tr>';
    $html .= '<tr>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:center; margin:0; padding:0; "></th>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; ">'.$SPONumber.'</th>';
        $html .= '<th style=" width:20%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; "></th>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; "></th>';
        $html .= '<th style=" width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:center; margin:0; padding:0; ">'.$Customer.'</th>';
        $html .= '<th style=" width:16%; height:30px; border-bottom:1px solid #000; "></th>';
    $html .= '</tr>';

    return $html;
}

// Sales Receipt - Items Table
function SalesReceiptItemsPart($SalesID){
	global $SalesOBJ, $InventoryOBJ;
	$SRIs = $SalesOBJ->GetTheItemsBySalesID($SalesID);
    $STax = $SalesOBJ->GetSalesInfo($SalesID, 'STax');      //AVD 12/27/18 2020
    $html = '';
	$html .= '<table cellspacing="0" style="width:100%;">';
	$html .= '<thead>';
        $html .= '<tr>';
            $html .= '<th style="width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; margin:0; padding:0;">ITEM#</th>';
            $html .= '<th style="width:50%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; margin:0; padding:0;">DESCRIPTION</th>';
            $html .= '<th style="width:10%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; margin:0; padding:0;">QTY</th>';
            $html .= '<th style="width:12%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; margin:0; padding:0;">UNIT PRICE</th>';
            $html .= '<th style="width:12%; height:30px; border-bottom:1px solid #000; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:700; margin:0; padding:0;">AMOUNT</th>';
        $html .= '</tr>';
    $html .= '</thead>';
	$html .= '<tbody>';

	if($SRIs){
		foreach ($SRIs as $SRI) {
			$INV_NAME = '';
			$INV_DESC = '';
			if($SRI->SDIOHID){
				$ONHAND_OBJ = $InventoryOBJ->GetOnHandItemByID($SRI->SDIOHID);
				if($ONHAND_OBJ){
					$ONHAND = $ONHAND_OBJ[0];
					$INV_OBJ = $InventoryOBJ->GetTheInventoryByID($ONHAND->IOHInventoryID);
					if($INV_OBJ){
						$INV = $INV_OBJ[0];
						$INV_NAME = getItemWithNumber($INV->INVID);
						$INV_DESC = $INV->INVDescription;
					}
				}
			}else{
				$NON_INV_OBJ = $InventoryOBJ->GetTheNonInventoryItem($SRI->SDNonInvID);
				if($NON_INV_OBJ){
					$INV_NAME = $NON_INV_OBJ->NonInvName;
					$INV_DESC = $NON_INV_OBJ->NonInvDescription;
				}
			}
			
			$AMT = $SRI->SDQuantity*$SRI->SDPrice;
			$html .= '<tr>';
	            $html .= '<td style="width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:left; margin:0; padding:0 12px 0 12px;">'.$INV_NAME.'</td>';
	            $html .= '<td style="width:50%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:left; margin:0; padding:0 12px 0 12px;">'.$INV_DESC.'</td>';
	            $html .= '<td style="width:10%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; margin:0; padding:0; text-align:center;">'.$SRI->SDQuantity.'</td>';
	            $html .= '<td style="width:12%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:right; margin:0; padding:0 15px 0 0;">'.number_format($SRI->SDPrice, 2).'</td>';
	            $html .= '<td style="width:12%; height:30px; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:right; margin:0; padding:0 24px 0 0;">'.number_format($AMT, 2).'</td>';
	        $html .= '</tr>';
		}
	}
		$html .= '<tr>';
            $html .= '<td style="width:16%; height:50px; border-right:1px solid #000; border-bottom:1px solid #000;"></td>';
            $html .= '<td style="width:50%; height:50px; border-right:1px solid #000; border-bottom:1px solid #000;"></td>';
            $html .= '<td style="width:10%; height:50px; border-right:1px solid #000; border-bottom:1px solid #000;"></td>';
            $html .= '<td style="width:12%; height:50px; border-right:1px solid #000; border-bottom:1px solid #000;"></td>';
            $html .= '<td style="width:12%; height:50px; border-bottom:1px solid #000;"></td>';
        $html .= '</tr>';
// AVD start - 12/27/18 2020
        $html .= '<tr>';
            $html .= '<td style="width:16%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:left; margin:0; padding:0 12px 0 12px;">TN Sales Tax</td>';
            $html .= '<td style="width:50%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:left; margin:0; padding:0 12px 0 12px;"></td>';
            $html .= '<td style="width:10%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; margin:0; padding:0; text-align:center;">1</td>';
            $html .= '<td style="width:12%; height:30px; border-right:1px solid #000; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:right; margin:0; padding:0 15px 0 0;"></td>';
            $html .= '<td style="width:12%; height:30px; border-bottom:1px solid #000; font-family:Arial; font-size:14px; color:#000; line-height:16px; font-weight:400; text-align:right; margin:0; padding:0 24px 0 0;">'.number_format($STax, 2).'</td>';
        $html .= '</tr>';
// AVD end - 12/27/18 2020
		$html .= '<tr>';
            $html .= '<th colspan="5" style="width:100%; height:40px; font-family:Arial; font-size:16px; color:#000; line-height:18px; font-weight:400; margin:0; padding:0;">GOODS REMAIN PROPERTY OF THE SELLER UNTIL PAID IN FULL</th>';
        $html .= '</tr>';
    	$html .= '</tbody>';
    $html .= '</table>';

    return $html;
}


//Sales Receipt
function SalesReceipt($SalesID){
	global $SalesOBJ;
	$SalesAmt = $SalesOBJ->GetSalesInfo($SalesID, 'SAmount');
	$html = '';
	$html .= '<!DOCTYPE html>';
	$html .= '<html lang="en">';
	$html .= '<head>';
	$html .= '<meta charset="utf-8">';
	$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
	$html .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">';
	$html .= '<meta name="description" content="">';
	$html .= '<meta name="author" content="">';
	$html .= '<title>TNbattery</title>';

	$html .= '</head>';
	$html .= '<body>';

	if($SalesOBJ->IsVoid($SalesID)){
		$html .= '<div style="position:relative;"> <h2 style="font-size:150px; color:rgba(0,0,0,.1); position:absolute; left:15%; top:32%; transform: rotate(-50deg); background:none;">VOID SALE</h2> <table style="position:relative; width:1000px; margin:0 auto; padding:35px 0 0; z-index:1;">';
	}else{
		$html .= '<div><table style="width:1000px; margin:0 auto; padding:35px 0 0; z-index:1;">';
	}
	
	    $html .= '<thead>';
	        $html .= '<tr>';
	            $html .= '<th>';
	            $html .= SalesReceiptHeaderPart();
	            $html .= '</th>';
	        $html .= '</tr>';
	        $html .= '<tr>';
	            $html .= '<th>';
	           $html .= SaleReceiptAddressPart($SalesID);
	            $html .= '</th>';
	        $html .= '</tr>';
	    $html .= '</thead>';
	    $html .= '<tbody>';
	        $html .= '<tr>';
	          $html .= '<td style=" font-family:Arial; font-size:20px; color:#000; line-height:22px; font-weight:400; text-align:center; margin:0; padding:25px 0 0; ">RECEIPT REQUIRED FOR RETURNS OR WARRANTY</td>';
	        $html .= '</tr>';
	        $html .= '<tr>';
	            $html .= '<td>';
	                $html .= '<table cellspacing="0" style=" width:100%; border:1px solid #000; border-radius:10px;">';
	                    $html .= '<thead>';
	                        $html .= SalesReceiptCustomer($SalesID);
	                        $html .= '<tr>';
	                            $html .= '<th colspan="6" style="padding:0;">';
	                                $html .= SalesReceiptItemsPart($SalesID);
	                            $html .= '</th>';
	                        $html .= '</tr>';
	                    $html .= '</thead>';
	                $html .= '</table>';
	            $html .= '</td>';
	        $html .= '</tr>';
	        $html .= '<tr>';
	            $html .= '<td>';
	                $html .= '<table style="width:100%; border:1px solid #000; border-radius:10px; padding:10px;">';
	                    $html .= '<tbody>';
	                        $html .= '<tr>';
	                            $html .= '<td style="padding:20px 0 50px;">';



	                   				$html .= '<div class="pm-block" id="pm-block" style="vertical-align:top;">';
										$html .= SalesPMDetails($SalesID);
									$html .= '</div>';

	                            $html .= '</td>';
	                             	$html .= '<td style="vertical-align:top; text-align:right;">';
	                                 	$html .= '<div style="padding:0 0 10px;">';
	                                     	$html .= '<h2 style="display:inline-block; font-family:Arial; font-size:16px; color:#000; line-height:18px; font-weight:400; margin:0; padding:0 8px 0 0;">SUBTOTAL </h2>';
	                                     	$html .= '<h3 style="display:inline-block; font-family:Arial; font-size:15px; color:#000; line-height:17px; font-weight:400; width:75px; border-bottom:2px solid #000; margin:0; padding:0;">$'.number_format($SalesAmt, 2).'</h3>';
	                                 	$html .= '</div>';
	                                 	$html .= '<div>';
	                                     	$html .= '<h4 style="display:inline-block; font-family:Arial; font-size:18px; color:#000; line-height:20px; font-weight:600; margin:0; padding:0 8px 0 0;">TOTAL </h4>';
	                                     	$html .= '<h5 style="display:inline-block; font-family:Arial; font-size:18px; color:#000; line-height:20px; font-weight:600; width:90px; border-bottom:2px solid #000; box-sizing:border-box; margin:0; padding:0 8px 0 0;">$'.number_format($SalesAmt, 2).'</h5>';
	                                 	$html .= '</div>';
	                                 	$html .= '<div style="vertical-align:bottom;">';
	                                     	$html .= '<h6 style="font-family:Arial; font-size:12px; color:#000; line-height:14px; font-weight:600; margin:0; padding:140px 0 0;">WIG-6-7.399.376</h6>';
	                                 	$html .= '</div>';
	                             	$html .= '</td>';
	                         	$html .= '</tr>';
	                     	$html .= '</tbody>';
	                	$html .= '</table>';
	            	$html .= '</td>';
	        	$html .= '</tr>';
	    	$html .= '</tbody>';
		$html .= '</table></div>';
	$html .= '</body>';
	$html .= '</html>';
	return $html;
}

/*echo '<pre>';
print_r(SalesPMDetails(2078)); die;*/

function GetCashDrawers(){
	global $SDPDO;
	$data = $SDPDO->MyQuery( 'SELECT * FROM '.TBL_CASH_DRAWERS );
	if($data){
		return $data;
	}
}

function GetTheCashDrawer($cdid){
	global $SDPDO;
	$data = $SDPDO->GetAll( TBL_CASH_DRAWERS, array('CashDrawerID' => $cdid) );
	if($data){
		return $data[0];
	}
}

function CashDrawerName($cdid){
	global $SDPDO;
	$data = $SDPDO->GetAll( TBL_CASH_DRAWERS, array('CashDrawerID' => $cdid) );
	if($data){
		return $data[0]->CashDrawerName;
	}
}

function GetCashDrawerIDByComputer($ComputerName){
	global $SDPDO;
	$data = $SDPDO->GetAll( TBL_CASH_DRAWERS, array('CDComputerName' => $ComputerName) );
	if($data){
		return $data[0]->CashDrawerID;
	}
}

function DrawerWithComputerArray(){
	global $SDPDO;
	$data = $SDPDO->GetAll( TBL_CASH_DRAWERS );
	foreach ($data as $CD) {
		$return[$CD->CDComputerName] = $CD->CashDrawerID;
	}
	return $return;
}

function LowStockItems(){
	global $InventoryOBJ;
	$LSIs = $InventoryOBJ->GetLowStockItems();
	if($LSIs){
		$html = '';
		$html .= '<table border=1>';
			$html .= '<thead>';
				$html .= '<tr>';
					$html .= '<th align="left">Item Name</th>';
					$html .= '<th align="left">Item Number</th>';
					$html .= '<th align="left">On Hand</th>';
				$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			foreach ($LSIs as $LSI) {
				$Item = $InventoryOBJ->GetTheInventoryByID($LSI->IOHInventoryID)[0];
				$Qty = $LSI->IOHQuantity;
				$html .= '<tr>';
					$html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetailsPage(this)" data-link="'.ItemUrl($Item->INVID).'">'.$Item->INVItemName.'</td>';
					$html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetailsPage(this)" data-link="'.ItemUrl($Item->INVID).'">'.$Item->INVItemNumber.'</td>';
					$html .= '<td onmouseover="RowOverEffect(this)" onmouseleave="RowLeaveEffect(this)" onclick="GoToItemDetailsPage(this)" data-link="'.ItemUrl($Item->INVID).'">'.$Qty.'</td>';
				$html .= '</tr>';
			}
			$html .= '</tbody>';
		$html .= '</table>';

		return $html;
	}else{
		return '<h2 class="text-center">No items found</h2>';
	}
}


function GetSectionPermissionValue($SectionID){
	global $SDPDO;
	$data = $SDPDO->GetAll(TBL_SECURITY_LEVELS, array('SLSectionID' => $SectionID));
	if($data){
		return $data[0]->SLRoleNeeded;
	}
}
/*echo '<pre>';
$time = strtotime('2018-10-31 15:00:37');

$newformat = date('Y-m-d H:i:s',$time);
echo $newformat;
print_r($SalesOBJ->SalesCashByDate($newformat, '10/31/2018')); die;*/

function CalculateSalesDisc($qty, $cost, $disc){
	$DiscVal = $disc;
	if($disc){
		if(strpos($disc, '$') !== false){
			$disc2 = str_replace('$', '', $disc);
			$DiscVal = $disc2;
		}
		if(strpos($disc, '%') !== false){
			$disc3 = str_replace('%', '', $disc);
			$DiscVal = (intval($qty)*floatval($cost))*(floatval($disc3) / 100);
		}
	}
	return $DiscVal;
}

//print_r(CalculateSalesDisc(1, 110, 12)); die;

function ActiveOnAction($action){
	if(is_array($action)){
		if(isset($_GET['action'])){
			foreach ($action as $value) {
				if($_GET['action'] == $value){
					echo "active";
				}
			}
		}else{
			return false;
		}
	}else{
		if(isset($_GET['action'])){
			if($_GET['action'] == $action){
				echo 'active';
			}
		}
		return false;
	}
}

function GetInvAndNonInvItems($selectid = 'item1', $selectedInvs = array(), $selectedNonInvs = array(), $inv=null, $noninv=null){
	global $InventoryOBJ;
	$Invs = $InventoryOBJ->GetAllInventories();
	$NonInvs = $InventoryOBJ->GetAllNonInventoryItems();
	$selected = '';
	$dropdown = '';
	$dropdown .= '<select class="form-control sd-select item" id="'.$selectid.'" name="item" data-show-subtext="true" data-live-search="true" class="form-control" onchange="getItemPrice(this)" onblur="getItemNetPrice(this)">';
	$dropdown .= '<option value="">Select Item</option>';
	if($Invs){
		$dropdown .= '<optgroup label="Inventory Items">';
		foreach ($Invs as $Inv) {
			$selected = '';
			if(!in_array($Inv->INVID, $selectedInvs)){
				if($inv){
					if($Inv->INVID == $inv){
						$selected = 'selected';
					}
				}else{
					$selected = '';
				}
				
				$dropdown .= '<option value="'.$Inv->INVID.'" data-type="inv" '.$selected.'>'.getItemWithNumber($Inv->INVID).'</option>';
			}
		}
		$dropdown .= '</optgroup>';
	}
	if($NonInvs){
		$dropdown .= '<optgroup label="Non Inventory Items">';
		foreach ($NonInvs as $NonInv) {
			$selected = '';
			if(!in_array($NonInv->NonInvID, $selectedNonInvs)){
				if($noninv){
					if($NonInv->NonInvID == $noninv){
						$selected = 'selected';
					}
				}else{
					$selected = '';
				}
				$dropdown .= '<option value="'.$NonInv->NonInvID.'" data-type="non-inv" '.$selected.'>'.$NonInv->NonInvName.'</option>';
			}
		}
		$dropdown .= '</optgroup>';
	}
	$dropdown .= '</select>';

	return $dropdown;
}


//print_r($OrderOBJ->LastOrderID()); die;

function VendorPaymentsHTML($VendorID){
	global $VendorOBJ, $ListTypeOBJ, $OrderOBJ;
	$VPs = $VendorOBJ->GetVendorPayableOrderIds($VendorID);
	$TheVendor = $VendorOBJ->GetTheVendorByID($VendorID);
	$html = '';

		$html .= '<div class="vendor-details">';
			$html .= '<h2>'.VC_Name($TheVendor->VCIDFName, $TheVendor->VCIDLname, $TheVendor->VendorCompanyName).'</h2>';
			if($TheVendor->VAAddress1 || $TheVendor->VAAddress2){
				$html .= '<p>'.$TheVendor->VAAddress1.' , '.$TheVendor->VAAddress2.'</p>';
			}

			if(!empty($TheVendor->VAState)){
				$VS = strtoupper($TheVendor->VAState);
				if(array_key_exists($VS, getUSAStates())){
					$VState = getUSAStates()[$VS];
				}
			}else{
				$VState = '';
			}

			$html .= '<i>'.$TheVendor->VACity.'</i> , <b>'.$VState.'</b>';
		$html .= '</div>';

		$html .= '</br>';

		$html .= '<table class="table-bordered vendor-payment-table">';
		$html .= '<thead>';
			$html .= '<th>Order ID</th>';
			$html .= '<th>Amount Due</th>';
			$html .= '<th>Amount being paid</th>';
		$html .= '</thead>';
		$html .= '<tbody>';
		if(!empty($VPs)){
			foreach ($VPs as $VP) {
				$DueAmt = $OrderOBJ->GetOrderDueAmount($VP);
				if($DueAmt > 0){

					$html .= '<tr>';
						$html .= '<td>#'.$VP.'</td>';
						$html .= '<td class="pay-amount">$'.$DueAmt.'</td>';

						$html .= '<td><input type="text" onkeyup="GetVendorPaymentTotal()" class="price-validation pay-vendor form-control" data-orderid="'.$VP.'" /></td>';

					$html .= '</tr>';
				}
				
			}
		}else{
			$html .= '<tr>';
				$html .= '<td colspan="4">No payments are pending.</td>';
			$html .= '</tr>';
		}
		$html .= '</tbody>';
	$html .= '</table>';

	$html .= '</br>';
	if(!empty($VPs)){
		$html .= '<div class="vendor-extra">';
			$html .= '<label for="extra-payment">Extra Payment : </label>';
			$html .= '<input type="text" onkeyup="GetVendorPaymentTotal()" class="form-control price-validation" id="vendor-extra-payment">';
			$html .= '</br></br>';
			$html .= '<label for="total-payment">Total to Pay : </label>';
			$html .= '<input type="text" class="form-control" id="vendor-total-payment" readonly>';
			$html .= '</br></br>';
			$html .= '<label for="payment-method">Payment Method : </label>';
			$html .= '<select class="vpm form-control" id="vpm" onchange="VpSet(this)">';
			$PMs = $ListTypeOBJ->GetAllPaymentMethods();
			foreach ($PMs as $PM) {
				$html .= '<option value="'.$PM->PMID.'">'.$PM->PaymentMethodName.'</option>';
			}
			$html .= '</select>';
			$html .= '</br></br>';

			$html .= '<div class="check-data" style="display:none;">';
				$html .= '<label for="check-number">Check Number : </label>';
				$html .= '<input type="text" class="form-control" id="vendor-check-number">';
				$html .= '</br></br>';
			$html .= '</div>';

			$html .= '<div class="card-data" style="display:none;">';
				$html .= '<label for="last5digits">Last 5 Digits : </label>';
				$html .= '<input type="text" class="form-control" id="last5digits">';
				$html .= '</br></br>';
				$html .= '<label for="authcode">Authorization Code : </label>';
				$html .= '<input type="text" class="form-control" id="authcode">';
				$html .= '</br></br>';
			$html .= '</div>';

			$html .= '<label for="vp-note">Note : </label>';
			$html .= '<textarea class="form-control" id="vp-note"></textarea>';

			$html .= '</br></br>';

			$html .= '<a href="javascript:void(0)" class="btn theme-default-orange text-right" data-vendorid="'.$VendorID.'" id="vendor-save-payment" onclick="SavePayment()">Save Payment</a>';
		$html .= '</div>';
	}
	return $html;
}

function CustomerPaymentHTML($CustomerID){
	global $CustomerOBJ, $SalesOBJ, $ListTypeOBJ;
	$Customer = $CustomerOBJ->GetTheCustomerByID($CustomerID);
	$AllSales = $SalesOBJ->AllSalesByCustomer($CustomerID);
	$html = '';
	$html .= '<div class="customer-details">';
		$html .= '<h2>'.VC_Name($Customer->CustomerFName, $Customer->CustomerLName, $Customer->CustomerCompanyName).'</h2>';

		if($Customer->CAAddress1){
			$html .= '<p>'.$Customer->CAAddress1.'</p>';
		}
		if($Customer->CAAddress2){
			$html .= '<p>'.$Customer->CAAddress2.'</p>';
		}

		if(!empty($Customer->CAState)){
			$CS = strtoupper($Customer->CAState);
			if(array_key_exists($CS, getUSAStates())){
				$CState = getUSAStates()[$CS];
			}
		}else{
			$CState = '';
		}
		if($Customer->CACity){
			$html .= '<i>'.$Customer->CACity.'</i> '.$CState;
		}

	$html .= '</div>';

	$html .= '</br>';

	$html .= '<table class="table-bordered customer-payment-table">';
		$html .= '<thead>';
			$html .= '<th>Invoice</th>';
			$html .= '<th>Amount Due</th>';
			$html .= '<th>Amount being paid</th>';
		$html .= '</thead>';
		$html .= '<tbody>';
		if(!empty($AllSales)){
			foreach ($AllSales as $Each) {
				$STotal = $SalesOBJ->Total($Each->SalesID);
				$SPaid = $SalesOBJ->PaidAmt($Each->SalesID);
				$SDue = $STotal - $SPaid;
				if($SDue > 0){

					$html .= '<tr>';
						$html .= '<td>#'.$Each->SalesInvoiceNumber.'</td>';
						$html .= '<td class="pay-amount">$'.$SDue.'</td>';

						$html .= '<td><input type="text" onkeyup="GetVendorPaymentTotal()" class="price-validation pay-vendor form-control" data-salesid="'.$Each->SalesID.'" /></td>';

					$html .= '</tr>';
				}
				
			}
		}else{
			$html .= '<tr>';
				$html .= '<td colspan="4">No payments are pending.</td>';
			$html .= '</tr>';
		}
		$html .= '</tbody>';
	$html .= '</table>';
	$html .= '</br>';

	if(!empty($AllSales)){
		$html .= '<div class="vendor-extra">';
			// $html .= '<label for="extra-payment">Extra Payment : </label>';
			// $html .= '<input type="text" onkeyup="GetVendorPaymentTotal()" class="form-control price-validation" id="vendor-extra-payment">';
			// $html .= '</br></br>';
			$html .= '<label for="total-payment">Total to Pay : </label>';
			$html .= '<input type="text" class="form-control" id="vendor-total-payment" readonly>';
			$html .= '</br></br>';
			$html .= '<label for="payment-method">Payment Method : </label>';
			$html .= '<select class="vpm form-control" id="vpm" onchange="VpSet(this)">';
			$PMs = $ListTypeOBJ->GetAllPaymentMethods();
			foreach ($PMs as $PM) {
				$html .= '<option value="'.$PM->PMID.'">'.$PM->PaymentMethodName.'</option>';
			}
			$html .= '</select>';
			$html .= '</br></br>';

			$html .= '<div class="check-data" style="display:none;">';
				$html .= '<label for="check-number">Check Number : </label>';
				$html .= '<input type="text" class="form-control" id="vendor-check-number">';
				$html .= '</br></br>';
			$html .= '</div>';

			$html .= '<div class="card-data" style="display:none;">';
				$html .= '<label for="last5digits">Last 5 Digits : </label>';
				$html .= '<input type="text" class="form-control" id="last5digits">';
				$html .= '</br></br>';
				$html .= '<label for="authcode">Authorization Code : </label>';
				$html .= '<input type="text" class="form-control" id="authcode">';
				$html .= '</br></br>';
			$html .= '</div>';

			//$html .= '<label for="vp-note">Note : </label>';
			//$html .= '<textarea class="form-control" id="vp-note"></textarea>';

			$html .= '</br></br>';

			$html .= '<a href="javascript:void(0)" class="btn theme-default-orange text-right" data-customerid="'.$CustomerID.'" id="vendor-save-payment" onclick="SaveCustomerPayment()">Save Payment</a>';

		$html .= '</div>';
	}
	return $html;
}